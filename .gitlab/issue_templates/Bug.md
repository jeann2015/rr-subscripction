## Description
<brief non technical description>

## Local Configuration
* **Enviroment:** <Staging: *https://rr-subscription-stg.wip.aerolab.co/*>
* **SO:** < mac os sierra >
* **Device:** <macbook air / desktop view>
* **Browser:** <Safari>

## Steps to reproduce
- <List all steps to reproduce the error>

## Actual Result
<Observed result> 

## Expected Result
<Expected behavior>

## Evidence
<Screenshots / Videos>