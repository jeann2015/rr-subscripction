FROM node:10.9

WORKDIR /usr/src/app

# Install dependencies
COPY package.json package-lock.json /usr/src/app/
RUN npm ci

# Build app
# (WARNING: FRONTEND NEEDS TO BE BUILT AGAIN ON START TO INJECT ENV VARIABLES PROPERLY)
COPY . /usr/src/app
RUN npm run build

CMD [ "npm", "start" ]