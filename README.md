### Run development environment

```
# Platform
docker-compose up database
yarn
npm run dev

http://localhost:3000

# Admin
yarn
npm run dev
http://localhost:3010
```

#### Import default products and info

```
INSERT INTO `product` (`id`, `box_type`, `price`, `days`, `description`)
VALUES
    (1, 'Meal Maker', 0, 0, 'Meal Maker'),
    (2, 'Starter', 9500, 14, 'Starter Box every 14 days'),
    (3, 'Regular', 9500, 14, 'Regular Box every 14 days'),
    (4, 'Regular', 9000, 14, 'Regular Box every 14 days at $90'),
    (5, 'Regular', 9500, 28, 'Regular Box every 28 Days'),
    (6, 'Starter', 9500, 28, 'Starter Box every 28 Days'),
    (7, 'Regular', 9000, 28, 'Regular Box every 28 days at $90'),
    (8, 'Regular', 9500, 42, 'Regular Box every 42 Days'),
    (9, 'Babycook Original Peacock', 4995, 0, 'BÉABA® Babycook® Original'),
    (10, 'Babycook Cloud', 9995, 0, 'BÉABA® Babycook® Cloud'),
    (11, 'Babycook Blueberry', 9995, 0, 'BÉABA® Babycook® Blueberry'),
    (12, 'Babycook Lemon', 9995, 0, 'BÉABA® Babycook® Lemon'),
    (13, 'Babycook Navy', 9995, 0, 'BÉABA® Babycook® Navy'),
    (14, 'Babycook Paprika', 9995, 0, 'BÉABA® Babycook® Paprika'),
    (15, 'Babycook Pistachio', 9995, 0, 'BÉABA® Babycook® Pistachio'),
    (16, 'Babycook Plus Cloud', 14995, 0, 'BÉABA® Babycook® Plus Cloud'),
    (17, 'Babycook Plus Rose Gold', 14995, 0, 'BÉABA® Babycook® Plus Rose Gold'),
    (18, 'Babycook Rose Gold', 9995, 0, 'BÉABA® Babycook® Rose Gold');


INSERT INTO `tag_day_name` (`id`, `name`)
VALUES
    (1, '1 DAY GROUND ST. LOUIS'),
    (2, '2 DAY GROUND ST. LOUIS'),
    (3, '2 DAY AIR ST. LOUIS'),
    (4, '1 DAY GROUND STOCKTON'),
    (5, '2 DAY GROUND STOCKTON');
```

NOTE:
    - 'cron' table shouldn't be empty. Add a cron row with ID 1 and NOW() time.

##### Import single admin

```
INSERT INTO `admin` (`name`, `password`, `email`)
VALUES
	('Admin', 'sha1$5666d1b8$1$fdbcf01de174a5615f1bd17a7a1faf048f5fcfbf', 'admin@aerolab.co');

```

Login in admin http://localhost:3010

User: admin@aerolab.co

Password: admin



### Run Cron

```
# Run monday's cron
npm run cron

# Run Paused subscriptions cron
npm run cronExpired
```

Update subscriptions, orders and skips to older date

```
  UPDATE `subscription` SET `start_from` = DATE_SUB(`start_from`, INTERVAL 2 WEEK);
  UPDATE `order` SET `created_at` = DATE_SUB(`created_at`, INTERVAL 2 WEEK);
  UPDATE `skip` SET `date` = DATE_SUB(`date`, INTERVAL 2 WEEK);
  UPDATE `cron` SET `created_at` = DATE_SUB(`created_at`, INTERVAL 2 WEEK);
```

