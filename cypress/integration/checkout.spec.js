const selectors = {
  progressBar: {
    stepOne: '.step-number-1',
    stepTwo: '.step-number-2',
    stepThree: '.step-number-3',
    stepFour: '.step-number-4',
    stepFive: '.step-number-5',
  },
  step1: {
    buttons: {
      mealFrequency2: 'button[data-name="2"]',
      mealFrequency4: 'button[data-name="4"]',
      mealQuantity12: 'button[data-name="21"]',
      mealQuantity24: 'button[data-name="22"]',
      nextStep: '.desktop-lg button[data-button-id="ButtonStepProductPlan"]',
    },
    container: '.StepProductPlan',
  },

  step2: {
    buttons: {
      addAnotherTinyHuman: '.add-tiny-human',
      calendarToday: index => `div[data-baby-index="${index}"] .CalendarDay--today > .CalendarDay__button`,
      nextStep: 'button[data-button-id="ButtonStepBaby"]',
    },
    container: '.StepTinyHuman',
    inputs: {
      date: index => `div[data-baby-index="${index}"] input[name="date"]`,
      name: index => `div[data-baby-index="${index}"] input[name="name"]`,
    },
  },
  step3: {
    buttons: {
      nextStep: '.desktop-lg button[data-button-id="ButtonStepEmail"]',
    },
    container: '.StepName',
    inputs: {
      email: 'input[name="email"]',
    },
  },
  step4: {
    buttons: {
      firstShippingDate: 'button[data-name="shipping-date-1"]',
      nextStep: 'button[data-button-id="ButtonStepShipping"]',
      secondShippingDate: 'button[data-name="shipping-date-2"]',
    },
    container: '.StepShipping',
    inputs: {
      address: 'input[name="address"]',
      addressExtra: 'input[name="addressExtra"]',
      city: 'input[name="city"]',
      company: 'input[name="company"]',
      lastName: 'input[name="lastName"]',
      name: 'input[name="name"]',
      phoneNumber: 'input[name="phoneNumber"]',
      state: 'select[name="state"]',
      zipCode: 'input[name="zipCode"]',
    },
  },
  step5: {
    buttons: {
      addMeal: '.counter',
      nextStep: 'button[data-button-id="ButtonStepBilling"]',
    },
    container: '.StepBilling',
    inputs: {
      cardNumber: 'input[name="cardNumber"]',
      cvv: 'input[name="cvc"]',
      expirationDate: 'input[name="expiration"]',
    },
    stepTitle: '.StepBilling > .Title',
    summaryTitle: '.Summary > .Title',
  },
  step6: {
    container: '.stepReferral',
  },
  title: '.Title',
};

const routes = {
  signUp: 'http://localhost:3000',
  step1: 'http://localhost:3000/#1',
  step2: 'http://localhost:3000/#2',
  step3: 'http://localhost:3000/#3',
  step4: 'http://localhost:3000/#4',
  step5: 'http://localhost:3000/#5',
};

describe('Raised Real Signup', () => {
  describe('Desktop view', () => {
    describe('Access to RR', () => {
      it('successfully loads', () => {
        cy.visit(routes.signUp);
      });
    });
    describe('Step 1', () => {
      it('Breadcrumb: should show correct state', () => {
        cy.wait(2000);
        cy.get(selectors.progressBar.stepOne).should('not.have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepTwo).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
        cy.get(selectors.progressBar.stepThree).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
        cy.get(selectors.progressBar.stepFour).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
      });

      // it('verify step 3 breadcrumb', () => {
      //   cy.get(selectors.progressBar.stepOne).should('have.class', 'clickeable')
      // .and('have.class', 'active');
      //   cy.get(selectors.progressBar.stepTwo).should('have.class', 'clickeable')
      //   .and('have.class', 'active');
      //   cy.get(selectors.progressBar.stepThree).should('not.have.class', 'clickeable')
      //   .and('have.class', 'active');
      //   cy.get(selectors.progressBar.stepFour).should('not.have.class', 'clickeable')
      //     .and('not.have.class', 'active');
      // });

      // it('verify the Step 1 title', () => {
      //   cy.get(selectors.title).should('have.text', 'MAKE IT YOURS');
      // });

      it('choose meal quantity and frequency', () => {
        cy.wait(1000);
        cy.get(selectors.step1.buttons.mealQuantity12).click();
        cy.get(selectors.step1.buttons.mealFrequency2).click();
        cy.get(selectors.step1.buttons.nextStep).click();
      });
    });

    describe('Step 2', () => {
      it('Breadcrumb: should show correct state', () => {
        cy.get(selectors.progressBar.stepOne).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepTwo).should('not.have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepThree).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
        cy.get(selectors.progressBar.stepFour).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
      });

      it('verify the Step 2 title', () => {
        cy.get(selectors.title).should('have.text', 'Your tiny human');
      });

      it('CTA style is disabled when DOB field is empty', () => {
        cy.get(selectors.step2.buttons.nextStep).should('be.visible')
          .and('have.class', 'isDisabled');
      });

      it('Error of DOB field should disappear when is completed', () => {
        cy.get(selectors.step2.inputs.date(0)).click();
        cy.get(selectors.step2.buttons.calendarToday(0)).click();
        cy.get(selectors.step2.inputs.date(0)).should('not.have.class', 'invalid');
      });

      it('CTA style should be enabled when DOB field is filled ', () => {
        cy.get(selectors.step2.buttons.nextStep).should('not.have.class', 'isDisabled');
      });

      it('Breadcrumb: should keep showing correct state', () => {
        cy.get(selectors.progressBar.stepOne).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepTwo).should('not.have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepThree).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
        cy.get(selectors.progressBar.stepFour).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
      });

      it('Add one more tiny human', () => {
        cy.get(selectors.step2.buttons.addAnotherTinyHuman).click();
        cy.get(selectors.step2.inputs.name(1)).type('2nd baby');
        cy.get(selectors.step2.inputs.date(1)).click();
        cy.get(selectors.step2.buttons.calendarToday(1)).click();
      });

      it('Complete baby name and go to next step', () => {
        cy.get(selectors.step2.inputs.name(0)).type('Coolname');
        cy.get(selectors.step2.buttons.nextStep).click();
      });
    });
    describe('Step 3', () => {
      it('verify the Step title', () => {
        cy.get(selectors.title).should('have.text', 'CREATE YOUR ACCOUNT');
      });

      it('CTA keeps disabled until valid email', () => {
        const time = Date.now();
        cy.get(selectors.step3.inputs.email).type(`test${time}`);
        cy.get(selectors.step3.buttons.nextStep)
          .should('be.visible')
          .and('have.class', 'isDisabled');

        cy.get(selectors.step3.inputs.email).type('@aerolab');
        cy.get(selectors.step3.buttons.nextStep)
          .should('have.class', 'isDisabled');

        cy.get(selectors.step3.inputs.email).type('.co');
        cy.get(selectors.step3.buttons.nextStep)
          .should('not.have.class', 'isDisabled');
      });

      it('can advance to next step when valid email entered', () => {
        cy.get(selectors.step3.buttons.nextStep).click();
      });
    });


    describe('Step 4', () => {
      it('Breadcrumb: should show correct state', () => {
        cy.get(selectors.progressBar.stepOne).should('have.class', 'clickeable')
        .and('have.class', 'active');
        cy.get(selectors.progressBar.stepTwo).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepThree).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepFour).should('not.have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepFive).should('not.have.class', 'clickeable')
          .and('not.have.class', 'active');
      });

      it('verify the Step 4 title', () => {
        cy.get(selectors.title).should('have.text', 'SHIPPING');
      });

      it('Complete the Shipping fields correctly', () => {
        cy.get(selectors.step4.inputs.name).type('test');
        cy.get(selectors.step4.inputs.lastName).type('LastNameTest');
        cy.get(selectors.step4.inputs.company).type('testcompany');
        cy.get(selectors.step4.inputs.address).type('Address');
        cy.get(selectors.step4.inputs.addressExtra).type('State');
        cy.get(selectors.step4.inputs.zipCode).type('90001');
        cy.get(selectors.step4.inputs.city).should('have.value', 'Los Angeles');
        cy.get(selectors.step4.inputs.state).should('have.value', 'CA');
        cy.get(selectors.step4.inputs.phoneNumber).type('1111111111');
      });

      it('Next shipment date should be selected by default', () => {
        cy.get(selectors.step4.buttons.firstShippingDate).should('have.class', 'selected');
      });

      it('change the default week', () => {
        cy.get(selectors.step4.buttons.secondShippingDate).click();
      });

      it('go to the payment step', () => {
        cy.get(selectors.step4.buttons.nextStep).click();
      });
    });

    describe('Step 5', () => {
      it('Breadcrumb: should show correct state', () => {
        cy.get(selectors.progressBar.stepOne).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepTwo).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepThree).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepFour).should('have.class', 'clickeable')
          .and('have.class', 'active');
        cy.get(selectors.progressBar.stepFive).should('not.have.class', 'clickeable')
          .and('have.class', 'active');
      });

      it('verify the Step 5 title', () => {
        cy.get(selectors.step5.stepTitle).should('have.text', 'YOUR BILLING INFO');
      });

      it('Complete payment info', () => {
        cy.get(selectors.step5.inputs.cardNumber).type('4242424242424242');
        cy.get(selectors.step5.inputs.expirationDate).type('1122');
        cy.get(selectors.step5.inputs.cvv).type('123');
      });

      it('verify summary title', () => {
        cy.get(selectors.step5.summaryTitle).should('have.text', 'ORDER SUMMARY');
      });

      it('add one more meal box', () => {
        cy.get(selectors.step5.buttons.addMeal).click();
      });

      it('click on Complete Order', () => {
        cy.get(selectors.step5.buttons.nextStep).click();
      });
    });

    describe('Step 6', () => {
      it('verify the step 6 title', () => {
        cy.get(selectors.step6.container, {
          timeout: 10000,
        });
        cy.get(selectors.title).should('have.text', 'ORDER CONFIRMED! WE DID IT.');
      });
    });
  });
});
