const selectors = {
  step1: {
    buttons: {
      mealFrequency2: 'button[data-name="2"]',
      mealFrequency4: 'button[data-name="4"]',
      mealQuantity12: 'button[data-name="21"]',
      mealQuantity24: 'button[data-name="22"]',
      nextStep: '.desktop-lg button[data-button-id="ButtonStepProductPlan"]',
    },
    container: '.StepProductPlan',
  },
  step3: {
    buttons: {
      nextStep: '.desktop-lg button[data-button-id="ButtonStepEmail"]',
    },
    container: '.StepName',
    inputs: {
      email: 'input[name="email"]',
    },
  },
  step4: {
    container: '.StepShipping',
    inputs: {
      address: 'input[name="address"]',
      addressExtra: 'input[name="addressExtra"]',
      city: 'input[name="city"]',
      company: 'input[name="company"]',
      lastName: 'input[name="lastName"]',
      name: 'input[name="name"]',
      phoneNumber: 'input[name="phoneNumber"]',
      state: 'select[name="state"]',
      zipCode: 'input[name="zipCode"]',
    },
  },
  step5: {
    buttons: {
      addMeal: '.counter',
      nextStep: 'button[data-button-id="ButtonStepBilling"]',
    },
    container: '.StepBilling',
    inputs: {
      cardNumber: 'input[name="cardNumber"]',
      cvv: 'input[name="cvc"]',
      expirationDate: 'input[name="expiration"]',
    },
    stepTitle: '.StepBilling > .Title',
    summaryTitle: '.Summary > .Title',
  },
  step6: {
    container: '.stepReferral',
  },
  title: '.Title',
};

describe('Raised Real Sign Up', () => {
  describe('Desktop view', () => {
    describe('Access to RR', () => {
      it('successfully loads', () => {
        cy.visit('http://localhost:3000');
      });
    });
    describe('Step 1', () => {
      it('choose meal quantity and frequency', () => {
        cy.wait(1000);
        cy.get(selectors.step1.buttons.mealQuantity12).click();
        cy.get(selectors.step1.buttons.mealFrequency2).click();
        cy.get(selectors.step1.buttons.nextStep).click();
      });
    });

    describe('Step 2', () => {
      it('Should show error when press CTA without filling DOB', () => {
        cy.get('.Button').click();
        cy.get('body').should('have.class', 'with-errors');
        cy.get('.desktopDateInput').should('have.class', 'invalid');
        cy.get('span.Alert')
          .should('have.class', 'error')
          .should('have.text', 'Oops. Please complete the fields marked in pink.');
      });
      it('Should show error when press CTA without filling DOB but completing Name', () => {
        cy.get(':nth-child(1) > .TextField > .input').type('Test Name');
        cy.get('.Button').click();
        cy.get('.Alert').should('have.text', 'Oops. Please complete the fields marked in pink.');
      });

      it('Complete de DOB with invalid data', () => {
        cy.get('#date').type('sd');
        cy.get('.Button').click();
      });

      it('Complete DOB and go to the next step', () => {
        cy.get('#date').clear();
        cy.get('#date').click();
        cy.get('.CalendarDay--today > .CalendarDay__button').click();
        cy.get('.Button').click();
      });
    });

    describe('Step 3', () => {
      it('CTA style is disabled when email field is empty', () => {
        cy.get(selectors.step3.buttons.nextStep)
          .should('be.visible')
          .should('have.class', 'isDisabled');
      });
      it('should show error when press CTA without filling email', () => {
        cy.get(selectors.step3.buttons.nextStep)
          .click();

        cy.get('body')
          .should('have.class', 'with-errors');

        cy.get('span.Alert')
          .should('have.class', 'error')
          .should('have.text', 'Oops. Please complete the fields marked in pink.');

        cy.get('.TextField').should('have.class', 'invalid');
      });
      it('CTA keeps disable when an invalid email is completed', () => {
        const time = Date.now();
        cy.get('input').type(`test${time}`);
        cy.get('.desktop-lg > .ButtonBar > .Button').should('have.class', 'isDisabled');
      });
      it('Complete the email field', () => {
        cy.get('input').type('@test.com');
        cy.get('.desktop-lg > .ButtonBar > .Button').click();
      });
    });

    describe('Step 4', () => {
      it('Leave all fields empty and try to continue', () => {
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', 'Oops. Please complete the fields marked in pink.');
      });
      it('Complete the Name with invalid data', () => {
        cy.get(selectors.step4.inputs.name).type('a333$&/(');
        cy.get('.Alert').should('have.text', "We like weird characters, just make sure you didn't accidentally enter any in your name (including numbers).");
      });

      it('Complete the fields with valid data', () => {
        cy.get(selectors.step4.inputs.name).clear();
        cy.get(selectors.step4.inputs.name).type('ThisIsAtest');
        cy.get(selectors.step4.inputs.lastName).type('Negative');
        cy.get(selectors.step4.inputs.company).type('testcompany');
        cy.get(selectors.step4.inputs.address).type('Address');
        cy.get(selectors.step4.inputs.addressExtra).type('State');
        cy.get(selectors.step4.inputs.zipCode).type('90001');
        cy.get(selectors.step4.inputs.city).should('have.value', 'Los Angeles');
        cy.get(selectors.step4.inputs.state).should('have.value', 'CA');
        cy.get(selectors.step4.inputs.phoneNumber).type('1111111111');
        cy.get('.ButtonBar > .Button').click();
      });
      it('Return to the previous step trough breadcrumb', () => {
        cy.get('.step-number-4').click();
        // cy.url().should('be.equal', 'http://localhost:3000/#3');
        cy.get('.StepShipping').should('be.visible');
      });
      it('Zipcode is completed with invalid data', () => {
        cy.get(selectors.step4.inputs.zipCode).clear();
        cy.get(selectors.step4.inputs.zipCode).type('asd');
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', 'Zipcode out of area.');
      });
      it('Correct zipcode and invalid phone', () => {
        cy.get(selectors.step4.inputs.zipCode).clear();
        cy.get(selectors.step4.inputs.zipCode).type('80001');
        cy.get(selectors.step4.inputs.phoneNumber).clear();
        cy.get(selectors.step4.inputs.phoneNumber).type('11');
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', 'Hmm, that number seems to be invalid. Please use this number format: (111) 222-3333');
      });
      it('Complete phone number and type out of area zipcode', () => {
        cy.get(selectors.step4.inputs.phoneNumber).clear();
        cy.get(selectors.step4.inputs.phoneNumber).type('1111111122');
        cy.get(selectors.step4.inputs.zipCode).clear();
        cy.get(selectors.step4.inputs.zipCode).type('96701');
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', 'Zipcode out of area.');
      });
      it('Correct zipcode and type invalid City', () => {
        cy.get(selectors.step4.inputs.zipCode).clear();
        cy.get(selectors.step4.inputs.zipCode).type('90001');
        cy.get(selectors.step4.inputs.city).type('123');
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', 'Not a valid city name.');
      });
      it('Correct City and go to the next step', () => {
        cy.get(selectors.step4.inputs.city).clear();
        cy.get(selectors.step4.inputs.city).type('City');
        cy.get('.ButtonBar > .Button').click();
      });
    });
    describe('Step 5', () => {
      it('Complete the promo code with invalid data', () => {
        cy.get('.discount > .TextField > .input').type('invalid');
        cy.get('.discount > .Button').click();
        cy.get('.Alert').should('have.text', 'Coupon not valid in this case');
      });
      it('Clear coupon field and complete an invalid CC', () => {
        cy.get('.discount > .TextField > .input').clear();
        cy.get(selectors.step5.inputs.cardNumber).type('1111000011110000');
        cy.get(selectors.step5.inputs.expirationDate).type('1122');
        cy.get(selectors.step5.inputs.cvv).type('123');
        cy.get('.ButtonBar > .Button').click();
        cy.wait(2000);
        // cy.get('.Alert').should('have text', 'Your card number is incorrect.'); //This test case is failling because the coupon error should disappear and its not happening.
      });
      it('Clear fields and complete with a rejected CC', () => {
        cy.get('.step-number-4').click();
        cy.wait(2000);
        cy.get('.ButtonBar > .Button').click(); // I want to clear all the error messages to see the new ones, so I go back and return. Not the best practice.
        cy.wait(2000);
        cy.get(selectors.step5.inputs.cardNumber).clear();
        cy.get(selectors.step5.inputs.cardNumber).type('4000000000000002'); // card declined
        cy.get('.ButtonBar > .Button').click();
        cy.wait(2000);
        cy.get('.Title').should('have.text', 'Oops!');
        // cy.get('.StepMessage > :nth-child(2)').should('have.text', "Your credit card doesn't seem to be valid.");
        cy.get('.StepMessage > :nth-child(3)').should('have.text', 'The problem we had is: Your card was declined.');
        cy.get('.Button').click();
        cy.wait(2000);
      });
      it('Complete with a CC that has no funds', () => {
        cy.get(selectors.step5.inputs.cardNumber).type('4000000000009995'); // card with no funds
        cy.get(selectors.step5.inputs.expirationDate).type('1122');
        cy.get(selectors.step5.inputs.cvv).type('123');
        cy.get('.ButtonBar > .Button').click();
        cy.wait(2000);
        cy.get('.Title').should('have.text', 'Oops!');
        cy.get('.StepMessage > :nth-child(3)').should('have.text', 'The problem we had is: Your card has insufficient funds.');
        cy.get('.Button').click();
        cy.wait(2000);
      });
      it('Complete with a valid CC and invalid exp date', () => {
        cy.get(selectors.step5.inputs.cardNumber).type('4242424242424242');
        cy.get(selectors.step5.inputs.expirationDate).type('111111');
        cy.get(selectors.step5.inputs.cvv).type('123');
        cy.get('.ButtonBar > .Button').click();
        cy.get('.Alert').should('have.text', "Your card's expiration year is invalid.");
      });
    });
  });
});
