

module.exports = {
  up(queryInterface, Sequelize, done) {
    return queryInterface.createTable('admin', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: Sequelize.STRING,
      password: Sequelize.STRING,
      email: Sequelize.STRING,
    })
      .then(() => queryInterface.createTable('coupon', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        code: Sequelize.STRING,
        percentOff: {
          type: Sequelize.INTEGER,
          field: 'percent_off',
        },
        amountOff: {
          type: Sequelize.INTEGER,
          field: 'amount_off',
        },
        maxRedemptions: {
          type: Sequelize.INTEGER,
          field: 'max_redemptions',
          allowNull: false,
          defaultValue: 1,
        },
        timesRedeemed: {
          type: Sequelize.INTEGER,
          field: 'times_redeemed',
          allowNull: false,
          defaultValue: 0,
        },
        valid: Sequelize.BOOLEAN,
      })).then(() => queryInterface.createTable('cron', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: 'created_at',
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: 'updated_at',
        },
      }))
      .then(() => queryInterface.createTable('user', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        customerId: {
          type: Sequelize.STRING,
          field: 'customer_id',
        },
        name: Sequelize.STRING,
        lastName: {
          type: Sequelize.STRING,
          field: 'last_name',
        },
        email: Sequelize.STRING,
        zipCode: {
          type: Sequelize.STRING,
          field: 'zip_code',
        },
        address: Sequelize.STRING,
        addressExtra: {
          type: Sequelize.STRING,
          field: 'address_extra',
        },
        city: Sequelize.STRING,
        state: Sequelize.STRING,
        phoneNumber: {
          type: Sequelize.STRING,
          field: 'phone_number',
        },
        hash: Sequelize.STRING,
        firstCharge: {
          type: Sequelize.STRING,
          field: 'first_charge',
        },
        billingName: {
          type: Sequelize.STRING,
          field: 'billing_name',
        },
        billingLastName: {
          type: Sequelize.STRING,
          field: 'billing_lastname',
        },
        billingAddress: {
          type: Sequelize.STRING,
          field: 'billing_address',
        },
        billingAddressExtra: {
          type: Sequelize.STRING,
          field: 'billing_address_extra',
        },
        billingCity: {
          type: Sequelize.STRING,
          field: 'billing_city',
        },
        billingState: {
          type: Sequelize.STRING,
          field: 'billing_state',
        },
        billingZipCode: {
          type: Sequelize.STRING,
          field: 'billing_zip_code',
        },
        hasMealMaker: {
          type: Sequelize.BOOLEAN,
          field: 'has_mealmaker',
        },
        deliveredMealMaker: {
          type: Sequelize.BOOLEAN,
          field: 'delivered_mealmaker',
          defaultValue: 0,
        },
      }))
      .then(() => queryInterface.createTable('child', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'user',
            key: 'id',
          },
          field: 'user_id',
          allowNull: false,
        },
        name: Sequelize.STRING,
        gender: Sequelize.STRING,
        food: Sequelize.STRING,
        allergies: Sequelize.STRING,
        birthdate: Sequelize.DATE,
      }))
      .then(() => queryInterface.createTable('product', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        product_name: Sequelize.STRING,
        price: Sequelize.INTEGER,
        repeated: Sequelize.INTEGER,
      }))
      .then(() => queryInterface.createTable('subscription', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          field: 'user_id',
          allowNull: false,
          references: {
            model: 'user',
            key: 'id',
          },
          onUpdate: 'cascade',
        },
        productId: {
          type: Sequelize.INTEGER,
          field: 'product_id',
          allowNull: false,
          references: {
            model: 'product',
            key: 'id',
          },
          onUpdate: 'cascade',
        },
        status: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        amount: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        startFrom: {
          type: Sequelize.DATE,
          field: 'start_from',
        },
        pausedUntil: {
          type: Sequelize.DATE,
          field: 'paused_until',
        },
        createdAt: {
          type: Sequelize.DATE,
          field: 'created_at',
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: 'updated_at',
        },
      }))
      .then(() => queryInterface.createTable('order', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          field: 'user_id',
          allowNull: false,
          references: {
            model: 'user',
            key: 'id',
          },
          onUpdate: 'cascade',
        },
        name: Sequelize.STRING,
        lastName: {
          type: Sequelize.STRING,
          field: 'last_name',
        },
        email: Sequelize.STRING,
        zipCode: {
          type: Sequelize.STRING,
          field: 'zip_code',
        },
        address: Sequelize.STRING,
        addressExtra: {
          type: Sequelize.STRING,
          field: 'address_extra',
        },
        city: Sequelize.STRING,
        state: Sequelize.STRING,
        productName: {
          type: Sequelize.STRING,
          field: 'product_name',
        },
        productPrice: {
          type: Sequelize.INTEGER,
          field: 'product_price',
        },
        subscriptionId: {
          type: Sequelize.INTEGER,
          field: 'subscription_id',
          references: {
            model: 'subscription',
            key: 'id',
          },
          onUpdate: 'cascade',
        },
        chargeToken: {
          type: Sequelize.STRING,
          field: 'charge_token',
        },
        trackId: {
          type: Sequelize.STRING,
          field: 'track_id',
          defaultValue: '',
        },
        tracked: {
          type: Sequelize.BOOLEAN,
          defaultValue: 0,
        },
        cronId: {
          type: Sequelize.INTEGER,
          field: 'cron_id',
          references: {
            model: 'cron',
            key: 'id',
          },
          onUpdate: 'cascade',
        },
        createdAt: {
          type: Sequelize.DATE,
          field: 'created_at',
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: 'updated_at',
        },
      }))
      .then(() => done());
  },
  down(queryInterface) {
    return queryInterface.dropAllTables();
  },
};
