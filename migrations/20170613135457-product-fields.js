

module.exports = {
  up(queryInterface, Sequelize, done) {
    return queryInterface.renameColumn('product', 'product_name', 'box_type')
      .then(() =>
        queryInterface.renameColumn('product', 'repeated', 'days'))
      .then(() =>
        queryInterface.addColumn('product', 'description', {
          type: Sequelize.STRING,
        }))
      .then(() =>
        queryInterface.renameColumn('order', 'product_name', 'box_type'))
      .then(() => done());
  },

  down(queryInterface, Sequelize, done) {
    return queryInterface.renameColumn('product', 'box_type', 'product_name').then(() =>
      queryInterface.renameColumn('product', 'days', 'repeated')).then(() =>
      queryInterface.removeColumn('product', 'description')).then(() =>
      queryInterface.renameColumn('order', 'box_type', 'product_name'))
      .then(() =>
        done());
  },
};
