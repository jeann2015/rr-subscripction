

module.exports = {
  up(queryInterface, Sequelize, done) {
    return queryInterface.addColumn('coupon', 'duration', {
      type: Sequelize.ENUM('once', 'forever'),
    }).then(() =>
      queryInterface.addColumn('coupon', 'product_id', {
        type: Sequelize.INTEGER,
      })).then(() =>
      queryInterface.addColumn('subscription', 'coupon_id', {
        type: Sequelize.INTEGER,
      }))
      .then(() => done());
  },

  down(queryInterface, Sequelize, done) {
    return queryInterface.removeColumn('coupon', 'duration').then(() =>
      queryInterface.removeColumn('coupon', 'product_id')).then(() =>
      queryInterface.removeColumn('subscription', 'coupon_id')).then(() =>
      done());
  },
};

/*
On migration, manually run query

ALTER TABLE coupon ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES product(id) ON UPDATE CASCADE;

ALTER TABLE subscription ADD CONSTRAINT fk_coupon_id FOREIGN KEY (coupon_id) REFERENCES coupon(id) ON UPDATE CASCADE;

*/
