

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.addColumn('user', 'status', {
      type: Sequelize.STRING,
    });
  },

  down(queryInterface) {
    return queryInterface.removeColumn('user', 'status');
  },
};
