

module.exports = {
  up(queryInterface, Sequelize, done) {
    return queryInterface.addColumn('subscription', 'next_discount', {
      type: Sequelize.INTEGER,
    })
      .then(() => queryInterface.addColumn('subscription', 'type_next_discount', {
        type: Sequelize.STRING,
      }))
      .then(() => done());
  },

  down(queryInterface, Sequelize, done) {
    return queryInterface.removeColumn('subscription', 'next_discount')
      .then(() =>
        queryInterface.removeColumn('subscription', 'type_next_discount'))
      .then(() => done());
  },
};
