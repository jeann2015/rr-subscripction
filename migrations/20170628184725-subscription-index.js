

module.exports = {
  up(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE subscription ADD CONSTRAINT fk_coupon_id FOREIGN KEY (coupon_id) REFERENCES coupon(id) ON UPDATE CASCADE;');
  },

  down(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE `subscription` DROP FOREIGN KEY `fk_coupon_id`;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE `subscription` DROP INDEX `fk_coupon_id`;');
    });
  },
};
