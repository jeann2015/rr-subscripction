

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('referral', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      referredBy: {
        type: Sequelize.INTEGER,
        field: 'referred_by',
      },
      me: Sequelize.INTEGER,
    })
      .then(() => {
        queryInterface.addColumn('user', 'credit', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        });
      });
  },

  down(queryInterface) {
    return queryInterface.dropTable('referral').then(() => {
      queryInterface.removeColumn('user', 'credit');
    });
  },
};
