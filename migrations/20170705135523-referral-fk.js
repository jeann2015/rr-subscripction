

module.exports = {
  up(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE referral ADD CONSTRAINT fk_user_id FOREIGN KEY (referred_by) REFERENCES user(id) ON UPDATE CASCADE;')
      .then(() => {
        queryInterface.sequelize.query('ALTER TABLE coupon ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES product(id) ON UPDATE CASCADE;');
      });
  },

  down(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE `referral` DROP FOREIGN KEY `fk_user_id`;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE `referral` DROP INDEX `fk_user_id`;').then(() => {
        queryInterface.sequelize.query('ALTER TABLE `coupon` DROP FOREIGN KEY `fk_product_id`;').then(() => {
          queryInterface.sequelize.query('ALTER TABLE `coupon` DROP INDEX `fk_product_id`;');
        });
      });
    });
  },
};

