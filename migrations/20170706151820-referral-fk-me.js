

module.exports = {
  up(queryInterface) {
    return queryInterface.sequelize.query('SET foreign_key_checks = 0;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE referral ADD CONSTRAINT fk_user_me_id FOREIGN KEY (me) REFERENCES user(id) ON UPDATE CASCADE;').then(() => {
        queryInterface.sequelize.query('SET foreign_key_checks = 1;');
      });
    });
  },

  down(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE `referral` DROP FOREIGN KEY `fk_user_me_id`;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE `referral` DROP INDEX `fk_user_me_id`;');
    });
  },
};

