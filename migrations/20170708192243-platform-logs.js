

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('log', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      type: Sequelize.ENUM('info', 'warn', 'error'),
      message: Sequelize.STRING,
      user_id: Sequelize.INTEGER,
      admin_id: Sequelize.INTEGER,
      created_at: Sequelize.DATE,
      updated_at: Sequelize.DATE,
    });
  },

  down(queryInterface) {
    return queryInterface.dropTable('log');
  },
};
