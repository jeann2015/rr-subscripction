

module.exports = {
  up(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE log ADD CONSTRAINT fk_log_user FOREIGN KEY (user_id) REFERENCES user(id) ON UPDATE CASCADE;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE log ADD CONSTRAINT fk_log_admin FOREIGN KEY (admin_id) REFERENCES admin(id) ON UPDATE CASCADE;');
    });
  },

  down(queryInterface) {
    return queryInterface.sequelize.query('ALTER TABLE `log` DROP FOREIGN KEY `fk_log_user`;').then(() => {
      queryInterface.sequelize.query('ALTER TABLE `log` DROP INDEX `fk_log_user`;').then(() => {
        queryInterface.sequelize.query('ALTER TABLE `log` DROP FOREIGN KEY `fk_log_admin`;').then(() => {
          queryInterface.sequelize.query('ALTER TABLE `log` DROP INDEX `fk_log_admin`;');
        });
      });
    });
  },
};
