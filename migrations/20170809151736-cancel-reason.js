

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.addColumn('subscription', 'cancel_reason', {
      type: Sequelize.STRING,
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.removeColumn('subscription', 'cancel_reason');
  },
};
