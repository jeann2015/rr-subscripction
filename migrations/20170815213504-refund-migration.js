

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('refund', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      charge_token: Sequelize.STRING,
      refund_id: Sequelize.STRING,
      amount: Sequelize.INTEGER,
      reason: Sequelize.STRING,
      adminId: {
        type: Sequelize.INTEGER,
        field: 'admin_id',
      },
      orderId: {
        type: Sequelize.INTEGER,
        field: 'order_id',
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },

  down(queryInterface) {
    return queryInterface.dropTable('refund');
  },
};
