module.exports = {
  up(queryInterface, Sequelize) {
    // Coupons may now have a days duration.
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn('coupon', 'times_usable', {
          type: Sequelize.INTEGER,
          defaultValue: -1,
        },
        { transaction: t }),
        queryInterface.addColumn('subscription', 'coupon_use_count', {
          type: Sequelize.INTEGER,
          defaultValue: 0,
        },
        { transaction: t }),
        queryInterface.addIndex('coupon', ['code'], { indexName: 'code_index', indicesType: 'UNIQUE' },
          { transaction: t }),
        queryInterface.changeColumn('coupon', 'duration', {
          type: Sequelize.ENUM('once', 'forever', 'custom'),
        }, { transaction: t }),
      ]);
    });
  },

  down(queryInterface, Sequelize) {
    // Removing the Index from COUPON.
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('coupon', 'times_usable', { transaction: t }),
        queryInterface.removeColumn('subscription', 'coupon_use_count', { transaction: t }),
        queryInterface.sequelize.query('ALTER TABLE `coupon` DROP INDEX `code_index`;', { transaction: t }),
        queryInterface.changeColumn('coupon', 'duration',
          { type: Sequelize.ENUM('once', 'forever'), allowNull: false }, { transaction: t }),
      ]);
    });
  },
};
