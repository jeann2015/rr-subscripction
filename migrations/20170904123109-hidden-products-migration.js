'use strict';

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    queryInterface.addColumn('product', 'hidden', {
      type: Sequelize.BOOLEAN,
      defaultValue: 0,
    })
    .then(() => done());
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('product', 'hidden');
  },
};
