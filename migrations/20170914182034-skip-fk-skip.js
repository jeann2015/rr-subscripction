module.exports = {
  up(queryInterface, Sequelize) {
    // Coupons may now have a days duration.
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([

        queryInterface.sequelize.query('ALTER TABLE skip ADD CONSTRAINT fk_skip_subscription FOREIGN KEY (subscription) REFERENCES subscription(id) ON UPDATE CASCADE;',
          { transaction: t }),
      ]);
    });
  },

  down(queryInterface, Sequelize) {
    // Removing the Index from COUPON.
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.sequelize.query('ALTER TABLE `skip` DROP FOREIGN KEY `fk_skip_subscription`;', { transaction: t }),
      ]);
    });
  },
};
