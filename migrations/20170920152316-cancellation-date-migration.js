'use strict';

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    queryInterface.addColumn('subscription', 'cancellation_date', {
      type: Sequelize.DATE,
    })
    .then(() => done());
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('subscription', 'cancellation_date');
  },
};
