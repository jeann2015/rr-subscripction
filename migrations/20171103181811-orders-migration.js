'use strict';

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    return queryInterface.addColumn('order', 'tracking_email', {
      type: Sequelize.ENUM('not_sent', 'sent', 'error'),
    })
      .then(() => queryInterface.addColumn('order', 'shipstation_status', {
        type: Sequelize.ENUM('awaiting_payment', 'awaiting_shipment', 'shipped', 'on_hold', 'cancelled'),
      }))
        .then(() => queryInterface.addColumn('order', 'shipstation_order', {
          type: Sequelize.STRING,
        }))
          .then(() => queryInterface.addColumn('order', 'box_number', {
            type: Sequelize.INTEGER,
          }))
            .then(() => done());
  },

  down: function (queryInterface, Sequelize, done) {
    return queryInterface.removeColumn('order', 'tracking_email')
      .then(() => queryInterface.removeColumn('order', 'shipstation_status'))
        .then(() => queryInterface.removeColumn('order', 'shipstation_order'))
          .then(() => queryInterface.removeColumn('order', 'box_number'))
            .then(() => done());
  },
};
