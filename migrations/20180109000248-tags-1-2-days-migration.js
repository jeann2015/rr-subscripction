'use strict';

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    return queryInterface.createTable('tag_day_name', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: Sequelize.STRING,
    })
      .then(() => {
        return queryInterface.createTable('tag_day', {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
          },
          zip_code: Sequelize.STRING,
          name: Sequelize.INTEGER,
        })
        .then(() => {
          return queryInterface.sequelize.query('ALTER TABLE tag_day ADD CONSTRAINT fk_tag_day_name FOREIGN KEY (name) REFERENCES tag_day_name(id) ON UPDATE CASCADE;')
            .then(() => {
              done();
            });
        });
      });
  },

  down: function (queryInterface, Sequelize, done) {
    return queryInterface.sequelize.query('ALTER TABLE `tag_day` DROP INDEX `fk_tag_day_name`;')
      .then(() => {
        queryInterface.dropTable('tag_day_name')
        .then(() => {
          queryInterface.dropTable('tag_day')
            .then(() => {
              done();
            });
        });
      });
  },
};
