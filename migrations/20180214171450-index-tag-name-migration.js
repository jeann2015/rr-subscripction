'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE `tag_day` ADD INDEX `fk_zip_code` (`zip_code`);');
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE `tag_day` DROP INDEX `fk_zip_code`;');
  },
};
