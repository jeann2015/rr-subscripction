'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('applied_discounts', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      user: Sequelize.INTEGER,
      referreer: Sequelize.INTEGER,
      referreerAmount: {
        type: Sequelize.INTEGER,
        field: 'referreer_amount',
      },
      coupon: Sequelize.INTEGER,
      couponAmount: {
        type: Sequelize.INTEGER,
        field: 'coupon_amount',
      },
      userCreditAmount: {
        type: Sequelize.INTEGER,
        field: 'user_credit_amount',
      },
      nextDiscountType: {
        type: Sequelize.STRING,
        field: 'next_discount_type',
      },
      nextDiscountAmount: {
        type: Sequelize.INTEGER,
        field: 'next_discount_amount',
      },
      nextDiscountPercent: {
        type: Sequelize.INTEGER,
        field: 'next_discount_percent',
      },
      stripeToken: {
        type: Sequelize.STRING,
        field: 'stripe_token',
      },
      cronId: {
        type: Sequelize.INTEGER,
        field: 'cron_id',
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    }).then(() => {
      return Promise.all([
        queryInterface.sequelize.query('ALTER TABLE applied_discounts ADD CONSTRAINT fk_applied_discounts_user FOREIGN KEY (user) REFERENCES user(id) ON UPDATE CASCADE;'),
        queryInterface.sequelize.query('ALTER TABLE applied_discounts ADD CONSTRAINT fk_applied_discounts_referreer FOREIGN KEY (referreer) REFERENCES user(id) ON UPDATE CASCADE;'),
        queryInterface.sequelize.query('ALTER TABLE applied_discounts ADD CONSTRAINT fk_applied_discounts_coupon FOREIGN KEY (coupon) REFERENCES coupon(id) ON UPDATE CASCADE;'),
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.sequelize.query('ALTER TABLE `applied_discounts` DROP FOREIGN KEY `fk_applied_discounts_coupon`;'),
      queryInterface.sequelize.query('ALTER TABLE `applied_discounts` DROP FOREIGN KEY `fk_applied_discounts_referreer`;'),
      queryInterface.sequelize.query('ALTER TABLE `applied_discounts` DROP FOREIGN KEY `fk_applied_discounts_coupon`;'),
    ]).then(() => {
      queryInterface.dropTable('applied_discounts');
    });
  },
};
