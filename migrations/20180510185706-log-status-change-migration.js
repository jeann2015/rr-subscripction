'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('log', 'user_status', {
      type: Sequelize.ENUM('active', 'canceled', 'pending', 'paused'),
    })
    .then(() => queryInterface.addColumn('log', 'user_status_date', {
      type: Sequelize.DATE,
    }));
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('log', 'user_status')
    .then(() => queryInterface.removeColumn('log', 'user_status_date'));
  },
};
