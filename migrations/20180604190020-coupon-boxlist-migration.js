'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('coupon', 'box_list', {
      type: Sequelize.TEXT,
    })
    .then(() => queryInterface.changeColumn('coupon', 'duration', {
      type: Sequelize.ENUM('once', 'forever', 'custom', 'custom_boxes'),
    }));
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('coupon', 'box_list')
    .then(() => queryInterface.changeColumn('coupon', 'duration', {
      type: Sequelize.ENUM('once', 'forever', 'custom'),
    }));
  },
};
