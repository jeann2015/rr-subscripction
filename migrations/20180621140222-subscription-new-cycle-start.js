'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('subscription', 'new_cycle_start', {
      type: Sequelize.DATE,
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('subscription', 'new_cycle_start');
  },
};
