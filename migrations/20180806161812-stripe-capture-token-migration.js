'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('subscription', 'capture_token', {
      type: Sequelize.TEXT,
    });
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('subscription', 'capture_token');
  },
};
