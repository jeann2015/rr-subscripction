'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn('order', 'shipstation_status', {
      type: Sequelize.ENUM('awaiting_payment', 'awaiting_shipment', 'shipped', 'on_hold', 'cancelled', 'pending_fulfillment'),
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn('order', 'shipstation_status', {
      type: Sequelize.ENUM('awaiting_payment', 'awaiting_shipment', 'shipped', 'on_hold', 'cancelled'),
    });
  },
};
