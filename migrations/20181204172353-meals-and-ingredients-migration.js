'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('meals', {
      sku: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      default: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      priority: {
        type: Sequelize.INTEGER,
      },
      deprecated: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      updated_at: {
        type: Sequelize.DATE,
      },
    })
    .then(() => {
      return queryInterface.createTable('ingredients', {
        id: {
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        created_at: {
          type: Sequelize.DATE,
        },
        updated_at: {
          type: Sequelize.DATE,
        },
      });
    })
    .then(() => {
      return queryInterface.createTable('meals_ingredients', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        meal_sku: {
          type: Sequelize.STRING,
          references: {
            model: 'meals',
            key: 'sku',
          },
          onUpdate: 'cascade',
          allowNull: false,
        },
        ingredient_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'ingredients',
            key: 'id',
          },
          onUpdate: 'cascade',
          allowNull: false,
        },
        created_at: {
          type: Sequelize.DATE,
        },
        updated_at: {
          type: Sequelize.DATE,
        },
      });
    })
    .then(() => {
      return queryInterface.createTable('excluded_ingredients', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        ingredient_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'ingredients',
            key: 'id',
          },
          onUpdate: 'cascade',
          allowNull: false,
        },
        subscription_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'subscription',
            key: 'id',
          },
          onUpdate: 'cascade',
          allowNull: false,
        },
        created_at: {
          type: Sequelize.DATE,
        },
        updated_at: {
          type: Sequelize.DATE,
        },
      });
    })
    .then(() => {
      return queryInterface.createTable('meals_stock', {
        id: {
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          type: Sequelize.INTEGER,
        },
        meal_sku: {
          type: Sequelize.STRING,
          references: {
            model: 'meals',
            key: 'sku',
          },
          onUpdate: 'cascade',
        },
        stock: {
          type: Sequelize.INTEGER,
        },
        local_stock: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        warehouse: {
          type: Sequelize.INTEGER,
        },
        created_at: {
          type: Sequelize.DATE,
        },
        updated_at: {
          type: Sequelize.DATE,
        },
      });
    })
    .then(() => {
      return queryInterface.createTable('box_detail_history', {
        id: {
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          type: Sequelize.INTEGER,
        },
        order_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'order',
            key: 'id',
          },
          allowNull: false,
          onUpdate: 'cascade',
        },
        meal_sku: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'meals',
            key: 'sku',
          },
          onUpdate: 'cascade',
        },
        meal_quantity: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        created_at: {
          type: Sequelize.DATE,
        },
        updated_at: {
          type: Sequelize.DATE,
        },
      });
    })
    .then(() => {
      return queryInterface.createTable('warehouses', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tag_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        warehouse: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      });
    })
    .then(() => {
      return queryInterface.addColumn(
        'order',
        'configuration',
        Sequelize.ENUM('standard', 'custom'),
      );
    })
    .then(() => queryInterface.sequelize.query('TRUNCATE tag_day'))
    .then(() => queryInterface.bulkDelete('tag_day_name', null, {}))
    .then(() => queryInterface.sequelize.query('ALTER TABLE `tag_day_name` AUTO_INCREMENT = 1'))
    .then(() => {
      return queryInterface.addColumn(
        'tag_day_name',
        'warehouse_id',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'warehouses',
            key: 'id',
          },
        },
      );
    })
    .then(() => queryInterface.renameColumn('tag_day_name', 'name', 'transit'))
    .then(() => queryInterface.sequelize.query("INSERT INTO `ingredients` (`name`) VALUES ('Mango'),('Carrot'),('Navy Beans'),('Turmeric'),('Coconut Butter'),('Raspberry'),('Sweet Potato'),('Ground Pumpkin Seeds'),('Cardamom'),('Butternut Squash'),('Nutmeg'),('Pear'),('Blueberry'),('Beet'),('Ginger'),('Zucchini'),('Sunflower Butter'),('Cinnamon'),('Hemp Seeds'),('Peas'),('Broccoli'),('Olive Oil'),('Mint'),('Papaya'),('Coconut Water'),('Lucuma Powder'),('Apple'),('Sacha Inchi Oil'),('Date'),('Spinach'),('Banana'),('White Quinoa'),('Chia Seeds'),('Chickpeas'),('Cauliflower'),('Tahini'),('Avocado Oil'),('Basil'),('Coconut Milk'),('Kale'),('Garlic'),('Green Beans'),('Flax Seeds');"))
    .then(() => queryInterface.sequelize.query("INSERT INTO `meals` (`sku`, `name`, `default`, `deprecated`) VALUES ('RRL70010','Broccoli + Green Beans',0,0),('RRL70020','Butternut Squash + Banana',0,0),('RRL70030','Cauliflower + Kale',0,0),('RRL70040','Sweet Potato + Mango',0,0),('RRL70050','Pear + Ginger',0,0),('RRL70060','Peas + Zucchini',0,0),('RRL70080','Chickpeas + Tahini',0,0),('RRL70110','Spinach + Banana',0,0),('RRL70120','Beet + Sweet Potato',0,0),('RRL70150','Carrot + Apple',0,0),('RRL70160','Papaya + Carrot',0,0),('RRL70170','Peas + Mint',0,0),('RRL70180','Blueberry + Sunbutter',0,0),('RRL70190','Blueberry + Beet',0,0),('RRL70200','Butternut Squash + Pear',0,0),('RRL70210','Raspberry + Sweet Potato',0,0),('RRL70220','Mango + Carrot',0,0);"))
    .then(() => queryInterface.sequelize.query("INSERT INTO `warehouses` (`tag_id`, `warehouse`) VALUES (4498,'Dallas, TX'),(4492,'Marshall, MN'),(4493,'Mt Juliet, TN'),(4497,'Raleigh, NC'),(4491,'Rochester, NY'),(4496,'Salt Lake City, UT'),(4494,'Sanford, FL'),(4495,'Santa Maria, CA');"))
    .then(() => queryInterface.sequelize.query("INSERT INTO `meals_ingredients` (`meal_sku`, `ingredient_id`) VALUES ('RRL70010',43),('RRL70020',10),('RRL70010',21),('RRL70010',34),('RRL70010',37),('RRL70010',42),('RRL70020',32),('RRL70020',31),('RRL70020',5),('RRL70020',9),('RRL70030',35),('RRL70030',40),('RRL70030',32),('RRL70030',41),('RRL70040',7),('RRL70030',37),('RRL70040',1),('RRL70040',33),('RRL70040',39),('RRL70040',26),('RRL70050',12),('RRL70050',35),('RRL70050',39),('RRL70050',15),('RRL70050',32),('RRL70060',20),('RRL70060',16),('RRL70060',37),('RRL70060',38),('RRL70060',19),('RRL70080',34),('RRL70080',36),('RRL70080',35),('RRL70080',4),('RRL70110',30),('RRL70110',31),('RRL70110',33),('RRL70110',32),('RRL70110',5),('RRL70120',14),('RRL70120',7),('RRL70120',29),('RRL70120',18),('RRL70150',2),('RRL70120',5),('RRL70150',27),('RRL70150',19),('RRL70150',4),('RRL70150',28),('RRL70160',24),('RRL70160',2),('RRL70160',19),('RRL70160',25),('RRL70170',20),('RRL70170',21),('RRL70160',26),('RRL70170',22),('RRL70170',23),('RRL70170',3),('RRL70180',13),('RRL70180',16),('RRL70180',17),('RRL70180',18),('RRL70180',19),('RRL70190',13),('RRL70190',14),('RRL70190',5),('RRL70190',3),('RRL70190',15),('RRL70200',10),('RRL70200',8),('RRL70200',11),('RRL70200',5),('RRL70210',6),('RRL70210',7),('RRL70200',12),('RRL70210',8),('RRL70210',9),('RRL70220',1),('RRL70210',5),('RRL70220',3),('RRL70220',2),('RRL70220',4),('RRL70220',5);"))
    .then(() => queryInterface.sequelize.query("INSERT INTO `tag_day_name` (`transit`, `warehouse_id`) VALUES ('1 DAY',1),('2 DAY',1),('1 DAY',2),('2 DAY',2),('1 DAY',3),('2 DAY',3),('1 DAY',4),('2 DAY',4),('1 DAY',5),('2 DAY',5),('1 DAY',6),('2 DAY',6),('1 DAY',7),('2 DAY',7),('1 DAY',8),('2 DAY',8);"))
    .then(() => queryInterface.sequelize.query(`
      INSERT INTO product (id, box_type, price, days, description, hidden)
      VALUES
        (1,'Meal Maker',0,0,'Meal Maker',1),
        (2,'Starter',9500,14,'Starter Box every 14 days',1),
        (3,'Regular',9500,14,'Regular Box every 14 days',1),
        (4,'Regular',9000,14,'Regular Box every 14 days at $90',1),
        (5,'Regular',9500,28,'Regular Box every 28 Days',1),
        (6,'Starter',9500,28,'Starter Box every 28 Days',1),
        (7,'Regular',9000,28,'Regular Box every 28 days at $90',1),
        (8,'Regular',9500,42,'Regular Box every 42 Days',1),
        (9,'Babycook Original Peacock',4995,0,'BÉABA® Babycook® Original',1),
        (10,'Babycook Cloud',9995,0,'BÉABA® Babycook® Cloud',1),
        (11,'Babycook Blueberry',9995,0,'BÉABA® Babycook® Blueberry',1),
        (12,'Babycook Lemon',9995,0,'BÉABA® Babycook® Lemon',1),
        (13,'Babycook Navy',9995,0,'BÉABA® Babycook® Navy',1),
        (14,'Babycook Paprika',9995,0,'BÉABA® Babycook® Paprika',1),
        (15,'Babycook Pistachio',9995,0,'BÉABA® Babycook® Pistachio',1),
        (16,'Babycook Plus Cloud',14995,0,'BÉABA® Babycook® Plus Cloud',1),
        (17,'Babycook Plus Rose Gold',14995,0,'BÉABA® Babycook® Plus Rose Gold',1),
        (18,'Babycook Rose Gold',9995,0,'BÉABA® Babycook® Rose Gold',1),
        (19,'Regular',9500,7,'Regular Box every 7 Days',1),
        (20,'Regular',9500,56,'Regular Box every 56 Days',1),
        (21,'12 Meals Box',6588,NULL,'12 Meals Box',0),
        (22,'24 Meals Box',11976,NULL,'24 Meals Box',0),
        (23,'24 Meals Box Migrated',9500,NULL,'24 Meals Box Migrated',1);`,
    ));
  },

  down: function (queryInterface) {
    return queryInterface.dropTable('excluded_ingredients')
    .then(() => queryInterface.sequelize.query('TRUNCATE tag_day'))
    .then(() => queryInterface.bulkDelete('tag_day_name', null, {}))
    .then(() => queryInterface.sequelize.query('ALTER TABLE `tag_day_name` AUTO_INCREMENT = 1'))
    .then(() => queryInterface.renameColumn('tag_day_name', 'transit', 'name'))
    .then(() => queryInterface.removeColumn('tag_day_name', 'warehouse_id'))
    .then(() => queryInterface.removeColumn('order', 'configuration'))
    .then(() => queryInterface.sequelize.query("INSERT INTO `tag_day_name` (`id`, `name`) VALUES (1,'1 DAY GROUND ST. LOUIS'),(2,'2 DAY GROUND ST. LOUIS'),(3,'2 DAY AIR ST. LOUIS'),(4,'1 DAY GROUND STOCKTON'),(5,'2 DAY GROUND STOCKTON'),(6,'1 DAY GROUND DELAWARE'),(7,'2 DAY GROUND DELAWARE'),(8,'2 DAY AIR DELAWARE');"))
    .then(() => queryInterface.dropTable('warehouses'))
    .then(() => queryInterface.dropTable('meals_ingredients'))
    .then(() => queryInterface.dropTable('ingredients'))
    .then(() => queryInterface.dropTable('meals_stock'))
    .then(() => queryInterface.dropTable('box_detail_history'))
    .then(() => queryInterface.dropTable('meals'));
  },
};
