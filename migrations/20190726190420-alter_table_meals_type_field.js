'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('meals', 'kind', {
      type: Sequelize.STRING(18),
      defaultValue: 'default',
      after: 'deprecated',
    });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('meals', 'kind');
  }
};
