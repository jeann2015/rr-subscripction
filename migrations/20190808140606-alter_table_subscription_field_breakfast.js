"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("subscription", "breakfast_count", {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      after: "frequency"
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn("subscription", "breakfast_count");
  }
};
