'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`
      INSERT INTO \`meals\` (\`sku\`, \`name\`, \`default\`, \`priority\`, \`deprecated\`, \`kind\`)
      VALUES
        ('RRL70240', 'Oats + Acai', 0, 1, 0, 'breakfast'),
        ('RRL70250', 'Oats + Berry', 0, 2, 0, 'breakfast'),
        ('RRL70260', 'Oats + Mango', 0, 4, 0, 'breakfast'),
        ('RRL70270', 'Oats + Cinnamon', 0, 3, 0, 'breakfast');
    `).then(() => {
      return queryInterface.sequelize.query("INSERT INTO `ingredients` (`name`) VALUES ('Oats'), ('Acai'), ('Cranberry Seed Oil'), ('Strawberry');");
    }).then(() => {
      return queryInterface.sequelize.query(`
        INSERT INTO \`meals_stock\` (\`meal_sku\`, \`stock\`, \`local_stock\`, \`warehouse\`)
        VALUES
          ('RRL70240', 0, 0, 1),
          ('RRL70240', 0, 0, 2),
          ('RRL70240', 0, 0, 3),
          ('RRL70240', 0, 0, 4),
          ('RRL70240', 0, 0, 5),
          ('RRL70240', 0, 0, 6),
          ('RRL70240', 0, 0, 7),
          ('RRL70240', 0, 0, 8),
          ('RRL70250', 0, 0, 1),
          ('RRL70250', 0, 0, 2),
          ('RRL70250', 0, 0, 3),
          ('RRL70250', 0, 0, 4),
          ('RRL70250', 0, 0, 5),
          ('RRL70250', 0, 0, 6),
          ('RRL70250', 0, 0, 7),
          ('RRL70250', 0, 0, 8),
          ('RRL70260', 0, 0, 1),
          ('RRL70260', 0, 0, 2),
          ('RRL70260', 0, 0, 3),
          ('RRL70260', 0, 0, 4),
          ('RRL70260', 0, 0, 5),
          ('RRL70260', 0, 0, 6),
          ('RRL70260', 0, 0, 7),
          ('RRL70260', 0, 0, 8),
          ('RRL70270', 0, 0, 1),
          ('RRL70270', 0, 0, 2),
          ('RRL70270', 0, 0, 3),
          ('RRL70270', 0, 0, 4),
          ('RRL70270', 0, 0, 5),
          ('RRL70270', 0, 0, 6),
          ('RRL70270', 0, 0, 7),
          ('RRL70270', 0, 0, 8) 
      `);
    }).then(() => {
      return queryInterface.sequelize.query(`
        INSERT INTO \`meals_ingredients\` (\`meal_sku\`, \`ingredient_id\`)
        VALUES
          ('RRL70240', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70240', ( SELECT id FROM ingredients AS i WHERE i.name = 'Acai' )),
          ('RRL70250', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70250', ( SELECT id FROM ingredients AS i WHERE i.name = 'Blueberry' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Cinnamon' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Mango' ))
      `);
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query("DELETE FROM `meals` WHERE sku in ('RRL70240', 'RRL70250', 'RRL70260', 'RRL70270');");
  },
};
