'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`
      INSERT INTO \`meals_ingredients\` (\`meal_sku\`, \`ingredient_id\`)
      VALUES
        ('RRL70240', ( SELECT id FROM ingredients AS i WHERE i.name = 'Banana' )),
        ('RRL70240', ( SELECT id FROM ingredients AS i WHERE i.name = 'Raspberry' )),
        ('RRL70240', ( SELECT id FROM ingredients AS i WHERE i.name = 'Cranberry Seed Oil' )),
        ('RRL70250', ( SELECT id FROM ingredients AS i WHERE i.name = 'Strawberry' )),
        ('RRL70250', ( SELECT id FROM ingredients AS i WHERE i.name = 'Date' )),
        ('RRL70250', ( SELECT id FROM ingredients AS i WHERE i.name = 'Cranberry Seed Oil' )),
        ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Pear' )),
        ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Sweet Potato' )),
        ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Date' )),
        ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Butternut Squash' )),
        ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Ginger' )),
        ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Coconut Butter' ))
    `);
  },

  down: (queryInterface) => {},
};
