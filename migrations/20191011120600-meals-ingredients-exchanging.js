'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`
      DELETE FROM meals_ingredients WHERE meal_sku IN ( 'RRL70260', 'RRL70270');
    `).then(() => {
      return queryInterface.sequelize.query(`
        INSERT INTO \`meals_ingredients\` (\`meal_sku\`, \`ingredient_id\`)
        VALUES
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Mango' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Butternut Squash' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Ginger' )),
          ('RRL70260', ( SELECT id FROM ingredients AS i WHERE i.name = 'Coconut Butter' )),

          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Oats' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Cinnamon' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Pear' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Sweet Potato' )),
          ('RRL70270', ( SELECT id FROM ingredients AS i WHERE i.name = 'Date' ));
      `);
    });
  },

  down: (queryInterface) => {},
};
