import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import inlineCSS from '../public/styles.css';

export default class MyDocument extends Document {
  render() {
    const getCustomStyles = () => {
      let stylesheet;
      if (process.env.NODE_ENV === 'production') {
            // In production, serve pre-built CSS file from /assets/{version}/index.css
            // TODO: Implement SSR for SASS
        const pathToCSS = '/assets/styles.css';
        stylesheet = <link rel='stylesheet' type='text/css' href={pathToCSS} />;
      } else {
            // In development, serve CSS inline (with live reloading) with webpack
            // NB: Not using dangerouslySetInnerHTML will cause problems with some CSS
        stylesheet = <style dangerouslySetInnerHTML={{ __html: inlineCSS }} />;
      }
      return stylesheet;
    };

    let trackScripts = false;
    if (qa_env) {
      trackScripts = true;
    }

    return (
      <html>
        <Head>
          <meta charSet='UTF-8' />
          <meta httpEquiv='Content-Type' content='text/html; charset=UTF-8' />
          <meta httpEquiv='X-UA-Compatible' content='IE=edge,chrome=1' />
          <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no,minimal-ui' />
          <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' />
          <script src='https://cdn.polyfill.io/v2/polyfill.min.js' />
          <script src='https://cdn.optimizely.com/js/8534050280.js' />
          <meta name='theme-color' content='#00203d' />
          {/* iOS Installable WebApp: Start */}
          <meta name='apple-mobile-web-app-capable' content='yes' />
          <meta name='apple-mobile-web-app-status-bar-style' content='black' />
          {/* home screen icon */}
          { getCustomStyles() }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              /*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="raisedreal.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();/*]]>*/
            ` }}
            />
          }

          { trackScripts &&
            <script src="//load.sumome.com/" data-sumo-site-id="9063f65b28e8a28f61554ecf8c4b96e8666b237e08b4cb425fc0ad7abaf19d2f" async="async" />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              /*<![CDATA[*/(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
              0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
              for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\\/\\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
              mixpanel.init("0ed0dcada861abfa62bae5b755fb2184");/*]]>*/
            ` }}
            />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '1268396543225849');
              fbq('track', 'PageView');
              fbq('track', 'Onboarding');
            ` }}
            />
          }
          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              adroll_adv_id = "XEQTC4MMDBB2LJV27MLD3D";
              adroll_pix_id = "MTZCBOPJ5RHFVGM33SBDAH";
              (function () {
              var _onload = function(){
              if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
              if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
              var scr = document.createElement("script");
              var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
              scr.setAttribute('async', 'true');
              scr.type = "text/javascript";
              scr.src = host + "/j/roundtrip.js";
              ((document.getElementsByTagName('head') || [null])[0] ||
              document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
              };
              if (window.addEventListener) {window.addEventListener('load', _onload, false);}
              else {window.attachEvent('onload', _onload)}
              }());
            ` }}
            />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
            (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
            })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-WMKP3ZB':true});
            ` }}
            />
          }

          { trackScripts &&
            <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4111793.js" />
          }

          { trackScripts &&
            <script src="https://js.b1js.com/tagcontainer.js?id=6bd4794e8eef434283e889592e4e579e&type=1" />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              !function(w,d){if(!w.rdt){var p=w.rdt=function(){p.sendEvent?p.sendEvent.apply(p,arguments):p.callQueue.push(arguments)};p.callQueue=[];var t=d.createElement("script");t.src="https://www.redditstatic.com/ads/pixel.js",t.async=!0;var s=d.getElementsByTagName("script")[0];s.parentNode.insertBefore(t,s)}}(window,document);rdt('init','t2_39keb5ds');rdt('track', 'PageVisit');
            ` }}
            />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              (function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
              {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
              a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
              r.src=n;var u=t.getElementsByTagName(s)[0];
              u.parentNode.insertBefore(r,u);})(window,document,
              'https://sc-static.net/scevent.min.js');
              snaptr('init', 'dfad81e7-af7b-4ab7-94a1-8e0d28b403f6', {});
              snaptr('track', 'PAGE_VIEW');
            ` }}
            />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'AW-866054548');
            ` }}
            />
          }

          { trackScripts &&
            <script dangerouslySetInnerHTML={{ __html: `
              !function(e){if(!window.pintrk){window.pintrk=function(){window.pintrk.queue.push(
              Array.prototype.slice.call(arguments))};var
              n=window.pintrk;n.queue=[],n.version="3.0";var
              t=document.createElement("script");t.async=!0,t.src=e;var
              r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
              pintrk('load', '2614303988544');
              pintrk('page');
            ` }}
            />
          }

          { trackScripts &&
            <script src="https://www.refersion.com/tracker/v3/pub_3c553cf8f1ea103521ed.js" />
          }
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
