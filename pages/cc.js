import React, { Component } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga';
import { EditCard } from '../public/components';


export default class Main extends Component {
  componentDidMount() {
    if (typeof window === 'object') {
      ReactGA.initialize(GA_ID);
      ReactGA.plugin.require('GTM-WMKP3ZB');
      ReactGA.pageview('/signup');
    }
  }

  render() {
    return (
      <div>
        <Head>
          <title>Raised Real: Onboarding</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
          />
          <meta name="theme-color" content="#63C29D" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="white" />
          <link rel="icon" type="image/png" href="/static/images/favicon.png" />
        </Head>
        <EditCard url={this.props.url} />
      </div>
    );
  }
}

Main.propTypes = {
  url: PropTypes.string,
};

Main.defaultProps = {
  url: '',
};
