import React, { Component } from 'react';
import Head from 'next/head';
import ReactGA from 'react-ga';

import { Signup } from '../public/components';

export default class Main extends Component {
  componentDidMount() {
    if (typeof window === 'object') {
      // ReactGA.initialize('UA-85576000-1');
      ReactGA.initialize(GA_ID);
      ReactGA.plugin.require('GTM-WMKP3ZB');
      ReactGA.pageview('/signup');
    }
  }

  render() {
    return (
      <div>
        <Head>
          <title>Raised Real: Onboarding</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
          />
          <meta name="theme-color" content="#63C29D" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="white" />
          <link rel="icon" type="image/png" href="/static/images/favicon.png" />

          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Homemade Baby Food For Real Life | Raised Real" />
          <meta property="og:description" content="Baby ready meals delivered every two weeks, and ready to steam in 15 minutes." />
          <meta property="og:url" content="http://www.raisedreal.com/" />
          <meta property="og:site_name" content="Raised Real" />
          <meta property="og:image" content="http://www.raisedreal.com/wp-content/uploads/2018/02/babies.jpg" />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:description" content="Baby ready meals delivered every two weeks, and ready to steam in 15 minutes." />
          <meta name="twitter:title" content="Homemade Baby Food For Real Life | Raised Real" />
          <meta name="twitter:image" content="http://www.raisedreal.com/wp-content/uploads/2018/02/babies.jpg" />
        </Head>
        <Signup />
      </div>
    );
  }
}
