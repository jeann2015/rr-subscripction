import React, { PureComponent } from 'react';
import styles from './AddTinyHuman.css';

export const AddTinyHuman = ({ onClick }) => (
  <div className='add-tiny-human' onClick={onClick}>
    <span>+</span> ADD ANOTHER TINY HUMAN
  </div>
);
