import React, { PureComponent } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

export class Alert extends PureComponent {
  static propTypes = {
    success: PropTypes.bool,
    error: PropTypes.bool,
  }

  static defaultProps = {
    success: false,
    error: false,
  }

  componentWillMount() {
    document.body.className = 'with-errors';
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    if (document.querySelectorAll('.Alert').length === 1) document.body.className = '';
  }

  render() {
    const { success, error, children } = this.props;
    return (
      <span className={cn('Alert', { success, error })}>
        {children}
      </span>
    );
  }
}
