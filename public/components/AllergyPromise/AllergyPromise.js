import React from 'react';
import { Paragraph } from '../../../public/components';
import styles from './AllergyPromise.css';


export const AllergyPromise = () => (
  <div className="AllergyPromise">
    <span className="subtitle">Allergy Promise</span>
    <div className="icons">
      <span className="gluten" />
      <span className="soy" />
      <span className="dairy" />
    </div>
    <Paragraph>
      All of our meals are gluten, soy, dairy, and nut-free. However, they are packaged in a facility that stores these ingredients. Check with your pediatrician if you have concerns.
    </Paragraph>
  </div>
);
