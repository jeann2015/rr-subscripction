import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { GroupField, TextField, Title } from '../../../public/components';
import Dropdown from '../Dropdown/Dropdown';
import { US_STATES } from '../../utils';

import './BillingForm.css';

export class BillingForm extends Component {
  static propTypes = {
    defaultValues: PropTypes.shape({
      billingName: PropTypes.string,
      billingLastName: PropTypes.string,
      billingAddress: PropTypes.string,
      billingAddressExtra: PropTypes.string,
      billingCity: PropTypes.string,
      billingState: PropTypes.string,
      billingZipCode: PropTypes.string,
    }),
  }

  static defaultProps = {
    defaultValues: {
      billingName: '',
      billingLastName: '',
      billingAddress: '',
      billingAddressExtra: '',
      billingCity: '',
      billingState: '',
      billingZipCode: '',
    },
  }

  render() {
    const { defaultValues, onInputChange, validation } = this.props;
    const { billingName, billingLastName, billingAddress, billingAddressExtra, billingCity, billingState, billingZipCode } = defaultValues;
    const dropdownOptions = Object.keys(US_STATES).map(country => ({ value: country, label: country }));
    return (
      <div className="BillingForm">
        <hr className="separator" />
        <Title>BILLING ADDRESS</Title>
        <GroupField className='cols-2'>
          <TextField
            className={cn({ invalid: !validation.validFirstname })}
            name="billingName"
            defaultValue={billingName}
            label="First name"
            onChange={onInputChange}
          />
          <TextField
            className={cn({ invalid: !validation.validLastname })}
            name="billingLastName"
            defaultValue={billingLastName}
            label="Last name"
            onChange={onInputChange}
            onBlur={onInputChange}
          />
        </GroupField>
        <GroupField className='cols-2-3'>
          <TextField
            className={cn({ invalid: !validation.validAddress })}
            defaultValue={billingAddress}
            name="billingAddress"
            label="Address line 1"
            onChange={onInputChange}
            onBlur={onInputChange}
          />
          <TextField
            defaultValue={billingAddressExtra}
            name="billingAddressExtra"
            label="Apt/Ste"
            onChange={onInputChange}
            onBlur={onInputChange}
          />
        </GroupField>
        <GroupField className='cols-3'>
          <TextField
            className={cn({ invalid: !validation.validCity })}
            defaultValue={billingCity}
            name="billingCity"
            label="City"
            onChange={onInputChange}
            onBlur={onInputChange}
          />
          <Dropdown
            name='billingState'
            options={dropdownOptions}
            onChange={(value) => {
              this.props.onInputChange({
                target: { name: 'billingState', value: value.value },
              });
              // setTimeout(onPressEnter, 250);
            }}
            value={billingState}
            placeholder="State"
            validDropdown={validation.validState}
          />

          <TextField
            className={cn({ invalid: !validation.validZipcode })}
            type="tel"
            name="billingZipCode"
            defaultValue={billingZipCode}
            maxLength={5}
            label="Zip code"
            onChange={onInputChange}
            onBlur={onInputChange}
          />
        </GroupField>
      </div>
    );
  }
}
