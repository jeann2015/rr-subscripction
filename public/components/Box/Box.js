import React from 'react';

export const Box = () => (
  <li className='box'>
    <img alt='box' src={this.props.image} width='180' height='180' />
    <p>{this.props.content}</p>
  </li>
);
