import React, { PureComponent } from 'react';
import cn from 'classnames';

export class Button extends PureComponent {
  static defaultProps = {
    disabled: false,
    borderless: false,
    active: false,
    black: false,
    filled: false,
    selected: false,
    onClick: () => {},
  }

  render() {
    const { children, onClick, borderless, disabled, filled, active, small, vertical, isDisabled, facebook, whatsapp, twitter, dataName, selected, instagram } = this.props;
    const buttonStyles = cn('Button', { borderless, active, filled, small, vertical, isDisabled, facebook, whatsapp, twitter, selected, instagram });
    const buttonId = this.props.buttonId || '';

    return (
      <button
        className={buttonStyles}
        data-button-id={`${buttonId}`}
        disabled={disabled}
        onClick={onClick}
        data-name={dataName}
      >
        {children}
      </button>
    );
  }
}
