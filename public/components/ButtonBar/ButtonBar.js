import React from 'react';
import styles from './ButtonBar.css';

export const ButtonBar = ({ children }) => (
  <div className="ButtonBar">{children}</div>
);
