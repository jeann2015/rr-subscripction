import React, { Component } from 'react';
import Payment from 'payment';
import cn from 'classnames';
import { Title, GroupField, TextField } from '../../../public/components';
import './CreditCardFields.css';
import TrackMixpanel from '../../utils/mixpanel';

export class CreditCardFields extends Component {

  componentDidMount() {
    const { cardNumber, expiration, cvc } = this.refs;
    Payment.formatCardNumber(cardNumber);
    Payment.formatCardExpiry(expiration);
    Payment.formatCardCVC(cvc);
  }

  onChangeForm = (evt) => {
    switch (evt.target.name) {
      case 'cardNumber':
        TrackMixpanel.trackEvent('Billing Info CC Number');
        break;
      case 'expiration':
        TrackMixpanel.trackEvent('Billing Info CC Expiration');
        break;
      case 'cvc':
        TrackMixpanel.trackEvent('Billing Info CC CVC');
        break;
      default:
        break;
    }

    const { cardNumber, expiration, cvc } = this.refs;
    const { onChange } = this.props;
    onChange(cardNumber.refs.TextField.value, expiration.refs.TextField.value, cvc.refs.TextField.value);
  }

  render() {
    const {
      user,
      isFormValid,
      validCard,
      validExpiry,
      validCvv,
      onExpirationChange,
      toggleCheckbox,
    } = this.props;

    return (
      <div className='creditCardFields'>
        <div className='ccfBody'>
          <GroupField>
            <TextField
              className={`ccInput ${cn({ invalid: !validCard })}`}
              type="tel" // This is a workaround to support the numberpad on mobile
              name="cardNumber"
              label="Credit card number"
              ref='cardNumber'
              mask='card'
              defaultValue={user.cardNumber}
              onBlur={isFormValid}
              onChange={this.onChangeForm}
            />
            <GroupField className='ccfBottomFields cols-2'>
              <TextField
                className={cn({ invalid: !validExpiry })}
                type="tel" // This is a workaround to support the numberpad on mobile
                name="expiration"
                label="Expiration date"
                ref='expiration'
                mask='expiry'
                defaultValue={user.expiration}
                onBlur={onExpirationChange}
                onChange={this.onChangeForm}
                small
              />
              <TextField
                className={cn({ invalid: !validCvv })}
                type="tel" // This is a workaround to support the numberpad on mobile
                name="cvc"
                label="CVV"
                ref='cvc'
                mask='cvc'
                defaultValue={user.cvc}
                onBlur={isFormValid}
                onChange={this.onChangeForm}
                small
                // type="number"
              />
            </GroupField>
            <GroupField>
              <div className={cn('item', 'check', { checked: user.sameAddress })} onClick={toggleCheckbox}>
                <span className="checkbox" />
                <span className="label">Billing address is same as shipping</span>
              </div>
            </GroupField>
          </GroupField>
        </div>
      </div>
    );
  }
}
