import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

const DEFAULT_PLACEHOLDER_STRING = 'Select...';

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.value || {
        label: props.placeholder || DEFAULT_PLACEHOLDER_STRING,
        value: '',
      },
      isOpen: false,
    };
    this.mounted = true;
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.fireChangeEvent = this.fireChangeEvent.bind(this);
    this.focus = this.focus.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value && newProps.value !== this.state.selected) {
      this.setState({ selected: newProps.value });
    } else if (!newProps.value) {
      this.setState({ selected: {
        label: newProps.placeholder || DEFAULT_PLACEHOLDER_STRING,
        value: '',
      },
      });
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.handleDocumentClick, false);
    document.addEventListener('touchend', this.handleDocumentClick, false);
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener('click', this.handleDocumentClick, false);
    document.removeEventListener('touchend', this.handleDocumentClick, false);
  }

  focus() {
    this.refs.Dropdown[0].focus();
    this.refs.Dropdown.focus();
    if (!this.props.disabled) {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  }

  handleMouseDown(event) {
    if (this.props.onFocus && typeof this.props.onFocus === 'function') {
      this.props.onFocus(this.state.isOpen);
    }
    if (event.type === 'mousedown' && event.button !== 0) return;
    event.stopPropagation();
    event.preventDefault();

    if (!this.props.disabled) {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  }

  setValue(value, label) {
    const newState = {
      selected: {
        value,
        label,
      },
      isOpen: false,
    };
    this.fireChangeEvent(newState);
    this.setState(newState);
  }

  fireChangeEvent(newState) {
    if (newState.selected !== this.state.selected && this.props.onChange) {
      this.props.onChange(newState.selected);
    }
  }

  renderOption(option) {
    const optionClass = classNames({
      [`${this.props.baseClassName}-option`]: true,
      'is-selected': option === this.state.selected,
    });

    const value = option.value || option.label || option;
    const label = option.label || option.value || option;

    return (
      <div
        key={value}
        className={optionClass}
        onMouseDown={this.setValue.bind(this, value, label)}
        onClick={this.setValue.bind(this, value, label)}
      >
        {label}
      </div>
    );
  }

  buildMenu() {
    const { options, baseClassName } = this.props;
    const ops = options.map((option) => {
      if (option.type === 'group') {
        const groupTitle = (<div className={`${baseClassName}-title`}>{option.name}</div>);
        const _options = option.items.map(item => this.renderOption(item));

        return (
          <div className={`${baseClassName}-group`} key={option.name}>
            {groupTitle}
            {_options}
          </div>
        );
      }
      return this.renderOption(option);
    });

    return ops.length ? ops : <div className={`${baseClassName}-noresults`}>No options found</div>;
  }

  handleDocumentClick(event) {
    if (this.mounted) {
      if (!ReactDOM.findDOMNode(this).contains(event.target)) {
        this.setState({ isOpen: false });
      }
    }
  }

  smartphoneOptions() {
    const { options } = this.props;
    const selectedOption = this.state.selected.label || this.state.selected;
    return options.map((option) => {
      if (option.label === selectedOption) return <option key={`key-${option.label}`} selected='selected' name={option.label}>{option.label}</option>;
      return <option key={`key-${option.label}`} name={option.label}>{option.label}</option>;
    });
  }

  handleSelectChange(event) {
    const value = (event.target.value === 'State') ? '' : event.target.value;
    const newState = {
      selected: {
        value,
        label: value,
      },
      isOpen: false,
    };
    this.fireChangeEvent(newState);
    this.setState(newState);
  }

  render() {
    const { baseClassName, className } = this.props;
    const disabledClass = this.props.disabled ? 'Dropdown-disabled' : '';
    const placeHolderValue = typeof this.state.selected === 'string' ? this.state.selected : this.state.selected.label;
    const value = (<div className={`${baseClassName}-placeholder`}>{placeHolderValue}</div>);
    const menu = this.state.isOpen ? <div className={`${baseClassName}-menu`}>{this.buildMenu()}</div> : null;
    const dropdownClass = classNames({
      [className]: true,
      [`${baseClassName}-root`]: true,
      'is-open': this.state.isOpen,
      hasValue: (this.state.selected.value !== ''),
      'dropdown-wrapper': true,
      invalidDropdown: !this.props.validDropdown,
    });
    return (
      <div className={dropdownClass}>
        <select name={this.props.name} onChange={this.handleSelectChange} ref='Dropdown'>
          <option>State</option>
          {this.smartphoneOptions()}
        </select>
        <div className={`${baseClassName}-control ${disabledClass}`} onMouseDown={this.handleMouseDown.bind(this)} onTouchEnd={this.handleMouseDown.bind(this)}>
          {value}
          <span className={`${baseClassName}-arrow`} />
        </div>
        {menu}
      </div>
    );
  }
}

Dropdown.defaultProps = { baseClassName: 'Dropdown' };
export default Dropdown;
