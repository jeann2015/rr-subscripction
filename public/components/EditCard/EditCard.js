import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import ReactGA from 'react-ga';

import { getBillingDataByHash } from '../../../public/utils';

import {
  ProgressBar,
  StepBilling,
  StepMessage,
  LoadingScreen,
  Button,
  Header,
} from '../../../public/components';

export class EditCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      step: 1,
      user: {
        billingName: '',
        billingLastName: '',
        billingAddress: '',
        billingAddressExtra: '',
        billingCity: '',
        billingState: '',
        billingZipCode: '',

        cardNumber: '',
        cvc: '',
        expiration: '',
        exp_month: 0,
        exp_year: 0,

        hash: '',
        validCC: false,
        stripeToken: null,
        stripeError: null,
        sameAddress: true,
      },
    };

    this.setLoadingState(true);

    getBillingDataByHash(this.props.url.query.hash, (user) => {
      this.setLoadingState(false);
      this.setState({ user });
    });
  }

  onInputChange = (event, cb) => {
    const { user } = this.state;
    const { target } = event;

    if (!target || !target.name) {
      return;
    }

    user[target.name] = target.value;
    this.setState({ user }, cb);
  }

  setLoadingState = (show) => {
    this.setState({ loading: show });
  }

  exitForm = (step) => {
    window.location.href = 'http://www.raisedreal.com';
  }

  toggleSameAddress = (cb) => {
    const { user } = this.state;

    this.setState({
      user: {
        ...user,
        sameAddress: !user.sameAddress,
      },
    }, cb);
  }

  showBreakingError = (step, errorDetails) => {
    ReactGA.pageview(`/signup/${step}`);
    this.setState({ step, errorDetails });
  }

  nextStep = () => {
    this.setState({ step: 'finish' });
  }

  setCCData = ({ cardNumber, cvc, expiration, exp_month, exp_year }) => {
    const { user } = this.state;
    user.cardNumber = cardNumber;
    user.cvc = cvc;
    user.expiration = expiration;
    user.exp_month = exp_month;
    user.exp_year = exp_year;
    this.setState({ user });
  }

  stepComponent = () => {
    const { user, step } = this.state;

    if (step === 1) {
      return (<StepBilling
        nextStep={this.nextStep}
        showBreakingError={this.showBreakingError}
        onInputChange={this.onInputChange}
        toggleSameAddress={this.toggleSameAddress}
        user={user}
        setCCData={this.setCCData}
        setLoadingState={this.setLoadingState}
        editBilling
      />);
    }
    const FinishButton = (
      <MediaQuery query='(max-width: 885px)'>
        <Button onClick={this.exitForm} green>Finish</Button>
      </MediaQuery>
    );

    return (<StepMessage
      title="You’re all set!."
      text={'Your credit card information was changed'}
      button={FinishButton}
    />);
  }

  render() {
    const { loading } = this.state;

    return (
      <div className="App">
        <LoadingScreen visible={loading} />
        <Header />
        <div className="Signup ccChange">
          <ProgressBar stepsCount={1} step={1} exitForm={this.exitForm} />
          { this.stepComponent() }
        </div>
      </div>
    );
  }
}
