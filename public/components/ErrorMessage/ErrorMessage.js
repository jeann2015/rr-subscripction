import React from 'react';
import './ErrorMessage.css';

export const ErrorMessage = ({ children }) => (
  <div className="ErrorMessage">{children}</div>
);
