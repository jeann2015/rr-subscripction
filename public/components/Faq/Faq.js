import React, { Component } from 'react';
import './Faq.css';

export class Faq extends Component {
  state = {
    currentQuestion: -1,
  };

  showQuestion = (id) => {
    if (this.state.currentQuestion === id) {
      this.setState({ currentQuestion: -1 });
    } else {
      this.setState({ currentQuestion: id });
    }
  };

  render() {
    return (
      <div className="Faq">
        <div className="faq-section">
          <div className="faq-title">{this.props.title}</div>
          <div className="faq-questions">
            {this.props.items.map((item, index) => (
              <div
                className="question-container"
                key={`question-${index}`}
                onClick={() => this.showQuestion(index)}
              >
                <div className="question-title">
                  <div className="plan-text">{item.question}</div>
                  <div
                    className={`question-icon ${index ===
                      this.state.currentQuestion && 'rotated'}`}
                  />
                </div>
                <div
                  className={`info-text ${index ===
                    this.state.currentQuestion && 'show-text'}`}
                >
                  {item.response}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
