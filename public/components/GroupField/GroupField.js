import React from 'react';
import cn from 'classnames';
import './GroupField.css';

export const GroupField = ({ className, children }) => {
  const inputStyles = cn('GroupField', className);
  return (
    <div className={inputStyles} >{children}</div>
  );
};
