import React from 'react';

export class Header extends React.Component {
  handlePrevClick = () => {
    this.props.previousStep();
  }

  render() {
    return (
      <header className="Header">
        <div className="wrapper">
          {this.props.step > 1 && <button className="button-prev" onClick={this.handlePrevClick}>Anterior</button>}
          <a className="logo" href={'https://www.raisedreal.com'}>
            <img className="image mobile" src="/static/images/logo-mobile.svg" alt="" />
          </a>
        </div>
      </header>
    )
  }
};
