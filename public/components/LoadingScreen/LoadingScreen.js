import React from 'react';
import cn from 'classnames';
import './LoadingScreen.css';

export const LoadingScreen = ({ visible }) => (
  <div className={cn('LoadingScreen', { visible })}>
    <span className="dot" />
    <span className="dot" />
    <span className="dot" />
  </div>
);
