import React from 'react';
import cn from 'classnames';

export const OptionSelector = ({ selectItem, value, selected, label }) => {
  return (
    <div
      className={cn({
        item: true,
        selected,
      })}
      onClick={() => selectItem(value)}
    >
      <span className='label'>{ label }</span>
    </div>
  );
};
