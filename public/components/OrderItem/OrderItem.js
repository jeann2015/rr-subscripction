import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './OrderItem.css';
 import { showKindMealsDefaultBreakfast } from '../../utils/index'

export class OrderItem extends PureComponent {
  static propTypes = {
    addAgain: PropTypes.func,
    discount: PropTypes.bool,
    frequency: PropTypes.number,
    hideIcon: PropTypes.bool,
    index: PropTypes.number,
    onDecrease: PropTypes.func,
    onIncrease: PropTypes.func,
    onRemove: PropTypes.func,
    originalPrice: PropTypes.string,
    productDescription: PropTypes.string,
    quantity: PropTypes.number,
    remove: PropTypes.func,
    strokeThrough: PropTypes.bool,
    total: PropTypes.string,
  }

  static defaultProps = {
    addAgain: () => {},
    discount: false,
    frequency: 4,
    hideIcon: false,
    index: 0,
    onDecrease: () => {},
    onIncrease: () => {},
    onRemove: () => {},
    originalPrice: '0',
    productDescription: '24 Meals Box',
    quantity: 1,
    remove: () => {},
    strokeThrough: false,
    total: '0',
  }

  render() {
    const { subscription, hideIcon, productDescription, quantity, onIncrease, onDecrease, index, discount, remove, onRemove, strokeThrough, addAgain, frequency, originalPrice, total } = this.props;
    const mealsDefaultBreakfast = showKindMealsDefaultBreakfast( Number.parseInt(productDescription.split(' '), 10), Number.parseInt(subscription.breakfastMealsCount) );
    return (
      <div className={cn({
        hideIcon,
        OrderItem: true,
        strokeThrough,
      })}
      >
        <div className="left">
          { quantity && (
            <span>
              <button className="counter plus" onClick={onIncrease.bind(this, index)}>
                <img className="image" src="/static/images/plus.svg" alt="" width='12' />
              </button>
              {(quantity > 1) && (
              <button className="counter minus" onClick={onDecrease.bind(this, index)}>
                <img className="image" src="/static/images/less.svg" alt="" width='12' />
              </button>
              )}
            </span>
          )}

          {quantity && <span className="quantity">{quantity}</span>}
        </div>

        <div className="center">
          <div>
            <span className="name">{`${productDescription}, every ${frequency} weeks`}</span>
            <br/>
            <span className="name">{`${ mealsDefaultBreakfast }`}</span>
            { discount &&
              <span className="detailsDesktop">w/ discount</span>
            }
          </div>
          { discount &&
            <span className="detailsMobile">w/ discount</span>
          }
        </div>
        <div className="right">
          { discount &&
            <span className="previousPrice">${ originalPrice }</span>
          }
          <div className="price"><span className="currency">$</span>{ total }</div>
        </div>
      </div>
    );
  }
}
