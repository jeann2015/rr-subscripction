import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Paragraph.css';

export class Paragraph extends PureComponent {
  static propTypes = {
    small: PropTypes.bool,
  }

  static defaultProps = {
    small: false,
  }

  render() {
    const { small, italic, children } = this.props;

    return (
      <p className={cn('Paragraph', { small, italic })}>{children}</p>
    );
  }
}
