import React, { PureComponent } from 'react';
import MediaQuery from 'react-responsive';
import cn from 'classnames';

export class ProgressBar extends PureComponent {
  state = {
    steps: [
      'Choose Plan',
      'Build Your Box',
      'Your Tiny Human',
      'Email',
      'Shipping',
      'Payment',
    ],
  };

  isActive = (stepAttr, step) => {
    // is active if the step is grater or equal to the step button.
    return step >= stepAttr;
  }

  isClickeable = (stepAttr, step) => {
    // Is clickeable is the step is one step further than the button .
    // You can't click email if you are on email step. You have to be one step further.
    return step > stepAttr;
  }

  render() {
    const { steps } = this.state;
    const { step, onChange } = this.props;
    let s = step;
    let stepAttr;
    /*
      steps is the current step on the view
      stepAttr is the number of step of the button ( Each button belongs to a specific step )
      onChange is the function which will manage the view steps
    */
    if (s === 'billingError' || s === 'retryCreditCard') s = 4;
    return (
      <div className="ProgressBar">
        {(s !== steps.length + 1) &&
          (steps && steps.map((stepName, index) => {
            stepAttr = index + 1;
            return (
              <div
                  key={`key-${index}`}
                className={cn(`step-number-${index + 1}`, {
                  clickeable: this.isClickeable(stepAttr, s),
                  step: true,
                  active: this.isActive(stepAttr, s),
                })}
                step={stepAttr}
                onClick={(e) => {
                  if (onChange) onChange(index + 1);
                }}
              >
                <span className="stepName">
                  {stepName}
                </span>
              </div>
            );
          }))
        }
      </div>
    );
  }
}
