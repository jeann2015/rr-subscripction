import React, { PureComponent } from 'react';
import cn from 'classnames';
import styles from './RadioButton.css';

export class RadioButton extends PureComponent {
  static defaultProps = {
    onClick: () => {},
  }

  render() {
    const { children, onClick, dataName, selected, meals } = this.props;
    const buttonStyles = cn('RadioButton', { selected, meals });
    const buttonId = this.props.buttonId || '';

    return (
      <button
        className={buttonStyles}
        data-button-id={`${buttonId}`}
        onClick={onClick}
        data-name={dataName}
      >
        {children}
      </button>
    );
  }
}
