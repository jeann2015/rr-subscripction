import React, { PureComponent } from 'react';
import './ReferralFooter.css';

export class ReferralFooter extends PureComponent {
  render() {
    return (

      <div className="referralFooter">
        <div className="referralFooterTitle">It's Easy to get started!</div>
        <div className="referralFooterFlex">
          <div className="referralFooterColumn">
            <span className="referralFooterColumnText suitcase">Spread the word with your link or over email.</span>
          </div>
          <div className="referralFooterColumn">
            <span className="referralFooterColumnText tag">Get $25 when they order their first box.</span>
          </div>
        </div>
      </div>

    );
  }
}
