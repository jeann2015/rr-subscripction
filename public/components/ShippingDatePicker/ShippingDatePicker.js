import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../../public/components';
import './ShippingDatePicker.css';

export class ShippingDatePicker extends Component {
  static propTypes = {
    dates: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func,
  }

  static defaultProps = {
    dates: [],
    onChange: () => {},
  }

  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0,
    };
  }

  changeDate = (selectedIndex) => {
    const { dates, onChange } = this.props;

    this.setState({ selectedIndex });
    onChange(dates[selectedIndex]);
  }

  render() {
    const { dates } = this.props;
    const { selectedIndex } = this.state;

    return (
      <div className="ShippingDatePicker">
        {dates.map((date, i) =>
          (<Button
            key={i}
            green={selectedIndex === i}
            onClick={this.changeDate.bind(null, i)}
            dataIndex={i}
            dataName={`shipping-date-${i + 1}`}
          >
            {date}
          </Button>),
        )}
      </div>
    );
  }
}
