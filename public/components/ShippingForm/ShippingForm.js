import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Dropdown from '../Dropdown/Dropdown';
import { GroupField, TextField } from '../../../public/components';
import { US_STATES } from '../../utils';

export class ShippingForm extends Component {
  static propTypes = {
    defaultValues: PropTypes.shape({
      name: PropTypes.string,
      lastName: PropTypes.string,
      address: PropTypes.string,
      addressExtra: PropTypes.string,
      city: PropTypes.string,
      state: PropTypes.string,
      zipCode: PropTypes.string,
      telephone: PropTypes.string,
      company: PropTypes.string,
      connectFields: PropTypes.object,
    }),
  }

  static defaultProps = {
    defaultValues: {
      name: '',
      lastName: '',
      address: '',
      addressExtra: '',
      city: '',
      state: '',
      zipCode: '',
      telephone: '',
      company: '',
    },
  }

  render() {
    const { defaultValues, onInputChange, validation, connectFields, onPressEnter, onChangeZipCode } = this.props;
    const { name, lastName, zipCode, state, company, address, addressExtra, city, phoneNumber } = defaultValues;
    const dropdownOptions = Object.keys(US_STATES).map(country => ({ value: country, label: country }));
    return (
      <div className="ShippingForm">
        <GroupField className='cols-2'>
          <TextField
            className={cn({ invalid: !validation.validFirstname })}
            name="name"
            defaultValue={name}
            label="First name"
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'name',
              ref,
              next: 'lastName',
              stateValid: 'validFirstname',
              index: 0,
            })}
          />
          <TextField
            className={cn({ invalid: !validation.validLastname })}
            name="lastName"
            defaultValue={lastName}
            label="Last name"
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'lastName',
              ref,
              next: 'company',
              stateValid: 'validLastname',
              index: 1,
            })}
          />
        </GroupField>
        <GroupField>
          <TextField
            name="company"
            defaultValue={company}
            label="Company (Optional)"
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'company',
              ref,
              next: 'address',
              stateValid: 'validCompany',
              index: 2,
            })}
          />
        </GroupField>
        <GroupField className='cols-2-3'>
          <TextField
            className={cn({ invalid: !validation.validAddress })}
            name="address"
            label="Address (No PO boxes)"
            defaultValue={address}
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'address',
              ref,
              next: 'addressExtra',
              stateValid: 'validAddress',
              index: 3,
            })}
          />
          <TextField
            name="addressExtra"
            label="Apt/Ste"
            onChange={onInputChange}
            defaultValue={addressExtra}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'addressExtra',
              ref,
              next: 'city',
              stateValid: 'validAddressExtra',
              index: 4,
            })}
          />
        </GroupField>

        <GroupField className='cols-3'>
          <TextField
            className={cn({ invalid: !validation.validZipCode })}
            name="zipCode"
            defaultValue={zipCode}
            maxLength={5}
            label="Zip code"
            onPressEnter={onPressEnter}
            onChange={onChangeZipCode}
            type="tel"
            ref={ref => connectFields.add({
              me: 'zipCode',
              ref,
              next: 'phoneNumber',
              stateValid: 'validZipCode',
              index: 5,
            })}
          />
          <TextField
            className={cn({ invalid: !validation.validCity })}
            name="city"
            defaultValue={city}
            label="City"
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            ref={ref => connectFields.add({
              me: 'city',
              ref,
              next: 'state',
              stateValid: 'validCity',
              index: 6,
            })}
          />
          <Dropdown
              key={state}
            validDropdown={validation.validState}
            name='state'
            options={dropdownOptions}
            onChange={(value) => {
              this.props.onInputChange({
                target: { name: 'state', value: value.value },
              });
              setTimeout(onPressEnter, 250);
            }}
            value={state}
            placeholder="State"
            ref={ref => connectFields.add({
              me: 'state',
              ref,
              next: 'zipCode',
              stateValid: 'validState',
              index: 7,
            })}
          />
        </GroupField>
        <GroupField>
          <TextField
            className={cn({ invalid: !validation.validPhone })}
            type="tel"
            inputMask='(111) 111-1111'
            name="phoneNumber"
            label="Phone number"
            defaultValue={phoneNumber}
            maxLength={14}
            onChange={onInputChange}
            onPressEnter={onPressEnter}
            autoCompleteOff
            ref={ref => connectFields.add({
              me: 'phoneNumber',
              ref,
              stateValid: 'validPhone',
              index: 8,
            })}
          />
        </GroupField>
      </div>
    );
  }
}
