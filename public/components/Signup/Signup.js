import React, { Component } from 'react';
import moment from 'moment';
import ReactGA from 'react-ga';
import {
  Header,
  ProgressBar,
  TinyHumans,
  LoadingScreen,
  StepFactory,
} from '../../../public/components';
import TrackMixpanel from '../../utils/mixpanel';
import { getShippingDate } from '../../../public/utils';
import './Signup.css';

export class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      stepsCount: 7,
      selectedChild: 0,
      loading: false,
      user: {
        id: null,
        email: '',
        name: '',
        lastName: '',
        address: '',
        addressExtra: '',
        city: '',
        state: '',
        zipCode: '',
        phoneNumber: '',

        billingName: '',
        billingLastName: '',
        billingAddress: '',
        billingAddressExtra: '',
        billingCity: '',
        billingState: '',
        billingZipCode: '',

        referralLink: false,

        couponCode: null,
        couponCodeTemp: null,
        company: '',

        cardNumber: '',
        cvc: '',
        expiration: '',
        exp_month: 0,
        exp_year: 0,

        hash: '',
        validCC: false,
        stripeToken: null,
        stripeError: null,
        sameAddress: true,

        referralCode: null,
        referralCodeTemp: null,
        totalSummary: 0,
      },
      newProducts: [],
      childs: [{ name: '', birthdate: null }],
      userHash: '',
      subscriptionStartingFrom: '',
      products: [],
      availableDates: [],
      showChilds: false,
      subscription: {
        frequency: 2,
        productId: 22,
        breakfastMealsCount: 0,
      },

    };
  }

  componentDidMount() {
    this.getProducts();
    window.addEventListener('hashchange', this.handleNewHash, false);

    this.syncCookies(this.setSyncedLocalStorage);
  }

  setSyncedLocalStorage = (message) => {
    for (const [key, value] of Object.entries(message.localStorage)) {
      localStorage.setItem(key, value);
    }
  };

  syncCookies = (onSuccess) => {
    // This is a huge potential vulnerability. Limit the allowed domains to these two.
    const validDomains = ['https://www.raisedreal.com'];

    function receiveMessage(event) {
      if (!validDomains.includes(event.origin)) return;
      onSuccess(event.data);
    }

    window.addEventListener('message', receiveMessage, false);

    const cookieService = document.createElement('iframe');
    cookieService.setAttribute('src', 'https://www.raisedreal.com/cookieservice.html');
    cookieService.style.width = '1px';
    cookieService.style.height = '1px';
    cookieService.style.opacity = '0.01';
    cookieService.style.border = 'none';

    // When the iframe loads, send a message to get the cookies and localstorage
    cookieService.onload = function () {
      cookieService.contentWindow.postMessage('getCookies', '*');
    };

    document.body.appendChild(cookieService);
  };

  getProducts = () => {
    fetch('/api/products/meals-boxes')
      .then(res => res.json())
      .then(data => this.setState({ newProducts: data }))
      .catch(err => console.log(err));
  }

  setLoadingState = (show) => {
    this.setState({ loading: show });
  }

  setUserId = (id) => {
    const { user } = this.state;
    user.id = id;
    this.setState(user);
  }

  setMainState = (state, cb) => {
    this.setState(state, cb);
  }

  setZipCode = (zipCode) => {
    const { user } = this.state;
    user.zipCode = zipCode;
    this.setState(user);
  }

  setPlace = (data) => {
    const { user } = this.state;
    user.city = data.city;
    user.state = data.state;
    this.setState(user);
  }

  setTotalSummary = (totalSummary) => {
    const { user } = this.state;
    user.totalSummary = totalSummary;
    this.setState(user);
  }

  toggleChilds = () => {
    this.setState({ showChilds: !this.state.showChilds });
    ReactGA.event({
      category: 'Lead',
      action: 'Toggle Childs Menu',
    });
  }

  nextStep = () => {
    const { step, stepsCount, availableDates } = this.state;

    if (availableDates.length === 0) {
      this.fetchShippingDate();
    }

    if (step < stepsCount) {
      this.setState({ step: step + 1 });
      history.pushState(null, null, `#${(step + 1)}`);
    }
    if (typeof window === 'object') {
      const a = <StepFactory step={step + 1} context={this} data={this.state} />;
      const newStep = step + 1;
      if (a && a.type && a.type.displayName) {
        const pageview = `/signup/${a.type.displayName}`;
        ReactGA.pageview(pageview);
      }
    }
  }

  previousStep = () => {
    const { step } = this.state;

    if (step > 0) {
      this.setState({ step: step - 1 });
    }
  }

  jumpToStep = (step) => {
    this.setState({ step });
    if (step === 'zipOutOfArea') {
      ReactGA.pageview('/signup/StepOutOfArea');
    }
  }

  showBreakingError = (step, { error, userHash }) => {
    ReactGA.pageview(`/signup/${step}`);
    this.setState({
      step,
      errorDetails: error,
      userHash,
    });
  }

  goToBillingStep = (step) => {
    const { user } = this.state;
    user.cardNumber = '';
    user.cvc = '';
    user.expiration = '';
    user.exp_month = 0;
    user.exp_year = 0;
    this.setState({
      step: 'retryCreditCard',
      user,
    });
  }

  exitForm = () => {
    window.location.href = 'http://www.raisedreal.com';
  }

  setUser = (user) => {
    user.cardNumber = '';
    user.cvc = '';
    user.expiration = '';
    user.exp_month = 0;
    user.exp_year = 0;
    this.setState({ user });
  }

  onInputChange = (event, cb) => {
    const { user } = this.state;
    const { target } = event;

    if (!target || !target.name) {
      return;
    }

    user[target.name] = target.value;
    this.setState({ user }, cb);
  }

  toggleSameAddress = (cb) => {
    const { user } = this.state;

    if (user.billingName === '') user.billingName = user.name;
    if (user.billingLastName === '') user.billingLastName = user.lastName;
    if (user.billingZipCode === '') user.billingZipCode = user.zipCode;


    this.setState({
      user: {
        ...user,
        sameAddress: !user.sameAddress,
      },
    }, cb);
  }

  onCheckboxChange = (event) => {
    const { user } = this.state;
    const { target } = event;

    user[target.name] = target.checked;
    this.setState({ user });
  }

  onChangeTinyHuman = (childs) => {
    const { user } = this.state;
    user.childs = childs;
    this.setState({ user });
  }

  onInputChangeChild = (event) => {
    const childIndex = this.state.user.child.length - 1;
    const newState = this.state.user.child[childIndex];
    newState[event.target.name] = event.target.value;
    this.setState({
      user: {
        ...this.state.user,
        child: [
          ...this.state.user.child.slice(0, childIndex),
          {
            ...this.state.user.child[childIndex],
            newState,
          },
        ],
      } });
  }

  setChildData = (data, cb) => {
    this.setState({ childs: data }, cb);
  }

  deleteChild = (index) => {
    let { childs } = this.state;
    childs = childs.filter((child, i) => i !== index);
    this.setState({ childs });
  }

  addNewChild = () => {
    const { childs, selectedChild } = this.state;
    this.setState({ selectedChild: childs.length }, () => {
      this.jumpToStep(2);
    });
  }

  saveUserHash = hash => this.setState({ userHash: hash })

  setShippingDate = (date, index) => {
    this.setState({
      subscriptionStartingFrom: date,
    });
    if (index) TrackMixpanel.trackEvent(`Shipping Info Week ${index + 1}`);
  }

  setEmail = (email) => {
    const { user } = this.state;
    user.email = email;
    this.setState({
      user: {
        ...user,
        email,
      },
    });
  }

  setProducts = (products) => {
    this.setState({
      products,
    });
  }

  fetchShippingDate = () => {
    const { user } = this.state;
    getShippingDate((response) => {
      this.setState({ availableDates: [
        response.firstDate,
        response.secondDate,
      ] });
      const defaultDate = moment(response.firstDate).format('MM-DD-YYYY');
      this.setShippingDate(defaultDate);
    }, user.zipCode);
  }

  handleNewHash = () => {
    const { step } = this.state;
    let location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');

    if (location.length !== 1) {
      return;
    }
    location = parseInt(location[0]);
    if (step !== location && step > location) {
      this.jumpToStep(location);
    }
  }

  setCoupon = () => {
    const { user } = this.state;
    user.couponCode = null;
    this.setState({ user });
  }

  setCCData = ({ cardNumber, cvc, expiration, exp_month, exp_year }) => {
    const { user } = this.state;
    user.cardNumber = cardNumber;
    user.cvc = cvc;
    user.expiration = expiration;
    user.exp_month = exp_month;
    user.exp_year = exp_year;
    this.setState({ user });
  }

  resumeCouponCode = () => {
    const { user } = this.state;
    user.couponCode = user.couponCodeTemp;
    user.couponCodeTemp = null;
    this.setState({ user });
  }

  resetCouponCode = () => {
    const { user } = this.state;
    user.couponCode = null;
    this.setState({ user, couponCode: null });
  }

  onClickBreadcrumb = (clickedStep) => {
    const { step } = this.state;
    if ((clickedStep) < step) {
      this.setState({
        step: clickedStep,
      });
    }
  }

  removeTinyHuman = (indexChild, cb) => {
    let { childs } = this.state;
    childs = childs.filter((item, index) => index !== indexChild);
    this.setState({ childs }, cb);
  }

  removeDiscountReferral = () => {
    const { user, referralLink } = this.state;
    if (referralLink) {
      user.referralCodeTemp = user.referralCode;
    }
    user.referralCode = null;
    this.setState({ user });
  }

  addDiscountReferral = () => {
    const { user } = this.state;
    user.referralCode = user.referralCodeTemp;
    user.referralCodeTemp = null;
    this.setState({ user });
  }

  setProduct = (productId) => {
    const { subscription, newProducts } = this.state;
    const selectedProduct = newProducts.find(prod => prod.id === parseInt(productId));
    subscription.productId = productId;
    this.setState({
      subscription,
      products: [selectedProduct],
    });
  }

  setProductFrequency = (frequency) => {
    const { subscription } = this.state;
    subscription.frequency = frequency;
    this.setState({ subscription });
  }

  setBreakfasts = (breakfastMealsCount) => {
    const { subscription } = this.state;
    subscription.breakfastMealsCount = breakfastMealsCount;
    this.setState({ subscription });
  }


  render() {
    const { step, stepsCount, childs, showChilds, loading } = this.state;

    return (
      <div className="App Flow-Normal-Signup">
        <LoadingScreen visible={loading} />
        <Header
          step={step}
          previousStep={this.previousStep}
        />
        <div className={`Signup Step-${step}`}>
          <TinyHumans showChilds={showChilds} childs={childs} deleteChild={this.deleteChild} />
          <ProgressBar stepsCount={stepsCount} step={step} onChange={this.onClickBreadcrumb} />
          <StepFactory step={step} context={this} data={this.state} />
        </div>
      </div>
    );
  }
}
