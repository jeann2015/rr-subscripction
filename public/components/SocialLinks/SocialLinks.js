import React, { PureComponent } from 'react';
import cn from 'classnames';

export class SocialLinks extends PureComponent {
  render() {
    return (
      <div className={cn('SocialLinks', this.props.className)}>
        <a className="link" target="_blank" href="//facebook.com/raisedreal">
          <img className="icon" src="/static/images/icons/facebook.svg" alt="" />
        </a>
        <a className="link" target="_blank" href="//instagram.com/raised_real">
          <img className="icon" src="/static/images/icons/instagram.svg" alt="" />
        </a>
      </div>
    );
  }
}
