import React, { PureComponent } from 'react';
import ReactGA from 'react-ga';
import { Title, Paragraph, ButtonBar, Button } from '../../../public/components';

export class StepAddChild extends PureComponent {
  nextStep = () => {
    const { nextStep, child, setChildData, selectedChild } = this.props;
    const data = child;
    data.show = true;
    setChildData(selectedChild, data);
    nextStep();
  }

  addNewChild = () => {
    const { nextStep, addNewChild, child, setChildData, selectedChild } = this.props;
    const data = child;
    data.show = true;
    setChildData(selectedChild, data);
    addNewChild();
  }

  logChild = () => {
    ReactGA.event({
      category: 'Lead',
      action: 'Added Child',
    });
  }

  componentDidMount() {
    this.logChild();
  }

  render() {
    const { child } = this.props;
    return (
      <div className='stepContent'>
        <Title>{ child.name } is all set!</Title>
        <Paragraph>Would you like to order food for another tiny human?</Paragraph>
        <ButtonBar>
          <Button onClick={this.addNewChild}>Yes</Button>
          <Button onClick={this.nextStep}>
            <span className="step_add_child__dinamic_text" />
          </Button>
        </ButtonBar>
      </div>
    );
  }
}
