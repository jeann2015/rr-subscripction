import React, { PureComponent } from 'react';
import MediaQuery from 'react-responsive';
import { Title, Paragraph, ButtonBar, Button } from '../../../public/components';

export class StepAllergies extends PureComponent {
  render() {
    return (
      <div className="stepContent StepAllergies">
        <Title>We’re almost there, but first...</Title>
        <Paragraph>We’ll create a balanced combination of meals<br /> for your tiny human</Paragraph>
        <hr className="separator" />
        <span className="subtitle">Allergy Notice:</span>
        <Paragraph>
          All of our recipes are free of dairy, gluten, and soy. However, they are packaged in a facility that stores these ingredients. Ask your pediatrician if you have concerns.
        </Paragraph>

        <ButtonBar>
          <Button onClick={this.props.nextStep} green buttonId='ButtonStepAllergies'>I understand</Button>
        </ButtonBar>
      </div>
    );
  }
}
