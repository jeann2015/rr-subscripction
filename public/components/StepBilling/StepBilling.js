import React, { Component } from 'react';
import cn from 'classnames';
import ReactGA from 'react-ga';
import { Title, ButtonBar, Button, TextField, BillingForm, CreditCardFields, Summary, Alert } from '../../../public/components';
import StripeJs from './Stripe';
import validationErrors from '../Signup/validationErrors.json';
import './StepBilling.css';
import { isFirstnameValid, isLastnameValid, isAddressValid, isCityValid, isStateValid, validateCVV, validateCreditCard, validateCardExpiry, createToken, updateCreditCardInformation, isZipCode } from '../../utils';
import TrackMixpanel from '../../utils/mixpanel';
import { trackPurchase } from '../../utils/trackingIntegrations';

let Stripe; // == `undefined`; It hasn't loaded yet

require('es6-promise').polyfill();
require('isomorphic-fetch');

export class StepBilling extends Component {
  constructor(props) {
    super(props);
    StripeJs(() => {
      Stripe = window.Stripe;
      Stripe.setPublishableKey(stripeApiPublicKey);
    });

    this.state = {
      validCCForm: false,
      validBillingForm: true,
      stripeToken: null,
      stripeError: null,
      testMode: true,
      formValid: false,
      validCard: true,
      validExpiry: true,
      validCvv: true,
      validFirstname: true,
      validLastname: true,
      validAddress: true,
      validCity: true,
      validState: true,
      validZipcode: true,
      errorMessage: '',
      errorDiscountCode: '',
      amountOff: null,
      percentOff: null,
      generalDiscount: null,
      recurringDiscount: null,
      validCode: null,
      duration: null,
      childUnderEight: null,
      alertCoupon: false,
      boxList: null,
    };
  }

  componentWillMount() {
    const { setUser, setLoadingState, userHash, user } = this.props;

    if (userHash) {
      setLoadingState(true);
      fetch(`/api/users/hash/${userHash}`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
        },
      })
        .then((res) => {
          setLoadingState(false);
          if (res.status === 200) {
            return res.json()
              .then((userResponse) => {
                const userData = userResponse;
                const couponCode = user.couponCode;
                userData.sameAddress = true;
                userData.couponCode = couponCode;
                setUser(userData);
              });
          }
        })
        .catch((res) => {
          console.log(res);
        });
    }

    if (user.couponCode) {
      this.applyCode();
      this.setState({ errorMessage: null });
    }
  }

  componentDidMount() {
    this.checkFormValid();
  }

  stripeResponseHandler = (status, res) => {
    const { setLoadingState, editBilling, userHash } = this.props;
    const { editUser, createUser, updateCreditCard } = this;
    setLoadingState(false);
    if (res.error) {
      // Set state to show invalid CC number
      if (res.error.param === 'number') {
        this.setState({ validCard: false });
      }

      this.setState({ stripeError: res.error, errorMessage: res.error.message });
      return;
    }

    // prepaid card for testing: 5105105105105100
    if (res.card.funding === 'prepaid') {
      this.setState({ errorMessage: 'Prepaid credit and debit cards are not permitted to purchase this subscription. Please try again using a different payment type.' });
      return;
    }

    this.setState({ stripeToken: res.id }, () => {
      // 3 cases: Edit user credit card, create a new credit card, o retry in signup.
      if (userHash) {
        updateCreditCard();
        return;
      }
      if (editBilling) {
        editUser();
        return;
      }
      createUser();
    });
  }

  getTypeCard = (cardNumber) => {
    return Stripe.card.cardType(cardNumber);
  }

  editUser = () => {
    const { user,
      nextStep,
      showBreakingError,
      setLoadingState,
    } = this.props;

    const userData = {};
    userData.billingName = user.billingName;
    userData.billingLastname = user.billingLastName;
    userData.billingAddress = user.billingAddress;
    userData.billingAddressExtra = user.billingAddressExtra;
    userData.billingCity = user.billingCity;
    userData.billingState = user.billingState;
    userData.billingZipCode = user.billingZipCode;
    setLoadingState(true);
    fetch(`/api/users/hash/${user.hash}`, {
      method: 'PUT',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        ...userData,
        token: this.state.stripeToken,
      }),
    })
      .then((res) => {
        setLoadingState(false);
        if (res.status === 402) {
          return res.json().then((data) => {
            showBreakingError('billingError', `The problem we had is: ${data.message}`);
            nextStep();
          });
        }
        ReactGA.pageview('/signup/StepSuccessful');
        nextStep();
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  createUser = () => {
    const { user,
      childs,
      products,
      subscriptionStartingFrom,
      nextStep,
      showBreakingError,
      setLoadingState,
      setUserId,
      subscription,
    } = this.props;


    const userData = { ...user };

    userData.billingName = user.name;
    userData.billingLastName = user.lastName;
    userData.billingAddress = user.address;
    userData.billingAddressExtra = user.addressExtra;
    userData.billingCity = user.city;
    userData.billingState = user.state;
    userData.billingZipCode = user.zipCode;


    if (!user.sameAddress) {
      userData.billingName = user.billingName;
      userData.billingLastName = user.billingLastName;
      userData.billingAddress = user.billingAddress;
      userData.billingAddressExtra = user.billingAddressExtra;
      userData.billingCity = user.billingCity;
      userData.billingState = user.billingState;
      userData.billingZipCode = user.billingZipCode;
    }

    subscription.product = products;
    subscription.startFrom = subscriptionStartingFrom;

    setLoadingState(true);
    fetch('/api/users/new', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        ...userData,
        child: childs,
        subscription,
        token: this.state.stripeToken,
      }),
    })
      .then(async (res) => {
        setLoadingState(false);
        if (res.status === 402) {
          return res.json().then((data) => {
            const { stripe_error, user_hash } = data;
            showBreakingError('billingError', {
              error: `The problem we had is: ${stripe_error.message}`,
              userHash: user_hash,
            });
            nextStep();
          });
        }

        const data = await res.json();

        // Trigger tracking pixels
        trackPurchase({
          value: this.props.user.totalSummary,
          productId: this.props.products[0].productId,
          productName: this.props.products[0].name,
          transactionId: data.id,
          userData,
        });

        setUserId(data.id);
        nextStep();
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  updateCreditCard = () => {
    const { userHash, user, setLoadingState, jumpToStep, showBreakingError, nextStep } = this.props;
    const { stripeToken } = this.state;
    setLoadingState(true);
    updateCreditCardInformation(userHash, user, stripeToken, (res) => {
      setLoadingState(false);
      if (res.status === 402) {
        return res.json().then((data) => {
          showBreakingError('billingError', {
            error: `The problem we had is: ${data.stripe_error.message}`,
            userHash: data.user_hash,
          });
          nextStep();
        });
      }
      ReactGA.pageview('/signup/StepSuccessful');
      jumpToStep(6);
    });
  }

  saveAndNext = () => {
    if (this.state.validCCForm && this.state.validBillingForm) {
      TrackMixpanel.trackEvent('Step 8 (Billing) - Clicked Next');
      this.props.setLoadingState(true);
      const { cardNumber, cvc, exp_month, exp_year } = this.props.user;
      createToken(
        cardNumber, cvc, exp_month, exp_year,
        this.stripeResponseHandler,
      );
    } else {
      this.showMissingInfoError();
    }
  }

  showMissingInfoError = () => {
    const { cardNumber, exp_month, exp_year, cvc, sameAddress, billingName, billingLastName, billingAddress, billingCity, billingState, billingZipCode } = this.props.user;
    const errorMessage = validationErrors.missingInfo;
    if (!cardNumber) this.setState({ validCard: false, errorMessage });
    if (!exp_month || !exp_year) this.setState({ validExpiry: false, errorMessage });
    if (!cvc) this.setState({ validCvv: false, errorMessage });
    if (!sameAddress) {
      this.setState({ validAddress: true });
      const errorMessage = validationErrors.missingInfo;
      if (!billingName) {
        this.setState({ validFirstname: false, errorMessage });
      } else {
        this.setState({ validFirstname: true, errorMessage });
      }

      if (!billingLastName) {
        this.setState({ validLastname: false, errorMessage });
      } else {
        this.setState({ validLastname: true, errorMessage });
      }

      if (!billingAddress) {
        this.setState({ validAddress: false, errorMessage });
      } else {
        this.setState({ validAddress: true, errorMessage });
      }

      if (!billingCity) {
        this.setState({ validCity: false, errorMessage });
      } else {
        this.setState({ validCity: true, errorMessage });
      }

      if (!billingState) {
        this.setState({ validState: false, errorMessage });
      } else {
        this.setState({ validState: true, errorMessage });
      }

      if (!billingZipCode) {
        this.setState({ validZipcode: false, errorMessage });
      } else {
        this.setState({ validZipcode: true, errorMessage });
      }
    }
  }

  changeBilling = (evt) => {
    const { onInputChange } = this.props;
    onInputChange(evt);
  }

  onExpirationChange = (event) => {
    const expiration = event.target.value.split('/');
    let data;
    if (expiration.length >= 1) {
      data = {
        target: {
          name: 'exp_month',
          value: expiration[0].trim(),
        },
      };
      this.isFormValid(data);
    }
    if (expiration.length >= 2) {
      data = {
        target: {
          name: 'exp_year',
          value: expiration[1].trim(),
        },
      };
      this.isFormValid(data);
    }
  }

  validateAll = () => {
    const { billingName, billingLastName, billingAddress, billingCity, billingState, billingZipCode, sameAddress } = this.props.user;

    const errorMessage = '';
    const validateCard = validateCreditCard(this.props.user.cardNumber);
    const validateExpiry = validateCardExpiry(this.props.user.exp_month, this.props.user.exp_year);
    const validateCvv = validateCVV(this.props.user.cvc);

    let state = {
      errorMessage,
      ...validateCard,
      ...validateExpiry,
      ...validateCvv,
    };

    let valid = state.errorMessage === '' && validateCard && validateExpiry && validateCvv;

    if (!sameAddress) {
      const validateFirstName = isFirstnameValid(billingName);
      const validateLastName = isLastnameValid(billingLastName);
      const validateAddress = isAddressValid(billingAddress);
      const validateCity = isCityValid(billingCity);
      const validateState = isStateValid(billingState);
      const validateZip = isZipCode(billingZipCode);

      state = {
        ...state,
        ...validateFirstName,
        ...validateLastName,
        ...validateAddress,
        ...validateCity,
        ...validateState,
        ...validateZip,
      };

      valid = state.errorMessage === '' && validateFirstName && validateLastName && validateAddress && validateCity && validateState && validateZip;
    }

    return valid;
  }

  isFormValid = (evt) => {
    switch (evt.target.name) {
      case 'billingName':
        TrackMixpanel.trackEvent('Billing Info Name');
        break;
      case 'billingLastName':
        TrackMixpanel.trackEvent('Billing Info Last Name');
        break;
      case 'billingAddress':
        TrackMixpanel.trackEvent('Billing Info Address');
        break;
      case 'billingAddressExtra':
        TrackMixpanel.trackEvent('Billing Info Apt/Ste');
        break;
      case 'billingCity':
        TrackMixpanel.trackEvent('Billing Info City');
        break;
      case 'billingZipCode':
        TrackMixpanel.trackEvent('Billing Info Zip Code');
        break;
      case 'billingState':
        TrackMixpanel.trackEvent('Billing Info State');
        break;
      default:
        break;
    }
    this.props.onInputChange(evt, () => {
      const formValid = this.validateAll();

      this.setState({ formValid });
      return formValid;
    });
  }

  toggleCheckbox = () => {
    const { user } = this.props;
    this.props.toggleSameAddress(() => {
      const formValid = this.validateAll();
      this.checkFormValid();
      this.setState({ formValid });
      if (user.sameAddress) this.setState({ validBillingForm: true });
    });
  }

  applyCode = (evt) => {
    const { couponCode } = this.props.user;
    const { products, resetCouponCode } = this.props;
    if (evt) evt.preventDefault();
    TrackMixpanel.trackEvent('Billing Info Promo Code Submit');
    fetch('/api/coupons/valid', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        couponCode,
      }),
    })
      .then((res) => {
        // Coupon doesnt exist
        if (res.status === 404) {
          this.setState({ errorDiscountCode: 'Coupon not valid in this case' });
          resetCouponCode();
          if (evt.target) evt.target.disabled = false;
          return;
        }

        // Coupon invalid
        if (res.status === 409) {
          this.setState({ errorDiscountCode: 'Expired code' });
          resetCouponCode();
          if (evt.target) evt.target.disabled = false;
          return;
        }

        if (res.status === 200) {
          return res.json();
        }
      })
      .then((body) => {
        const { amountOff, percentOff, product, productId, duration, boxList } = body;

        // If coupon is not valid for the product user selected
        if (productId !== null && parseInt(products[0].productId) !== parseInt(productId)) {
          this.setState({ errorDiscountCode: 'This coupon does not work with this plan' });
          resetCouponCode();
          return;
        }

        const obj = {
          validCode: true,
          errorDiscountCode: '',
          amountOff: null,
          percentOff: null,
          generalDiscount: !product,
          recurringDiscount: duration === 'forever',
          duration,
          alertCoupon: true,
          boxList: null,
        };

        if (amountOff) {
          obj.amountOff = amountOff / 100;
        } else if (percentOff) {
          obj.percentOff = percentOff;
        } else if (boxList && boxList.length > 0) {
          try {
            const parsedBoxList = JSON.parse(boxList);
            obj.boxList = parsedBoxList;
            const discountForFirstBox = parsedBoxList.filter(rule => rule.boxNumber === 1 || rule.starts === 1);
            if (discountForFirstBox.length > 0) {
              if (discountForFirstBox[0].type === 'percentOff') {
                obj.percentOff = discountForFirstBox[0].ammount;
              }

              if (discountForFirstBox[0].type === 'ammountOff') {
                obj.amountOff = discountForFirstBox[0].ammount / 100;
              }
            }
          } catch (error) {
            console.log(error);
          }

          this.setState(obj);
          return;
        }

        this.setState(obj);
      });
  }

  onDiscountCodeChange = (evt) => {
    TrackMixpanel.trackEvent('Billing Info Promo Code Input');
    evt.target.value = evt.target.value.toUpperCase();
    this.props.onInputChange(evt);
  }

  onCCChange = (cardNumber, expiration, cvc) => {
    const { setCCData } = this.props;
    const card = {};
    const expirationSplit = expiration.split('/');

    card.cardNumber = cardNumber;
    card.cvc = cvc;
    card.expiration = expiration;

    if (expirationSplit.length >= 1) {
      card.exp_month = expirationSplit[0].trim();
    }
    if (expirationSplit.length >= 2) {
      card.exp_year = expirationSplit[1].trim();
    }
    setCCData(card);

    this.checkFormValid();
  }

  checkFormValid = () => {
    const { billingName, billingLastName, billingAddress, billingCity, billingState, billingZipCode, sameAddress } = this.props.user;

    const validateCard = validateCreditCard(this.props.user.cardNumber);
    const validateExpiry = validateCardExpiry(this.props.user.exp_month, this.props.user.exp_year);
    const validateCvv = validateCVV(this.props.user.cvc);

    if (!sameAddress) {
      const validateFirstName = isFirstnameValid(billingName);
      const validateLastName = isLastnameValid(billingLastName);
      const validateAddress = isAddressValid(billingAddress);
      const validateCity = isCityValid(billingCity);
      const validateState = isStateValid(billingState);
      const validateZip = isZipCode(billingZipCode);

      const billingValid = validateFirstName
                           && validateLastName
                           && validateAddress
                           && validateCity
                           && validateState
                           && validateZip;

      if (billingValid === undefined) {
        this.setState({
          validBillingForm: false,
        });
      } else {
        this.setState({
          validBillingForm: true,
        });
      }
    }

    const ccValid = validateCard && validateExpiry && validateCvv;

    // Validate expiration date entry
    if (validateExpiry.validExpiry === false) {
      this.setState({
        validExpiry: false,
      });
    }

    if (validateExpiry.validExpiry) {
      this.setState({
        validExpiry: true,
      });
    }

    // Validate CVV entry
    if (validateCvv.validCvv === false) {
      this.setState({
        validCvv: false,
      });
    }

    if (validateCvv.validCvv) {
      this.setState({
        validCvv: true,
      });
    }

    if (ccValid === false) {
      this.setState({
        validCCForm: false,
      });
      return;
    }

    if (ccValid.validCvv === false) {
      this.setState({
        validCCForm: false,
      });
      return;
    }

    if (ccValid.validCvv) {
      this.setState({
        validCCForm: true,
      });
      return;
    }
  }

  removeDiscount = () => {
    this.setState({
      errorDiscountCode: '',
      alertCoupon: false,
      amountOff: null,
      percentOff: null,
      generalDiscount: null,
      recurringDiscount: null,
      validCode: null,
      duration: null,
    });
    this.props.resetCouponCode();
  }

  render() {
    const { user, editBilling, childs, products, setProducts, resumeCouponCode, setTotalSummary, subscription } = this.props;
    const { errorDiscountCode, amountOff, percentOff, errorMessage, validCard, validExpiry, validCvv, validCode, generalDiscount, recurringDiscount, duration, validCCForm, alertCoupon, boxList } = this.state;

    if (!editBilling) {
      let recurrentTotal = products.reduce((total, product) => {
        return total + (product.price * product.quantity);
      }, 0);
      let totalToday = 0;
      let previousPrice = 0;
      products.forEach((product) => {
        let aux = product.price * product.quantity;
        previousPrice += aux;
        if (product.amountOff) {
          aux -= product.amountOff;
        }
        if (product.percentOff) {
          const totalDiscount = aux * (product.percentOff * 0.01);
          aux -= totalDiscount;
        }
        totalToday += aux;
      });
      if (amountOff) {
        totalToday -= amountOff;
      }
      let totalDiscount;
      if (percentOff) {
        totalDiscount = totalToday * (percentOff * 0.01);
        totalToday -= totalDiscount;
      }
      if (recurringDiscount) {
        recurrentTotal = totalToday;
      }
    }

    return (
      <div>
        <div className="StepBilling">
          { alertCoupon &&
            <Alert success>Look at that! It worked. We knew it would.</Alert>
          }

          { errorMessage &&
            <Alert error>{ errorMessage }</Alert>
          }

          { errorDiscountCode &&
            <Alert error>{ errorDiscountCode }</Alert>
          }

          <Title>YOUR BILLING INFO</Title>

          <hr className="separator" />

          <CreditCardFields
            user={user}
            isFormValid={this.isFormValid}
            validCard={validCard}
            validExpiry={validExpiry}
            validCvv={validCvv}
            onExpirationChange={this.onExpirationChange}
            toggleCheckbox={this.toggleCheckbox}
            onChange={this.onCCChange}
          />

          {!user.sameAddress &&
            <BillingForm
              validation={this.state}
              defaultValues={user}
              onInputChange={this.isFormValid}
              onBlur={this.isFormValid}
            />}

          {!editBilling &&
            <Summary
              user={user}
              childs={childs}
              products={products}
              setProducts={setProducts}
              amountOff={amountOff}
              percentOff={percentOff}
              validCode={validCode}
              generalDiscount={generalDiscount}
              duration={duration}
              removeDiscount={this.removeDiscount}
              resumeCouponCode={resumeCouponCode}
              setTotalSummary={setTotalSummary}
              boxList={boxList}
              subscription={subscription}
            >
              { !validCode && !editBilling &&
                <form onSubmit={this.applyCode} className="discount">
                  <TextField
                    onChange={this.onDiscountCodeChange}
                    label="PROMO CODE"
                    name="couponCode"
                    tinyLabel
                    tiny
                    defaultValue={null}
                    className={cn({ invalid: errorDiscountCode })}
                    disabled={validCode}
                  />
                  <Button onClick={this.applyCode} disabled={validCode} isDisabled={validCode}>APPLY</Button>
                </form>
              }
            </Summary>
          }

          <ButtonBar>
            <Button onClick={this.saveAndNext} buttonId='ButtonStepBilling' isDisabled={!validCCForm} isCompleteOrderButton>
              { editBilling ? 'Save' : 'COMPLETE ORDER' }
            </Button>
          </ButtonBar>
        </div>
      </div>
    );
  }
}
