let cachedStripeJsObject = null;

const StripeJsProvider = (readyCallback) => {
  if (typeof document !== 'object') {
    return;
  }

  // return early if we've already done all the hard work
  if (cachedStripeJsObject) {
    return readyCallback(this, cachedStripeJsObject);
  }

  // Find a good place to inject it, before the first script seems like a logical choice since
  // we definitely know one exists since this script is running.
  const firstScript = document.getElementsByTagName('script')[0];

  // create a new script
  const script = document.createElement('script');

  // set it to stripe.js
  script.src = 'https://js.stripe.com/v2/';

  // might not be necessary depending on when you inject
  script.async = true;
  script.defer = true;

  // get a callback when it's done
  script.onload = () => {
    // delete the handler, just for memory reasons;
    script.onload = undefined;
    // Cache the object so we don't reinject
    cachedStripeJsObject = window.Stripe;
    // call the callback, which I've decided randomly gets the object sent via the global
    // that what you don't have to mention the global variable ever again.
    readyCallback.apply(this, window.Stripe);
  };

  // Error handling/timeouts can be handled here as well.

  // Inject the script into the dom, which sets off the loading/execution
  firstScript.parentElement.insertBefore(script, firstScript);
};

export default StripeJsProvider;
