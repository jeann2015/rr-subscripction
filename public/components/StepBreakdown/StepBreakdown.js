/* eslint-disable react/no-did-mount-set-state */
import React, { Component } from 'react';
import Slider from 'rc-slider';
import {
  Title,
  ButtonBar,
  Button,
  GroupField,
  Faq,
} from '../../../public/components';
import './StepBreakdown.css';
import ReactGA from "react-ga";
import TrackMixpanel from "../../utils/mixpanel";

const faq = [
  {
    question: "WHAT'S THE DIFFERENCE BETWEEN BREAKFAST OATS AND ANYTIME MEALS?",
    response: (
      <div>All of our breakfast oats have an oatmeal base and are perfect for whenever your tiny human decides that it’s morning. Our anytime meals can be served—you guessed it—at any time of the day. All of our meals (breakfast included!) include combinations of fruits and vegetables, healthy fats, and spices.</div>
    ),
  },
  {
    question: 'CAN I CHANGE MY ASSORTMENT LATER ON?',
    response: (
      <div><b>Yes!</b> You'll be able to update those preferences at any time in your account.</div>
    ),
  },
  {
    question: 'HOW MANY OF EACH MEAL WILL I RECEIVE?',
    response: (
      <div>You’ll receive 6 different meals week to week. If you are on the 12 meal plan, you’ll receive 2 of each meal. If you are on the 24 meal plan, you’ll receive 4 of each meal.
      If you opt to include breakfast in your delivery, you may see some repeats as we continue to roll out new flavors.</div>
    ),
  },
];

export class StepBreakdown extends Component {
  state = {
    currentQuestion: -1,
  };

  componentDidMount() {
    const { productId } = this.props.subscription;

    switch (productId.toString()) {
      case '22':
        this.setState({ max: 24, step: 4 });
        break;
      case '21':
        this.setState({ max: 12, step: 2 });
        break;
      default:
        console.log(productId);
        break;
    }

    this.checkBreakfastCount();
  }

  componentDidUpdate() {
    this.checkBreakfastCount();
  }

  componentWillUnmount() {
    const { subscription } = this.props;
    subscription.breakfastMealsCount > 0 ? this.setTrackEventChangeBreakfastCount() : "";

    this.setTrackEvent();
    this.logAssortment();
  }

  checkBreakfastCount = () => {
    const { productId } = this.props.subscription;
    const { subscription } = this.props;
    const breakfastMealsCount = subscription.breakfastMealsCount;

    if ((productId.toString() === '22') && ((breakfastMealsCount % 4) !== 0)) {
      this.props.setBreakfasts(breakfastMealsCount + 2);
    }
  }

  logAssortment = () => {
    const { subscription } = this.props;
    const breakfastMealsCount = subscription.breakfastMealsCount;
    ReactGA.event({
      category: 'Lead',
      action: 'Selected Assortment',
      value: {breakfastMealsCount },
    });
  }

  setTrackEvent = () => {
    TrackMixpanel.trackEvent(`Step 2 (Build Your Box) - Clicked Next`);
  }

  setTrackEventChangeBreakfastCount = () => {
    TrackMixpanel.trackEvent(`Assortment Selection`);
  }

  onSliderChange = (value) => {
    this.props.setBreakfasts(value);
  };

  showQuestion = (id) => {
    if (this.state.currentQuestion === id) {
      this.setState({ currentQuestion: -1 });
    } else {
      this.setState({ currentQuestion: id });
    }
  };

  saveAndNext = () => {
    const { nextStep } = this.props;
    nextStep();
  };

  render() {
    const { max, step } = this.state;
    const { subscription } = this.props;

    return (
      <div className='StepBreakdown'>
        <Title>BUILD YOUR BOX</Title>
        <p className='description'>We take care of mornings too. <br />Select your assortment below.</p>
        <section className='slider-section'>
          <div className='slider-texts'>
            <p className='slider-text'>
              <strong>{subscription.breakfastMealsCount}</strong>
              <span>BREAKFAST OATS</span>
            </p>
            <p className='slider-text'>
              <strong>{max - subscription.breakfastMealsCount}</strong>
              <span>ANYTIME MEALS</span>
            </p>
          </div>
          <Slider
            value={subscription.breakfastMealsCount}
            min={0}
            max={max}
            step={step}
            onChange={this.onSliderChange}
          />
        </section>

        <GroupField className={'center'}>
          <div className='desktop-lg'>
            <ButtonBar>
              <Button
                onClick={this.saveAndNext}
                green
                buttonId='ButtonStepBreakdown'
              >
                CONTINUE TO CHECKOUT
              </Button>
            </ButtonBar>
          </div>
        </GroupField>

        <div className='mobile-sm'>
          <ButtonBar>
            <Button
              onClick={this.saveAndNext}
              green
              buttonId='ButtonStepBreakdown'
            >
              CONTINUE TO CHECKOUT
            </Button>
          </ButtonBar>
        </div>

        <Faq
          title="Frequently Asked Questions"
          items={faq}
        />
      </div>
    );
  }
}
