import React, { Component } from 'react';

export class StepButtons extends Component {
  render() {
    const { clickPrev, clickNext, prevDisabled, nextDisabled } = this.props;
    const classPrevButton = (prevDisabled) ? 'button_hidden' : '';
    const classNextButton = (nextDisabled) ? '' : 'button_active';

    return (
      <div className='button_group'>
        <button
          onClick={clickPrev}
          className={classPrevButton}
        >
          Back</button>
        <button
          onClick={clickNext}
          className={classNextButton}
          disabled={nextDisabled}
        >
          Next</button>
      </div>
    );
  }
}
