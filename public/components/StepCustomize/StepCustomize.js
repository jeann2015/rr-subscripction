import React, { Component } from 'react';
import cn from 'classnames';
import { Box, ButtonBar, Button, Title, Paragraph } from '../../../public/components';

export class StepCustomize extends Component {
  constructor(props) {
    super(props);

    this.state = {
      toggleInfo: false,
    };
  }

  isFormValid = () => {
    return true;
  }

  componentWillMount() {
    const { selectedChild, setChildData, child, nextStep } = this.props;
    if (child.monthsOld > 8) {
      const data = child;
      data.singleIngredient = false;
      setChildData(selectedChild, data);
      nextStep();
    }
  }

  childNeedsStarter = (starter) => {
    const { selectedChild, setChildData, child, nextStep } = this.props;

    const data = child;
    data.singleIngredient = starter;
    setChildData(selectedChild, data);
    nextStep();
  }

  toggleInfo = () => {
    console.log('test');
    this.setState({ toggleInfo: !this.state.toggleInfo });
  }

  render() {
    const { nextStep, selectedChild, setChildData, child } = this.props;
    return (
      <div>
        <div className='stepContent'>
          <Title>Do you want {child.name} to start with single ingredient meals?</Title>
          <Paragraph>
            Unsure? Click to <a href="#" onClick={this.toggleInfo}>check in with our PhD, Michelle Davenport</a>
          </Paragraph>
          <div className={cn({ hidden: !this.state.toggleInfo, info: true })}>
            <Paragraph italic>
              According to the American Academy of Pediatrics, it’s fine to introduce foods in no particular order. It’s up to you to choose either starters or regular meals. If you have a family history of food allergies or just want to keep a closer eye on potential allergies, or stomach troubles (their little digestive systems take some time to adjust too), you can keep introducing the same meals over a few days, or alternate between meals. If you prefer to introduce baby food slowly, opt for our starters, which are single ingredient meals like sweet potatoes + cinnamon, or carrots + thyme.
            </Paragraph>
            <Paragraph>
              <a href="#" onClick={this.toggleInfo}>Hide</a>
            </Paragraph>
          </div>
          <ButtonBar>
            <Button onClick={this.childNeedsStarter.bind(this, true)}>Yes</Button>
            <Button onClick={this.childNeedsStarter.bind(this, false)}>No</Button>
          </ButtonBar>
        </div>
      </div>
    );
  }
}
