import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { OptionSelector, ButtonBar, Button, Title, Paragraph } from '../../../public/components';

export class StepEating extends Component {
  constructor(props) {
    super(props);

    this.selectItem(this.options[this.optionsSelectedIndex]);
  }

  isFormValid = () => {
    return true;
  }

  selectItem = (selected) => {
    const { setChildData, selectedChild, child } = this.props;
    const data = child;
    data.foodType = selected;
    setChildData(selectedChild, data);
  }

  isOptionSelected = (item) => {
    const { child } = this.props;

    if (child.foodType && child.foodType === item) {
      return true;
    }
    return false;
  }

  optionsSelectedIndex = 1
  options = [
    'Milk/formula only',
    'Purées',
    'Finger foods',
  ]


  render() {
    const { child } = this.props;
    return (
      <div>
        <div className="StepEating">
          <Title>What is {child.name} eating?</Title>
          <div className='line'>
            {this.options.map((option, i) => {
              return <OptionSelector key={i} value={option} label={option} selectItem={this.selectItem} selected={this.isOptionSelected(option)} />;
            })}
          </div>
        </div>
        <ButtonBar>
          <Button onClick={this.props.nextStep} buttonId='ButtonStepEating' disabled={!this.isFormValid()} green>Next</Button>
        </ButtonBar>
      </div>
    );
  }
}
