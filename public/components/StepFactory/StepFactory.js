import React, { Component } from "react";
import {
  StepName,
  StepTinyHuman,
  StepProductPlan,
  StepBreakdown,
  StepShipping,
  StepBilling,
  StepMessage,
  StepReferral,
  Button
} from "../../../public/components";

export class StepFactory extends Component {
  render() {
    const {
      user,
      childs,
      selectedChild,
      products,
      subscriptionStartingFrom,
      availableDates,
      userHash,
      subscription,
      breakfastMealsCount,
      newProducts,
    } = this.props.data;

    const {
      onInputChange,
      nextStep,
      previousStep,
      saveUserHash,
      jumpToStep,
      setChildData,
      setShippingDate,
      resetCouponCode,
      setLoadingState,
      toggleSameAddress,
      showBreakingError,
      setProducts,
      setUserId,
      setUser,
      goToBillingStep,
      setEmail,
      setCCData,
      setCoupon,
      removeTinyHuman,
      removeDiscountReferral,
      addDiscountReferral,
      resumeCouponCode,
      onChangeZipCode,
      setPlace,
      setZipCode,
      setReferral,
      setTotalSummary,
      setProduct,
      setProductFrequency,
      setBreakfasts,
    } = this.props.context;

    switch (this.props.step) {
      case 1:
        return (
          <div>
            <StepProductPlan
              setProduct={setProduct}
              setProductFrequency={setProductFrequency}
              nextStep={nextStep}
              subscription={subscription}
              newProducts={newProducts}
            />
          </div>
        );

      case 2:
        return (
          <div>
            <StepBreakdown
              nextStep={nextStep}
              setBreakfasts={setBreakfasts}
              subscription={subscription}
              breakfastMealsCount={breakfastMealsCount}
            />
          </div>
        );

      case 3:
        return (
          <StepTinyHuman
            selectedChild={selectedChild}
            setChildData={setChildData}
            childs={childs}
            nextStep={nextStep}
            backStep={previousStep}
            removeTinyHuman={removeTinyHuman}
          />
        );

      case 4:
        return (
          <StepName
            setEmail={setEmail}
            nextStep={nextStep}
            backStep={previousStep}
            onCreateUser={saveUserHash}
            jumpToStep={jumpToStep}
            user={user}
          />
        );

      case 5:
        return (
          <StepShipping
            user={user}
            childs={childs}
            setZipCode={setZipCode}
            setPlace={setPlace}
            onChangeZipCode={onChangeZipCode}
            onInputChange={onInputChange}
            nextStep={nextStep}
            backStep={previousStep}
            setShippingDate={setShippingDate}
            setLoadingState={setLoadingState}
            subscriptionStartingFrom={subscriptionStartingFrom}
            availableDates={availableDates}
            subscription={subscription}
          />
        );

      case 6:
        return (
          <StepBilling
            setReferral={setReferral}
            resetCouponCode={resetCouponCode}
            resumeCouponCode={resumeCouponCode}
            nextStep={nextStep}
            backStep={previousStep}
            showBreakingError={showBreakingError}
            onInputChange={onInputChange}
            setCCData={setCCData}
            setCoupon={setCoupon}
            toggleSameAddress={toggleSameAddress}
            userHash={userHash}
            user={user}
            childs={childs}
            products={products}
            setProducts={setProducts}
            subscriptionStartingFrom={subscriptionStartingFrom}
            setLoadingState={setLoadingState}
            setUserId={setUserId}
            jumpToStep={jumpToStep}
            removeDiscountReferral={removeDiscountReferral}
            addDiscountReferral={addDiscountReferral}
            setTotalSummary={setTotalSummary}
            subscription={subscription}
          />
        );

      case 7:
        return (
          <StepReferral
            user={user}
            subscriptionStartingFrom={subscriptionStartingFrom}
            trackWithAdhesiveco
          />
        );

      case -1:
      case "zipOutOfArea":
        return (
          <StepMessage
            title="OOPS"
            text="We don't go that far yet. We will let you know when we do :)"
            social
          />
        );

      case "billingError":
        const { errorDetails } = this.props.data || null;
        const RetryButton = (
          <Button onClick={goToBillingStep} green>
            Try with another.
          </Button>
        );
        return (
          <StepMessage
            title="Oops!"
            text="Your credit card doesn't seem to be valid. "
            details={errorDetails}
            social
            button={RetryButton}
          />
        );
      case "retryCreditCard":
        return (
          <StepBilling
            jumpToStep={jumpToStep}
            toggleSameAddress={toggleSameAddress}
            userHash={userHash}
            user={user}
            childs={childs}
            products={products}
            subscriptionStartingFrom={subscriptionStartingFrom}
            setLoadingState={setLoadingState}
            setUserId={setUserId}
            setUser={setUser}
            removeDiscountReferral={removeDiscountReferral}
            addDiscountReferral={addDiscountReferral}
            resetCouponCode={resetCouponCode}
            resumeCouponCode={resumeCouponCode}
            nextStep={nextStep}
            backStep={previousStep}
            showBreakingError={showBreakingError}
            onInputChange={onInputChange}
            setCCData={setCCData}
            setCoupon={setCoupon}
            setProducts={setProducts}
            subscription={subscription}
          />
        );
      default:
        break;
    }
  }
}
