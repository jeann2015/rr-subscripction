import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Title, Paragraph, ButtonBar, Button } from '../../../public/components';
import './StepMessage.css';

export class StepMessage extends PureComponent {
  render() {
    const { title, text, details, image, button } = this.props;

    return (
      <div className="StepMessage">
        {title && <Title>{title}</Title>}
        {text && <Paragraph>{text}</Paragraph>}
        {details && <Paragraph>{details}</Paragraph>}
        {image}
        {button && <ButtonBar>{button}</ButtonBar>}
      </div>
    );
  }
}
