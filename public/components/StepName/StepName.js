import React, { Component } from 'react';
import cn from 'classnames';
import ReactGA from 'react-ga';
import { Title, Paragraph, ButtonBar, Button, TextField, GroupField, Alert } from '../../../public/components';
import './StepName.css';
import { isEmailValid } from '../../utils';
import validationErrors from '../Signup/validationErrors.json';
import TrackMixpanel from '../../utils/mixpanel';
import { trackLead } from '../../utils/trackingIntegrations';

require('es6-promise').polyfill();
require('isomorphic-fetch');

export class StepName extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      formValid: false,
      errorMessage: '',
      emailValid: true,
      emailError: '',
      emailDuplicated: false,
      buttonDisabled: true,
    };
  }

  saveAndNext = () => {
    if (!this.state.buttonDisabled) {
      this.setState({ loading: true, buttonDisabled: true });

      this.isNotUsed((valid) => {
        this.setState({ emailDuplicated: !valid, loading: false });

        if (valid) {
          this.setState({ emailDuplicated: false, loading: false });
          // trigger tracking pixels
          trackLead();

          this.props.nextStep();
        }
      });
    } else {
      this.showMissingInfoError();
    }
  }

  showMissingInfoError = () => {
    const { email } = this.props.user;
    const errorMessage = validationErrors.missingInfo;
    if (email === '' || isEmailValid(email) === undefined || isEmailValid(email).emailValid === false) {
      this.setState({ emailValid: false, errorMessage });
    }
  }

  validateAll = () => {
    const { email } = this.props.user;
    this.setState({ errorMessage: '' });
    if (email === '') {
      this.showMissingInfoError();
      return;
    }
    if (isEmailValid(email)) this.setState(isEmailValid(email));
    if (!isEmailValid(email).emailValid) {
      return false;
    }
    return true;
  }

  isFormValid = (evt) => {
    this.props.onInputChange(evt, () => {
      const formValid = this.validateAll();
      this.setState({ formValid });
      return formValid;
    });
  }

  onPressEnter = (evt) => {
    const { setEmail } = this.props;
    this.state.emailDuplicated = '';
    setEmail(evt.target.value);
    this.saveAndNext();
  }

  isNotUsed = (cb) => {
    const { name, lastName, email, zipCode } = this.props.user;
    fetch('/api/users/email', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        email,
      }),
    }).then((res) => {
      if (res.status === 409) {
        this.setState({ validState: false, buttonDisabled: false });
        return cb(false);
      }
      TrackMixpanel.setId(email);
      TrackMixpanel.track({
        $name: name,
        $lastName: lastName,
        $email: email,
        $zipCode: zipCode,
      });
      TrackMixpanel.trackEvent('Step 3 (Info) - Clicked Next');
      return cb(true);
    });
  }

  toggleConditions = () => {
    const { terms } = this.state;
    let { termsValid } = this.state;
    if (!terms) termsValid = true;
    this.setState({
      ...this.state,
      terms: !terms,
      termsValid,
    });
  }

  onChange = (evt) => {
    const email = evt.target.value;
    const { setEmail } = this.props;
    setEmail(email);
    this.setState({ buttonDisabled: !isEmailValid(email).emailValid });
    TrackMixpanel.trackEvent('Step 1 (Info) - Input email');
  }

  checkMailRef = (ref) => {
    if (ref.value !== '') {
      const email = ref.value;
      const { setEmail } = this.props;
      setEmail(email);
      this.setState({ buttonDisabled: !isEmailValid(email).emailValid });
    }
  }

  render() {
    const { user } = this.props;
    const { emailDuplicated, emailValid, errorMessage, buttonDisabled } = this.state;

    return (
      <div className="StepName">
        { emailDuplicated &&
          <Alert error>
            <div className="space-p">
              <p className="space-p">
                Looks like you're already a member!
              </p>
              <p className="space-p">
                Head over to <a className="error-link" href="https://account.raisedreal.com/" target="_blank">account.raisedreal.com</a> instead.
              </p>
            </div>
          </Alert>
        }
        { errorMessage &&
          <Alert error>{ errorMessage }</Alert>
        }

        <Title>CREATE YOUR ACCOUNT</Title>

        <GroupField>
          <TextField
            className={cn({ invalid: emailDuplicated || !emailValid })}
            name="email"
            label='Email'
            defaultValue={user.email}
            onChange={this.onChange}
            onBlur={this.isFormValid}
            onPressEnter={this.onPressEnter}
            checkMailRef={this.checkMailRef}
            check
            medium
          />
          <div className="desktop-lg">
            <ButtonBar>
              <Button onClick={this.saveAndNext} isDisabled={(buttonDisabled === true)} green buttonId='ButtonStepEmail'>Right this way</Button>
            </ButtonBar>
          </div>

        </GroupField>

        <div className="mobile-sm">
          <hr className='separator' />

          <ButtonBar>
            <Button onClick={this.saveAndNext} isDisabled={(buttonDisabled === true)} green buttonId='ButtonStepEmail'>Right this way</Button>
          </ButtonBar>
        </div>

        <Paragraph>
          By clicking continue you agree to our <a href='https://www.raisedreal.com/terms-conditions/' target='_blank'>Terms and Conditions</a><br />and consent to our <a href='https://www.raisedreal.com/privacy-policy/' target='_blank'>Privacy Policy</a>.
        </Paragraph>
      </div>
    );
  }
}
