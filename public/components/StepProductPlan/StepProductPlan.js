import React, { Component } from 'react';
import { Title, ButtonBar, StyledParagraph, Button, RadioButton, GroupField, Faq } from '../../../public/components';
import './StepProductPlan.css';
import { trackAddToCart } from '../../utils/trackingIntegrations';
import ReactGA from "react-ga";
import TrackMixpanel from "../../utils/mixpanel";

const faq = [
  {
    question: 'CAN I PICK AND CHOOSE THE MEALS I RECEIVE?',
    response: <div>While our team carefully selects each meal you’ll receive, you'll always have the option to <b>set your ingredient preferences in your account</b>. We’ll make sure you don’t receive any of the meals that contain ingredients you opted out of. You can also select how many breakfast meals you’d like to receive with each delivery. You'll be able to update those preferences at any time. We’ll take care of the rest.</div>,
  },
  {
    question: 'WHICH PLAN IS RIGHT FOR ME?',
    response: <div>If you plan on serving Raised Real once a day, or if your tiny human is just getting started on solids, we recommend starting with 12 meals every 2 weeks or 24 meals every 4 weeks. If you want to serve Raised Real two or three times a day, go for 24 meals every 2 weeks. Don't sweat it; you can change your plan anytime.</div>,
  },
  {
    question: 'DO YOU USE ORGANIC INGREDIENTS?',
    response: <div><b>Yes.</b> 98% of the ingredients we currently use are organic. If we use conventional ingredients, we make sure that they are from sustainable sources - and whenever we can source the same ingredient organic, we do. We will never use conventional ingredients that appear on the Environmental Working Group's "dirty dozen" list. What we're really proud of, is our flash freezing process, which ensures the nutrients are locked in. Because, what good are organic ingredients, if they've lost all of their oomph before they get to your tiny human.</div>,
  },
  {
    question: 'HOW LONG WILL THE MEALS STAY GOOD FOR?',
    response: <div>Because our meals are flash frozen, <b>they will stay fresh in your freezer for 3 to 6 months after you receive them,</b> provided they are not exposed to heat at any period in which they become unfrozen. They can hang out in your freezer until you're ready to make them.</div>,
  },
  {
    question: 'CAN I CANCEL ANYTIME?',
    response: <div><b>Absolutely</b>. There is no commitment ever. After your first box, you can skip a box or cancel your plan by sending us a message. You can also change the amount of meals you get and/or your frequency. You've got the power.</div>,
  },
  {
    question: 'ARE YOUR MEALS GLUTEN, DAIRY AND NUT FREE?',
    response: <div>Yes! <b>All of our meals are big 8 allergen-free.</b> No tree nuts, peanuts, shellfish, fish, wheat, dairy, soy, or egg – some of our meals contain coconut, which is technically a tree nut, but is not known to trigger allergic reactions. Please check with your pediatrician if your tiny human has a known tree-nut allergy.</div>,
  },
];

export class StepProductPlan extends Component {

  state = {
    selectedProduct: this.props.subscription.productId,
    newFrequency: this.props.subscription.frequency,
  }

  componentDidMount() {
    this.logPlan();
  }

  componentWillUnmount() {
    this.setTrackEvent();
  }

  setProductFrequency = (event) => {
    const value = event.target.dataset.name;
    this.props.setProductFrequency(value);
    this.setState({ newFrequency: value });
  }

  setProduct = (event) => {
    const value = event.target.dataset.name;
    this.props.setProduct(value);
    this.setState({ selectedProduct: value });
  }

  trackAddToCartPixels = () => {
    const productData = this.props.newProducts.filter(prod => prod.id === Number(this.state.selectedProduct))[0];
    const productPrice = productData ? productData.price : 0;
    trackAddToCart(productPrice);
  }

  logPlan = () => {
    ReactGA.event({
      category: 'Lead',
      action: 'Selected Box Size',
    });
  }

  setTrackEvent = () => {
    TrackMixpanel.trackEvent(`Step 1 (Choose Plan) - Clicked Next`);
  }

  saveAndNext = () => {
    const { nextStep, setProduct } = this.props;
    setProduct(this.state.selectedProduct);
    this.trackAddToCartPixels();
    nextStep();
  }

  render() {
    const frequency = [2, 4];
    const { newFrequency, selectedProduct } = this.state;
    const { newProducts } = this.props;
    const productData = newProducts.filter(prod => prod.id === Number(selectedProduct))[0];

    return (<div className="StepProductPlan">
      <Title>CHOOSE YOUR PLAN</Title>
      <StyledParagraph className="alignCenter">
        <div className="price-text">${productData ? productData.unitaryPrice : '4.99'}/meal</div>
        <div className="shipping-text">free shipping</div>
      </StyledParagraph>

      <div className="selectors">
        <StyledParagraph>
          <GroupField className={'center row'}>
            { newProducts.map((prod, i) => {
              return (
                <RadioButton
                  key={`key-${i}`}
                  index={i}
                  onClick={this.setProduct}
                  dataName={prod.id}
                  green
                  isDisabled
                  selected={(Number(selectedProduct) === prod.id) ? 'selected' : ''}
                  square
                >
                  { (i === 0) ? '12' : '24'}
                </RadioButton>
              );
            })}
            <p className="selector-text">meals</p>
          </GroupField>
        </StyledParagraph>

        <p className="middle-text">every</p>

        <StyledParagraph>
          <GroupField className={'center row'}>
            {frequency.map((val, i) => {
              return (
                <RadioButton
                  key={`key-${i}`}
                  index={i}
                  onClick={this.setProductFrequency}
                  dataName={val}
                  selected={(Number(newFrequency) === val) ? 'selected' : ''}
                  square
                >
                  { val }
                </RadioButton>
              );
            })}
            <p className="selector-text">weeks</p>
          </GroupField>
        </StyledParagraph>
      </div>

      <GroupField className={'center'}>
        <div className="desktop-lg">
          <ButtonBar>
            <Button onClick={this.saveAndNext} green buttonId='ButtonStepProductPlan'>CONTINUE TO CHECKOUT</Button>
          </ButtonBar>
        </div>
      </GroupField>

      <div className="mobile-sm">
        <ButtonBar>
          <Button onClick={this.saveAndNext} green buttonId='ButtonStepProductPlan'>CONTINUE TO CHECKOUT</Button>
        </ButtonBar>
      </div>

      <Faq
        title="Frequently Asked Questions"
        items={faq}
      />
    </div>);
  }
}
