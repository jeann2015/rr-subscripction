import React, { PureComponent } from 'react';
import moment from 'moment';

import { Title, Paragraph, Button } from '../../../public/components';
import './StepReferral.css';

export class StepReferral extends PureComponent {

  componentDidMount() {
    const { user, trackWithAdhesiveco } = this.props;
    if (trackWithAdhesiveco && qa_env) {
      const script = document.createElement('script');
      script.src = `https://js.b1js.com/tagcontainer.js?id=6bd4794e8eef434283e889592e4e579e&type=0&onload=false&order_id=${user.id}&value=${use}`;
      script.async = true;
      document.body.appendChild(script);
    }
  }

  render() {
    const { subscriptionStartingFrom } = this.props;
    const startingFrom = moment(subscriptionStartingFrom, 'MM-Do-YYYY').format('MM/DD');

    return (
      <div className="stepSocial">
        <div className="stepReferral">
          <div className="stepReferralTitleWrapper">
            <Title>ORDER CONFIRMED! WE DID IT.</Title>

            {subscriptionStartingFrom &&
              <div className="comments-container">
                <Paragraph>
                  Your box will ship the week of {startingFrom}. Keep an eye out for your tracking email.
                </Paragraph>
                <Paragraph>
                  Fun fact: Meals always arrive on Thursdays.<br />
                  Also a fun fact: Deer eat broccoli. Yum.
                </Paragraph>
              </div>
            }

            <div className='line-mid' />
            <div className="secondary-title">Up late at night?</div>
            <Paragraph>
              Follow us, we’ll keep you company.
            </Paragraph>
            <div className="ButtonBar">
              <Button facebook>
                <a href="https://www.facebook.com/raisedreal" target="_blank">FACEBOOK</a>
              </Button>
              <Button instagram>
                <a href="https://www.instagram.com/raisedreal" target="_blank">INSTAGRAM</a>
              </Button>
            </div>

            {qa_env &&
              <img src={`//20801452p.rfihub.com/ca.gif?rb=35762&ca=20801452&_o=35762&_t=20801452&ra=${Math.floor((Math.random() * 10000) + 1)}`} height={0} width={0} style={{ display: 'none' }} alt='Rocket Fuel' />
            }
          </div>
        </div>
      </div>
    );
  }
}
