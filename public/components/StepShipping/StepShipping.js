import React, { Component } from 'react';
import moment from 'moment';
import ReactGA from 'react-ga';
import validationErrors from '../Signup/validationErrors.json';
import { Title, Paragraph, ButtonBar, Button, ShippingForm, Alert } from '../../../public/components';
import { isBlank, isFirstnameValid, isLastnameValid, isAddressValid, isCityValid, isStateValid, isPhoneNumber, isZipValidWithData, outOfArea } from '../../utils';
import TrackMixpanel from '../../utils/mixpanel';
import ConnectFields from '../../utils/connect-fields';
import { trackNoPayment } from '../../utils/trackingIntegrations.js';

export class StepShipping extends Component {

  componentWillMount() {
    this.checkUserProps();

    this.state = {
      formValid: false,
      validFirstname: true,
      validLastname: true,
      validAddress: true,
      validCity: true,
      validState: true,
      validPhone: true,
      validCompany: true,
      validZipCode: true,
      errorMessage: '',
    };
  }

  onChangeZipCode = (e) => {
    const { setLoadingState, setPlace, setZipCode } = this.props;
    const zipCode = e.target.value;
    setZipCode(zipCode);
    if (zipCode.length >= 5) {
      setLoadingState(true);
      isZipValidWithData(zipCode)
        .then((data) => {
          setLoadingState(false);
          setPlace(data.data);
          this.validateAll();
          this.checkUserProps();
        });
    }

    TrackMixpanel.trackEvent('Shipping Info Zip Code');
  }

  checkUserProps = () => {
    let validForm = true;
    const { setLoadingState } = this.props;
    const { name, lastName, address, city, state, phoneNumber, zipCode } = this.props.user;
    if (!isFirstnameValid(name)) validForm = false;
    if (!isLastnameValid(lastName)) validForm = false;
    if (!isAddressValid(address)) validForm = false;
    if (!isCityValid(city)) validForm = false;
    if (!isStateValid(state)) validForm = false;
    if (!isPhoneNumber(phoneNumber)) validForm = false;
    if (zipCode === '') validForm = false;

    if (validForm) {
      setLoadingState(true);
      isZipValidWithData(zipCode)
        .then((res) => {
          setLoadingState(false);
          if (res.valid) {
            this.setState({
              formValid: true,
            });
          }
        });
    }
  }

  validateAll = () => {
    const { name, lastName, address, city, state, phoneNumber } = this.props.user;
    this.setState({ errorMessage: '' });
    if (isFirstnameValid(name)) this.setState(isFirstnameValid(name));
    if (isLastnameValid(lastName)) this.setState(isLastnameValid(lastName));
    if (isAddressValid(address)) this.setState(isAddressValid(address));
    if (isCityValid(city)) this.setState(isCityValid(city));
    if (isStateValid(state)) this.setState(isStateValid(state));

    if (!isFirstnameValid(name) || !isFirstnameValid(name).validFirstname
      || !isLastnameValid(lastName) || !isLastnameValid(lastName).validLastname
      || isBlank(address)
      || !isCityValid(city) || !isCityValid(city).validCity
      || !isStateValid(state) || !isStateValid(state).validState
      || !isPhoneNumber(phoneNumber) || !isPhoneNumber(phoneNumber).validPhone
    ) {
      return false;
    }

    return true;
  }

  isFormValid = (evt) => {
    switch (evt.target.name) {
      case 'name':
        TrackMixpanel.trackEvent('Shipping Info First Name');
        break;

      case 'lastName':
        TrackMixpanel.trackEvent('Shipping Info Last Name');
        break;

      case 'company':
        TrackMixpanel.trackEvent('Shipping Info Company');
        break;

      case 'address':
        TrackMixpanel.trackEvent('Shipping Info Address');
        break;

      case 'addressExtra':
        TrackMixpanel.trackEvent('Shipping Info Apt/Ste');
        break;

      case 'city':
        TrackMixpanel.trackEvent('Shipping Info City');
        break;

      case 'state':
        TrackMixpanel.trackEvent('Shipping Info State');
        break;

      case 'phoneNumber':
        TrackMixpanel.trackEvent('Shipping Info Phone Number');
        break;

      default:
        break;
    }

    this.props.onInputChange(evt, () => {
      const formValid = this.validateAll();
      this.setState({ formValid });
      return formValid;
    });
  }

  checkAndNext = () => {
    const { setLoadingState, user } = this.props;
    const { childs } = this.props;
    const { zipCode, state } = user;
    if (this.state.formValid) {
      isZipValidWithData(zipCode)
        .then((res) => {
          setLoadingState(false);
          if (res.valid) {
            const { phoneNumber } = this.props.user;
            TrackMixpanel.track({
              $phoneNumber: phoneNumber,
            });

            fetch('/api/users/nopayment', {
              method: 'POST',
              headers: {
                accept: 'application/json',
                'content-type': 'application/json',
              },
              body: JSON.stringify({
                ...user,
                childs,
              }),
            }).then((res) => {
              // Trigger tracking pixels
              trackNoPayment();

              TrackMixpanel.trackEvent('Step 6 (Shipping) - Clicked Next');
              this.props.nextStep();
            });
            return;
          }
          this.setState({ validZipCode: false });
          this.showOutOfAreaError();
          outOfArea(user)
            .then((res) => {
              console.log(res);
            });
        });
    } else {
      this.showMissingInfoError();
      if (isStateValid(state) !== undefined) {
        if (!isStateValid(state).validState && state !== '') {
          outOfArea(user)
          .then((res) => {
            console.log(res);
          });
        }
      }
    }
  }


  showMissingInfoError = () => {
    const { name, lastName, address, city, state, zipCode, phoneNumber } = this.props.user;

    const errorMessage = validationErrors.missingInfo;
    if (!name) this.setState({ validFirstname: false, errorMessage });
    if (!lastName) this.setState({ validLastname: false, errorMessage });
    if (!address) this.setState({ validAddress: false, errorMessage });
    if (!city) this.setState({ validCity: false, errorMessage });
    if (!state) this.setState({ validState: false, errorMessage });
    if (!phoneNumber) {
      this.setState({ validPhone: false, errorMessage });
    } else if (isPhoneNumber(phoneNumber)) this.setState(isPhoneNumber(phoneNumber));
    if (!zipCode) {
      this.setState({ validZipCode: false, errorMessage });
    } else {
      this.setState({ validZipCode: true });
    }
  }

  showOutOfAreaError = () => {
    const { validZipCode } = this.state;
    const errorMessage = validationErrors.zipcodeOutOfArea;
    if (!validZipCode) this.setState({ errorMessage });
  }

  onPressEnter = () => {
    const { name, lastName, address, city, state, phoneNumber, company, addressExtra, zipCode } = this.props.user;

    const newState = {
      validFirstname: true,
      validLastname: true,
      validAddress: true,
      validCity: true,
      validState: true,
      validPhone: true,
      validCompany: true,
      validZipCode: true,
    };

    if (!name) newState.validFirstname = false;
    if (!lastName) newState.validLastname = false;
    if (!address) newState.validAddress = false;
    if (!city) newState.validCity = false;
    if (!state) newState.validState = false;
    if (!phoneNumber) newState.validPhone = false;
    if (!zipCode) newState.validZipCode = false;
    if (!company) newState.validCompany = false;
    if (!addressExtra) newState.validAddressExtra = false;

    ConnectFields('StepShipping').gotoNext(newState);
  }

  render() {
    const { user, subscriptionStartingFrom, setShippingDate, availableDates } = this.props;
    const { errorMessage } = this.state;

    return (
      <div className="StepShipping">
        <Title>SHIPPING</Title>

        { errorMessage &&
          <Alert error>{ errorMessage }</Alert>
        }

        <div className="headline">
          <Paragraph>WHEN DO YOU WANT TO START?</Paragraph>
        </div>
        <div className="ShippingDatePicker">
          { availableDates.map((date, i) => {
            const cronDate = moment.utc(date).format('MM-DD-YYYY');
            const shippingDate = moment.utc(date).format('MM-DD-YYYY');
            return (
              <Button
                  key={`key-${i}`}
                selected={cronDate === subscriptionStartingFrom}
                onClick={setShippingDate.bind(null, cronDate, i)}
                dataIndex={i}
                dataName={`shipping-date-${i + 1}`}
                vertical
              >
                {`Week of ${shippingDate}`}
              </Button>
            );
          })}
        </div>

        <ShippingForm
          onChangeZipCode={this.onChangeZipCode}
          validation={this.state}
          defaultValues={user}
          onInputChange={this.isFormValid}
          onPressEnter={this.onPressEnter}
          connectFields={ConnectFields('StepShipping')}
        />

        <Paragraph>
          You can skip or cancel anytime. Fun fact: meals are usually delivered on Thursdays.
        </Paragraph>

        <ButtonBar>
          <Button onClick={this.checkAndNext} green buttonId='ButtonStepShipping' isDisabled={!this.state.formValid}>CONTINUE TO CHECKOUT</Button>
        </ButtonBar>
      </div>
    );
  }
}
