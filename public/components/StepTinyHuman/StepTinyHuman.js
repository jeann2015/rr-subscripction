import React, { Component } from 'react';
import cn from 'classnames';
import { ButtonBar, Button, Title, AllergyPromise, AddTinyHuman, Alert } from '../../../public/components';
import { TinyGroupField } from './TinyGroupField';
import TrackMixpanel from '../../utils/mixpanel';

import validationErrors from '../Signup/validationErrors.json';
import { isBabyNameValid, isDateValid, isGraterThanNow } from '../../utils';

import ConnectFields from '../../utils/connect-fields';

export class StepTinyHuman extends Component {
  constructor(props) {
    super(props);

    const firstLoadStatusForm = this.checkUserProps();
    this.state = {
      date: null,
      focused: false,
      formValid: firstLoadStatusForm,
      totalTinyHumans: 0,
      currentIndex: 0,
      errorMessage: '',
      groupFieldsStateErrors: this.props.childs.map((c) => {
        return { name: true, birthDate: true, birthDateGreater: true };
      }) || [{ name: true, birthDate: true, birthDateGreater: true }],
    };

    ConnectFields('StepTinyHuman').onComplete(() => { });
  }

  checkUserProps = () => {
    const { childs } = this.props;
    const groupFieldsStateErrors = [];
    let formValid = true;
    childs.forEach((child) => {
      groupFieldsStateErrors.push({ name: true, birthDate: true, birthDateGreater: true });
      if (!isDateValid(child.birthdate)) formValid = false;
      if (isGraterThanNow(child.birthdate)) formValid = false;
    });
    this.setState({ groupFieldsStateErrors });
    return formValid;
  }

  validateAll = () => {
    const { childs } = this.props;
    this.setState({ errorMessage: '' });
    childs.forEach((child) => {
      if (isDateValid(child.birthdate)) this.setState(isDateValid(child.birthdate));
      if (!isDateValid(child.birthdate) || !isDateValid(child.birthdate).validBirth
      ) {
        return false;
      }
    });

    return true;
  }

  checkValidStateOfForm = () => {
    this.setState({ errorMessage: '' });
    const { childs } = this.props;
    let formValid = true;
    childs.forEach((child) => {
      if (!isDateValid(child.birthdate)) formValid = false;
      if (isGraterThanNow(child.birthdate)) formValid = false;
    });
    this.setState({ formValid });
  }

  isFormValid = () => {
    const formValid = this.validateAll();
    this.setState({ formValid });
    return formValid;
  }

  addNewTinyHuman = () => {
    const { setChildData, childs } = this.props;
    const { groupFieldsStateErrors } = this.state;
    childs.push({});
    groupFieldsStateErrors.push({ name: true, birthDate: true, birthDateGreater: true });
    this.setState({ groupFieldsStateErrors });
    setChildData(childs);
    this.checkValidStateOfForm();
  }

  updateBirth = (birthdate, currentIndex, cb) => {
    const { setChildData, childs } = this.props;
    childs[currentIndex].birthdate = birthdate;
    setChildData(childs);
    this.checkValidStateOfForm();
    TrackMixpanel.trackEvent('Tiny Human DOB');
  }

  updateName = (name, currentIndex) => {
    const { setChildData, childs } = this.props;
    childs[currentIndex].name = name;
    setChildData(childs);
    this.checkValidStateOfForm();
    TrackMixpanel.trackEvent('Tiny Human Name');
  }

  saveAndNext = () => {
    if (this.state.formValid) {
      this.props.nextStep();
    } else {
      this.showMissingInfoError();
    }
  }

  showMissingInfoError = () => {
    const { missingInfo, futureDates } = validationErrors;
    const { childs } = this.props;
    const groupFieldsStateErrors = [];
    let errorMessage;

    childs.forEach((child, index) => {
      groupFieldsStateErrors[index] = {};
      groupFieldsStateErrors[index].name = true;
      groupFieldsStateErrors[index].birthDate = true;
      groupFieldsStateErrors[index].birthDateGreater = true;

      if (!isDateValid(child.birthdate)) {
        groupFieldsStateErrors[index].birthDate = false;
        errorMessage = missingInfo;
      }
      if (isGraterThanNow(child.birthdate)) {
        groupFieldsStateErrors[index].birthDateGreater = false;
        errorMessage = futureDates;
      }
    });
    this.setState({ errorMessage, groupFieldsStateErrors });
    console.log(groupFieldsStateErrors);
  }

  onPressEnter = () => {
    const { childs } = this.props;
    const newState = { };

    childs.forEach((child, index) => {
      newState[`validName-${index}`] = false;
      newState[`validBirthDate-${index}`] = false;
      newState[`validGreaterBirthDate-${index}`] = false;
      if (isBabyNameValid(child.name)) newState[`validName-${index}`] = true;
      if (isDateValid(child.birthdate)) newState[`validBirthDate-${index}`] = true;
      if (!isGraterThanNow(child.birthdate)) newState[`validGreaterBirthDate-${index}`] = true;
    });
    ConnectFields('StepTinyHuman').gotoNext(newState);
  }

  render() {
    const { childs, removeTinyHuman } = this.props;
    const { focused, errorMessage, formValid, groupFieldsStateErrors } = this.state;
    return (
      <div style={{ width: '100%' }}>
        <div className={cn('StepTinyHuman', { focused })}>
          { errorMessage &&
            <Alert error>{ errorMessage }</Alert>
          }
          <Title>Your tiny human</Title>
          {childs.map && childs.map((child, index) => (
            <TinyGroupField
                key={`key-${index}`}
              connectFields={ConnectFields('StepTinyHuman')}
              errors={groupFieldsStateErrors[index]}
              updateName={this.updateName}
              updateBirth={this.updateBirth}
              onPressEnter={this.onPressEnter}
              child={child}
              removeTinyHuman={(index) => {
                removeTinyHuman(index, this.checkValidStateOfForm);
              }}
              index={index}
              {...this.state}
            />
          ))}
          <AddTinyHuman onClick={this.addNewTinyHuman} />
          <hr className='separator' />
          <AllergyPromise />
        </div>
        <ButtonBar>
          <Button onClick={this.saveAndNext} isDisabled={!formValid} green buttonId='ButtonStepBaby'>I UNDERSTAND. CONTINUE TO CHECKOUT</Button>
        </ButtonBar>
      </div>
    );
  }
}

