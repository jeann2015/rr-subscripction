import React, { Component } from 'react';
import cn from 'classnames';
import moment from 'moment';
import { SingleDatePicker, isInclusivelyBeforeDay } from 'react-dates';
import { ButtonBar, Button, Title, Paragraph, GroupField, TextField, ErrorMessage, AllergyPromise, AddTinyHuman } from '../../../public/components';


export class TinyGroupField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      date: props.child.birthdate,
    };
  }
  render() {
    const { index, child, updateState, updateBirth, updateName, errors, onPressEnter, connectFields, removeTinyHuman } = this.props;
    const startIndex = index * 2;
    const { focused, date } = this.state;
    const formatDate = date ? date.format('YYYY-MM-DD') : '';
    return (
      <div data-baby-index={index} style={{ width: '100%' }}>
        <div className="Contain">
          <GroupField>
            <TextField
              className={cn({ invalid: !errors.name })}
              name="name"
              label="Tiny human's name"
              defaultValue={child.name}
              onChange={evt => updateName(evt.target.value, index)}
              onPressEnter={onPressEnter}
              ref={ref => connectFields.add({
                me: `name-${index}`,
                ref,
                next: `birthDate-${index}`,
                stateValid: `validName-${index}`,
                index: startIndex,
              })}
              medium
            />
          </GroupField>
          <GroupField className={cn({ invalid: !errors.birthDate, desktopDateInput: true })}>
            <SingleDatePicker
              placeholder="DOB"
              onDateChange={(date) => {
                this.setState({ date });
                updateBirth(date, index);
                onPressEnter();
              }}
              ref={ref => connectFields.add({
                me: `birthDate-${index}`,
                ref: { openDatePicker: () => this.setState({ focused: true }) },
                next: `name-${(index + 1)}`,
                stateValid: `validBirthDate-${index}`,
                index: startIndex + 1,
              })}
              date={date}
              focused={focused}
              onFocusChange={({ focused }) => this.setState({ focused })}
              numberOfMonths={1}
              daySize={35}
              isOutsideRange={day => !isInclusivelyBeforeDay(day, moment())}
              hideKeyboardShortcutsPanel
            />
          </GroupField>
          <GroupField className={cn({ smartphoneDateInput: true })}>
            <TextField
              className={cn({ invalid: !errors.birthDate })}
              name="dob"
              label="DOB"
              type="date"
              defaultValue={formatDate}
              onChange={(evt) => {
                const date = moment(evt.target.value);
                this.setState({ date });
                updateBirth(date, index);
              }}
              medium
            />
          </GroupField>
        </div>
        {index > 0 && <div className='removeTiny' onClick={() => removeTinyHuman(index)}>-</div>}
      </div>
    );
  }
}
