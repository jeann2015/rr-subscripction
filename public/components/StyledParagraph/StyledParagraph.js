import React, { PureComponent } from 'react';
import cn from 'classnames';
import styles from './StyledParagraph.css';

export class StyledParagraph extends PureComponent {
  static defaultProps = {
    disabled: false,
    borderless: false,
    green: false,
    filled: false,
    onClick: () => {},
  }

  render() {
    const { className } = this.props;
    return (
      <div
        className={cn('StyledParagraph', className)}
      >
        {this.props.children}
      </div>
    );
  }
}
