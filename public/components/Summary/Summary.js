import React, { Component } from 'react';
import { Title, OrderItem } from '../../../public/components';
import plans from './plans.json';
import './Summary.css';
import { getDiscountByBox } from '../../../server/helper/boxListsValues';

export class Summary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errorDiscountCode: '',
      amountOff: null,
      percentOff: null,
      generalDiscount: null,
      recurringDiscount: null,
      validCode: null,
      duration: null,
    };
  }

  componentDidMount() {
    const { childs, products, setProducts, subscription } = this.props;
    let items = products;

    //  Depending on childs, we generate an array with the selected product (we check that on the subscrption) and set it as state to Signup.js
    const pushProduct = () => {
      childs.map(() => {
        const productId = subscription.productId;
        const hasItem = items.filter(item => item.productId === productId);
        if (hasItem.length) {
          const index = items.findIndex(item => item.productId = productId);
        } else {
          items.push({ ...plans[productId],
            productId,
            quantity: 1,
            discount: false });
        }
      });

      setProducts(items);
    };
    // If user went back and changed product, we need to reset the product array on parent state. Then add the new one.
    if (items && items.length > 0) {
      items = [];
      setProducts(items);
      pushProduct();
      return;
    }

    // No childs? Get away from here mother fucker.
    if (!childs) {
      return;
    }

    pushProduct();
  }

  onIncrease = (index) => {
    const { products, setProducts } = this.props;
    const items = products;
    items[index].quantity = items[index].quantity + 1;
    setProducts(items);
  }

  onDecrease = (index) => {
    const { products, setProducts } = this.props;
    const items = products;

    items[index].quantity = items[index].quantity - 1;

    if (items[index].quantity < 1 && products.length === 1) {
      items[index].quantity = 1;
    }

    if (items[index].quantity === 0 && products.length > 1) {
      items.splice(index, 1);
    }

    setProducts(items);
  }

  saveAndNext = () => {
    const { validCode } = this.state;
    const { nextStep, user, childs, resetCouponCode } = this.props;
    if (!validCode) {
      resetCouponCode();
    }

    fetch('/api/users/nopayment', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        ...user,
        child: childs,
      }),
    }).then(() => {
      nextStep();
    });
  }

  removeDiscount = () => {
    this.props.removeDiscount();
  }

  resumeCouponCode = () => {
    this.props.resumeCouponCode();
  }

  getSecondBoxPrice = (product, boxList) => {
    const discount = getDiscountByBox(boxList, 2);

    if (discount.amountOff) {
      const boxPrice = parseFloat(product.price) - (discount.amountOff / 100);
      return boxPrice;
    }

    if (discount.percentOff) {
      const boxPrice = parseFloat(product.price) * ((100 - (discount.percentOff)) / 100);
      return boxPrice;
    }
    return product.price;
  }

  render() {
    const { duration, products, user, amountOff, percentOff, validCode, generalDiscount, recurringDiscount, setTotalSummary, boxList, subscription, children } = this.props;

    const frequencyString = (Number(subscription.frequency)) === 2 ? 'two' : 'four';
    const product = products[0];

    let recurrentTotal = products.reduce((total, product) => {
      return total + (product.price * product.quantity);
    }, 0);
    let totalToday = 0;
    let previousPrice = 0;
    products.forEach((product) => {
      let aux = product.price * product.quantity;
      previousPrice += aux;
      if (product.amountOff) {
        aux -= product.amountOff;
      }
      if (product.percentOff) {
        const totalDiscount = aux * (product.percentOff * 0.01);
        aux -= totalDiscount;
      }
      totalToday += aux;
    });

    let totalDiscount;

    if (user.couponCode) {
      if (amountOff) {
        totalToday -= amountOff;
      }
      if (percentOff) {
        totalDiscount = totalToday * (percentOff * 0.01);
        totalToday -= totalDiscount;
      }
      // If total is negative, set it to 0.
      totalToday = totalToday < 0 ? 0 : totalToday;
    }
    if (recurringDiscount) {
      recurrentTotal = totalToday;
    }

    if (setTotalSummary) {
      if (user.totalSummary !== totalToday) setTotalSummary(totalToday);
    }

    const total = totalToday.toFixed(2);
    const originalPrice = (parseFloat(product.price) * product.quantity).toFixed(2);
    const priceDiscounted = originalPrice !== total;

    return (
      <div className="Summary">
        <hr className="separator" />
        <Title>ORDER SUMMARY</Title>

        { product &&
          <OrderItem
            onIncrease={this.onIncrease}
            onDecrease={this.onDecrease}
            frequency={subscription.frequency}
            productDescription={product.description}
            originalPrice={originalPrice}
            quantity={product.quantity}
            discount={priceDiscounted}
            total={total}
            subscription={subscription}
          />
        }

        <hr className="separator" />
        <div className="perks">
          <div className="item">
            <span className="label">Shipping</span>
            <span className="price"><span className="number">Always FREE</span></span>
          </div>
        </div>

        <div className="total-wrapper">
          <div className="total">
            <span className='bold'>TODAY'S TOTAL:</span>
            { validCode &&
              <span className="ex-total bold">${previousPrice.toFixed(2)}</span>
            }
            <span>${total}</span>
          </div>
          { children }
          { duration === 'forever' &&
            <div className="subtitle charged-text">
              You’ll be charged ${total}
              <span> every {frequencyString} weeks for your membership.</span>
            </div>
          }
          { duration === 'custom' &&
            <div className="subtitle charged-text">
              You’ll be charged ${total} this time. Next time you'll be charged $
              <span className="line">{duration === 'custom_boxes' ? () => this.getSecondBoxPrice(product, boxList).toFixed(2) : total}. </span>
            </div>
          }
          { duration === 'custom_boxes' &&
            <div className="subtitle charged-text">
              You’ll be charged ${total} this time. You used a special coupon that applies different discounts in following orders. Any discount will always be applied to your account. Yay money!
            </div>
          }
          { ((validCode && (duration === 'once' || !duration))) &&
            <div className="subtitle charged-text">
                You’ll be charged ${total} this time. Next time you'll be charged $
              <span className="line">{previousPrice.toFixed(2)}</span>
              <span> every {frequencyString} weeks for your membership.</span>
            </div>
          }
          { (!validCode) &&
            <div className="subtitle charged-text">
                You’ll be charged ${previousPrice.toFixed(2)}
              <span> every {frequencyString} weeks for your membership.</span>
            </div>
          }
        </div>
      </div>
    );
  }
}
