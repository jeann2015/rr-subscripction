import React, { Component } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Payment from 'payment';
import MaskedInput from 'react-maskedinput';

export class TextField extends Component {
  static propTypes = {
    className: PropTypes.string,
    type: PropTypes.oneOf(['text', 'password', 'number', 'tel', 'date']),
    name: PropTypes.string,
    mask: PropTypes.string,
    defaultValue: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    disabled: PropTypes.bool,
    small: PropTypes.bool,
    medium: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    maxLength: PropTypes.number,
    inputMask: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onPressEnter: PropTypes.func,
    checkMailRef: PropTypes.func,
    autoCompleteOff: PropTypes.bool,
  }

  static defaultProps = {
    className: '',
    type: 'text',
    name: '',
    mask: null,
    defaultValue: '',
    placeholder: '',
    label: '',
    small: false,
    medium: false,
    disabled: false,
    onChange: () => { },
    onBlur: () => { },
    maxLength: 100,
    inputMask: false,
    onPressEnter: () => { },
    checkMailRef: () => { },
    autoCompleteOff: false,
  }

  constructor(props) {
    super(props);
    const { defaultValue } = this.props;
    const focus = (defaultValue !== '');
    this.state = {
      focus,
      value: this.props.defaultValue,
    };
  }

  onFocus = (event) => {
    this.setState({ focus: true });
  }

  onBlur = (event) => {
    if (this.state.value !== '') {
      return;
    }
    this.setState({ focus: false });
  }

  componentDidReceiveProps = () => {
    this.setState({ focus: this.props.defaultValue !== '', value: this.props.defaultValue });
  }

  componentDidMount() {
    const { TextField } = this.refs;
    const { mask, name } = this.props;
    if (mask === 'card') {
      Payment.formatCardNumber(TextField);
    }
    if (mask === 'expiry') {
      Payment.formatCardExpiry(TextField);
    }
    if (mask === 'cvc') {
      Payment.formatCardCVC(TextField);
    }
    if (name === 'email') {
      this.props.checkMailRef(this.refs.TextField);
    }
  }

  render() {
    const {
      className,
      type,
      name,
      defaultValue,
      placeholder,
      label,
      tiny,
      small,
      medium,
      onChange,
      onBlur,
      maxLength,
      disabled,
      tinyLabel,
      inputMask,
      onPressEnter,
      autoCompleteOff,
    } = this.props;
    const textfieldStyles = cn('TextField', className, { focus: defaultValue !== '' || this.state.focus });
    const inputStyles = cn('input', { tiny, small, medium });
    const labelStyles = cn('label', { tinyLabel });
    return (
      <div className={textfieldStyles}>
        {label && <span className={labelStyles}>{label}</span>}
        { inputMask &&
          <MaskedInput
            ref='TextField'
            mask={inputMask}
            type={type}
            name={name}
            className={inputStyles}
            placeholder={placeholder}
            value={defaultValue}
            defaultValue={defaultValue}
            autoComplete={autoCompleteOff ? 'off' : 'on'}
            onChange={(e) => {
              this.setState({
                ...this.state,
                value: e.target.value,
              });
              onChange(e);
            }}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            disabled={disabled}
            maxLength={maxLength}
            onKeyUp={(e) => {
              if (e.keyCode === 13) onPressEnter(e);
            }}
          />
        }
        { !inputMask &&
          <input
            ref='TextField'
            type={type}
            name={name}
            className={inputStyles}
            placeholder={placeholder}
            value={defaultValue}
            onChange={(e) => {
              this.setState({
                ...this.state,
                value: e.target.value,
              });
              onChange(e);
            }}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            disabled={disabled}
            maxLength={maxLength}
            onKeyUp={(e) => {
              if (e.keyCode === 13) onPressEnter(e);
            }}
          />
        }
        {this.props.errorMessage}
      </div>
    );
  }
}
