import React, { Component } from 'react';
import cn from 'classnames';
import { TinyHumansItem } from '../../../public/components';
import './TinyHumans.css';

export class TinyHumans extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showingModal: false,
    };
  }

  setShowingModal = (value) => {
    this.setState({ showingModal: value });
  }

  render() {
    const { showChilds, deleteChild } = this.props;
    const { showingModal } = this.state;
    const childs = this.props.childs.filter(child => 'show' in child);
    const canDeleteChild = !showingModal && childs.length >= 2;

    return childs.length ? (
      <div className={cn('TinyHumans', { visible: showChilds })}>
        <h3 className="title">Your tiny humans</h3>
        <div className="list">
          {childs.map((child, i) =>
            (<TinyHumansItem
              key={i}
              index={i}
              child={child}
              canDeleteChild={canDeleteChild}
              deleteChild={deleteChild}
              setShowingModal={this.setShowingModal}
            />),
          )}
        </div>
      </div>
    ) : null;
  }
}
