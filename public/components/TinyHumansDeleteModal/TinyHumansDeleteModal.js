import React, { Component } from 'react';
import cn from 'classnames';
import ReactGA from 'react-ga';
import { Button, ButtonBar } from '../../../public/components';
import './TinyHumansDeleteModal.css';

export class TinyHumansDeleteModal extends Component {
  deleteChild = () => {
    const { index, deleteChild } = this.props;

    deleteChild(index);
    ReactGA.event({
      category: 'Lead',
      action: 'Deleted Child',
    });
  }

  render() {
    const { child, deleteChild, hideDeleteModal, visible } = this.props;

    return (
      <div className={cn('TinyHumansDeleteModal', { visible })}>
        <div className="top">
          <button className="close" onClick={hideDeleteModal}>
            <img alt='close' src="/static/images/close.svg" />
          </button>
        </div>
        <h3 className="title">Your tiny humans</h3>
        <div className="name">{child.name}</div>
        <p className="text">
          Do you want to remove<br />
          {`${child.name}’s order?`}</p>
        <ButtonBar>
          <Button onClick={hideDeleteModal}>No</Button>
          <Button onClick={this.deleteChild}>Yes</Button>
        </ButtonBar>
      </div>
    );
  }
}
