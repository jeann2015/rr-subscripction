import React, { PureComponent } from 'react';
import cn from 'classnames';
import { TinyHumansDeleteModal } from '../../../public/components';

import './TinyHumansItem.css';

export class TinyHumansItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showDeleteModal: false,
    };
  }

  showDeleteModal = () => {
    if (!this.props.showingModal) {
      this.props.setShowingModal(true);
      this.setState({ showDeleteModal: true });
    }
  }

  hideDeleteModal = () => {
    this.props.setShowingModal(false);
    this.setState({ showDeleteModal: false });
  }

  deleteChild = (index) => {
    this.props.deleteChild(index);
    this.hideDeleteModal();
  }

  render() {
    const { child, index, deleteChild, canDeleteChild } = this.props;
    const { showDeleteModal } = this.state;

    return (
      <div className="TinyHumansItem">
        {!showDeleteModal && (
          <div className={cn('row', { canDeleteChild })}>
            <img className="line" src="/static/images/line.svg" alt="" />
            <button className="delete" onClick={this.showDeleteModal}>
              <img src="/static/images/delete.svg" alt="" />
            </button>
            <span className="name">{child.name}</span>
          </div>
        )}
        <TinyHumansDeleteModal
          index={index}
          visible={showDeleteModal}
          child={child}
          deleteChild={this.deleteChild}
          hideDeleteModal={this.hideDeleteModal}
        />
      </div>
    );
  }
}
