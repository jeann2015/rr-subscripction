import React, { PureComponent } from 'react';
import styles from './Title.css';

export class Title extends PureComponent {
  render() {
    return (
      <h2 className="Title">{this.props.children}</h2>
    );
  }
}
