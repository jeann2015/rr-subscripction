
const connectedFields = [];
const onComplete = [];

const getStatusStepName = (stepName, state) => {
  return connectedFields[stepName].filter(f => state[f.stateValid] === false);
};

export default (stepName) => {
  if (connectedFields[stepName] === undefined) connectedFields[stepName] = [];
  if (onComplete[stepName] === undefined) onComplete[stepName] = [];

  return {

    add: ({ me, ref, next, stateValid, index }) => {
      connectedFields[stepName][index] = { me, ref, next, stateValid };
    },

    gotoNext: (state) => {
      const filteredArray = getStatusStepName(stepName, state);

      if (filteredArray.length !== 0) {
        if (filteredArray[0].ref.refs) {
          if (filteredArray[0].ref.refs.TextField) {
            filteredArray[0].ref.refs.TextField.focus();
            return;
          }
        }
        if (filteredArray[0].ref.focus) {
          filteredArray[0].ref.focus();
          return;
        }
        if (filteredArray[0].ref.openDatePicker) {
          filteredArray[0].ref.openDatePicker();
        }
        return;
      }
      onComplete[stepName]();
    },

    onComplete: (cb) => {
      onComplete[stepName] = cb;
    },

  };
};
