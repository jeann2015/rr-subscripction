import moment from 'moment';
import StripeJs from '../components/StepBilling/Stripe';
import validationErrors from '../components/Signup/validationErrors.json';

require('es6-promise').polyfill();
require('isomorphic-fetch');

let Stripe;


export function showKindMealsDefaultBreakfast(boxQty, breakfastMealsQty) {
  let qtyMealsDefault = '';
  if ( breakfastMealsQty > 0 ) {
    qtyMealsDefault = boxQty - breakfastMealsQty;
  } else {
    qtyMealsDefault = boxQty;
  }
  return `(${breakfastMealsQty} Breakfast Oats, ${qtyMealsDefault} Anytime Meals)`
}

const isAlphanumeric = (string) => {
  const noNumbers = /^([^0-9]*)$/;
  if (string && string.length && noNumbers.test(string)) {
    return true;
  }
  return false;
};

const hasNoNumbers = (name) => {
  const noNumbers = /^([^0-9]*)$/;
  if (name && name.length && noNumbers.test(name)) {
    return true;
  }
  return false;
};


const isPhoneNumber = (phone) => {
  const isPhone = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
  if (isPhone.test(phone)) {
    if (phone.length) return { validPhone: true };
  } else if (phone.length) return { validPhone: false, errorMessage: validationErrors.phone };
};

const isBlank = (str) => {
  return (!str || /^\s*$/.test(str));
};

const isEmailValid = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (re.test(email)) {
    if (email.length) return { emailValid: true };
  } else if (email.length) return { emailValid: false, errorMessage: validationErrors.email };
};

const isEmailValidRefactor = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

const isZipValid = (zipCode) => {
  return new Promise((resolve, reject) => {
    fetch('/api/users/check-zipcode', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        zipCode,
      }),
    })
      .then(res => res.json())
      .then(res => resolve(res));
  });
};


const isZipValidWithData = (zipCode) => {
  return new Promise((resolve, reject) => {
    fetch('/api/users/check-zipcode', {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        zipCode,
      }),
    })
      .then(res => res.json())
      .then(res => resolve(res));
  });
};

const isZipCode = (zip) => {
  const length = /^\w{5}$/;
  const numbers = /^\d{5}$/;
  const obj = {};
  if (length.test(zip)) {
    if (numbers.test(zip)) {
      obj.validZipcode = true;
      obj.errorMessage = null;
      return obj;
    }
    obj.validZipcode = false;
    obj.errorMessage = validationErrors.zipcode;
    return obj;
  }
};

const isBabyNameValid = (name) => {
  const valid = hasNoNumbers(name);
  if (valid) {
    return { validBabyName: true };
  }
  if (name && name.length) return { validBabyName: false, errorMessage: validationErrors.babyName };
};

const isFirstnameValid = (firstname) => {
  const valid = hasNoNumbers(firstname);
  if (valid) {
    return { validFirstname: true };
  }
  if (firstname.length) return { validFirstname: false, errorMessage: validationErrors.firstname };
};

const isLastnameValid = (lastname) => {
  const valid = hasNoNumbers(lastname);
  if (valid) {
    return { validLastname: true };
  }
  if (lastname.length) return { validLastname: false, errorMessage: validationErrors.lastname };
};

const isDateValid = (date) => {
  const valid = (moment(date, 'MM-DD-YYYY').isValid());
  if (valid) {
    return { validBirth: true };
  }
  if (date) return { validBirth: false, errorMessage: validationErrors.birth };
};

const isGraterThanNow = (date) => {
  const tinyHumanDate = moment(date).startOf('day');
  const now = moment().startOf('day');
  return tinyHumanDate > now;
};


const isAddressValid = (str) => {
  const valid = !isBlank(str);
  if (valid) {
    if (str) return { validAddress: true };
  } else if (str) return { validAddress: false, errorMessage: validationErrors.address };
};

const isCityValid = (city) => {
  const valid = isAlphanumeric(city);
  if (valid) {
    return { validCity: true };
  }
  if (city.length) return { validCity: false, errorMessage: validationErrors.city };
};

const outOfArea = (user) => {
  return fetch('/api/users/outofarea', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      ...user,
    }),
  });
};

const isStateValid = (state) => {
  const valid = isAlphanumeric(state);
  if (valid) {
    if (state === 'AK' || state === 'HI') return { validState: false, errorMessage: validationErrors.stateOutOfArea };
    return { validState: true };
  }
  if (state.length) return { validState: false, errorMessage: validationErrors.state };
};


const getCookie = (name) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) {
    return parts.pop()
      .split(';')
      .shift();
  }
};

const getShippingDate = (success, zipCode) => {
  fetch(`/api/crons/time?zipCode=${zipCode}`)
  .then(res => res.json())
    .then(success)
    .catch((err) => {
      console.log(err);
    });
};

const isAndroid = () => {
  const ua = window.navigator.userAgent.toLowerCase();
  return ua.indexOf('android') > -1;
};

const validateCVV = (cvv) => {
  if (!cvv || !cvv.length) return false;
  if (cvv.length >= 3) {
    return { validCvv: true };
  }
  return { validCvv: false, errorMessage: 'Cvv number is invalid' };
};

StripeJs(() => {
  Stripe = window.Stripe;
  Stripe.setPublishableKey(stripeApiPublicKey);
});


const validateCreditCard = (cardNumber) => {
  if (!cardNumber.length) return false;

  if (typeof Stripe === 'undefined') {
    return { validCard: false };
  }

  if (Stripe.card.validateCardNumber(cardNumber) === true) {
    return { validCard: true };
  }
  return { validCard: false, errorMessage: 'Card number is invalid.' };
};

const validateCardExpiry = (month, year) => {
  if ((!month && !year) || (!month.length && !year.length)) return false;

  if (typeof Stripe === 'undefined') {
    return { validExpiry: false };
  }

  if (Stripe.card.validateExpiry(month, year)) {
    return { validExpiry: true };
  }
  return { validExpiry: false, errorMessage: 'Expiration date is invalid.' };
};

const updateCreditCardInformation = (userHash, user, token, success) => {
  fetch(`/api/users/${userHash}/updateCreditCard`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      user,
      token: token,
      referralCode: user.referralCode,
    }),
  })
    .then(success)
    .catch((err) => {
      throw new Error(err);
    });
};

const createToken = (cardNumber, cvc, exp_month, exp_year, success) => {
  Stripe.card.createToken({
    number: cardNumber,
    cvc,
    exp_month,
    exp_year,
  }, success);
};

const getBillingDataByHash = (hash, success) => {
  fetch(`/api/users/hash/${hash}`, {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
  })
    .then((res) => {
      if (res.status === 200) {
        return res.json().then(success);
      }
    })
    .catch((res) => {
      console.log(res);
    });
};


const US_STATES = {
  AL: 'Alabama',
  AK: 'Alaska',
  AZ: 'Arizona',
  AR: 'Arkansas',
  CA: 'California',
  CO: 'Colorado',
  CT: 'Connecticut',
  DE: 'Delaware',
  DC: 'District Of Columbia',
  FL: 'Florida',
  GA: 'Georgia',
  HI: 'Hawaii',
  ID: 'Idaho',
  IL: 'Illinois',
  IN: 'Indiana',
  IA: 'Iowa',
  KS: 'Kansas',
  KY: 'Kentucky',
  LA: 'Louisiana',
  ME: 'Maine',
  MD: 'Maryland',
  MA: 'Massachusetts',
  MI: 'Michigan',
  MN: 'Minnesota',
  MS: 'Mississippi',
  MO: 'Missouri',
  MT: 'Montana',
  NE: 'Nebraska',
  NV: 'Nevada',
  NH: 'New Hampshire',
  NJ: 'New Jersey',
  NM: 'New Mexico',
  NY: 'New York',
  NC: 'North Carolina',
  ND: 'North Dakota',
  OH: 'Ohio',
  OK: 'Oklahoma',
  OR: 'Oregon',
  PA: 'Pennsylvania',
  RI: 'Rhode Island',
  SC: 'South Carolina',
  SD: 'South Dakota',
  TN: 'Tennessee',
  TX: 'Texas',
  UT: 'Utah',
  VT: 'Vermont',
  VA: 'Virginia',
  WA: 'Washington',
  WV: 'West Virginia',
  WI: 'Wisconsin',
  WY: 'Wyoming',
};

export {
  isAlphanumeric,
  isPhoneNumber,
  isBlank,
  isEmailValid,
  isEmailValidRefactor,
  isZipCode,
  isBabyNameValid,
  isFirstnameValid,
  isLastnameValid,
  isDateValid,
  isAddressValid,
  isCityValid,
  isStateValid,
  hasNoNumbers,
  getCookie,
  isAndroid,
  isZipValid,
  getShippingDate,
  getBillingDataByHash,
  validateCreditCard,
  validateCVV,
  validateCardExpiry,
  createToken,
  updateCreditCardInformation,
  US_STATES,
  isZipValidWithData,
  outOfArea,
  isGraterThanNow,
};

