
export default class TrackMixpanel {

  static setId(id) {
    if (window.mixpanel) mixpanel.identify(this.id);
  }

  static track(config) {
    if (window.mixpanel) mixpanel.people.set(config);
  }

  static trackEvent(event) {
    // init history of tracking events
    if (!Object.prototype.hasOwnProperty.call(window, 'mixPanelTrackingEvents')) {
      window.mixPanelTrackingEvents = {};
    }

    if (!Object.prototype.hasOwnProperty.call(window.mixPanelTrackingEvents, event)) {
      console.log(`Track mixpanel event: ${event}`); // this console.log is only for QA use
      window.mixPanelTrackingEvents[event] = true;
      if (window.mixpanel) mixpanel.track(event);
    }
  }

}
