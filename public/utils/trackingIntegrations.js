import ReactGA from 'react-ga';

export const trackAddToCart = (value) => {
  // Facebook
  if (typeof fbq !== 'undefined') {
    fbq('track', 'AddToCart', {
      value,
      currency: 'USD',
    });
  }

  // Reddit
  if (typeof rdt !== 'undefined') {
    rdt('track', 'AddToCart');
  }

  // Snapchat
  if (typeof snaptr !== 'undefined') {
    snaptr('track', 'ADD_CART');
  }

  // Pinterest
  if (typeof pintrk !== 'undefined') {
    pintrk('track', 'AddToCart', {
      value,
      currency: 'USD',
    });
  }
};

export const trackLead = () => {
  // Google Ads
  if (typeof ReactGA !== 'undefined') {
    ReactGA.event({
      category: 'Lead',
      action: 'New Lead',
    });
  }

  // Facebook
  if (typeof fbq !== 'undefined') {
    fbq('track', 'Lead', {
      value: 10.00,
      currency: 'USD',
    });
  }

  // Reddit
  if (typeof rdt !== 'undefined') {
    rdt('track', 'Lead');
  }

  // Snapchat
  if (typeof snaptr !== 'undefined') {
    snaptr('track', 'START_CHECKOUT');
  }

  // Pinterest
  if (typeof pintrk !== 'undefined') {
    pintrk('track', 'PageVisit');
  }
};

export const trackPurchase = ({ value, productId, productName, transactionId, userData }) => {
  if (typeof ReactGA !== 'undefined') {
    ReactGA.event({
      category: 'Lead',
      action: 'Customer',
    });

    ReactGA.pageview('/signup/StepSuccessful');
  }

  if (typeof gtag !== 'undefined') {
    gtag('event', 'conversion', {
      send_to: 'AW-866054548/iqA-CPSy3pEBEJTj-5wD',
      value,
    });
  }

  if (typeof fbq !== 'undefined') {
    fbq('track', 'Purchase', {
      value,
      currency: 'USD',
      content_ids: productId,
      content_type: productName,
    });
  }

  if (typeof rdt !== 'undefined') {
    rdt('track', 'Purchase');
  }

  if (typeof snaptr !== 'undefined') {
    snaptr('track', 'PURCHASE', {
      price: value,
      currency: 'USD',
      transaction_id: transactionId,
    });
  }

  if (typeof pintrk !== 'undefined') {
    pintrk('track', 'Checkout', {
      value,
      currency: 'USD',
      product_id: productId,
      product_name: productName,
    });
  }

  // Refersion
  if (typeof _refersion !== 'undefined') {
    _refersion(() => {
      _rfsn._addItem({
        sku: productName,
        price: value,
        name: productName,
        quantity: 1,
      });

      _rfsn._addCustomer({
        first_name: userData.name,
        last_name: userData.lastName,
        email: userData.email,
      });

      _rfsn._addTrans({
        order_id: transactionId,
        currency_code: 'USD',
      });

      _rfsn._sendConversion();
    });
  }
};

export const trackNoPayment = () => {
  if (typeof ReactGA !== 'undefined') {
    ReactGA.event({
      category: 'Lead',
      action: 'No payment',
    });
  }

  if (typeof rdt !== 'undefined') {
    rdt('track', 'ViewContent');
  }

  if (typeof snaptr !== 'undefined') {
    snaptr('track', 'VIEW_CONTENT');
  }
};
