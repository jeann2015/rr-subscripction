import passportJWT from 'passport-jwt';

export default {
  stripeApiKey: process.env.STRIPE_KEY || 'sk_test_RW4S1wemkT63QP9D7fVlq8n1',
  segmentKey: process.env.SEGMENT_KEY || 'EyunE2yD4nq1dHTSRFiNc8YIg63W5RIh',
  zendeskUrl: process.env.ZENDESK_URL || 'raisedreal.zendesk.com',
  zendeskKey: process.env.ZENDESK_KEY || 'EBZuqrwrPmEUGL3ACIGfQ7UhmwDdo2HAWD7g8DTV',
  zendeskUser: process.env.ZENDESK_USER || 'aerolab.rr@gmail.com',
  zeroDay: process.env.ZERO_DAY || '20170501',
  dayWhenRepeat: process.env.CRON_EVERY_DAYS || 7,
  adminUrl: process.env.ADMIN_URL || 'http://localhost:3010',
  zendeskEnvName: process.env.ZENDESK_ENV_NAME || 'local-env',
  shouldRunCrons: process.env.SHOULD_RUN_CRONS || false,
  outbound: process.env.OUTBOUND || '62cc0a420c94b7c01a8c5ee65cae1176',
  outboundEnv: process.env.OUTBOUND_ENV || 'local-env',
  fallbackWarehouse: process.env.FALLBACK_WAREHOUSE || 2,
};

export const jwtOptions = () => {
  const ExtractJwt = passportJWT.ExtractJwt;
  const jwtOptionsObject = {};
  jwtOptionsObject.jwtFromRequest = ExtractJwt.fromAuthHeader();
  jwtOptionsObject.secretOrKey = 'secretKeyJWT';
  return jwtOptionsObject;
};
