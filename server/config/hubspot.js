
export default {
  hubspotApi: process.env.HUBSPOT_API || 'fca9722e-d889-49af-8108-78b19c683322',
  hubspotWeeklyList: process.env.HUBSPOT_WEEKLY_LIST_ID || 9,

  portalId: process.env.HUBSPOT_PORTAL_ID || '6157110',

  workflows: {
    newCustomer: process.env.HUBSPOT_WORKFLOW_NEW_CUSTOMER || '2565184',
    userChangeEmail: process.env.HUBSPOT_WORKFLOW_USER_CHANGE_EMAIL || '2549738',
  },
  forms: {
    signup: process.env.HUBSPOT_FORM_SIGNUP || '49025597-82dc-4f42-99f0-2fb5bbfa9261',
  },
  sendEmailTraking: {
    send: process.env.EMAILID || '10663193575',
  },
};

