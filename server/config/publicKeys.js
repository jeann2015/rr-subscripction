module.exports = {
  stripeApiPublicKey: process.env.STRIPE_PUBLIC_KEY || 'pk_test_72L4pF8QYrUQdwsGmPKnlBYh',
  APP_URL: process.env.APP_URL || 'http://localhost:3000',
  environment: process.env.NODE_ENV || 'development',
  qa_env: process.env.QA || false,
  GA_ID: process.env.GA_ID || 'UA-102598806-2',
};
