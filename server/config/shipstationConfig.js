export default {
  shipstationApiKey: process.env.SHIPSTATION_API_KEY || 'df4ff7d1144244ee824176f27fafd6df',
  shipstationApiSecret: process.env.SHIPSTATION_API_SECRET || 'aae14d7e9de34a33910c301a4eca7120',

  tag1Day: process.env.TAG_1_DAY || 8335,
  tag2Day: process.env.TAG_2_DAY || 8336,

  custom: process.env.TAG_CONFIG_CUSTOM || 8323,
  standard: process.env.TAG_CONFIG_STANDARD || 8324,

  tagQuantity12: process.env.TAG_QUANTITY_12 || 8333,
  tagQuantity24: process.env.TAG_QUANTITY_24 || 8334,

  shipstationEnv: process.env.SHIPSTATION_ENV || 'QA', // don't use '-' in env
};
