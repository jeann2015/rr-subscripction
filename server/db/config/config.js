const mysql = process.env.DATABASE_URL || 'mysql://root:root@localhost:3306/raisedreal';
const mysqlRegex = /mysql:\/\/(\w*):(\w*)@([\w-].*):(\d*)\/([\w-]*)/;
const mysqlConn = mysql.match(mysqlRegex);

module.exports = {
  development: {
    username: 'root',
    password: 'root',
    database: 'raisedreal',
    host: '127.0.0.1',
    dialect: 'mysql',
    logging: false,
  },
  test: {
    username: 'root',
    password: 'root',
    database: process.env.MYSQL_DATABASE || 'raisedreal_test',
    host: process.env.MYSQL_HOST || '127.0.0.1',
    dialect: 'mysql',
  },
  production: {
    username: mysqlConn[1],
    password: mysqlConn[2],
    database: mysqlConn[5],
    host: mysqlConn[3],
    port: mysqlConn[4],
    dialect: 'mysql',
  },
};
