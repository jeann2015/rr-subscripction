/* eslint vars-on-top: "off" */
import Sequelize from 'sequelize';
import dbConfig from './config/config';

// Sequelize Virtual Fields does some code injection, so it's loaded before setup
// require('sequelize-virtual-fields')(Sequelize)

const env = process.env.NODE_ENV || 'development';
const config = dbConfig[env];
const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  dialect: config.dialect,
  logging: config.logging,
  dialectOptions: {
    multipleStatements: true,
  },
  define: {
    hooks: {
      beforeUpdate: (instance, opts) => {
        instance.dataValues.dataChanged = [];
        opts.fields.forEach((field) => {
          const obj = {};
          const oldValue = instance._previousDataValues[field];
          const newValue = instance.dataValues[field];
          obj.field = field;
          obj[`new_${field}`] = newValue;
          obj[`old_${field}`] = oldValue;
          instance.dataValues.dataChanged.push(obj);
        });
      },
    },
  },
});

// Test
// if (process.env.NODE_ENV === 'test') {
//   sequelize.sync({ force: true });
// }

// Prod
if (process.env.NODE_ENV !== 'test') {
  sequelize.sync();
}

// Load models ~ Name of the Entitie.
const models = [
  'User',
  'Child',
  'Order',
  'Product',
  'Admin',
  'Cron',
  'Subscription',
  'Coupon',
  'Referral',
  'Log',
  'Refund',
  'Skip',
  'TagDay',
  'TagDayName',
  'AppliedDiscounts',
  'BoxDetailHistory',
  'Ingredients',
  'MealsIngredients',
  'Meals',
  'ExcludedIngredients',
  'MealsStock',
  'Warehouses',
];

models.forEach((model) => {
  module.exports[model] = sequelize.import(`../entities/${model}/model`);
});

(function (m) {
  m.Admin.hasMany(m.Log);

  m.User.belongsTo(m.TagDay, { as: 'TagDay', foreignKey: 'zip_code', targetKey: 'zipCode' });
  m.User.hasMany(m.Child);
  m.User.hasMany(m.Order);
  m.User.hasMany(m.Subscription);
  m.User.hasMany(m.Log);
  m.User.hasMany(m.Referral, {
    foreignKey: 'referredBy',
    as: 'userReferred',
  });
  m.User.hasMany(m.Referral, {
    foreignKey: 'me',
    as: 'userReferree',
  });

  m.User.hasMany(m.AppliedDiscounts, {
    foreignKey: 'user',
    as: 'couponUsed',
  });

  m.User.hasMany(m.AppliedDiscounts, {
    foreignKey: 'user',
    as: 'nextDiscount',
  });

  m.AppliedDiscounts.belongsTo(m.Coupon, {
    foreignKey: 'coupon',
    targetKey: 'id',
    as: 'couponApplied',
  });

  m.Subscription.belongsTo(m.User);
  m.Subscription.belongsTo(m.Product);
  m.Subscription.belongsTo(m.Coupon);
  m.Subscription.hasMany(m.Order);
  m.Subscription.hasMany(m.Skip, {
    foreignKey: 'subscription',
  });
  m.Subscription.hasMany(m.Order, {
    as: 'orderCount',
  });
  m.Subscription.hasMany(m.ExcludedIngredients);

  m.Order.belongsTo(m.Subscription);
  m.Order.belongsTo(m.User);
  m.Order.belongsTo(m.Cron);
  m.Order.belongsTo(m.TagDay, {
    foreignKey: 'zipCode',
    targetKey: 'zipCode',
    as: 'tagDayName',
  });
  m.Order.hasMany(m.BoxDetailHistory, {
    as: 'boxDetail',
  });

  m.Product.hasMany(m.Subscription);

  m.Cron.hasMany(m.Order);

  m.Child.belongsTo(m.User);

  m.Coupon.hasMany(m.Subscription, {
    foreignKey: 'couponId',
  });
  m.Coupon.belongsTo(m.Product);

  m.Referral.belongsTo(m.User, {
    foreignKey: 'referredBy',
    as: 'userReferred',
  });

  m.Referral.belongsTo(m.User, {
    foreignKey: 'me',
    as: 'userReferree',
  });

  m.Referral.belongsTo(m.Subscription, {
    foreignKey: 'referredBy',
    targetKey: 'userId',
    as: 'subscriptionReferred',
  });

  m.Referral.belongsTo(m.Subscription, {
    foreignKey: 'me',
    targetKey: 'userId',
    as: 'subscriptionReferree',
  });

  m.Log.belongsTo(m.User);
  m.Log.belongsTo(m.Admin);

  m.Refund.belongsTo(m.Order);
  m.Refund.belongsTo(m.Admin);

  m.TagDay.belongsTo(m.TagDayName, { as: 'TagDayName', foreignKey: 'name' });

  m.TagDayName.belongsTo(m.Warehouses, { as: 'Warehouse', foreignKey: 'warehouse_id' });

  m.BoxDetailHistory.belongsTo(m.Order);
  m.BoxDetailHistory.belongsTo(m.Meals);

  m.Ingredients.hasMany(m.ExcludedIngredients);
  m.Ingredients.hasMany(m.MealsIngredients);

  m.MealsStock.belongsTo(m.Meals);

  m.Meals.hasMany(m.MealsStock, {
    foreignKey: 'meal_sku',
  });
  m.Meals.hasMany(m.MealsIngredients, {
    foreignKey: 'meal_sku',
  });

  m.MealsIngredients.belongsTo(m.Ingredients);
  m.MealsIngredients.belongsTo(m.Meals);

  m.ExcludedIngredients.belongsTo(m.Ingredients);
  m.ExcludedIngredients.belongsTo(m.Subscription);
}(module.exports));

// Export connection.
module.exports.sequelize = sequelize;
