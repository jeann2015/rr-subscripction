import jwt from 'jsonwebtoken';
import passswordHash from 'password-hash';
import { jwtOptions } from '../../config/config';

const db = require('../../db/connection');

const Admin = db.Admin;
const jop = jwtOptions();

export const login = (req, res, next) => {
  req.assert('email', 'email is required').notEmpty();
  req.assert('password', 'password is required').notEmpty();
  req.getValidationResult().then((result) => {
    const errors = result.array();
    if (errors.length > 0) {
      res.status(400).send(errors);
    } else {
      Admin.find({
        where: { email: req.body.email },
        raw: true,
      }).then((admin) => {
        if (!admin) {
          res.status(401).send({ status: 'User didnt match' });
          return;
        }

        if (passswordHash.verify(req.body.password, admin.password)) {
          const payload = { id: admin.id };
          const token = jwt.sign(payload, jop.secretOrKey);
          res.status(200).send({ status: 'Ok', token });
        } else {
          res.status(401).send({ status: 'Passwords didnt match' });
          return;
        }
      }, (err) => {
        next(err);
      });
    }
  }, (err) => {
    next(err);
  });
};

export const createAdmin = (req, res, next) => {
  req.assert('username', 'username is required').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password is required').notEmpty();
  req.getValidationResult().then((result) => {
    const errors = result.array();
    if (errors.length > 0) {
      res.status(400).send(errors);
    } else {
      const pass = passswordHash.generate(req.body.password);
      Admin.findOrCreate({
        where: { email: req.body.email },
        defaults: {
          name: req.body.username,
          password: pass,
        },
      })
      .spread((admin, created) => {
        if (created) {
          res.status(200).send({ status: 'ok' });
        } else {
          res.status(409).send('admin repeated');
        }
      }, (err) => {
        next(err);
      });
    }
  }, (err) => {
    next(err);
  });
};
