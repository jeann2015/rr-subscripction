export default (sequelize, DataTypes) => {
  return sequelize.define('admin', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
