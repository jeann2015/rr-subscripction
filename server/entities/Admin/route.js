import passport from 'passport';
import { createAdmin, login } from './controller';

export default (router) => {
  router.post('/admin/new', passport.authenticate('jwt', { session: false }),
    createAdmin,
  );
  router.post('/admin/login',
    login,
  );
};
