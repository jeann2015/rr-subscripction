const db = require('../../db/connection');

const AppliedDiscounts = db.AppliedDiscounts;
const Coupon = db.Coupon;


export const getAppliedCoupons = async (req, res, next) => {
  const { userId } = req.query;
  AppliedDiscounts.findAll({
    where: {
      user: userId,
      coupon: {
        $not: null,
      },
    },
    group: 'DATE(createdAt)',
    include: {
      model: Coupon,
      as: 'couponApplied',
      order: '"updatedAt" DESC',
    },
  })
    .then((coupons) => {
      res.set('X-Total-Count', coupons.length);
      res.status(200).send(coupons);
    })
    .catch(err => next(err));
};

export const getAppliedDiscounts = async (req, res, next) => {
  const { userId } = req.query;
  AppliedDiscounts.findAll({
    where: {
      user: userId,
      nextDiscountType: {
        $or: ['percent', 'amount'],
      },
    },
  })
    .then((coupons) => {
      res.set('X-Total-Count', coupons.length);
      res.status(200).send(coupons);
    })
    .catch(err => next(err));
};

const getRelatedDiscount = (order, allDiscounts) => {
  const userDiscounts = allDiscounts.filter(disc => disc.user === order.userId);

  if (!userDiscounts.length) return []; // If no discounts for user, return.

  // For free charge orders, if it is the first one, return first discount.
  // Return same cron id discount otherwise
  if (order.chargeToken === 'free charge') {
    if (order.boxNumber === 1) {
      return userDiscounts[0];
    }
    return userDiscounts.find(disc => disc.cronId === order.cronId) || [];
  }

  return userDiscounts.find(disc => disc.stripeToken === order.chargeToken) || [];
};

const filterDiscountsApplied = (discount) => {
  const discountTypes = ['nextDiscountAmount', 'nextDiscountPercent', 'referreerAmount', 'userCreditAmount', 'couponAmount'];
  const applied = [];
  for (const type of discountTypes) {
    if (discount[type] && discount[type] !== null) applied.push({ [type]: discount[type] });
  }
  return applied;
};

const getDiscountsApplied = (discountObj) => {
  const labels = {
    nextDiscountAmount: 'Next Discount',
    nextDiscountPercent: 'Next Discount',
    referreerAmount: 'Referrer',
    userCreditAmount: 'Credit',
    couponAmount: 'Coupon',
  };

  const discounts = filterDiscountsApplied(discountObj);

  if (!discounts.length) return null;
  if (discounts.length > 1) return 'Multiple';
  return labels[Object.keys(discounts[0])];
};

// Temporal functions to fix percent discounts calculation
// ---------------
const getOrderProductPrice = (order, products) => {
  if (order.boxType.includes('Regular') || order.boxType.includes('Migrated')) {
    return products.find(prod => prod.boxType === '24 Meals Box Migrated').price;
  }
  if (order.boxType.includes('12 Meals')) {
    return products.find(prod => prod.boxType === '12 Meals Box').price;
  }
  return products.find(prod => prod.boxType === '24 Meals Box').price;
};

const getDiscountsAppliedAmount = (discountObj, order, products) => {
  const discounts = filterDiscountsApplied(discountObj);
  return discounts.reduce((prev, curr) => {
    const key = Object.keys(curr)[0];
    if (key.includes('percent')) return prev + getOrderProductPrice(order, products) * (curr[key] / 100);
    return prev + curr[key];
  }, 0);
};
// ---------------

export const addDiscountsToOrders = (orders) => {
  return new Promise(async (resolve, reject) => {
    try {
      const discounts = await db.AppliedDiscounts.findAll({});
      const products = await db.Product.findAll({});

      const plainDiscounts = discounts.map(disc => disc.get({ plain: true }));

      const withDisc = orders.map((order) => {
        const discount = getRelatedDiscount(order, plainDiscounts);
        order.discount = discount;
        order.discountApplied = {
          applied: getDiscountsApplied(discount),
          amountApplied: getDiscountsAppliedAmount(discount, order, products),
        };
        return order;
      });
      resolve(withDisc);
    } catch (error) {
      reject(error);
    }
  });
};
