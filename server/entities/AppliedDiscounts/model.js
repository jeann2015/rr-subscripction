export default (sequelize, DataTypes) => {
  return sequelize.define('applied_discounts', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    user: DataTypes.INTEGER,
    referreer: DataTypes.INTEGER,
    referreerAmount: {
      type: DataTypes.INTEGER,
      field: 'referreer_amount',
    },
    coupon: DataTypes.INTEGER,
    couponAmount: {
      type: DataTypes.INTEGER,
      field: 'coupon_amount',
    },
    userCreditAmount: {
      type: DataTypes.INTEGER,
      field: 'user_credit_amount',
    },
    nextDiscountType: {
      type: DataTypes.STRING,
      field: 'next_discount_type',
    },
    nextDiscountAmount: {
      type: DataTypes.INTEGER,
      field: 'next_discount_amount',
    },
    nextDiscountPercent: {
      type: DataTypes.INTEGER,
      field: 'next_discount_percent',
    },
    stripeToken: {
      type: DataTypes.STRING,
      field: 'stripe_token',
    },
    cronId: {
      type: DataTypes.INTEGER,
      field: 'cron_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
  });
};
