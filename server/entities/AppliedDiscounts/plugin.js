import eachLimit from 'async/eachLimit';

const db = require('../../db/connection');

const AppliedDiscounts = db.AppliedDiscounts;
const Log = db.Log;
const Coupon = db.Coupon;

export const registerAppliedDiscount = async (discounts, user, stripeToken, cronId) => {
  return new Promise((resolve, reject) => {
    const discountData = { stripeToken, user, cronId };
    discounts.forEach((discount) => {
      switch (discount.type) {
        case 'subscriptionCoupon':
          discountData.coupon = discount.couponId;
          discountData.couponAmount = discount.amount;
          break;
        case 'subscriptionNextDiscount':
          discountData.nextDiscountAmount = discount.amount;
          discountData.nextDiscountType = discount.typeNextDiscount;
          discountData.nextDiscountPercent = discount.percent;
          break;
        case 'subscriptionReferral':
          discountData.referreerAmount = discount.amount;
          discountData.referreer = discount.referreer;
          break;
        case 'userCredit':
          discountData.userCreditAmount = discount.credit;
          break;
        default:
          break;
      }
    });
    return AppliedDiscounts.create(discountData);
  });
};


export const fillOldCoupons = async (req, res, next) => {
  const MY_SQL_REGEX_LIST = [
    String.raw`Coupon ([A-Z0-9a-z_]+) was redeemed by ([A-Za-z_ ]+)`,
    String.raw`Coupon ([A-Z0-9a-z_]+) was used by ([A-Za-z_ ]+) in ([0-9]+)`,
    String.raw`A coupon \\(\\#([0-9]+)\\) was applied for \\$([0-9.]+)`,
  ];
  const JS_REGEX_LIST = [
    String.raw`Coupon ([A-Z0-9a-z_]+) was redeemed by ([A-Za-z_ ]+)`,
    String.raw`Coupon ([A-Z0-9a-z_]+) was used by ([A-Za-z_ ]+) in ([0-9]+)`,
    String.raw`A coupon \(\#([0-9]+)\) was applied for \$([0-9.]+)`,
  ];
  const where = MY_SQL_REGEX_LIST.map((reg, index) => `message REGEXP '${String(MY_SQL_REGEX_LIST[index].toString())}'`).join(' OR ');
  const logsCoupons = await Log.findAll({
    attributes: ['id', 'message', 'userId', 'createdAt'],
    where: [
      `${where} AND created_at < '2018-03-28 19:24:32'`,
    ],
  });
  eachLimit(logsCoupons, 1, async ({ id, message, userId, createdAt }, next) => {
    let isCouponId = false;
    const match = JS_REGEX_LIST.find((reg, index) => {
      if (index === 2) {
        isCouponId = true;
      }
      return new RegExp(reg).test(message);
    });
    const codeOrId = new RegExp(match).exec(message)[1];

    const whereCoupon = !isCouponId ? { code: codeOrId } : { id: parseInt(codeOrId) };

    try {
      const coupon = await Coupon.findOne({
        attributes: ['id', 'percentOff', 'amountOff', 'maxRedemptions', 'duration'],
        where: whereCoupon,
      });
      const plainCoupon = coupon.get({ plain: true });
      let discount = 0;
      if (plainCoupon.amountOff) {
        discount = plainCoupon.amountOff;
      }
      if (plainCoupon.percentOff) {
        discount = 9500 * (plainCoupon.percentOff / 100);
      }
      const appliedDiscountPayload = {
        user: userId,
        coupon: coupon.id,
        couponAmount: discount,
        createdAt,
        updatedAt: createdAt,
      };
      try {
        const buildDiscount = await AppliedDiscounts.create(appliedDiscountPayload);
        console.log(`Applied Discount for the log ${id} was succesful created`);
      } catch (err) {
        console.log(`Applied Discount for the log ${id} can't be created`);
      }
      next();
    } catch (err) {
      console.log(`There was an error with the log ${id}: '${err}'`);
      next();
    }
  }, (err) => {
    res.status(404).send('migration ok!');
  });
};
