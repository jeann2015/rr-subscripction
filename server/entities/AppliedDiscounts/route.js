import passport from 'passport';

import { getAppliedCoupons, getAppliedDiscounts } from './controller';
import { fillOldCoupons } from './plugin';

export default (router) => {
  router.get('/applied-coupons',
    passport.authenticate('jwt', { session: false }),
    getAppliedCoupons,
  );
  router.get('/applied-discounts',
    passport.authenticate('jwt', { session: false }),
    getAppliedDiscounts,
  );
  router.get('/applied-coupons/fill-old-coupons',
    // passport.authenticate('jwt', { session: false }),
    fillOldCoupons,
  );
};

