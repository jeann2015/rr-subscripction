import { LogDB } from '../Log/plugin';

const db = require('../../db/connection');

const logSaved = (orderId) => {
  const logInfo = {
    user: null,
    admin: null,
    type: 'info',
    message: `Box detail saved for order ${orderId}`,
  };
  LogDB(logInfo);
};

export const saveBoxDetail = (data, orderId) => {
  const dataToSave = data.boxDetail.map(meal => ({
    orderId,
    mealSku: meal.mealSku,
    mealQuantity: meal.quantity,
  }));

  db.BoxDetailHistory.bulkCreate(dataToSave)
    .then(() => logSaved(orderId))
    .catch(error => console.log(`saveBoxDetail() Error: cant save box detail in DB for order ${orderId} -> ${error}`));
};
