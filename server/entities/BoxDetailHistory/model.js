export default (sequelize, DataTypes) => {
  return sequelize.define('box_detail_history', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    orderId: {
      type: DataTypes.INTEGER,
      field: 'order_id',
    },
    mealSku: {
      type: DataTypes.STRING,
      field: 'meal_sku',
      allowNull: false,
    },
    mealQuantity: {
      type: DataTypes.INTEGER,
      field: 'meal_quantity',
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
    underscored: true,
  });
};
