import { LogDB } from '../Log/plugin';

const db = require('../../db/connection');

const { formatUpdatedData, updateIntegration } = require('../../utils');

const Child = db.Child;


export const getChilds = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const limit = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};
  if (req.query.userId) {
    clause.userId = req.query.userId;
  }
  Child.findAll({
    order: [
      [sort, order],
    ],
    limit: Number(limit),
    offset: Number(offset),
    where: clause,
  })
    .then((childs) => {
      res.set('X-Total-Count', childs.length);
      res.status(200).send(childs);
    })
    .catch(err => next(err));
};

// get user by id
export const getChildById = (req, res, next) => {
  Child.find({
    where: {
      id: req.params.id,
    },
  })
    .then((child) => {
      if (!child) {
        res.status(404).send('Child not found');
        return;
      }
      res.status(200).send(child);
    })
    .catch(err => next(err));
};


export const updateChild = async (req, res, next) => {
  const dataToUpdate = {
    name: req.body.name,
    birthdate: req.body.birthdate,
    food: req.body.food,
  };
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  try {
    const child = await Child.findById(req.body.id);
    const childUpdated = await child.update(dataToUpdate);
    const formatedData = formatUpdatedData(childUpdated.get('dataChanged'));
    logInfo.user = child.get('userId');
    logInfo.type = 'info';
    logInfo.message = `#(${req.user.id})~${req.user.name} update the child #(${child.get('id')})~${JSON.stringify(formatedData)}`;
    LogDB(logInfo);
    const integrationChildren = await updateIntegration(child.get('userId'));
    res.status(200).send({ success: true });
  } catch (err) {
    logInfo.type = 'error';
    logInfo.message = `Database error in updateChild(update child) -> Name:${err.name} ~ Message: ${err.message}`;
    LogDB(logInfo);
    return next(err);
  }
};

export const deleteAllChilds = async (user) => {
  await Promise.all(user.children.map(async (child) => {
    await child.destroy();
  }));
};
