export default (sequelize, DataTypes) => {
  return sequelize.define('child', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      allowNull: false,
    },
    name: DataTypes.STRING,
    gender: DataTypes.STRING,
    food: DataTypes.STRING,
    allergies: DataTypes.STRING,
    birthdate: DataTypes.DATE,
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
