import passport from 'passport';

import { getChilds, getChildById, updateChild } from './controller';

export default (router) => {
  router.get('/childs',
    passport.authenticate('jwt', { session: false }),
    getChilds,
  );

  router.get('/childs/:id', passport.authenticate('jwt', { session: false }),
    getChildById,
  );
  router.put('/childs/:id', passport.authenticate('jwt', { session: false }),
    updateChild,
  );
};
