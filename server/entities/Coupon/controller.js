import { LogDB } from '../Log/plugin';
import { validCoupon } from './plugin';
import { formatUpdatedData } from '../../utils';

const db = require('../../db/connection');

const Coupon = db.Coupon;
const Log = db.Log;

const sequelize = db.sequelize;


export const getCoupons = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;
  let clause = {};
  let incl = {};
  let limit = { limit: Number(end) - Number(offset) };
  if (req.query.code) {
    clause.code = {
      $like: `%${req.query.code}%`,
    };
  }

  if (req.query.userId) {
    incl = { include: [{
      model: sequelize.models.subscription,
      as: 'subscriptions',
    }] };

    clause = { '$subscriptions.user_id$': req.query.userId };
    limit = '';
  }

  Coupon.findAndCountAll({
    ...incl,
    where: clause,
    order: [[sort, order]],
    ...limit,
    offset: Number(offset),
  })
    .then((coupons) => {
      coupons.rows = coupons.rows.map((c) => {
        c.dataValues.type = (c.percentOff === null) ? 'amountOff' : 'percentOff';
        if (c.dataValues.amountOff !== null) {
          c.dataValues.amountOff /= 100;
          c.dataValues.amountOff = c.dataValues.amountOff.toFixed(2);
        }
        return c;
      });
      res.set('X-Total-Count', coupons.count);
      res.status(200).send(coupons.rows);
    })
    .catch(err => next(err));
};

export const getCouponById = (req, res, next) => {
  Coupon.find({
    where: {
      id: req.params.id,
    },
  })
    .then((coupon) => {
      if (!coupon) {
        return res.status(404).send('coupon not found');
      }
      coupon.dataValues.type = (coupon.dataValues.percentOff === null) ? 'amountOff' : 'percentOff';
      if (coupon.dataValues.amountOff !== null) {
        coupon.dataValues.amountOff /= 100;
        coupon.dataValues.amountOff = coupon.dataValues.amountOff.toFixed(2);
      }
      if (coupon.dataValues.productId === null) coupon.dataValues.productId = 'both';
      res.status(200).send(coupon);
    })
    .catch(err => next(err));
};

/**
 * Validates coupon
 */

export const validateCoupon = async function (req, res, next) {
  req.assert('couponCode', 'Coupon code is required').notEmpty();
  try {
    const validationResult = await req.getValidationResult();
    const { couponCode } = req.body;

    if (validationResult) {
      // TODO: What if no validation?
      const errors = validationResult.array();

      if (errors.length > 0) {
        return res.status(400).send(errors);
      }

      const coupon = await validCoupon(couponCode);
      if (coupon.expired) {
        return res.status(409).send('Expired coupon');
      }

      if (coupon.data) {
        return res.status(200).send(coupon.data);
      }

      return res.status(404).send('Coupon does not exist');
    }
  } catch (error) {
    return next(error);
  }
};

export const createCoupon = (req, res, next) => {
  const timesUsable = (req.body.duration === 'custom') ? req.body.timesUsable : -1;
  let amountOff = null;
  let percentOff = null;
  console.log(req.body);
  if (req.body.type === 'amountOff') {
    amountOff = req.body.amountOff * 100;
  }

  if (req.body.type === 'percentOff') {
    percentOff = req.body.percentOff;
  }
  const couponPayload = {
    code: req.body.code,
    amountOff,
    percentOff,
    maxRedemptions: req.body.maxRedemptions,
    timesRedeemed: req.body.timesRedeemed,
    duration: req.body.duration,
    productId: (req.body.productId !== 'both') ? req.body.productId : null,
    valid: true,
    timesUsable,
    boxList: req.body.rules,
  };
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  Coupon.findOne({
    where: {
      code: req.body.code,
    },
  })
    .then((coupon) => {
      if (coupon) {
        return res.status('500').send({ message: 'Coupon exists', error: false });
      }
      Coupon.create(couponPayload)
        .then((c) => {
          logInfo.type = 'info';
          logInfo.message = `#(${req.user.id})~${req.user.name} create a coupon -> #(${c.get('id')})~${req.body.code}`;
          LogDB(logInfo);
          res.status(200).send(c);
        })
        .catch((err) => {
          logInfo.type = 'error';
          logInfo.message = `Database error in createCoupon(create coupon) -> Name:${err.name} ~ Message: ${err.message}`;
          LogDB(logInfo);
        });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in createCoupon(find one coupon) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      next(err);
    });
};

export const updateCoupon = async (req, res) => {
  const timesUsable = (req.body.duration === 'custom') ? req.body.timesUsable : -1;
  let amountOff = null;
  let percentOff = null;
  if (req.body.type === 'percentOff') {
    percentOff = req.body.percentOff;
  } else {
    amountOff = req.body.amountOff * 100;
  }
  const dataToUpdate = {
    amountOff,
    percentOff,
    maxRedemptions: req.body.maxRedemptions,
    timesRedeemed: req.body.timesRedeemed,
    valid: req.body.valid,
    duration: req.body.duration,
    productId: (req.body.productId !== 'both') ? req.body.productId : null,
    timesUsable,
  };
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  try {
    const coupon = await Coupon.findById(req.body.id);
    const couponUpdated = await coupon.update(dataToUpdate);
    const formatedData = formatUpdatedData(couponUpdated.get('dataChanged'));
    logInfo.type = 'info';
    logInfo.message = `#(${req.user.id})~${req.user.name} update a coupon -> #(${coupon.get('id')})~${JSON.stringify(formatedData)}`;
    LogDB(logInfo);
    res.status(200).json({ success: true });
  } catch (err) {
    logInfo.type = 'error';
    logInfo.message = `Database error in updateCoupon(update coupon) -> Name:${err.name} ~ Message: ${err.message}`;
    LogDB(logInfo);
  }
};

export const deleteCoupon = async (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  const couponToDestroy = await Coupon.findById(req.params.id);
  Coupon.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      logInfo.type = 'info';
      logInfo.message = `#(${req.user.id})~${req.user.name} delete a coupon -> #(${req.params.id})~${couponToDestroy.get('code')}`;
      LogDB(logInfo);
      res.status(200).send({ status: 'ok' });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in deleteCoupon(delete coupon) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      next(err);
    });
};

export const getCouponLogs = async (req, res, next) => {
  // const sort = (req.query._sort) ? req.query._sort : 'id';
  // const order = (req.query._order) ? req.query._order : 'DESC';
  // const end = (req.query._end) ? req.query._end : 10;
  // const offset = (req.query._start) ? req.query._start : 0;

  // Find coupon data (ID, Code)
  const coupon = await Coupon.find({
    where: {
      id: req.query.couponId,
    },
  })
  .then((obj) => {
    const data = obj.get({ plain: true });
    const couponData = { id: data.id.toString(), code: data.code };
    return couponData;
  });

  // Get logs for coupon
  const logs = await Log.findAll({
    where: {
      $or: [
        { message: { $like: `%${coupon.code}%` } },
        {
          $and: [
            { message: { $like: '%coupon%' } },
            { message: { $like: `%${coupon.id}%` } },
          ],
        },
      ],
    },
  });

  // Add coupon ID to each log
  const logsWithCouponId = logs.map((coupon) => {
    const couponData = coupon.get({ plain: true });
    couponData.couponId = req.query.couponId;
    return couponData;
  });

  res.set('X-Total-Count', logsWithCouponId.length);
  res.status(200).send(logsWithCouponId);
};
