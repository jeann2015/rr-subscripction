export default (sequelize, DataTypes) => {
  return sequelize.define('coupon', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    productId: {
      type: DataTypes.INTEGER,
      field: 'product_id',
    },
    code: {
      type: DataTypes.STRING,
      indexName: 'code_index',
      indicesType: 'UNIQUE',
    },
    percentOff: {
      type: DataTypes.INTEGER,
      field: 'percent_off',
    },
    amountOff: {
      type: DataTypes.INTEGER,
      field: 'amount_off',
    },
    maxRedemptions: {
      type: DataTypes.INTEGER,
      field: 'max_redemptions',
      allowNull: false,
      defaultValue: 1,
    },
    timesRedeemed: {
      type: DataTypes.INTEGER,
      field: 'times_redeemed',
      allowNull: false,
      defaultValue: 0,
    },
    duration: DataTypes.ENUM('once', 'forever', 'custom', 'custom_boxes'),
    valid: DataTypes.BOOLEAN,
    timesUsable: {
      type: DataTypes.INTEGER,
      field: 'times_usable',
      defaultValue: -1,
    },
    boxList: {
      type: DataTypes.JSON,
      field: 'box_list',
      allowNull: true,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
