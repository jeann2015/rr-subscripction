import Stripe from 'stripe';

import config from '../../config/config';
import { LogDB } from '../Log/plugin';

const db = require('../../db/connection');

const Product = db.Product;
const Coupon = db.Coupon;

const stripe = Stripe(config.stripeApiKey);

const logInfo = {};

const getCoupons = (limit = 100, startingAfter) => {
  return new Promise((resolve) => {
    stripe.coupons.list({
      limit,
      startingAfter,
    }, (err, coupons) => {
      console.log(err);
      const amount = coupons.data.length;
      eachLimit(coupons.data, 10, (coupon, cb) => {
        if (coupon.duration !== 'once') {
          return cb();
        }
        if (!coupon.max_redemptions) {
          coupon.max_redemptions = 9999;
        }

        Coupon.findOrCreate({
          where: {
            code: coupon.id,
          },
          defaults: {
            percentOff: coupon.percent_off,
            amountOff: coupon.amount_off,
            maxRedemptions: coupon.max_redemptions,
            timesRedeemed: coupon.times_redeemed,
            valid: coupon.valid,
            product_id: coupon.product_id,
          },
        }).then(() => {
          cb();
        })
          .catch(error => console.log(error));
      }, () => {
        const last = coupons.data[amount - 1];
        resolve({ amount, last });
      });
    });
  });
};

// Start it with migrateAll(100)
export const migrateAll = (limit, startingAfter) => {
  getCoupons(limit, startingAfter).then(({ amount, last }) => {
    console.log(last);
    console.log('Finished with', amount);
    if (amount === 100) {
      migrateAll(limit, last.id);
    }
  });
};

export const getInfoAboutCoupon = (coupon, charge) => {
  const valid = coupon.get('valid');
  const percentOff = coupon.get('percentOff');
  const amountOff = coupon.get('amountOff');
  const maxRedemptions = coupon.get('maxRedemptions');
  const timesRedeemed = coupon.get('timesRedeemed');
  const code = coupon.get('code');
  if (valid && (maxRedemptions > timesRedeemed)) {
    if (percentOff) {
      const totalDiscount = charge.finalAmount * (percentOff * 0.01);
      charge.discount = `${percentOff}%`;
      charge.finalAmount -= totalDiscount;
    }
    if (amountOff) {
      charge.discount = amountOff;
      charge.finalAmount -= amountOff;
    }
  }
  charge.valid = valid;
  charge.code = code;
  return charge;
};

export const calcCouponDiscount = (percentOff, amountOff, subscriptionPrice) => {
  if (percentOff === 0 && amountOff === 0) {
    return subscriptionPrice;
  }
  let finalAmount = subscriptionPrice;
  let totalDiscount;
  if (percentOff) {
    totalDiscount = subscriptionPrice * (percentOff * 0.01);
  }
  if (amountOff) {
    totalDiscount = amountOff;
  }
  finalAmount -= totalDiscount;
  return Math.floor(finalAmount);
};

export const redeemCoupon = (coupon, user) => {
  coupon.increment({
    timesRedeemed: 1,
  });
  const plainUser = user.get({ plain: true });

  // Log coupon use for custom ones
  if (coupon.get('duration') === 'custom') {
    logInfo.user = plainUser.id;
    logInfo.type = 'info';
    logInfo.message = `Coupon ${coupon.get('code')} used ${plainUser.subscriptions[0].couponUseCount + 1} time out of ${coupon.get('timesUsable')}`;
    LogDB(logInfo);
  }

  return coupon.save();
};

export const findCouponByCode = (code) => {
  return Coupon.findOne({
    where: {
      code,
    },
    include: [Product],
  });
};

export const validCoupon = async function (code) {
  const couponStatus = {
    data: null,
    expired: false,
    notExist: false,
  };
  try {
    const coupon = await findCouponByCode(code);
    if (!coupon) {
      couponStatus.notExist = true;
      return couponStatus;
    }
    const valid = coupon.get('valid');
    const maxRedemptions = coupon.get('maxRedemptions');
    const timesRedeemed = coupon.get('timesRedeemed');
    if (valid && (maxRedemptions > timesRedeemed)) {
      couponStatus.data = coupon;
      return couponStatus;
    }
    couponStatus.expired = true;
    return couponStatus;
  } catch (error) {
    return error;
  }
};

export const setCouponToSubscription = async (body) => {
  const userToReturn = {
    data: null,
    error: null,
  };
  if (!body.couponCode) {
    userToReturn.data = body;
    return userToReturn;
  }
  const coupon = await validCoupon(body.couponCode);
  if (coupon.expired) {
    userToReturn.error = 'Coupon expired';
    return userToReturn;
  }
  if (coupon.notExist) {
    userToReturn.error = 'Coupon not exist';
    return userToReturn;
  }
  const subscriptionProduct = body.subscription.product.find((s) => {
    return s.productId === coupon.data.productId;
  });
  if (subscriptionProduct) {
    body.subscription.product.forEach((pr) => {
      if (pr.productId === subscriptionProduct.productId) {
        pr.couponId = coupon.data.id;
      }
    });
    userToReturn.data = body;
    return userToReturn;
  }
  body.subscription.couponId = coupon.data.id;
  userToReturn.data = body;
  return userToReturn;
};
