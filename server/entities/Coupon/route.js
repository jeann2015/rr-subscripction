import passport from 'passport';
import { getCoupons, getCouponById, validateCoupon, updateCoupon, createCoupon, deleteCoupon, getCouponLogs } from './controller';

export default (router) => {
  router.get('/coupons',
    passport.authenticate('jwt', { session: false }),
    getCoupons,
  );
  router.get('/coupons/:id',
    passport.authenticate('jwt', { session: false }),
    getCouponById,
  );
  router.post('/coupons',
    passport.authenticate('jwt', { session: false }),
    createCoupon,
  );
  router.put('/coupons/:id',
    passport.authenticate('jwt', { session: false }),
    updateCoupon,
  );
  router.post('/coupons/valid',
    validateCoupon,
  );
  router.delete('/coupons/:id',
    passport.authenticate('jwt', { session: false }),
    deleteCoupon,
  );
  router.get('/coupons-logs',
    passport.authenticate('jwt', { session: false }),
    getCouponLogs,
  );
};
