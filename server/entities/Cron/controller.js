import moment from 'moment';
import path from 'path';
import fs from 'fs';
import zipcodes from 'zipcodes';
import csv from 'csvtojson';

import { paginate } from './plugin';
import { simulateCronAndGenerateCSV } from '../../services/cron_simulator';
import config from '../../config/config';
import { add3PLtoOrders } from '../../utils';
import { getCronDate } from '../../services/cron_new';
import { getSubscriptionsAndRunOrders } from '../../services/cron_runner';
import { addDiscountsToOrders } from '../AppliedDiscounts/controller';

const db = require('../../db/connection');

const User = db.User;
const Cron = db.Cron;
const Order = db.Order;

// NODE_ENV IN THE FUTURE.
export const zeroDay = moment.utc(config.zeroDay, 'YYYYMMD');
zeroDay.set({
  hour: 16, // 9 SAN FRANCISCO > 16 UTC
  minutes: 0,
});

export const dayWhenRepeat = config.dayWhenRepeat;
// =)
export const timeToNextCrons = (req, res) => {
  if (req.query.zipCode) {
    const prenationalFirstDate = moment('2018/01/08 03:00:00', 'YYYY/MM/DD hh:mm:ss');
    const prenationalSecondDate = moment('2018/01/22 03:00:00', 'YYYY/MM/DD hh:mm:ss');
    const prenationalFromDate = moment('2017/12/12', 'YYYY/MM/DD');
    const prenationalToDate = moment('2017/12/26', 'YYYY/MM/DD');
    const now = moment();

    if (/* now >= prenationalFromDate &&  */now <= prenationalToDate) {
      const { zipCode } = req.query;
      const data = zipcodes.lookup(zipCode);

      if (data) {
        const validStates = ['CA', 'OR', 'WA', 'NV', 'AZ', 'DC'];
        if (validStates.indexOf(data.state) === -1) {
          res.status(200).send({
            firstDate: prenationalFirstDate,
            secondDate: prenationalSecondDate,
          });
          return;
        }
      }
    }
  }

  const now = moment.utc();
  const diff = now.diff(zeroDay, 'days');

  const diffRemainder = diff % dayWhenRepeat;
  const cyclesPassed = (diff - diffRemainder) / dayWhenRepeat;

  const daysFirstCycle = (cyclesPassed + 1) * dayWhenRepeat;
  const firstShippingDate = zeroDay.clone().add(daysFirstCycle, 'days');

  const daysSecondCycle = (cyclesPassed + 2) * dayWhenRepeat;
  const secondShippingDate = zeroDay.clone().add(daysSecondCycle, 'days');

  res.status(200).send({
    firstDate: firstShippingDate,
    secondDate: secondShippingDate,
  });
};

export const dateNextCrons = (req, res, next) => {
  const TOTAL_TIMES = 10;
  const times = [];
  let i = 0;

  const now = moment();
  const diff = now.diff(zeroDay, 'days');
  const diffRemainder = diff % dayWhenRepeat;
  if (diffRemainder === 0) i = -1;

  while (i <= TOTAL_TIMES) {
    const shippingDate = getCronDate(i);
    times.push({ id: shippingDate.startOf('day').utc().toISOString(), name: `${shippingDate.format('MM/DD/YYYY')}` });
    i++;
  }

  res.set('X-Total-Count', TOTAL_TIMES);
  res.status(200).send(times);
};


export const getLastCron = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;
  const clause = {};
  if (req.query.userId) {
    clause.id = req.query.userId;
  }
  Cron.findAll({
    order: [
      ['createdAt', 'DESC'],
    ],
    end: 1,
  })
    .then((cron) => {
      Order.findAndCountAll({
        include: [{
          model: User,
          where: clause,
        }],
        order: [[sort, order]],
        limit: Number(end) - Number(offset),
        offset: Number(offset),
        where: {
          cron_id: cron[0].get('id'),
        },
      }).then(async (iorder) => {
        res.set('X-Total-Count', iorder.count);
        const plainOrders = await add3PLtoOrders(iorder.rows.map(o => o.get({ plain: true })));
        const withDiscounts = await addDiscountsToOrders(plainOrders);
        res.status(200).send(withDiscounts);
      }, (err) => {
        next(err);
      });
    })
    .catch(err => next(err));
};


export const simulateCronCustomDate = (req, res, next) => {
  let simulationDate = getCronDate(0).startOf('day');
  if (req.params.date !== 'undefined') {
    simulationDate = moment(decodeURIComponent(req.params.date), 'MM/DD/YYYY').startOf('day');
  }

  simulateCronAndGenerateCSV(simulationDate, req, res, next);
};

export const downloadSimulatedOrders = async (req, res, next) => {
  let simulationDate = getCronDate(0).format('MM-DD-YYYY');
  if (req.params.date !== 'undefined') {
    simulationDate = moment(decodeURIComponent(req.params.date), 'MM/DD/YYYY').format('MM-DD-YYYY');
  }

  try {
    const csvFileName = `simulatedOrders${simulationDate}.csv`;

    if (fs.existsSync(csvFileName)) {
      const file = path.join(csvFileName);
      res.download(file);
    }
  } catch (error) {
    return next(error);
  }
};

export const getSimulatedOrders = async (req, res, next) => {
  const now = moment();
  const diff = now.diff(zeroDay, 'days');
  const diffRemainder = diff % dayWhenRepeat;
  const indexCron = (diffRemainder === 0) ? -1 : 0;
  let simulationDate = getCronDate(indexCron).format('MM-DD-YYYY');
  if (req.query.date !== undefined) {
    simulationDate = moment(decodeURIComponent(req.query.date), 'MM/DD/YYYY').format('MM-DD-YYYY');
  }


  try {
    const end = (req.query._end) ? req.query._end : 10;
    const offset = (req.query._start) ? req.query._start : 0;

    let orders = [];

    const csvFileName = `simulatedOrders${simulationDate}.csv`;

    if (fs.existsSync(csvFileName)) {
      const headers = ['id', 'userId', 'boxType', 'boxNumber', 'productPrice', '_3pl', 'ship_time', 'email', 'name', 'lastName', 'address', 'addressExtra', 'city', 'state', 'zipCode'];
      orders = await csv({ headers }).fromFile(csvFileName);
    }

    const ordersPaginated = paginate(orders, offset, end);
    res.set('X-Total-Count', orders.length);
    res.status(200).send(ordersPaginated);
  } catch (error) {
    return next(error);
  }
};

export const runAuthCronFallback = (req, res, next) => {
  getSubscriptionsAndRunOrders(true);
  res.status(200).send('Manually executed Auth Cron');
};

export const runBillingCronFallback = (req, res, next) => {
  getSubscriptionsAndRunOrders(false);
  res.status(200).send('Manually executed Billing Cron');
};
