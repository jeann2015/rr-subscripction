import moment from 'moment';
import eachSeries from 'async/each';
import path from 'path';

import config from '../../config/config';

import { PENDING, ACTIVE, PAUSED, CANCELED } from '../../services/status';
import { calcCouponDiscount } from '../Coupon/plugin';

import { exportCsv } from '../Order/plugin';

const db = require('../../db/connection');

const Cron = db.Cron;
const Subscription = db.Subscription;
const User = db.User;
const Referral = db.Referral;

const sequelize = db.sequelize;

export const simulateCreateFirstOrders = (orders, daysToMove) => {
  return new Promise((resolve, reject) => {
    const daySimulationCron = moment().utc().add(daysToMove, 'days').set({
      hour: 16,
      minute: 0,
      second: 0,
      millisecond: 0,
    });

    const query = `
      SELECT DISTINCT s.id, s.status, DATE_SUB(\`start_from\`, INTERVAL ${daysToMove} DAY) as start_from, DATE_SUB(\`paused_until\`, INTERVAL ${daysToMove} DAY) as paused_until, s.user_id, s.amount, sk.date AS next_skip_date,
            (SELECT COUNT(*) FROM \`order\`
              WHERE \`subscription_id\` = s.id) AS orders,
              p.\`box_type\`, p.\`price\`,
              c.\`percent_off\`,c.\`amount_off\`,c.\`duration\`,c.\`code\`,
              u.customer_id,u.name,u.last_name,u.email,u.address,u.address_extra,u.city,u.state,u.zip_code, u.\`has_mealmaker\`, u.\`delivered_mealmaker\`, u.\`first_charge\`
      FROM \`subscription\` AS s
      LEFT OUTER JOIN \`product\` AS p ON s.\`product_id\` = p.\`id\`
      LEFT OUTER JOIN \`user\` AS u ON s.\`user_id\` = u.\`id\`
      LEFT OUTER JOIN \`coupon\` AS c ON s.\`coupon_id\` = c.\`id\`
      LEFT JOIN \`skip\` sk ON sk.\`subscription\` = s.\`id\` AND sk.\`date\` = (
        SELECT MIN(sk.\`date\`)
        FROM skip sk
        WHERE sk.\`subscription\` = s.\`id\` AND sk.\`date\` >= '${daySimulationCron.format('YYYY-MM-DD')}'
      )
      HAVING orders = 0
    `;
    const opts = {
      model: Subscription,
    };
    const results = [];
    const dataToReturn = {
      subscriptionsNotExecuted: [],
      subscriptionsExecuted: [],
      createdBy: 'Create First',
    };
    let countSubscriptionsNotExecuted = 0;
    let countSubscriptionsExecuted = 0;

    sequelize.query(query, opts)
      .then(async (subscriptions) => {
        const lastCron = await Cron.findOne({
          order: [['id', 'DESC']],
        });
        const cronId = lastCron.get('id');
        const now = moment.utc();
        const justSentMealMaker = [];
        const groupUsersById = subscriptions.reduce((obj, sub) => {
          obj[sub.get('user_id')] = obj[sub.get('user_id')] || [];
          obj[sub.get('user_id')].push(sub);
          return obj;
        }, {});
        eachSeries(subscriptions, async (subscription, cb) => {
          const subscriptionId = subscription.get('id');
          const status = subscription.get('status');
          const startFrom = subscription.get('start_from');
          const pausedUntil = subscription.get('paused_until');
          const subscriptionDuration = subscription.get('duration');
          const discountAmountOff = subscription.get('amount_off');
          const discountPercentOff = subscription.get('percent_off');
          const userId = subscription.get('user_id');

          const nextSkipDate = (subscription.dataValues.next_skip_date === null) ? null : moment.utc(subscription.dataValues.next_skip_date).set({
            hour: 16,
            minute: 0,
            second: 0,
            millisecond: 0,
          });

          if (subscription.dataValues.next_skip_date) {
            if (status === ACTIVE && nextSkipDate && nextSkipDate.isSame(daySimulationCron, 'day')) {
              const subscriptionNotExecuted = {
                id: subscription.id,
                status: `Date ${nextSkipDate.format('YYYY-MM-DD')} was found on the skip table`,
              };
              dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
              countSubscriptionsNotExecuted += 1;
              return cb();
            }
          }

          if (status === PENDING || status === CANCELED || (status === PAUSED && !pausedUntil)) {
            const subscriptionNotExecuted = {
              id: subscription.id,
              status,
            };
            dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
            countSubscriptionsNotExecuted += 1;
            return cb();
          }

          if (status === PAUSED && pausedUntil) {
            const dateToRestart = moment.utc(pausedUntil);
            const diffPausedUntil = Math.round(now.diff(dateToRestart, 'days', true));
            if (diffPausedUntil < 0) {
              const subscriptionNotExecuted = {
                id: subscription.id,
                status: `Paused until ${dateToRestart.format('YYYY-MM-DD')}`,
              };
              dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
              countSubscriptionsNotExecuted += 1;
              return cb();
            }
          }

          if (startFrom) {
            const dateToStart = moment.utc(startFrom).set({
              hour: 16,
              minute: 0,
              second: 0,
              millisecond: 0,
            });
            const diffStart = Math.round(now.diff(dateToStart, 'days', true));
            if (diffStart < 0) {
              const subscriptionNotExecuted = {
                id: subscription.id,
                status: `Start from ${dateToStart.format('YYYY-MM-DD')}`,
              };
              dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
              countSubscriptionsNotExecuted += 1;

              return cb();
            }
          }
          const subscriptionExecuted = {
            id: subscription.id,
            ordersCreated: [],
            info: {
              coupon: {
                discount: {},
              },
              referral: {},
              charge: {},
              startFrom: '',
              hasMealMaker: subscription.get('has_mealmaker'),
              deliveredMealMaker: subscription.get('delivered_mealmaker'),
            },
          };
          countSubscriptionsExecuted += 1;
          const firstCharge = subscription.get('first_charge');
          const hasMealMaker = subscription.get('has_mealmaker');
          const deliveredMealMaker = subscription.get('delivered_mealmaker');
          const amountOfOrders = subscription.get('orders');
          const userUpdated = {
            deliveredMealMaker,
            firstCharge,
          };
          if (hasMealMaker && !userUpdated.deliveredMealMaker && justSentMealMaker.indexOf(userId) === -1) {
            justSentMealMaker.push(userId);
            userUpdated.deliveredMealMaker = true;
            const newOrder = {
              userId,
              cronId,
              chargeToken: null,
              name: subscription.get('name'),
              lastName: subscription.get('last_name'),
              address: subscription.get('address'),
              addressExtra: subscription.get('address_extra'),
              email: subscription.get('email'),
              city: subscription.get('city'),
              state: subscription.get('state'),
              zipCode: subscription.get('zip_code'),
              boxType: 'Meal Maker',
              productPrice: 0,
            };
            subscriptionExecuted.ordersCreated.push(newOrder);
            orders.push(newOrder);
          }

          let totalPrice = subscription.get('price') * subscription.get('amount');
          if (!userUpdated.firstCharge) {
            if (discountPercentOff || discountAmountOff) {
              totalPrice = calcCouponDiscount(discountPercentOff, discountAmountOff, totalPrice);
              subscriptionExecuted.info.coupon.code = subscription.get('code');
              subscriptionExecuted.info.coupon.discount.type = (discountPercentOff) ? 'percent' : 'amount';
              subscriptionExecuted.info.coupon.discount.value = discountPercentOff || discountAmountOff;
              subscriptionExecuted.info.coupon.discount.total = totalPrice;
            }
            for (let i = 0; i < subscription.get('amount'); i++) {
              const charge = {
                userId,
                cronId,
                customerId: subscription.get('customer_id'),
                subscriptionId,
                chargeToken: firstCharge || null,
                name: subscription.get('name'),
                lastName: subscription.get('last_name'),
                address: subscription.get('address'),
                addressExtra: subscription.get('address_extra'),
                email: subscription.get('email'),
                city: subscription.get('city'),
                state: subscription.get('state'),
                zipCode: subscription.get('zip_code'),
                boxType: subscription.get('box_type'),
                productPrice: totalPrice / subscription.amount,
              };
              subscriptionExecuted.info.charge = charge;
              orders.push(charge);
            }
          }

          if (userUpdated.firstCharge) {
            userUpdated.firstCharge = null;
            if (discountPercentOff || discountAmountOff) {
              totalPrice = calcCouponDiscount(discountPercentOff, discountAmountOff, totalPrice);
              subscriptionExecuted.info.coupon.code = subscription.get('code');
              subscriptionExecuted.info.coupon.discount.type = (discountPercentOff) ? 'percent' : 'amount';
              subscriptionExecuted.info.coupon.discount.value = discountPercentOff || discountAmountOff;
              subscriptionExecuted.info.coupon.duration = subscriptionDuration;
              subscriptionExecuted.info.coupon.discount.total = totalPrice;
            }
            const isReferral = await Referral.findOne({
              where: {
                me: userId,
              },
            });

            if (isReferral) {
              totalPrice -= config.referralFirstDiscount / groupUsersById[userId].length;
              subscriptionExecuted.info.referral.credit = totalPrice;
            }

            const boxType = 'First Box';
            for (let i = 0; i < subscription.get('amount'); i++) {
              // Create new order for the selected product
              const newOrder = {
                userId,
                cronId,
                subscriptionId,
                chargeToken: firstCharge || null,
                name: subscription.get('name'),
                lastName: subscription.get('last_name'),
                address: subscription.get('address'),
                addressExtra: subscription.get('address_extra'),
                email: subscription.get('email'),
                city: subscription.get('city'),
                state: subscription.get('state'),
                zipCode: subscription.get('zip_code'),
                boxType: boxType,
                productPrice: totalPrice / subscription.amount,
              };
              subscriptionExecuted.ordersCreated.push(newOrder);
              orders.push(newOrder);
            }
          }
          dataToReturn.subscriptionsExecuted.push(subscriptionExecuted);
          return cb();
        }, (err) => {
          if (err) {
            reject(err);
          }
          results.push(dataToReturn);
          resolve({ results: {
            subscriptions: results,
            subscriptionsExecuted: countSubscriptionsExecuted,
            subscriptionsNotExecuted: countSubscriptionsNotExecuted,
          } });
        });
      });
  });
};

export const simulateGetLatestOrders = (orders, daysToMove) => {
  return new Promise((resolve, reject) => {
    const allOrders = [];
    const daySimulationCron = moment().utc().add(daysToMove, 'days').set({
      hour: 16,
      minute: 0,
      second: 0,
      millisecond: 0,
    });
    const query = `
      SELECT DISTINCT s.id, s.status, DATE_SUB(\`start_from\`, INTERVAL ${daysToMove} DAY) as start_from, DATE_SUB(\`paused_until\`, INTERVAL ${daysToMove} DAY) as paused_until, s.user_id, s.amount, s.next_discount, s.type_next_discount, sk.date AS next_skip_date,
              (SELECT DATE_SUB(\`created_at\`, INTERVAL ${daysToMove} DAY) FROM \`order\`
                WHERE \`subscription_id\` = s.id
                ORDER BY created_at DESC LIMIT 1) AS lastOrder,
                p.\`box_type\`, p.\`price\`, p.\`days\`,
                c.\`percent_off\`,c.\`amount_off\`,c.\`code\`,
                u.customer_id,u.name,u.last_name,u.email,u.address,u.address_extra,u.city,u.state,u.zip_code, u.\`has_mealmaker\`, u.\`delivered_mealmaker\`, u.\`first_charge\`
        FROM \`subscription\` AS s
        LEFT OUTER JOIN \`product\` AS p ON s.\`product_id\` = p.\`id\`
        LEFT OUTER JOIN \`user\` AS u ON s.\`user_id\` = u.\`id\`
        LEFT OUTER JOIN \`coupon\` AS c ON s.\`coupon_id\` = c.\`id\`
        LEFT JOIN \`skip\` sk ON sk.\`subscription\` = s.\`id\` AND sk.\`date\` = (
          SELECT MIN(sk.\`date\`)
          FROM skip sk
          WHERE sk.\`subscription\` = s.\`id\` AND sk.\`date\` >= CURDATE()
        )
    `;
    const opts = {
      model: Subscription,
    };
    const results = [];
    const dataToReturn = {
      subscriptionsNotExecuted: [],
      subscriptionsExecuted: [],
      createdBy: 'Get Latest',
    };
    let countSubscriptionsExecuted = 0;
    let countSubscriptionsNotExecuted = 0;
    sequelize.query(query, opts).then((subscriptions) => {
      const now = moment.utc();
      eachSeries(subscriptions, async (sub, cb) => {
        const subscription = sub.get({ plain: true });
        const orderCreated = moment.utc(subscription.lastOrder);
        const diff = Math.round(now.diff(orderCreated, 'days', true));
        const subscriptionId = subscription.id;
        const status = subscription.status;
        const startFrom = subscription.start_from;
        const pausedUntil = subscription.paused_until;
        let totalPrice = subscription.price * subscription.amount;
        const discountAmountOff = subscription.amount_off;
        const discountPercentOff = subscription.percent_off;
        const nextSkipDate = (subscription.next_skip_date === null) ? null : moment.utc(subscription.next_skip_date).set({
          hour: 16,
          minute: 0,
          second: 0,
          millisecond: 0,
        });

        if (orderCreated.isSame(moment.utc(), 'day')) {
          const subscriptionNotExecuted = {
            id: subscription.id,
            status,
            message: 'Last order created by cron on createFirstOrders',
          };
          dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
          countSubscriptionsNotExecuted += 1;
          return cb();
        }

        if (status === PENDING || status === CANCELED || (status === PAUSED && !pausedUntil)) {
          const subscriptionNotExecuted = {
            id: subscription.id,
            status,
          };
          dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
          countSubscriptionsNotExecuted += 1;

          return cb();
        }

        if (status === PAUSED && pausedUntil) {
          const dateToRestart = moment.utc(pausedUntil);
          const diffPausedUntil = Math.round(now.diff(dateToRestart, 'days', true));
          if (diffPausedUntil < 0) {
            const subscriptionNotExecuted = {
              id: subscription.id,
              status: `Paused until ${dateToRestart.format('YYYY-MM-DD')}`,
            };
            dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
            countSubscriptionsNotExecuted += 1;
            return cb();
          }
        }

        if (subscription.next_skip_date !== null) {
          if (status === ACTIVE && nextSkipDate.isSame(daySimulationCron, 'day')) {
            const subscriptionNotExecuted = {
              id: subscription.id,
              status: `Date ${nextSkipDate.format('YYYY-MM-DD')} was found on the skip table`,
            };
            dataToReturn.subscriptionsNotExecuted.push(subscriptionNotExecuted);
            countSubscriptionsNotExecuted += 1;
            return cb();
          }
        }

        const subscriptionExecuted = {
          id: subscription.id,
          totalPrice,
          info: {
            coupon: {
              discount: {},
            },
            referral: {
              credit: {},
            },
            charge: {},
            nextDiscount: {},
            leftTimeToExecute: 0,
          },
        };
        countSubscriptionsExecuted += 1;
        if (diff >= subscription.days) {
          if (discountPercentOff || discountAmountOff) {
            totalPrice = calcCouponDiscount(discountPercentOff, discountAmountOff, totalPrice);
            subscriptionExecuted.info.coupon.code = sub.get('code');
            subscriptionExecuted.info.coupon.discount.type = (discountPercentOff) ? 'percent' : 'amount';
            subscriptionExecuted.info.coupon.discount.value = discountPercentOff || discountAmountOff;
            subscriptionExecuted.info.coupon.discount.total = totalPrice;
          }
          if (subscription.next_discount) {
            switch (subscription.type_next_discount) {
              case 'percent':
                const discount = totalPrice * (subscription.next_discount * 0.01);
                totalPrice -= discount;
                subscriptionExecuted.info.nextDiscount.type = 'percent';
                break;
              case 'amount':
                totalPrice -= subscription.next_discount;
                subscriptionExecuted.info.nextDiscount.type = 'amount';
                break;
              default:
                break;
            }
            subscriptionExecuted.info.nextDiscount.discount = totalPrice;
          }

          // Get actual instance!!
          const user = await User.findById(subscription.user_id);
          if (user.get('credit') > 0) {
            let creditDiscount = 0;
            subscriptionExecuted.info.referral.credit.total = user.get('credit');
            if (totalPrice - user.get('credit') >= 0) {
              totalPrice -= user.get('credit');
            } else {
              creditDiscount = user.get('credit') - totalPrice;
              totalPrice = 0;
            }
            subscriptionExecuted.info.referral.userId = user.get('id');
            subscriptionExecuted.info.referral.credit.used = creditDiscount;
            subscriptionExecuted.info.referral.credit.toUse = (user.get('credit') - creditDiscount > 0) ? user.get('credit') - creditDiscount : 0;
          }

          const lastCron = await Cron.findOne({
            order: [['id', 'DESC']],
          });
          for (let i = 0; i < subscription.amount; i++) {
            const dataForCharge = {
              userId: subscription.user_id,
              customerId: subscription.customer_id,
              cronId: lastCron.get('id') + 1,
              subscriptionId: subscription.id,
              chargeToken: null,
              name: subscription.name,
              lastName: subscription.last_name,
              address: subscription.address,
              addressExtra: subscription.address_extra,
              email: subscription.email,
              city: subscription.city,
              state: subscription.state,
              zipCode: subscription.zip_code,
              boxType: subscription.box_type,
              productPrice: totalPrice / subscription.amount,
            };
            subscriptionExecuted.info.charge = dataForCharge;
            orders.push(dataForCharge);
          }
        }

        if ((subscription.days - diff) > 0) {
          subscriptionExecuted.info.leftTimeToExecute = subscription.days - diff;
        }
        dataToReturn.subscriptionsExecuted.push(subscriptionExecuted);
        return cb();
      }, (err) => {
        if (err) {
          reject(err);
        }
        results.push(dataToReturn);
        resolve({ results: {
          subscriptions: results,
          subscriptionsExecuted: countSubscriptionsExecuted,
          subscriptionsNotExecuted: countSubscriptionsNotExecuted,
        } });
      });
    });
  });
};

export const paginate = (array, page_size, page_number) => array.slice(page_size, page_number);

// npm run cronCustomDate --date=YYYY/MM/DD
export const simulateRunCronCustomDate = () => {
  const orders = [];

  const customDate = moment(new Date(process.env.npm_config_date)).set({
    hour: 16,
    minute: 0,
    second: 0,
    millisecond: 0,
  });

  const now = moment.utc().set({
    hour: 16,
    minute: 0,
    second: 0,
    millisecond: 0,
  });

  const daysToMove = customDate.diff(moment(now), 'days');

  simulateCreateFirstOrders(orders, daysToMove)
    .then(() => {
      simulateGetLatestOrders(orders, daysToMove)
        .then(() => {
          const fields = [{
            label: 'Plan',
            value: 'boxType',
            default: 'Meal Maker',
          }, 'productPrice', 'email', 'name', 'lastName', 'address', 'addressExtra', 'city', 'state', 'zipCode', 'trackId', 'userId'];
          const fieldNames = ['Plan', 'Product Price', 'Email', 'First Name', 'Last Name', 'Street 1', 'Street 2', 'City', 'State', 'Zipcode', 'Track Id', 'User Id'];
          const opts = {
            data: orders,
            fields,
            fieldNames,
          };
          exportCsv(opts, 'fakeOrders.csv', () => {
            const file = path.join('fakeOrders.csv');
            console.log('CSV File Generated');
          });
        });
    })
    .catch(err => next(err));
};
