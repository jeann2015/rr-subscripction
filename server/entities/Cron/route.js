import passport from 'passport';
import { getLastCron, timeToNextCrons, dateNextCrons, simulateCronCustomDate, getSimulatedOrders, downloadSimulatedOrders, runAuthCronFallback, runBillingCronFallback } from './controller';

export default (router) => {
  router.get('/crons', // Get orders last cron
    passport.authenticate('jwt', { session: false }),
    getLastCron,
  );

  router.get('/simulatedCrons', // -> Get data /crons-simulate?date=11%2F13%2F2017
    passport.authenticate('jwt', { session: false }),
    getSimulatedOrders,
  );

  router.get('/simulatedCrons/download/:date', // -> Download CSV
    passport.authenticate('jwt', { session: false }),
    downloadSimulatedOrders,
  );

  router.get('/simulatedCrons/runSimulation/:date', // -> Simulate /crons/simulate/10-30-2017
    passport.authenticate('jwt', { session: false }),
    simulateCronCustomDate,
  );

  router.get('/crons/time',
    timeToNextCrons,
  );

  router.get('/crons/time/all',
    dateNextCrons,
  );

  router.post('/crons/run-auth-cron',
    passport.authenticate('jwt', { session: false }),
    runAuthCronFallback,
  );

  router.post('/crons/run-billing-cron',
    passport.authenticate('jwt', { session: false }),
    runBillingCronFallback,
  );
};

