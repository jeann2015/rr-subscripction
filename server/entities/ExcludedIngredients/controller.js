import { LogDB } from '../Log/plugin';
import { updateIntegration } from '../../utils';
import {sequelize} from "../../db/connection";
import { exportCsv } from '../Order/plugin';
import path from "path";
import moment from 'moment';

const db = require('../../db/connection');

const Subscription = db.Subscription;


export const downloadCSV = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  sequelize.query(`SELECT
            \`user\`.id,
            \`excluded_ingredients\`.subscription_id,            
            \`subscription\`.status,
            \`user\`.email            
            from \`excluded_ingredients\`
            inner join \`subscription\` on \`excluded_ingredients\`.subscription_id  =  \`subscription\`.id            
            inner join \`user\` on \`subscription\`.user_id = \`user\`.id
            where \`excluded_ingredients\`.ingredient_id = ${req.params.id} order by \`user\`.id`,
      { type: sequelize.QueryTypes.SELECT },
  )
      .then((excludes) => {
        const data = [];
        const fields = ['id', 'subscription_id', 'status', 'email'];
        const fieldNames = ['User ID', 'Subscription ID', 'Subscription Status', 'Email'];
        let name_ingredient = '';
        excludes.map((exclude) => {
          data.push(exclude);
          name_ingredient =  exclude.name;
        });

        const opts = {
          fields,
          fieldNames,
          data,
        };

        const filename = `Ingredients ${ name_ingredient } exclusion list - ${moment().format('YYYY-MM-DD')}`;
        exportCsv(opts, filename, () => {
          const file = path.join(filename);
          res.download(file);
          logInfo.type = 'info';
          logInfo.message = `#(${req.user.id})~${req.user.name} download the Exclude Ingredients ${ name_ingredient } exclusion list - ${moment().format('YYYY-MM-DD')} csv.`;
          LogDB(logInfo);
        });
      });
};

/**
 * Logs in DB an update of excluded ingredients for a user.
 * @param {Object} params.admin - Session user info (Admin)
 * @param {number} params.user - Id of the user being edited
 * @param {Array} params.updatedData - Array of Sequelize instances returned after Sequelize.bulkCreate()
 * @param {Number} params.subId - Subscription ID
 */
const logExcludedIngrsUpdate = ({ admin, user, updatedData, subId }) => {
  const ingrsString = updatedData.map(instance => instance.get('ingredient_id')).join(', ');
  const messageData = {
    user: user || null,
    admin: admin.id || null,
    type: 'info',
    message: `#(${admin.id})~${admin.name} updated excluded ingredients for subscription ${subId} -> Now [${ingrsString}] are excluded`,
  };

  try {
    LogDB(messageData);
  } catch (error) {
    console.log(error);
  }
};

/**
 * Returns list of excluded ingredients for subscription.
 * @param {Number} subId - Subscription's ID
 * @returns {Object} { subscriptionId: Number, excludedIngredients: Array }.
 */
export const getExcludedIngredients = (subId, options) => {
  const dataNeeded = options && options.onlyIds === 'true' ? 'ingredientId' : 'ingredient';

  return new Promise(async (resolve, reject) => {
    try {
      const excludedIngrs = await db.ExcludedIngredients.findAll({
        where: { subscriptionId: parseInt(subId) },
        include: [db.Ingredients],
      });

      const excludedIngredients = excludedIngrs
        .map(ingr => ingr.get({ plain: true }))
        .map(plain => plain[dataNeeded]);

      resolve({
        subscriptionId: subId,
        excludedIngredients,
      });
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * Updates excluded ingredients of a subscription in DB.
 * @param {Object} params.admin - Session user info (Admin)
 * @param {number} params.user - Id of the user being edited
 * @param {Number} params.subId - Subscription ID
 * @param {Array} params.ingredients - Array of ingredients' ids (integers). Empty to remove all ingredients.
 * @returns {Object} { success (boolean), message (string) }
 */
export const updateSubscriptionExcludedIngredients = ({ admin, user, subId, ingredients, excludeAllMealsBreakfast }) => {
  const response = (success, message) => ({ success, message });

  return new Promise(async (resolve, reject) => {
    if (!admin || !user || !subId || !ingredients || !Array.isArray(ingredients) || typeof subId !== 'number') {
      return resolve(response(false, 'Error: A parameter is missing or wrong format'));
    }

    const noRepeatedIngrs = Array.from(new Set(ingredients));
    const dataToSave = noRepeatedIngrs.map(id => ({
      subscriptionId: subId,
      ingredientId: id,
    }));

    // Update breakFastCount
    if (excludeAllMealsBreakfast === 1) {
      const subscription = await Subscription.findById(subId);
      const dataToUpdateSubs = {
        breakFastCount: 0,
      };
      const subscriptionUpdated = await subscription.update(dataToUpdateSubs);
    }

    // Delete current excluded ingredients
    try {
      await db.ExcludedIngredients.destroy({
        where: { subscriptionId: subId },
      });
    } catch (error) {
      return reject(
        response(false, `Error when deleting excluded ingredients for subscription ${subId}: ${error}`),
      );
    }

    // Add updated ingredients
    try {
      const updated = await db.ExcludedIngredients.bulkCreate(dataToSave);
      const subData = await db.Subscription.findOne({ where: { id: subId } });

      // Log update
      logExcludedIngrsUpdate({ admin, user, updatedData: updated, subId });

      // Avoid integration update processes on test
      if (process.env.NODE_ENV !== 'test') {
        updateIntegration(subData.get('userId'));
      }

      return resolve(response(true, 'Excluded ingredients successfully updated'));
    } catch (error) {
      return reject(
        response(false, `Error when updating excluded ingredients for subscription ${subId}: ${error}`),
      );
    }
  });
};

export const handleExcludedIngrUpdate = async (req, res, next) => {
  const { id, user, ingredients, excludeAllMealsBreakfast } = req.body;
  try {
    const response = await updateSubscriptionExcludedIngredients({ admin: req.user, user, subId: parseInt(id), ingredients, excludeAllMealsBreakfast });
    return res.status(200).send(response);
  } catch (error) {
    return next(error);
  }
};

export const serveSubExcludedIngredients = async (req, res, next) => {
  try {
    const response = await getExcludedIngredients(req.params.id);
    return res.status(200).send(response);
  } catch (error) {
    return next(error);
  }
};
