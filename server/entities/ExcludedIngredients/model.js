export default (sequelize, DataTypes) => {
  return sequelize.define('excluded_ingredients', {
    subscriptionId: {
      type: DataTypes.INTEGER,
      field: 'subscription_id',
      allowNull: false,
    },
    ingredientId: {
      type: DataTypes.INTEGER,
      field: 'ingredient_id',
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
    underscored: true,
  });
};
