import passport from 'passport';
import { handleExcludedIngrUpdate, serveSubExcludedIngredients, downloadCSV } from './controller';

export default (router) => {
  router.post('/excluded-ingredients/update',
    passport.authenticate('jwt', { session: false }),
    handleExcludedIngrUpdate,
  );

  router.get('/excluded-ingredients/:id',
    passport.authenticate('jwt', { session: false }),
    serveSubExcludedIngredients,
  );

  router.get('/excluded/download/:id',
     passport.authenticate('jwt', { session: false }),
     downloadCSV,
  );
};
