import db, { sequelize } from '../../db/connection';
import moment from "moment";
import {exportCsv} from "../Order/plugin";
import path from "path";
import {LogDB} from "../Log/plugin";

export const getIngredients = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const ingredients = await db.Ingredients.findAll({});
      const plainIngr = ingredients
        .map(ingr => ingr.get({ plain: true }))
        .sort((a, b) => a.name > b.name ? 1 : -1);
      resolve(plainIngr);
    } catch (error) {
      reject(error);
    }
  });
};

export const getIngredientsWithStats = (req) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  return new Promise(async (resolve, reject) => {
    try {
      const activeSubscriptions = await db.Subscription.findAndCount({
        where: { status: 'active' },
      });

      const ingredients = await sequelize.query(
        `SELECT i.id, i.name, COUNT(*) AS excludedAmount FROM \`ingredients\` AS i
        LEFT JOIN \`excluded_ingredients\` AS e ON e.\`ingredient_id\` = i.id
        INNER JOIN \`subscription\` AS s ON s.\`id\` = e.subscription_id
        INNER JOIN \`user\` AS u ON u.\`id\` = s.user_id   
        GROUP BY i.name
        ORDER BY ${sort} ${order}
        LIMIT ${Number(offset)}, ${Number(end) - Number(offset)}
        `, { type: sequelize.QueryTypes.SELECT },
      );

      const withActiveSubs = ingredients.map((ingr) => {
        ingr.activeSubs = activeSubscriptions.count;
        return ingr;
      });

      resolve(withActiveSubs);
    } catch (error) {
      reject(error);
    }
  });
};

export const serveIngredients = async (req, res, next) => {
  try {
    const response = await getIngredients();
    return res.status(200).send(response);
  } catch (error) {
    return next(error);
  }
};


export const downloadCSV = async (req, res, next) => {

  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  const activeSubscriptions = await db.Subscription.findAndCount({
    where: {status: 'active'},
  });

  sequelize.query(`SELECT i.id, i.name, COUNT(*) AS excludedAmount FROM \`ingredients\` AS i
        LEFT JOIN \`excluded_ingredients\` AS e ON e.\`ingredient_id\` = i.id
        INNER JOIN \`subscription\` AS s ON s.\`id\` = e.subscription_id
        INNER JOIN \`user\` AS u ON u.\`id\` = s.user_id   
        GROUP BY i.name
        ORDER BY ${sort} ${order}`,
      {type: sequelize.QueryTypes.SELECT},
  )
      .then((excludes) => {
        const data = [];
        const fields = ['id', 'name', 'excludedAmount', 'exclusion'];
        const fieldNames = ['Ingredient ID', 'Ingredient Name', 'Time excluded', 'Exclusion Rate'];
        excludes.map((exclude) => {
          const excludeValue = ((exclude.excludedAmount / activeSubscriptions.count) * 10);
          exclude.exclusion = excludeValue.toFixed(1);
          data.push(exclude);
        });
        const opts = {
          fields,
          fieldNames,
          data,
        };

        const filename = `Ingredients exclusion list - ${moment().format('YYYY-MM-DD')}`;
        exportCsv(opts, filename, () => {
          const file = path.join(filename);
          res.download(file);
          logInfo.type = 'info';
          logInfo.message = `#(${req.user.id})~${req.user.name} download the Ingredients exclusion stats - ${moment().format('YYYY-MM-DD')} csv.`;
          LogDB(logInfo);
        });
      });
};
export const serveIngredientsWithStats = async (req, res, next) => {
  try {
    const response = await getIngredientsWithStats(req);
    res.set('X-Total-Count', response.length);
    return res.status(200).send(response);
  } catch (error) {
    return next(error);
  }
};

export const getIngredientsWithStatsExclude = (req) => {
  const where = (req.params.id) ? `i.id = ${req.params.id}` : '1=1';

  return new Promise(async (resolve, reject) => {
    try {
      const ingredients = await sequelize.query(
          `SELECT u.id as userId,s.id as subscriptionId,u.email, i.name as nameIngredient FROM \`ingredients\` AS i
        LEFT JOIN \`excluded_ingredients\` AS e ON e.\`ingredient_id\` = i.id
        LEFT JOIN \`subscription\` AS s ON s.\`id\` = e.subscription_id
        LEFT JOIN \`user\` AS u ON u.\`id\` = s.user_id   
        where 
        ${where}
        ORDER BY u.id        
        `, { type: sequelize.QueryTypes.SELECT },
      );
      resolve(ingredients);
    } catch (error) {
      reject(error);
    }
  });
};

export const getIngredientsExclude = async (req, res, next) => {
  try {
    const response = await getIngredientsWithStatsExclude(req);
    res.set('X-Total-Count', response.length);
    return res.status(200).send(response);
  } catch (error) {
    return next(error);
  }
};

