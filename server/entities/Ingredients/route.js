import passport from 'passport';
import { serveIngredients, serveIngredientsWithStats, getIngredientsExclude, downloadCSV } from './controller';

export default (router) => {
  router.get('/ingredients',
    passport.authenticate('jwt', { session: false }, null),
    serveIngredients,
  );

  router.get('/ingredients-with-stats',
    passport.authenticate('jwt', { session: false }, null),
    serveIngredientsWithStats,
  );

  router.get('/ingredients-with-stats-exclude/:id',
      passport.authenticate('jwt', { session: false }, null),
      getIngredientsExclude,
  );

  router.get('/ingredients/download',
      passport.authenticate('jwt', { session: false }, null),
      downloadCSV,
  );
};
