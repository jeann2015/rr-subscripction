const db = require('../../db/connection');

const Log = db.Log;
const User = db.User;

export const getLogs = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};
  if (req.query.userId) {
    clause.userId = req.query.userId;
  }
  if (req.query.type) {
    clause.type = req.query.type;
  }

  Log.findAndCountAll({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: clause,
    include: [User],
  })
    .then((logs) => {
      res.set('X-Total-Count', logs.count);
      res.status(200).send(logs.rows);
    })
    .catch(err => next(err));
};
