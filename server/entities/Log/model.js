export default (sequelize, DataTypes) => {
  return sequelize.define('log', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
    },
    adminId: {
      type: DataTypes.INTEGER,
      field: 'admin_id',
    },
    type: DataTypes.ENUM('info', 'warn', 'error'),
    message: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
    userStatus: {
      type: DataTypes.ENUM('active', 'canceled', 'pending', 'paused'),
      field: 'user_status',
    },
    userStatusDate: {
      type: DataTypes.DATE,
      field: 'user_status_date',
    },
  }, {
    freezeTableName: true,
  });
};
