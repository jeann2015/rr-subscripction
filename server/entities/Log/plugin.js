const db = require('../../db/connection');

const Log = db.Log;

export const LogDB = ({ user, admin, message, type }) => {
  Log.create({
    userId: user,
    adminId: admin,
    type,
    message,
  });
};

export const LogDBUserStatus = ({ userId, userStatus, userStatusDate }) => {
  Log.create({
    type: 'info',
    userId,
    userStatus,
    userStatusDate,
  });
};
