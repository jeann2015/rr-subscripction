import passport from 'passport';
import { getLogs } from './controller';

export default (router) => {
  router.get('/logs',
    passport.authenticate('jwt', { session: false }),
    getLogs,
  );
};
