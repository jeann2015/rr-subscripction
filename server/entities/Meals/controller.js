import Parse from 'csv-parse';
import { each, eachLimit } from 'async';
import { LogDB } from '../Log/plugin';
const db = require('../../db/connection');

export const getMeals = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'sku';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const offset = (req.query._start) ? req.query._start : 0;
  const kindValue =  (req.query._kind) ? req.query._kind : 'default';
  const clause = { kind: kindValue }

  db.Meals.findAndCountAll({
    order: [[sort, order]],
    offset: Number(offset),
    where: clause,
  }).then(async (iorder) => {
    res.set('X-Total-Count', iorder.count);
    const plainMeals = iorder.rows.map(o => o.get({ plain: true }));
    res.status(200).send(plainMeals);
  }, (err) => {
    next(err);
  });
};

export const setDefault = (req, res, next) => {
  const { meals } = req.body;
  each(meals, async (p, cb) => {
    try {
      const mealRecord = await db.Meals.findOne({ where: { sku: p.sku } });
      mealRecord.default = p.default;
      mealRecord.priority = p.priority;
      mealRecord.kind = 'default';
      mealRecord.save().then(() => cb());
    } catch (err) {
      return cb();
    }
  }, () => {
    res.status(200).send({ status: 'ok' });
  });
};

export const setBreakFast = (req, res, next) => {
  const { meals } = req.body;
  each(meals, async (p, cb) => {
    try {
      const mealRecord = await db.Meals.findOne({ where: { sku: p.sku } });
      mealRecord.default = '0';
      mealRecord.priority = p.priority;
      mealRecord.kind = 'breakfast';
      mealRecord.save().then(() => cb());
    } catch (err) {
      return cb();
    }
  }, () => {
    res.status(200).send({ status: 'ok' });
  });
};

export const uploadCSV = (req, res, next) => {
  const source = fs.createReadStream(req.file.path);
  const mealRecords = [];
  const parser = Parse({
    delimiter: ',',
    columns: true,
  });
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  const response = {
    created: [],
    skipped: [],
  };

  parser.on('readable', () => {
    let record;
    while (record = parser.read()) {
      const meal = {
        sku: record.meal_sku,
        name: record.meal_name,
        ingredients: [],
      };
      for (let i = 1; i < 6; i++) {
        if (record[`ingredient_${i}`].length > 1) meal.ingredients.push(record[`ingredient_${i}`]);
      }
      mealRecords.push(meal);
    }
  });

  parser.on('error', (error) => {
    logInfo.type = 'error';
    logInfo.message = `#(${req.user.id})~${req.user.name} failed to upload a meals .csv file -> ${error.message}`;
    LogDB(logInfo);
    res.status(500).send({ message: 'Unable to read file' });
    return next(error);
  });

  parser.on('end', () => {
    each(mealRecords, async (meal, cb) => {
      const { sku, name, ingredients } = meal;
      const responseRecord = { sku };

      try {
        // Check if meal exists
        const existingMeal = await db.Meals.findOne({ where: { sku } });
        if (existingMeal) {
          responseRecord.reason = 'A meal with the same sku already exists';
          response.skipped.push(responseRecord);
          return cb();
        }

        // Check if ingredients exist
        const ingredientsId = [];
        for (const ingredient of ingredients) {
          const ingredientRecord = await db.Ingredients.findOne({ where: { name: ingredient } }); // eslint-disable-line no-await-in-loop
          if (ingredientRecord) {
            ingredientsId.push(ingredientRecord.id);
          } else {
            responseRecord.reason = `Ingredient '${ingredient}' does not exist in the database`;
            response.skipped.push(responseRecord);
            return cb();
          }
        }

        // Create meal
        await db.Meals.create({
          sku,
          name,
          default: 0,
          deprecated: 0,
        });

        // Create meal ingredients relation record
        for (const ingredientId of ingredientsId) {
          await db.MealsIngredients.create({ // eslint-disable-line no-await-in-loop
            mealSku: sku,
            ingredientId,
          });
        }

        return cb();
      } catch (err) {
        responseRecord.reason = err.message;
        response.skipped.push(responseRecord);
        return cb();
      }
    }, () => {
      res.status(200).send(response);
    });
  });

  source.pipe(parser);
};


export const getMealsIngredients = async (req, res, next) => {

  const mealsBreakFast = await db.Meals.findAll({
    where: {deprecated: false, kind: 'breakfast'},
    include: [{
      model: db.MealsIngredients,
      include: [db.Ingredients],
    }],
  });

  const plainMeals = mealsBreakFast.map((meal) => {
    const plain = meal.get({plain: true});
    const ingredients = plain.meals_ingredients.map(ingr => ingr.ingredient);

    return ({
      sku: plain.sku,
      name: plain.name,
      default: plain.default,
      priority: plain.priority,
      kind: plain.kind,
      ingredients,
    });
  });

  res.set('X-Total-Count', plainMeals.count);
  res.status(200).send(plainMeals);
};


