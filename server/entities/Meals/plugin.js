const db = require('../../db/connection');

/**
 * Returns all meals with curated ingredients list
 * @returns {object}
 */
export const getMeals = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const meals = await db.Meals.findAll({
        where: { deprecated: false },
        include: [{
          model: db.MealsIngredients,
          include: [db.Ingredients],
        }],
      });

      const plainMeals = meals.map((meal) => {
        const plain = meal.get({ plain: true });
        const ingredients = plain.meals_ingredients.map(ingr => ingr.ingredient);

        return ({
          sku: plain.sku,
          name: plain.name,
          default: plain.default,
          priority: plain.priority,
          kind: plain.kind,
          ingredients,
        });
      });

      resolve(plainMeals);
    } catch (error) {
      reject(error);
    }
  });
};
