import passport from 'passport';
import multer from 'multer';
import {uploadCSV, getMeals, setDefault, setBreakFast, getMealsIngredients} from './controller';

const upload = multer({ dest: 'uploads/' });

export default (router) => {
  router.get('/meals',
    passport.authenticate('jwt', { session: false }),
    getMeals,
  );

  router.post('/meals/default',
    passport.authenticate('jwt', { session: false }),
    setDefault,
  );

  router.post('/meals/breakfast',
      passport.authenticate('jwt', { session: false }),
      setBreakFast,
  );

  router.put('/meals/upload',
    passport.authenticate('jwt', { session: false }),
    upload.single('meals'),
    uploadCSV,
  );

  router.get('/meals/ingredients',
      passport.authenticate('jwt', { session: false }),
      getMealsIngredients,
  );

};
