export default (sequelize, DataTypes) => {
  return sequelize.define('meals_ingredients', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    mealSku: {
      type: DataTypes.STRING,
      field: 'meal_sku',
      allowNull: false,
    },
    ingredientId: {
      type: DataTypes.INTEGER,
      field: 'ingredient_id',
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
  });
};
