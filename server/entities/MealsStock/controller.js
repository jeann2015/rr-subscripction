import Parse from 'csv-parse';
import { each } from 'async';

import { LogDB } from '../Log/plugin';
import { runStockSimulation } from '../../services/cron_simulator';

const db = require('../../db/connection');

class StatusError extends Error {
  constructor(status, message) {
    super(message);
    this.status = status;
  }
}

export const addSimulatedLocalStock = (stock, simulatedConsumed) => {
  return stock.map((meal) => {
    const updatedStock = simulatedConsumed.find(item => item.id === meal.id).localStock;
    meal.localStock = updatedStock;
    return meal;
  });
};

export const getStock = async (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};

  const warehousesList = await db.Warehouses.findAll({})
    .map(warehouse => warehouse.get({ plain: true }));

  db.MealsStock.findAndCountAll({
    order: [[sort, order]],
    offset: Number(offset),
    include: [db.Meals],
    where: clause,
  }).then(async (iorder) => {
    // Run stock simulation for next Cron
    const simulated = await runStockSimulation();
    // Get plain
    const plainStock = iorder.rows.map(o => o.get({ plain: true }));
    // Add warehouse data
    const withWarehouseCity = plainStock.map((item) => {
      item.warehouseData = warehousesList.find(el => el.id === parseInt(item.warehouse));
      return item;
    });
    // Add simulated stock
    const withSimulatedStock = addSimulatedLocalStock(withWarehouseCity, simulated);

    res.set('X-Total-Count', iorder.count);
    res.status(200).send(withSimulatedStock);
  }, (err) => {
    next(err);
  });
};

export const getUserWarehouse = async (userId, options = {}) => {
  const user = await db.User.findOne({
    where: { id: userId },
    include: [{
      model: db.TagDay,
      as: 'TagDay',
      include: [{
        model: db.TagDayName,
        as: 'TagDayName',
        include: [{
          model: db.Warehouses,
          as: 'Warehouse' }],
      }],
    }],
  });

  if (!user.TagDay.TagDayName.Warehouse) {
    if (!user) throw new StatusError(400, `No user with id '${userId}' found`);
    if (!user.TagDay) throw new StatusError(400, `No tag_day for zip code '${user.zip_code}' found`);
    if (!user.TagDay.TagDayName) throw new StatusError(400, `No tag_day_name for id '${user.TagDay.name}' found`);
    throw new StatusError(400, `No warehouse for id ${user.TagDay.TagDayName.warehouse_id} found`);
  }

  if (options.alldata === true) {
    return user.TagDay;
  }

  return user.TagDay.TagDayName.Warehouse;
};

export const getStockGroupedByWarehouse = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const stockData = await db.MealsStock.findAll({});
      const stockGrouped = {};
      for (const c of stockData) {
        if (!stockGrouped[c.warehouse]) stockGrouped[c.warehouse] = {};
        if (!stockGrouped[c.warehouse][c.mealSku]) stockGrouped[c.warehouse][c.mealSku] = 0;
        stockGrouped[c.warehouse][c.mealSku] += c.stock;
      }
      resolve(stockGrouped);
    } catch (error) {
      reject(error);
    }
  });
};

const getSkuStock = async (mealSku, warehouse) => {
  const stockSingle = await db.MealsStock.findOne({ where: { mealSku, warehouse } })
    .catch((err) => {
      throw new StatusError(500, 'An unknown error ocurred');
    });
  if (!stockSingle || stockSingle.localStock === undefined) {
    throw new StatusError(400, `No stock info for warehouse(${warehouse})/sku(${mealSku})`);
  }

  return stockSingle;
};

export const consultStock = async (req, res, next) => {
  const { userId, mealSku } = req.query;
  try {
    const warehouseSingle = await getUserWarehouse(userId);
    const skuStock = await getSkuStock(mealSku, warehouseSingle.tagId);
    res.status(200).send({ stock: skuStock.localStock });
  } catch (err) {
    res.status(err.status).send({ message: err.message });
    return next(err);
  }
};

export const consumeStock = async (req, res, next) => {
  const { userId, mealSku, amount } = req.body;

  let skuStock = null;

  try {
    const warehouseSingle = await getUserWarehouse(userId);
    skuStock = await getSkuStock(mealSku, warehouseSingle.tagId);
  } catch (err) {
    res.status(err.status).send({ message: err.message });
    return next(err);
  }

  const localStock = skuStock.localStock;
  if (localStock < amount) {
    res.status(400).send({ message: 'Not enough stock' });
    return;
  }

  try {
    skuStock.localStock = localStock - amount;
    await skuStock.save();
    res.status(200).send({ status: 'ok' });
  } catch (err) {
    res.status(500).send({ message: 'An unknown error ocurred' });
    return next(err);
  }
};

export const uploadCSV = async (req, res, next) => {
  const source = fs.createReadStream(req.file.path);
  const stockRecords = [];
  const parser = Parse({
    delimiter: ',',
    columns: true,
  });
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  let updatedStocks = 0;
  let createdStocks = 0;

  parser.on('readable', () => {
    let record;
    while (record = parser.read()) {
      const stock = {
        mealSku: record.meal_sku,
        stock: record.stock,
        localStock: record.stock,
        warehouse: record.stock_center,
      };
      stockRecords.push(stock);
    }
  });

  parser.on('error', (error) => {
    logInfo.type = 'error';
    logInfo.message = `#(${req.user.id})~${req.user.name} failed to upload a stock .csv file -> ${error.message}`;
    LogDB(logInfo);
    res.status(500).send({ message: 'Unable to read file' });
    return next(error);
  });

  parser.on('end', () => {
    each(stockRecords, (stockSingle, cb) => {
      const { mealSku, warehouse } = stockSingle;
      db.MealsStock.findOne({
        where: {
          mealSku,
          warehouse,
        },
      }).then((existingStock) => {
        if (existingStock) {
          existingStock.update(stockSingle)
            .then((c) => {
              updatedStocks++;
            })
            .catch((err) => {
              logInfo.type = 'error';
              logInfo.message = `Database error in uploadCSV(update stock) -> Name:${err.name} ~ Message: ${err.message}`;
              LogDB(logInfo);
            });
          return cb();
        }
        db.MealsStock.create(stockSingle)
          .then((c) => {
            createdStocks++;
            logInfo.type = 'info';
            logInfo.message = `#(${req.user.id})~${req.user.name} created a stock entry -> [${mealSku} | ${warehouse}]`;
            LogDB(logInfo);
          })
          .catch((err) => {
            logInfo.type = 'error';
            logInfo.message = `Database error in uploadCSV(create stock) -> Name:${err.name} ~ Message: ${err.message}`;
            LogDB(logInfo);
          });
        cb();
      });
    }, () => {
      if (updatedStocks > 0) {
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} updated ${updatedStocks} stock entries`;
        LogDB(logInfo);
      }
      if (createdStocks > 0) {
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} created ${createdStocks} stock entries`;
        LogDB(logInfo);
      }
      res.status(200).send({ createdStocks, updatedStocks });
    });
  });

  source.pipe(parser);
};

export const handleStockSimulation = (req, res, next) => {
  try {
    runStockSimulation(req.body.date).then((result) => {
      res.status(200).send(result);
    });
  } catch (error) {
    next(error);
  }
};

export const countStockConsumed = (data, boxDetail) => {
  const stock = data.stockConsumed;

  if (!boxDetail.length) return {};

  for (const meal of boxDetail) {
    if (!stock[meal.mealSku]) {
      stock[meal.mealSku] = meal.quantity;
    } else {
      stock[meal.mealSku] += meal.quantity;
    }
  }
  return stock;
};

export const saveStockConsumed = async (data) => {
  for (const warehouse in data) {
    for (const sku in data[warehouse]) {
      try {
        // eslint-disable-next-line
        await db.MealsStock.update(
          { stock: data[warehouse][sku] },
          { where: {
            warehouse,
            mealSku: sku,
          } },
        );
        if (process.env.NODE_ENV !== 'test') console.log(`Stock saved for meal ${sku} in warehouse ${warehouse}`);
      } catch (error) {
        console.log(error);
      }
    }
  }
};
