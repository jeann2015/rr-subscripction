export default (sequelize, DataTypes) => {
  return sequelize.define('meals_stock', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    mealSku: {
      type: DataTypes.STRING,
      field: 'meal_sku',
      allowNull: false,
      primaryKey: true,
    },
    stock: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    localStock: {
      type: DataTypes.INTEGER,
      field: 'local_stock',
      allowNull: false,
    },
    warehouse: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
