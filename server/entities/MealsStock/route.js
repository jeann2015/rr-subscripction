import passport from 'passport';
import multer from 'multer';
import { uploadCSV, consumeStock, consultStock, getStock, handleStockSimulation } from './controller';

const upload = multer({ dest: 'uploads/' });

export default (router) => {
  router.get('/stock',
    passport.authenticate('jwt', { session: false }),
    getStock,
  );

  router.put('/stock/upload',
    passport.authenticate('jwt', { session: false }),
    upload.single('stock'),
    uploadCSV,
  );

  router.post('/stock/consume',
    passport.authenticate('jwt', { session: false }),
    consumeStock,
  );

  router.get('/stock/consult',
    passport.authenticate('jwt', { session: false }),
    consultStock,
  );

  router.post('/stock/simulate',
    passport.authenticate('jwt', { session: false }),
    handleStockSimulation,
  );
};
