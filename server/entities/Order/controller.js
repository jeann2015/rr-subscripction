import path from 'path';
import fs from 'fs';
import Parse from 'csv-parse';
import { eachLimit } from 'async';

import { exportCsv, sendAllEmails, getLastAllOrders } from './plugin';
import { updateWeeklyList} from '../../utils/hubspot';
import { LogDB } from '../Log/plugin';
import { getOrdersAndSendToShipstation } from '../../services/shipstation_runner';
import { add3PLtoOrders, updateIntegration } from '../../utils';
import { triggerOutboundOrderShippedEvent } from '../../services/outbound';
import {
  getShipstationOrdersSinceLastCron,
  getShipstationTrackingNumbersSinceLastCron,
  saveOrderStatusToDb,
  saveTrackingNumbersToDb,
} from '../../services/shipstation_sync';
import { sequelize } from '../../db/connection';
import { addDiscountsToOrders } from '../AppliedDiscounts/controller';

const db = require('../../db/connection');

const User = db.User;
const Cron = db.Cron;
const Product = db.Product;
const Order = db.Order;
const Subscription = db.Subscription;

const logInfo = {
  user: null,
  admin: null,
  type: '',
  message: '',
};

const getDateClause = (queryObj) => {
  const sinceDate = queryObj.createdSince ? queryObj.createdSince.split('T')[0] : null; // Remove timestamp
  const toDate = queryObj.createdTo ? queryObj.createdTo.split('T')[0] : null;

  const query = {};
  if (sinceDate) query.$gte = sinceDate;
  if (toDate) query.$lte = toDate;

  return query;
};

export const getOrders = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};

  if (req.query.userId) {
    clause.userId = req.query.userId;
  }

  if (req.query.email) {
    clause.email = { $like: `%${req.query.email}%` };
  }

  if (req.query.createdSince || req.query.createdTo) {
    clause.createdAt = getDateClause({
      createdSince: req.query.createdSince,
      createdTo: req.query.createdTo,
    });
  }

  Order.findAndCountAll({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: clause,
  }).then(async (iorder) => {
    res.set('X-Total-Count', iorder.count);
    const plainOrders = iorder.rows.map(o => o.get({ plain: true }));
    const add3plToOrdersResponse = await add3PLtoOrders(plainOrders);
    const withDiscounts = await addDiscountsToOrders(add3plToOrdersResponse);
    res.status(200).send(withDiscounts);
  }, (err) => {
    next(err);
  });
};

export const downloadAllCSV = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  const where = {};
  where.boxType = {
    $and: [
      { $notLike: '%Babycook%' },
      { $notLike: 'Pre-Cron Box' },
    ],
  };

  // Add clause if admin order list has date filters
  if (Object.keys(req.body).length !== 0) {
    where.createdAt = getDateClause(req.body);
  }

  Order.findAll({
    where,
    include: [{
      model: User,
      attributes: ['id', 'name', 'lastName', 'address', 'city', 'state', 'company'],
    }, {
      model: Subscription,
      attributes: ['productId'],
      include: [Product],
    }],
    raw: true,
  })
    .then(async (data) => {
      const withDiscounts = await addDiscountsToOrders(data);
      const fields = ['id', 'boxType', 'productPrice', 'discountApplied.applied', 'discountApplied.amountApplied', 'email', 'name', 'lastName', 'address', 'addressExtra', 'user.company', 'city', 'state', 'zipCode', 'trackId', 'createdAt'];
      const fieldNames = ['Order Id', 'Plan', 'Amount Charged', 'Discount', 'Discount Amount', 'Email', 'First Name', 'Last Name', 'Street 1', 'Street 2', 'Company', 'City', 'State', 'Zipcode', 'Track Id', 'Order date'];
      const opts = {
        data: withDiscounts,
        fields,
        fieldNames,
      };

      exportCsv(opts, 'orders.csv', () => {
        const file = path.join('orders.csv');
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} download orders.`;
        LogDB(logInfo);
        return res.download(file);
      });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in downloadAllCSV(find all orders) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      return next(err);
    });
};

export const downloadCSV = (req, res, next) => {
  Cron.findOne({
    order: 'id DESC',
  })
    .then((cron) => {
      const cronId = cron.get('id');
      const logInfo = {
        user: null,
        admin: req.user.id,
        type: '',
        message: '',
      };
      Order.findAll({
        where: {
          cronId,
        },
        include: [{
          model: User,
        }, {
          model: Subscription,
          include: [Product],
        }],
      })
        .then(async (data) => {
          const fields = ['id', 'boxType', 'productPrice', 'discountApplied.applied', 'discountApplied.amountApplied', 'boxNumber', 'email', 'name', 'lastName', 'address', 'addressExtra', 'user.company', 'city', 'state', 'zipCode', 'trackId', '_3pl'];
          const fieldNames = ['Order Id', 'Plan', 'Amount Charged', 'Discount', 'Discount Amount', 'Box Number', 'Email', 'First Name', 'Last Name', 'Street 1', 'Street 2', 'Company', 'City', 'State', 'Zipcode', 'Track Id', 'Center'];

          const with3pl = await add3PLtoOrders(data);
          const withDiscounts = await addDiscountsToOrders(with3pl);

          const opts = {
            data: withDiscounts,
            fields,
            fieldNames,
          };
          exportCsv(opts, 'orders.csv', () => {
            const file = path.join('orders.csv');
            logInfo.type = 'info';
            logInfo.message = `#(${req.user.id})~${req.user.name} download the weekly(#${cronId}) orders.`;
            LogDB(logInfo);
            return res.download(file);
          });
        })
        .catch((err) => {
          logInfo.type = 'error';
          logInfo.message = `Database error in downloadCSV(find all orders) -> Name:${err.name} ~ Message: ${err.message}`;
          LogDB(logInfo);
          next(err);
        });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in downloadCSV(find one cron) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      next(err);
    });
};

export const uploadCSV = (req, res, next) => {
  const source = fs.createReadStream(req.file.path);
  const ordersToUpdate = [];
  const parser = Parse({
    delimiter: ',',
    columns: true,
  });
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  parser.on('readable', () => {
    let record;
    while (record = parser.read()) {
      const shipping = {
        boxType: record.Plan,
        trackId: record['Track Id'],
        orderId: Number(record['Order Id']),
      };
      ordersToUpdate.push(shipping);
    }
  });

  parser.on('error', (error) => {
    logInfo.type = 'error';
    logInfo.message = `#(${req.user.id})~${req.user.name} upload a csv and this one fail. -> ${error.message}`;
    LogDB(logInfo);
    next(error);
  });

  parser.on('end', () => {
    eachLimit(ordersToUpdate, 1, (shipping, cb) => {
      if (shipping.boxType === 'First Box' || shipping.boxType === 'Regular' || shipping.boxType.indexOf('Babycook') !== -1) {
        return cb();
      }

      Order.update({
        trackId: shipping.trackId,
        tracked: true,
        trackingEmail: 'not_sent',
      }, {
        where: {
          id: shipping.orderId,
        },
      })
        .then(() => {
          Order.findOne({
            include: [User],
            where: {
              id: shipping.orderId,
              trackId: {
                $ne: '',
              },
              tracked: {
                $eq: 0,
              },
            },
          })
            .then(async (order) => {
              if (!order) {
                cb();
              }
              if (order) {
                logInfo.user = user.get('id');
                logInfo.type = 'info';
                logInfo.message = `#(${req.user.id})~${req.user.name}. Meal Maker track id for #(${user.get('id')})~${user.get('name')} -> ${order.trackId}`;
                LogDB(logInfo);
                cb();
              }
            })
            .catch(err => next(err));
        })
        .catch((err) => {
          next(err);
        });
    }, async () => {
        // Sync Users
      const weeklyOrders = await getLastAllOrders();
      const weeklyOrdersUserIds = weeklyOrders.map(order => order.get('userId'));
      const weeklyOrdersUserIdsUnique = [...new Set(weeklyOrdersUserIds)];
      weeklyOrdersUserIdsUnique.forEach((userId) => {
        updateIntegration(userId)
          .then(res => console.log(`User Integration: #${userId} integrated SUCCESS`))
          .catch(err => console.log(`User Integration: #${userId} integrated ERROR: (${err})`));
      });
      res.status(200).send({ status: 'ok' });
    });
  });

  source.pipe(parser);
};

export const hubspotSendContactsToWeeklyList = async (req, res, next) => {
  try {
    await updateWeeklyList();
    res.status(200).send({ status: 'ok' });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getOrderById = (req, res, next) => {
  Order.find({
    where: {
      id: req.params.id,
    },
  })
    .then((order) => {
      if (!order) {
        res.status(404).send('order not found');
        return;
      }
      res.status(200).send(order);
    })
    .catch(err => next(err));
};

export const deleteAllOrders = async (user) => {
  await Promise.all(user.orders.map(async (order) => {
    await order.destroy();
  }));
};

const syncUsers = async () => {
  const weeklyOrders = await getLastAllOrders();
  const weeklyOrdersUserIds = weeklyOrders.map(order => order.get('userId'));
  const weeklyOrdersUserIdsUnique = [...new Set(weeklyOrdersUserIds)];
  weeklyOrdersUserIdsUnique.forEach((userId) => {
    updateIntegration(userId)
      .then(res => console.log(`User Integration: #${userId} integrated SUCCESS`))
      .catch(err => console.log(`User Integration: #${userId} integrated ERROR: (${err})`));
  });
};

const runIntegrations = (orders) => {
  console.log('Call Function Trigger Outbound from controller');
  // Send outbound shipment status
  triggerOutboundOrderShippedEvent(orders);
  syncUsers();
};

export const shipstationSync = async (req, res, next) => {
  let orders;
  let trackingNumbers;
  console.log('ShipStation sync started');

  // First we retrieve orders from ShipStation
  try {
    orders = await getShipstationOrdersSinceLastCron();
    // Log
    logInfo.admin = req.user.id;
    logInfo.type = 'info';
    logInfo.message = 'ShipStation sync started. All orders had been successfully retrieved.';
    LogDB(logInfo);
  } catch (error) {
    next(error);
  }

  // And save order status
  try {
    const savedOrders = await saveOrderStatusToDb(orders);

    // Log
    logInfo.admin = req.user.id;
    logInfo.type = 'info';
    logInfo.message = savedOrders.message;
    LogDB(logInfo);
    console.log(savedOrders.message);
  } catch (error) {
    next(error);
  }

  // Same process but with the tracking numbers
  try {
    trackingNumbers = await getShipstationTrackingNumbersSinceLastCron();

    // Log
    logInfo.admin = req.user.id;
    logInfo.type = 'info';
    logInfo.message = 'All tracking numbers had been successfully retrieved from ShipStation';
    LogDB(logInfo);
  } catch (error) {
    next(error);
  }

  // Save tracking numbers
  try {
    const savedTrackingNumbers = await saveTrackingNumbersToDb(trackingNumbers);

    // Log
    logInfo.admin = req.user.id;
    logInfo.type = 'info';
    logInfo.message = savedTrackingNumbers.message;
    LogDB(logInfo);
    console.log(savedTrackingNumbers.message);
    console.log('ShipStation sync successfully finished.');

    res.status(200).send({ status: 'ok' });

    // Update integrations
    runIntegrations(orders);
  } catch (error) {
    next(error);
  }
};

export const shipstationCreateOrders = async (req, res, next) => {
  try {
    await getOrdersAndSendToShipstation();
    // Log
    logInfo.admin = req.user.id;
    logInfo.type = 'info';
    logInfo.message = 'All orders had been successfully sent to ShipStation';
    LogDB(logInfo);

    res.status(200).send({ status: 'ok' });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const shipstationSendEmails = async (req, res, next) => {
  try {
    await sendAllEmails();
    res.status(200).send({ status: 'ok' });
  } catch (error) {
    next(error);
  }
};

export const dimensionsByOrders = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT
      o.id AS 'OrderId',
      u.id AS 'UserId',
      u.status,
      u.state,
      TRUNCATE(o.product_price/100, 2) AS 'ProductPrice',
      TRUNCATE(pp.product_price/100, 2) AS 'FirstCharge',
      o.box_number AS 'BoxNumber',
      DATE_FORMAT(o.created_at, "%Y-%m-%d") AS 'ShippingDate',
      FLOOR(DATEDIFF(s.start_from, c.birthdate)/30) AS 'BabyAgeAtStart',
      FLOOR(DATEDIFF(s.cancellation_date, c.birthdate)/30) AS 'BabyAgeAtCancellation',
      FLOOR(DATEDIFF(CURDATE(), c.birthdate)/30) AS 'BabyAgeToday',
      DATE_FORMAT(s.created_at, "%Y-%m") AS 'SignupDate',
      DATE_FORMAT(s.cancellation_date, "%Y-%m-%d") AS 'CancellationDate',
      s.cancel_reason AS 'CancelationReason',
      DATE_FORMAT(s.start_from, "%Y-%m-%d") AS 'StartFrom',

      (SELECT nd.next_discount_type WHERE DATE_FORMAT(nd.created_at, "%Y-%m-%d") = DATE_FORMAT(o.created_at, "%Y-%m-%d")) AS TypeNextDiscount,
      (SELECT nd.DiscountAmount WHERE DATE_FORMAT(nd.created_at, "%Y-%m-%d") = DATE_FORMAT(o.created_at, "%Y-%m-%d")) AS DiscountAmount,
      COALESCE((SELECT cp.Coupon WHERE DATE_FORMAT(cp.created_at, "%Y-%m-%d") = DATE_FORMAT(o.created_at, "%Y-%m-%d")), 'No Coupon') AS Coupon,
      TRUNCATE((SELECT cp.coupon_amount WHERE DATE_FORMAT(cp.created_at, "%Y-%m-%d") = DATE_FORMAT(o.created_at, "%Y-%m-%d")) / 100, 2) AS CouponAmount,

      CASE s.status
          WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.created_at)/30)
          WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.created_at)/30)
          ELSE NULL
      END AS 'timeActive',
      IF (referral.me IS NOT NULL, 'Yes', 'No') AS isReferred,
      user_referrer.id AS referrerId

      FROM \`order\` AS o
      LEFT JOIN \`user\` AS u ON o.user_id = u.id
      LEFT JOIN subscription AS s ON o.user_id = s.user_id
      LEFT JOIN referral ON referral.me = o.user_id
      LEFT JOIN user AS user_referrer ON referral.referred_by = user_referrer.id
      LEFT JOIN
      (SELECT * FROM child GROUP BY user_id) AS c ON o.user_id = c.user_id
      LEFT JOIN
      (SELECT o2.user_id, o2.product_price FROM \`order\` AS o2
          WHERE o2.box_type NOT LIKE 'Meal Maker' AND o2.box_type NOT LIKE '%Babycook%' AND o2.box_type NOT LIKE 'Pre-Cron Box' AND o2.box_number = 1
          GROUP BY o2.user_id) AS pp ON o.user_id = pp.user_id
      LEFT JOIN
      (SELECT DISTINCT
      \`applied_discounts\`.user,
      \`applied_discounts\`.created_at,
      \`coupon\`.code as Coupon,
      \`applied_discounts\`.coupon_amount
      FROM \`applied_discounts\`
      INNER JOIN \`coupon\` ON \`coupon\`.id = \`applied_discounts\`.coupon
      WHERE coupon IS NOT NULL) AS cp ON o.user_id = cp.user
      LEFT JOIN
      (SELECT DISTINCT
      user,
      created_at,
      next_discount_type,
      TRUNCATE(next_discount_amount/100, 2) AS 'DiscountAmount'
      FROM \`applied_discounts\`
      ) AS nd ON o.user_id = nd.user
      WHERE o.box_type NOT LIKE 'Meal Maker' AND o.box_type NOT LIKE '%Babycook% AND o.box_type NOT LIKE 'Pre-Cron Box'
      GROUP BY o.id
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};



