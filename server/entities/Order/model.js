export default (sequelize, DataTypes) => {
  return sequelize.define('order', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      allowNull: false,
    },
    name: DataTypes.STRING,
    lastName: {
      type: DataTypes.STRING,
      field: 'last_name',
    },
    email: DataTypes.STRING,
    zipCode: {
      type: DataTypes.STRING,
      field: 'zip_code',
    },
    address: DataTypes.STRING,
    addressExtra: {
      type: DataTypes.STRING,
      field: 'address_extra',
    },
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    boxType: {
      type: DataTypes.STRING,
      field: 'box_type',
    },
    productPrice: {
      type: DataTypes.INTEGER,
      field: 'product_price',
    },
    subscriptionId: {
      type: DataTypes.INTEGER,
      field: 'subscription_id',
    },
    chargeToken: {
      type: DataTypes.STRING,
      field: 'charge_token',
    },
    trackId: {
      type: DataTypes.STRING,
      field: 'track_id',
      defaultValue: '',
    },
    tracked: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0,
    },
    cronId: {
      type: DataTypes.INTEGER,
      field: 'cron_id',
    },
    trackingEmail: {
      type: DataTypes.STRING,
      field: 'tracking_email',
    },
    shipstationStatus: {
      type: DataTypes.STRING,
      field: 'shipstation_status',
    },
    shipstationOrder: {
      type: DataTypes.STRING,
      field: 'shipstation_order',
    },
    boxNumber: {
      type: DataTypes.INTEGER,
      field: 'box_number',
    },
    configuration: {
      type: DataTypes.ENUM('standard', 'custom'),
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
  });
};
