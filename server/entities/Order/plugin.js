import json2csv from 'json2csv';
import fs from 'fs';

import db from '../../db/connection';

const sequelize = db.sequelize;
const Order = db.Order;
const Cron = db.Cron;

export const exportCsv = (opts, filename, cb) => {
  opts.data.forEach((order) => {
    order.price /= 100;
  });
  const csv = json2csv(opts);
  fs.writeFile(filename, csv, (err) => {
    if (err) throw err;
    cb();
  });
};

export const getLastTrackingLink = (userId) => {
  return new Promise(async (resolve, reject) => {
    const ordersFromUser = await Order.findOne({
      order: [
        ['createdAt', 'DESC'],
      ],
      where: {
        userId,
        tracked: true,
        $or: [
          { boxType: 'First Box' },
          { boxType: 'Regular' },
        ],
      },
    });
    resolve(ordersFromUser === null ? '' : `https://www.ups.com/track?loc=en_US&tracknum=${ordersFromUser.get('trackId')}&requester=NES&agreeTerms=yes/trackdetails`);
  });
};

export const getLastOrders = async () => {
  const lastCron = await Cron.findAll({
    order: [
      ['createdAt', 'DESC'],
    ],
    end: 1,
  });

  const orders = await Order.findAll({
    where: {
      cron_id: lastCron[0].get('id'),
      box_type: { $notLike: 'Meal Maker' },
    },
    include: [
      db.User,
    ],
  });

  return orders;
};

export const getLastAllOrders = async () => {
  const lastCron = await Cron.findAll({
    order: [
      ['createdAt', 'DESC'],
    ],
    end: 1,
  });

  const orders = await Order.findAll({
    where: {
      cron_id: lastCron[0].get('id'),
    },
    include: [
      db.User,
    ],
  });

  return orders;
};

const boxOverBox = () => {
  return new Promise(async (resolve, reject) => {
    const boxNumbers = await sequelize.query(`
      SELECT
        BoxNumber,
        count(SubscriptionStatusActive) as Actives,
        count(SubscriptionStatusCancelled) as Cancelleds,
        count(BoxNumber) as Orders,
        (1 - (count(SubscriptionStatusCancelled) / count(BoxNumber)) ) as BoxOverBox
      FROM
        (SELECT
            MAX(box_number) as BoxNumber,
            SA.status as SubscriptionStatusActive,
            SC.status as SubscriptionStatusCancelled
        FROM
            \`order\`
        LEFT JOIN
          \`subscription\` as SA
        ON
          SA.user_id = \`order\`.user_id AND SA.status = 'active'
        LEFT JOIN
          \`subscription\` as SC
        ON
          SC.user_id = \`order\`.user_id AND (SC.status = 'canceled' OR SC.status = 'pending' OR SC.status = 'paused')
        WHERE
            \`order\`.box_number IS NOT NULL
        GROUP BY
          \`order\`.user_id
        ORDER BY
            BoxNumber) as BN
        GROUP BY
          BoxNumber
    `, { type: sequelize.QueryTypes.SELECT });

    const totalOrders = boxNumbers.reduce((prev, curr) => prev + curr.Orders, 0);
    const withOverBox = boxNumbers.map((element, index) => {
      element.BoxOverBox = index === 0 ? 0 : (1 - (boxNumbers[index - 1].Cancelleds / totalOrders));
      return element;
    });

    // fallback to complete missing indexes
    const missingIndex = [];
    let i = withOverBox[0].BoxNumber;
    withOverBox.forEach((element) => {
      if (element.BoxNumber !== i) {
        missingIndex.push(i);
        i++;
      }
      i++;
    });
    missingIndex.map((element) => {
      withOverBox.splice(element, 0, {
        BoxNumber: element,
        Actives: 0,
        Cancelleds: 0,
        Orders: 0,
        BoxOverBox: 0,
        TotalRetention: 0,
      });
    });
    resolve(withOverBox);
  });
};

export const getBoxOverBox = async (req, res, next) => {
  try {
    const rows = await boxOverBox();
    res.status(200).send(rows);
  } catch (err) {
    next(err);
  }
};

export const getTotalRetention = async (req, res, next) => {
  try {
    const boxOverBoxRows = await boxOverBox();
    const withTotalRetention = boxOverBoxRows.map((element, index) => {
      element.TotalRetention = 1;
      if (index > 0) {
        element.TotalRetention = boxOverBoxRows[index - 1].TotalRetention * element.BoxOverBox;
      }
      return element;
    });
    res.status(200).send(withTotalRetention);
  } catch (err) {
    next(err);
  }
};

export const normalizeBoxTypes = (boxType) => {
  if (boxType === 'First Box 24 Meals') return '24 Meals Box';
  if (boxType === 'First Box 24 Meals Migrated') return '24 Meals Box';
  if (boxType === 'First Box 12 Meals') return '12 Meals Box';
  return boxType;
};

export const getOrdersWithTrackingIdData = async () => {
  const lastCronId = await Cron.max('id');
  return new Promise(async (resolve, reject) => {
    try {
      const OrdersWithTrackingId = await Order.findAll({
        where: {
          cronId: lastCronId,
          trackingEmail: 'not_sent',
          shipstationStatus: 'shipped',
          trackId: { $ne: null },
        },
        include: [db.User],
      });

      resolve(OrdersWithTrackingId);
    } catch (error) {
      reject(error);
    }
  });
};

export const setUpdateOrderSentEmailTrackingHubSpot = async (OrderId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const OrdersWithTrackingId = await sequelize.query(
          `update orders set tracking_email ='sent' where id = ${OrderId}`, {type: sequelize.QueryTypes.UPDATE},
      );
      resolve(OrdersWithTrackingId);
    } catch (error) {
      reject(error);
    }
  });
};

export const updateEmailStatus = async (orders) => {
  try {
    for (const order of orders) {
      order.set('trackingEmail', 'sent');
      await order.save();
    }
    console.log('all saved');
  } catch (error) {
    console.log(error);
  }
}
