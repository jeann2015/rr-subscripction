import passport from 'passport';
import multer from 'multer';
import { downloadCSV, downloadAllCSV, uploadCSV, getOrders, getOrderById, shipstationSync, shipstationCreateOrders, shipstationSendEmails, hubspotSendContactsToWeeklyList, dimensionsByOrders } from './controller';
import { getBoxOverBox, getTotalRetention } from './plugin';

const upload = multer({ dest: 'uploads/' });

export default (router) => {
  router.get(['/orders', '/orders-all'],
    passport.authenticate('jwt', { session: false }),
    getOrders,
  );

  router.post(['/orders/downloadall'],
    passport.authenticate('jwt', { session: false }),
    downloadAllCSV,
  );

  router.get(['/orders/download'],
    passport.authenticate('jwt', { session: false }),
    downloadCSV,
  );

  router.get('/orders/shipstation-create-orders',
    passport.authenticate('jwt', { session: false }),
    shipstationCreateOrders,
  );

  router.get('/orders/send-notifications',
    passport.authenticate('jwt', { session: false }),
    shipstationSendEmails,
  );

  router.get('/orders/shipstation-sync',
    passport.authenticate('jwt', { session: false }),
    shipstationSync,
  );

  router.get('/orders/:id',
    passport.authenticate('jwt', { session: false }),
    getOrderById,
  );

  router.get('/hubspot-send-contacts-to-weekly-list',
    passport.authenticate('jwt', { session: false }),
    hubspotSendContactsToWeeklyList,
  );

  router.put('/orders/upload',
    passport.authenticate('jwt', { session: false }),
    upload.single('orders'),
    uploadCSV,
  );

  router.get('/orders-box-over-box',
    getBoxOverBox,
  );

  router.get('/orders-total-retention',
    getTotalRetention,
  );

  router.get('/orders-get-dimensions-by-orders-sR2PTBXb4Z6LRYneprkOzNWafrMdz4',
    dimensionsByOrders,
  );

};
