const db = require('../../db/connection');

export const getProducts = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;
  const where = (req.query.deprecated) ? { deprecated: { $eq: req.query.deprecated } } : {};

  db.Product.findAndCountAll({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where,
  })
    .then((products) => {
      res.set('X-Total-Count', products.count);
      res.status(200).send(products.rows);
    })
    .catch(err => next(err));
};

const getUnitaryPrice = (product) => {
  const boxQty = Number.parseInt(product.boxType.split(' '));
  const mealPrice = Math.round(product.price / boxQty) / 100;
  return mealPrice;
};

export const getMealsBoxes = (req, res, next) => {
  db.Product.findAll({ where: { hidden: false } })
    .then((data) => {
      const plain = data.map(meal => meal.get({ plain: true }));
      const withUnitaryPrice = plain.map((product) => {
        product.unitaryPrice = getUnitaryPrice(product);
        return product;
      });
      res.status(200).send(withUnitaryPrice);
    })
    .catch(err => next(err));
};

export const getProductById = (req, res, next) => {
  if (req.params.id === 'both') res.status(200).json({ text: 'Product both' }); // hack for remove error on admin-on-rest
  db.Product.find({
    where: {
      id: req.params.id,
    },
  })
    .then((product) => {
      if (!product) {
        res.status(404).json({ error: 'Product not found' });
        return;
      }
      res.status(200).send(product);
    })
    .catch(err => next(err));
};

/**
 * Returns unitary price of meals for Meals Boxes
 * @returns {array} Objects {boxType, mealPrice}.
 */
export const getMealsUnitaryPrice = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const products = await db.Product.findAll({
        where: { boxType: { $like: '%Meals Box%' } },
      });

      const prices = products.map((product) => {
        return ({
          boxType: product.boxType,
          mealPrice: getUnitaryPrice(product),
        });
      });

      resolve(prices);
    } catch (error) {
      reject(error);
    }
  });
};

export const getMealsUnitaryPriceEndpoint = (req, res, next) => {
  getMealsUnitaryPrice()
    .then(prices => res.status(200).send(prices))
    .catch(error => next(error));
};
