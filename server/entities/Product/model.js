export default (sequelize, DataTypes) => {
  return sequelize.define('product', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    description: DataTypes.STRING,
    boxType: {
      type: DataTypes.STRING,
      field: 'box_type',
    },
    price: DataTypes.INTEGER,
    hidden: DataTypes.BOOLEAN,
    // days: DataTypes.INTEGER,
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
