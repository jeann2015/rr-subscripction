import passport from 'passport';
import { getProducts, getProductById, getMealsUnitaryPriceEndpoint, getMealsBoxes } from './controller';

export default (router) => {
  router.get('/products',
    passport.authenticate('jwt', { session: false }),
    getProducts,
  );

  router.get('/products/meals-price',
    getMealsUnitaryPriceEndpoint,
  );

  router.get('/products/meals-boxes',
    getMealsBoxes,
  );

  router.get('/products/:id',
    passport.authenticate('jwt', { session: false }),
    getProductById,
  );
};
