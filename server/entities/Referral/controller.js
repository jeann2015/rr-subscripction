import json2csv from 'json2csv';
import path from 'path';
import fs from 'fs';
import eachLimit from 'async/eachLimit';
import { getReferralsData } from '../../services/referrals_query';
import { exportCsv } from '../Order/plugin';
import { getUserLastBoxNumber } from '../User/controller';

const db = require('../../db/connection');

const Referral = db.Referral;
const User = db.User;
const Subscription = db.Subscription;

/*
Get users join referrals
*/
export const getReferrals = (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};
  const filters = [];
  let rowFilter = [];

  if (req.query.userReferree) {
    rowFilter = [`referred_by = ${req.query.userReferree}`];
  }

  if (req.query.name) filters.push('name');
  if (req.query.last_name) filters.push('last_name');
  if (req.query.email) filters.push('email');

  if (req.query.name || req.query.last_name || req.query.email) {
    rowFilter = [filters.map(field => `(userReferree.${field} like '%${req.query[field]}%' OR userReferred.${field} like '%${req.query[field]}%')`).join(' AND ')];
  }

  Referral.findAndCountAll({
    order: [[sort, order],
    ],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: rowFilter,
    include: [{
      model: User,
      as: 'userReferree',
      required: true,
    }, {
      model: User,
      as: 'userReferred',
      required: true,
    }, {
      model: Subscription,
      as: 'subscriptionReferree',
      required: true,
    }],
  })
    .then((referrals) => {
      const finalReferrals = [];
      eachLimit(referrals.rows, 1, async (row, done) => {
        const plain = row.get({ plain: true });
        plain.userReferree.boxNumber = await getUserLastBoxNumber(plain.userReferree.id);
        plain.userReferred.boxNumber = await getUserLastBoxNumber(plain.userReferred.id);
        finalReferrals.push(plain);
        done();
      }, () => {
        res.set('X-Total-Count', referrals.count);
        res.status(200).send(finalReferrals);
      });
    })
    .catch(err => next(err));
};

export const getReferralById = (req, res, next) => {
  Referrals.find({
    where: {
      id: req.params.id,
    },
  })
    .then((referral) => {
      if (!referral) {
        res.status(404).send('referral not found');
        return;
      }
      res.status(200).send(referral);
    })
    .catch(err => next(err));
};

export const deleteAllReferral = (user) => {
  return new Promise((resolve, reject) => {
    Referral.update({ referredBy: null }, { where: { referredBy: user.id } })
      .then(() => {
        Referral.destroy({ where: { me: user.id } })
          .then(() => {
            resolve('User referral delete ok.');
          })
          .catch((err) => {
            reject(err.message);
          });
      })
      .catch((err) => {
        reject(err.message);
      });
  });
};

export const downloadCSV = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  const fields = [
    'referrerUserId',
    'referrerName',
    'referrerLastName',
    'referrerEmail',
    'referrerBoxNumber',
    'referrerStatus',
    'referreeUserId',
    'referreeName',
    'referreeLastName',
    'referreeEmail',
    'referreeBoxNumber',
    'referreeStatus',
    'dateReferralUsed',
  ];
  const fieldNames = ['User ID', 'First Name', 'Last Name', 'Email', 'Box #', 'Status', 'User ID', 'First Name', 'Last Name', 'Email', 'Box #', 'Status', 'Date Referral Used'];
  const fileName = 'referrals.csv';

  getReferralsData().then((data) => {
    const opts = {
      data,
      fields,
      fieldNames,
    };

    const csv = `"Referrer Info","","","","","","Referree Info","","","","",""\n ${json2csv(opts)}`;
    fs.writeFile(fileName, csv, (err) => {
      if (err) throw err;
      const file = path.join(fileName);
      res.download(file);
    });
  });
};
