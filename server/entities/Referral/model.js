export default (sequelize, DataTypes) => {
  return sequelize.define('referral', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    referredBy: {
      type: DataTypes.INTEGER,
      field: 'referred_by',
    },
    me: DataTypes.INTEGER,
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
