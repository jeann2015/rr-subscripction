import passport from 'passport';

import { getReferrals, getReferralById, downloadCSV } from './controller';

export default (router) => {
  router.get('/referrals',
    passport.authenticate('jwt', { session: false }),
    getReferrals,
  );

  router.get('/referral/:id',
    passport.authenticate('jwt', { session: false }),
    getReferralById,
  );

  router.get('/referrals/download',
    passport.authenticate('jwt', { session: false }),
    downloadCSV,
  );
};
