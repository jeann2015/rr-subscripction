import { refundCharge } from './plugin';

const db = require('../../db/connection');

const Order = db.Order;

export const refundOrder = async (req, res, next) => {
  try {
    const order = await Order.find({
      charge_token: req.body.charge_token,
    });

    if (!order) {
      res.status(404).send('order not found');
      return;
    }

    try {
      const refundStripe = await refundCharge({
        adminId: req.user.id,
        userId: req.body.user_id,
        chargeToken: req.body.charge_token,
        reason: req.body.reason,
        amount: (req.body.refund * 100), // in cents
        orderId: order.id,
      });

      res.status(200).send(refundStripe);
    } catch (error) {
      return next(error);
    }
  } catch (err) {
    return next(error);
  }
};
