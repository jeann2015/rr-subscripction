export default (sequelize, DataTypes) => {
  return sequelize.define('refund', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    charge_token: DataTypes.STRING,
    refund_id: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    reason: DataTypes.STRING,
    adminId: {
      type: DataTypes.INTEGER,
      field: 'admin_id',
    },
    orderId: {
      type: DataTypes.INTEGER,
      field: 'order_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    freezeTableName: true,
  });
};

