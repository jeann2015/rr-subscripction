import Stripe from 'stripe';
import config from '../../config/config';
import { LogDB } from '../Log/plugin';

const db = require('../../db/connection');

const stripe = Stripe(config.stripeApiKey);
const Refund = db.Refund;


export const refundCharge = (dataToRefund) => {
  const { chargeToken, reason, amount, adminId, orderId, userId } = dataToRefund;

  return new Promise((resolve, reject) => {
    stripe.refunds.create({
      charge: chargeToken,
      reason,
      amount,
    })
      .then((response) => {
        if (response.status === 'succeeded') {
          const logInfo = {
            user: userId,
            admin: adminId,
            type: 'info',
            message: '',
          };

          if (response.errorStripe) {
            logInfo.message = `Refound error -> ${amount / 100}  -> ${refund.errorStripe}`;
            LogDB(logInfo);
            return res.status(409).json({ error: response.errorStripe });
          }

          Refund.create({
            charge_token: chargeToken,
            refund_id: response.id,
            reason,
            amount,
            adminId,
            orderId,
          })
            .then((refundResponse) => {
              logInfo.message = `New refund -> $ ${amount / 100}  -> ${chargeToken} (${refundResponse.refund_id})`;
              LogDB(logInfo);
              return resolve({ success: true, ...response });
            }, (err) => {
              next(err);
            });
        } else {
          reject({ error: true, response });
        }
      })
      .catch(err => reject(err));
  });
};
