import passport from 'passport';

import { refundOrder } from './controller';

export default (router) => {
  router.post('/orders/refund',
    passport.authenticate('jwt', { session: false }),
    refundOrder,
  );
};
