import moment from 'moment';
import { formatUpdatedData } from '../../utils';
import { LogDB } from '../Log/plugin';
import { subscriptionInSkipDate } from './plugin';

const db = require('../../db/connection');

const Skip = db.Skip;

export const clearAllSkipDates = (subscription_id) => {
  return new Promise((resolve, reject) => {
    Skip.destroy({
      where: {
        subscription: subscription_id,
      },
    })
    .then((skip) => {
      resolve();
    })
    .catch(err => reject(err.message));
  });
};

export const getSkipDates = async (subscription_id) => {
  return new Promise((resolve, reject) => {
    Skip.findAll({
      where: {
        subscription: subscription_id,
        /* date: {
          $gt: moment.utc(),
        }, */
      },
      order: [
        ['date', 'ASC'],
      ],
    })
    .then((skip) => {
      const skips = skip.length === 0 ? [] : skip.map(item => item.date);
      resolve(skips);
    })
    .catch(err => reject(err.message));
  });
};

export const updateSkipDates = (subscription_id, dates) => {
  return new Promise((resolve, reject) => {
    // First empty all dates
    Skip.destroy({
      where: {
        subscription: subscription_id,
      },
    })
    .then(() => {
      // Next add the selected
      if (dates) {
        const skip_dates = dates;
        skip_dates.forEach((date) => {
          Skip.findOrCreate({
            where: {
              subscription: subscription_id,
              date: date,
            },
            defaults: {
              subscription: subscription_id,
              date: date,
              createdAt: new Date(),
            },
          }).then((skip, created) => {
            if (created) {
              console.log('skip dates saved');
            }
            resolve();
          })
          .catch((error) => {
            const logInfo = {
              user: null,
              admin: null,
              type: 'error',
              message: `Skip Dates in Subscription #${subscription_id} Could not be saved`,
            };
            LogDB(logInfo);
            reject();
          });
        });
      }
    });
  });
};
