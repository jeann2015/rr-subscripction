import moment from 'moment';

const db = require('../../db/connection');

const Skip = db.Skip;
const sequelize = db.sequelize;

export const subscriptionInSkipDate = (subscriptionId) => {
  return new Promise((resolve, reject) => {
    const crondate = moment().format('YYYY-MM-DD');
    const crondateOneDayAdded = moment().add(1, 'days').format('YYYY-MM-DD');

    Skip.findAll({
      where: {
        date: {
          $gte: crondate,
          $lte: crondateOneDayAdded,
        },
        subscription: subscriptionId,
      },
    })
    .then((skips) => {
      resolve(!(skips.length === 0));
    })
    .catch((error) => {
      reject(error);
    });
  });
};
