import path from 'path';
import moment from 'moment';
import { eachLimit } from 'async';

import { sendEmail } from '../../utils/sendgrid';
import configSendgrid from '../../config/sendgrid';
import { updateIntegration, getSubscriptionStatus, formatUpdatedData, removeTimeFromDate } from '../../utils';
import { calculateNextShipment, standarizeStartFrom } from '../../services/cron_new';
import { LogDB, LogDBUserStatus } from '../Log/plugin';
import { exportCsv } from '../Order/plugin';
import { updateSkipDates, getSkipDates, clearAllSkipDates } from '../Skip/controller';
import { getUserLastBoxNumber } from '../User/controller';
import { sequelize } from '../../db/connection';
import { getNewCycleStartFromOptions } from './plugin';
import { triggerOutboundStatusBySubscriptionId } from '../../services/outbound';

const db = require('../../db/connection');

const Subscription = db.Subscription;
const Product = db.Product;
const User = db.User;

const getStartFrom = (subscription, skipDates) => {
  const days = subscription.frequency * 7;
  const formatSkipDates = skipDates.map(removeTimeFromDate);
  const startFrom = moment(subscription.startFrom).utc().weekday(1);
  while (formatSkipDates.indexOf(String(removeTimeFromDate(startFrom))) !== -1) {
    startFrom.add(days, 'day');
  }
  return startFrom;
};

const getNextShipmentNewVersion = ({ days, skip_date, start_from }) => {
  const skip = skip_date ? skip_date.split(' / ') : [];
  const start = moment.utc(start_from, 'MM-DD-YYYY').hour(9).minute(0).weekday(1);
  const today = moment.utc().hour(9).minute(0);
  const daysForNext = days - (today.diff(start, 'days') % days);
  const nextShipment = today > start ? today.add(daysForNext, 'days') : start;
  while (skip.indexOf(String(nextShipment.format('YYYY-MM-DD'))) !== -1) {
    nextShipment.add(days, 'day');
  }
  return nextShipment.format('MM-DD-YYYY');
};

export const getSubscriptions = async (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;

  const clause = {};
  const userClause = {};

  const eqSubscriptionFields = ['userId', 'status', 'productId'];
  eqSubscriptionFields.forEach((field) => {
    const val = req.query[field];
    if (val) {
      clause[field] = {
        $eq: val,
      };
    }
  });

  const likeUserFields = ['email', 'name', 'lastName', 'phoneNumber', 'zipCode'];
  likeUserFields.forEach((field) => {
    const val = req.query[field];
    if (val) {
      userClause[field] = {
        $like: `%${val}%`,
      };
    }
  });

  const countSubscriptions = await Subscription.count({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: clause,
  });

  Subscription.findAndCountAll({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    include: [{
      model: User,
      where: userClause,
    },
      db.Product,
      db.Skip,
      db.ExcludedIngredients,
    {
      model: db.Order,
      order: [
        ['createdAt', 'DESC'],
      ],
      required: false,
    },
    ],
    where: clause,
  })
    .then(async (subscriptions) => {
      const result = [];
      eachLimit(subscriptions.rows, 1, async (row, done) => {
        const r = row.toJSON();
        const nextShipment = await calculateNextShipment(row);
        const skipDates = await getSkipDates(r.id);
        const startFrom = getStartFrom(r, skipDates);
        const boxNumber = await getUserLastBoxNumber(r.userId);
        result.push({
          ...r,
          skipDates,
          startFrom,
          nextShipment,
          boxNumber,
        });
        done();
      }, () => {
        res.set('X-Total-Count', countSubscriptions);
        res.status(200).send(result);
      });
    })
    .catch(err => next(err));
};

export const getSubscriptionById = (req, res, next) => {
  Subscription.find({
    where: {
      id: req.params.id,
    },
    include: [
      Product,
      {
        model: db.ExcludedIngredients,
        include: [db.Ingredients],
      },
    ],
  })
    .then((subscription) => {
      if (!subscription) {
        res.status(404).send('subscription not found');
        return;
      }
      getSkipDates(req.params.id)
      .then(async (skipDates) => {
        const data = {
          amount: subscription.amount,
          user: { id: subscription.userId },
        };

        const boxNumber = await getUserLastBoxNumber(subscription.userId);
        const newCycleStartFromOptions = await getNewCycleStartFromOptions(req.params.id);

        if (skipDates) {
          const subscriptionWithSkipDates = subscription;
          subscriptionWithSkipDates.dataValues.skipDates = skipDates;
          subscriptionWithSkipDates.dataValues.realStartFrom = standarizeStartFrom(subscriptionWithSkipDates.dataValues.startFrom);
          subscriptionWithSkipDates.dataValues.startFrom = getStartFrom(subscriptionWithSkipDates, skipDates);
          subscriptionWithSkipDates.dataValues.cycles = ['a', 'b', 'c'];
          subscriptionWithSkipDates.dataValues.boxNumber = boxNumber;
          subscriptionWithSkipDates.dataValues.newCycleStartFromOptions = newCycleStartFromOptions;

          res.status(200).send(subscriptionWithSkipDates);
        } else {
          subscription.dataValues.boxNumber = boxNumber;
          subscription.dataValues.newCycleStartFromOptions = newCycleStartFromOptions;
          res.status(200).send(subscription);
        }
      });
    })
    .catch(err => next(err));
};

export const updateSubscription = async (req, res, next) => {
  const dataToUpdate = {
    status: req.body.status,
    amount: req.body.amount,
    // startFrom: req.body.startFrom,
    productId: req.body.productId,
    nextDiscount: req.body.nextDiscount,
    typeNextDiscount: req.body.typeNextDiscount,
    pausedUntil: null,
    cancelReason: '',
    cancellationDate: null,
    skipDates: [],
    newCycleStart: req.body.newCycleStart,
    frequency: req.body.frequency,
    breakFastCount: req.body.breakFastCount,
  };

  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  if (dataToUpdate.status === 'paused' && req.body.pausedUntil) {
    dataToUpdate.pausedUntil = req.body.pausedUntil;
  }

  if (dataToUpdate.status === 'canceled' && req.body.cancelReason) {
    dataToUpdate.cancelReason = req.body.cancelReason;
    dataToUpdate.cancellationDate = new Date();
    triggerOutboundStatusBySubscriptionId(req.body.id, 'Cancel membership');
  }

  if (req.body.skipDates) {
    dataToUpdate.skipDates = req.body.skipDates;
  }


  try {
    // First update the subscription.
    const subscription = await Subscription.findById(req.body.id);
    const subscriptionUpdated = await subscription.update(dataToUpdate);
    const dataUpdated = subscriptionUpdated.get('dataChanged');

    // Trigger Outbound Event for Reactivate Subscriptions
    if (dataUpdated.length > 0) {
      dataUpdated.filter((el) => {
        if (Object.prototype.hasOwnProperty.call(el, 'new_status') && Object.prototype.hasOwnProperty.call(el, 'old_status')) {
          if (el.new_status === 'active') {
            triggerOutboundStatusBySubscriptionId(req.body.id, 'Reactivate Subscription');
          }
        }
      });
    }
    // Load the Name Product by ID
    const shouldLoadProductsName = dataUpdated.filter(f => f.field === 'productId').length > 0;
    if (shouldLoadProductsName) {
      const productsIds = dataUpdated.filter(f => f.field === 'productId');
      const productsInfo = await Product.findAll({
        attributes: ['description', 'id'],
        where: {
          $or: [
            { id: [productsIds[0].new_productId, productsIds[0].old_productId] },
          ],
        },
      });
      dataUpdated.map((f) => {
        if (f.field === 'productId') {
          f.new_productId = productsInfo.filter(r => r.id === f.new_productId)[0].dataValues.description;
          f.old_productId = productsInfo.filter(r => r.id === f.old_productId)[0].dataValues.description;
        }
      });
    }

    logInfo.user = subscriptionUpdated.get('userId');
    logInfo.type = 'info';
    logInfo.message = `#(${req.user.id})~${req.user.name} update the subscription #(${req.body.id}). UPDATING USER.`;

    const changes = subscriptionUpdated.get('dataChanged');

    if (Object.keys(changes).length !== 0) {
      const newStatus = subscriptionUpdated.get('dataChanged')[0].new_status;
      const userId = subscriptionUpdated.get('userId');

      if (newStatus !== undefined) {
        LogDBUserStatus({
          userId: userId,
          userStatus: newStatus,
          userStatusDate: moment(),
        });
      }
    }

    LogDB(logInfo);


    // Update skip table
    if (dataToUpdate.skipDates.length > 0) {
      const oldSkippedDates = await getSkipDates(req.body.id);
      if (oldSkippedDates.length > 0) {
        const newArr = JSON.stringify(dataToUpdate.skipDates);
        const oldArr = JSON.stringify(oldSkippedDates);
        if (newArr !== oldArr) {
          triggerOutboundStatusBySubscriptionId(req.body.id, 'Skipped delivery');
        }
      }
      await updateSkipDates(req.body.id, dataToUpdate.skipDates);
    } else {
      clearAllSkipDates(req.body.id);
    }

    // Time to update the user status.
    const user = await User.find({
      where: {
        id: req.body.userId,
      },
      include: [{
        model: Subscription,
        as: 'subscriptions',
      }],
    });
    const status = getSubscriptionStatus(user.subscriptions);

    const userUpdated = await user.update({ status });
    const formatedUserData = formatUpdatedData(userUpdated.get('dataChanged'));
    if (formatedUserData.length > 0) { // check if change user data
      logInfo.user = userUpdated.get('id');
      logInfo.type = 'info';
      logInfo.message = `#(${req.user.id})~${req.user.name} update the user #(${req.body.userId}), ${JSON.stringify(formatedUserData)} UPDATED USER.`;
      LogDB(logInfo);
    }

    if (status === 'none') {
      try {
        const res = await sendEmail({
          to: configSendgrid.SENDGRID_TO_EMAIL,
          substitutions: {
            body: `
              Name: ${userUpdated.get('name')} <br />
              Lastname: ${userUpdated.get('lastName')} <br />
              E-mail: ${userUpdated.get('email')}
            `,
          },
          subject: 'Alert: User with status None',
          template_id: configSendgrid.templates.rrTemplate,
        });
        console.log('Sendgrid Status None Ok');
      } catch (err) {
        console.log(err);
      }
    }

    updateIntegration(user.id)
      .then((d) => {
        console.log(d);
      })
      .catch(err => console.log(err));
    res.status(200).json({ success: true });
  } catch (err) {
    logInfo.type = 'error';
    logInfo.message = `Database error in updateSubscription(user update attributes) -> Name:${err.name} ~ Message: ${err.message}`;
    LogDB(logInfo);
    return next(err);
  }
};

export const deleteAllSubscriptions = async (user) => {
  await Promise.all(user.subscriptions.map(async (subscription) => {
    await subscription.destroy();
  }));
};

export const downloadCSV = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  sequelize.query(
    `SELECT s.id, s.user_id, u.name, u.last_name, u.email, s.product_id, p.box_type, s.frequency, s.status, s.amount, DATE_FORMAT(s.start_from, '%m-%d-%Y') AS 'start_from', s.paused_until, s.cancel_reason, DATE_FORMAT(s.cancellation_date, '%m-%d-%Y') AS 'cancellation_date', DATE_FORMAT(s.created_at, '%m-%d-%Y') AS 'created_at', DATE_FORMAT(s.updated_at, '%m-%d-%Y') AS 'updated_at', s.next_discount, s.type_next_discount, s.coupon_id,
    COALESCE((SELECT box_number FROM \`order\`
        WHERE \`order\`.box_type NOT LIKE '%Babycook%' AND \`order\`.box_type NOT LIKE '%Meal Maker%' AND \`order\`.box_type NOT LIKE 'Pre-Cron Box' AND \`order\`.user_id = s.user_id
        ORDER BY \`created_at\` DESC
        LIMIT 1), 0) AS box_number,
        p.days,
    (SELECT GROUP_CONCAT(DISTINCT DATE_FORMAT(skip.date, '%m-%d-%Y') SEPARATOR ' / ') FROM skip WHERE skip.subscription = s.id GROUP BY subscription) as skip_date
    FROM \`subscription\` AS s
    INNER JOIN \`user\` AS u ON s.user_id = u.id
    INNER JOIN \`product\` AS p ON s.product_id = p.id`,
    { type: sequelize.QueryTypes.SELECT },
    )
    .then((subscriptions) => {
      const data = [];
      const fields = ['id', 'user_id', 'box_type', 'frequency', 'name', 'last_name', 'email', 'status', 'amount', 'start_from', 'paused_until', 'cancel_reason', 'cancellation_date', 'created_at', 'updated_at', 'next_discount', 'type_next_discount', 'coupon_id', 'box_number', 'days', 'next_shipment', 'skip_date'];
      const fieldNames = ['ID', 'User ID', 'Product', 'Frequency', 'Name', 'Lastname', 'Email', 'Status', 'Amount', 'Start From', 'Paused Until', 'Cancel Reason', 'Cancellation Date', 'Created At', 'Updated At', 'Next Discount', 'Type Next Discount', 'Coupon ID', 'Box #', 'Days', 'Next Shipment', 'Skipped Dates'];

      subscriptions.map((subscription) => {
        subscription.next_shipment = subscription.status === 'active' ? getNextShipmentNewVersion(subscription) : null;
        data.push(subscription);
      });

      const opts = {
        fields,
        fieldNames,
        data,
      };

      const filename = 'subscription.csv';
      exportCsv(opts, filename, () => {
        const file = path.join(filename);
        res.download(file);
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} download the subscription csv.`;
        LogDB(logInfo);
      });
    });
};

export const getSubscriptionIdByUserId = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT id
        FROM subscription
          WHERE user_id = ${req.params.userId}
    `, { type: sequelize.QueryTypes.SELECT });
    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};

export const ReactivationsCancelsSkipsPerShipment = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT
      DATE(created_at) as ShippingDate,
      (SELECT
        COUNT(DISTINCT user_id)
        FROM log
        WHERE
          message LIKE "%changed to active%" AND
          log.created_at < ShippingDate AND
          log.created_at > DATE_SUB(ShippingDate, INTERVAL 14 DAY)

      ) as Reactivations,

        (SELECT
        COUNT(DISTINCT user_id)
        FROM log
        WHERE
          message LIKE "%changed to cancel%" AND
          log.created_at < ShippingDate AND
          log.created_at > DATE_SUB(ShippingDate, INTERVAL 14 DAY)

      ) as Cancellations,

        (SELECT
        COUNT(DISTINCT user_id)
        FROM log
        WHERE
          message LIKE "%Subscription has Skip Dates" AND
          DATE(log.created_at) = ShippingDate

      ) as Skips

      FROM cron
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};
