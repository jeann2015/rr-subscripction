export default (sequelize, DataTypes) => {
  return sequelize.define('subscription', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      allowNull: false,
    },
    couponId: {
      type: DataTypes.INTEGER,
      field: 'coupon_id',
    },
    productId: {
      type: DataTypes.INTEGER,
      field: 'product_id',
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    startFrom: {
      type: DataTypes.DATE,
      field: 'start_from',
    },
    pausedUntil: {
      type: DataTypes.DATE,
      field: 'paused_until',
    },
    cancelReason: {
      type: DataTypes.STRING,
      field: 'cancel_reason',
    },
    nextDiscount: {
      type: DataTypes.INTEGER,
      field: 'next_discount',
    },
    typeNextDiscount: {
      type: DataTypes.STRING,
      field: 'type_next_discount',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
    couponUseCount: {
      type: DataTypes.INTEGER,
      field: 'coupon_use_count',
      defaultValue: 0,
    },
    cancellationDate: {
      type: DataTypes.DATE,
      field: 'cancellation_date',
    },
    captureToken: {
      type: DataTypes.STRING,
      field: 'capture_token',
    },
    newCycleStart: {
      type: DataTypes.DATE,
      field: 'new_cycle_start',
    },
    frequency: {
      type: DataTypes.INTEGER,
      field: 'frequency',
    },
    breakFastCount: {
      type: DataTypes.INTEGER,
      field: 'breakfast_count',
    },
  }, {
    freezeTableName: true,
  });
};
