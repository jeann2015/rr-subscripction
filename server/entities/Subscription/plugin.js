import { sequelize } from '../../db/connection';
import { getCronDate } from '../../services/cron_new';

const moment = require('moment');
const db = require('../../db/connection');

const Subscription = db.Subscription;
const Log = db.Log;
const Order = db.Order;

export const getPausedUntil = () => {
  const now = moment();
  return Subscription.findAll({
    where: {
      pausedUntil: {
        $lt: now,
      },
    },
  });
};

export const getStartFrom = (subscriptions) => {
  subscriptions.forEach((subscription) => {
    const startFrom = moment.utc(subscription.get('startFrom'));
    subscription.startFrom = startFrom.weekday(1).format('MM/DD/YYYY');
  });
  return subscriptions;
};

const getSubscriptionsActiveByMonth = () => sequelize.query(`
    SELECT 
      count(id) as Subscriptions,
      CASE s.status
            WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.start_from)/30)
            WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.start_from)/30)
            ELSE NULL
        END AS TimeActive
    FROM
      \`subscription\` as s
    WHERE
      s.status = 'canceled' OR s.status = 'active'
    GROUP BY
      TimeActive
`, { type: sequelize.QueryTypes.SELECT });

export const getSubscriptionsTimeActive = async (req, res, next) => {
  try {
    let subscriptionActiveTime = await getSubscriptionsActiveByMonth();

    // remove wrong `TimeActive`
    subscriptionActiveTime = subscriptionActiveTime.filter(e => e.TimeActive >= 0);

    subscriptionActiveTime = subscriptionActiveTime.map((e, i) => {
      let boxToBoxRetention = 1;
      let totalRetention = 1;

      if (i > 0) {
        totalRetention = e.Subscriptions / subscriptionActiveTime[0].Subscriptions;
        boxToBoxRetention = e.Subscriptions / subscriptionActiveTime[i - 1].Subscriptions;
      }
      return { ...e, boxToBoxRetention, totalRetention };
    });
    return res.status(200).send(subscriptionActiveTime);
  } catch (err) {
    return next(err);
  }
};

export const getNewCycleStartFromOptions = async (subsId) => {
  const dateOptions = [];

  // Get user's last order date
  const lastOrder = await Order.findOne({
    where: {
      subscriptionId: subsId,
    },
    order: [
      ['boxNumber', 'DESC'],
    ],
  });

  if (lastOrder) {
    const order = moment(lastOrder.get('createdAt')).format('MM-DD-YYYY');
    const orderDateObj = {
      id: order,
      name: `Last order: ${order}`,
    };
    dateOptions.push(orderDateObj);
  }

  // get next 10 Cron dates
  for (let index = 0; index < 10; index++) {
    const cronDate = moment(getCronDate(index)).format('MM-DD-YYYY');
    const dateObj = {
      id: cronDate,
      name: `${cronDate} Cron`,
    };
    dateOptions.push(dateObj);
  }

  return dateOptions;
};
