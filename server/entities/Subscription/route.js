import passport from 'passport';
import { getSubscriptions, getSubscriptionById, updateSubscription, downloadCSV, ReactivationsCancelsSkipsPerShipment, getSubscriptionIdByUserId } from './controller';
import { getSubscriptionsTimeActive } from './plugin';

export default (router) => {
  router.get('/subscriptions',
    passport.authenticate('jwt', { session: false }),
    getSubscriptions,
  );

  router.get('/subscriptions/download',
    passport.authenticate('jwt', { session: false }),
    downloadCSV,
  );
  router.get('/subscriptions/:id',
    passport.authenticate('jwt', { session: false }),
    getSubscriptionById,
  );
  router.put('/subscriptions/:id',
    passport.authenticate('jwt', { session: false }),
    updateSubscription,
  );
  router.get('/subscriptions-box-to-box-time-active',
    getSubscriptionsTimeActive,
  );
  router.get('/reactivations-cancels-and-skips-per-shipment',
    ReactivationsCancelsSkipsPerShipment,
  );
  router.get('/subscription/subscriptionIdByUser/:userId',
    getSubscriptionIdByUserId,
  );
};
