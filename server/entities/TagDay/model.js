export default (sequelize, DataTypes) => {
  return sequelize.define('tag_day', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    zipCode: {
      type: DataTypes.STRING,
      field: 'zip_code',
    },
    name: DataTypes.INTEGER,
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
