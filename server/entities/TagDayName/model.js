export default (sequelize, DataTypes) => {
  return sequelize.define('tag_day_name', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    transit: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    warehouseId: {
      type: DataTypes.INTEGER,
      field: 'warehouse_id',
      allowNull: false,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
