import moment from 'moment';
import each from 'async/each';
import eachLimit from 'async/eachLimit';
import path from 'path';
import json2csv from 'json2csv';
import fs from 'fs';
import { sequelize } from '../../db/connection';

import { sendEmail } from '../../utils/sendgrid';
import { sendHubspotForm } from '../../utils/hubspot';
import configSendgrid from '../../config/sendgrid';
import configHubspot from '../../config/hubspot';
import { createCard, updateDefaultCard, deleteStripeCard, createUser, updateBillingInfo, setCard, updateUserStatusActive, generateFirstCharge, checkUSAZipcodeWithData, sendConfirmationEmail } from './plugin';
import { getSubscriptionStatus, updateIntegration, deleteContact, formatUpdatedData } from '../../utils';
import { triggerOutboundEvent } from '../../utils/outbound';
import { setCouponToSubscription, redeemCoupon, calcCouponDiscount } from '../Coupon/plugin';
import { LogDB, LogDBUserStatus } from '../Log/plugin';
import { deleteAllChilds } from '../Child/controller';
import { deleteAllOrders } from '../Order/controller';
import { deleteAllSubscriptions } from '../Subscription/controller';
import { deleteAllReferral } from '../Referral/controller';
import { exportCsv } from '../Order/plugin';
import { upsertZendesk, isUserCreatedOnZendesk } from '../../utils/zendesk';
import { simulateCron } from '../../services/cron_simulator';

import config from '../../config/config';

const db = require('../../db/connection');

const User = db.User;
const Child = db.Child;
const Order = db.Order;
const Subscription = db.Subscription;
const Coupon = db.Coupon;
const Referral = db.Referral;
const AppliedDiscounts = db.AppliedDiscounts;
const Cron = db.Cron;
const Log = db.Log;

export const getUserLastBoxNumber = (userId) => {
  return new Promise((resolve, reject) => {
    Order.findAll({  // TODO: replace with findOne
      where: {
        userId,
        $and: [
          { boxType: { $notLike: 'Meal Maker' } },
          { boxType: { $notLike: '%Babycook%' } },
        ],
      },
      order: [
        ['boxNumber', 'DESC'],
      ],
    })
    .then((orders) => {
      if (orders.length === 0) {
        return resolve(0);
      }
      const lastBoxNumber = orders[0].boxNumber;
      return resolve(lastBoxNumber);
    })
    .catch(err => reject(err));
  });
};

export const updateCreditCard = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: null,
    type: '',
    message: '',
  };
  const user = req.body.user;
  logInfo.user = user.id;
  logInfo.type = 'info';
  logInfo.message = '"Change Credit Card" link was successfully applied';
  LogDB(logInfo);
  setCard(user.id, req.body, true)
    .then((operationInfo) => {
      if (operationInfo.errorStripe) {
        logInfo.type = 'error';
        logInfo.message = `#(${user.id})~${user.name} ${user.lastName} has an (stripe) error with setting credit card: ${JSON.stringify(operationInfo.errorStripe)}`;
        LogDB(logInfo);
        return res.status(402).json({
          stripe_error: operationInfo.errorStripe,
          user_hash: user.hash,
        });
      }
      if (operationInfo.error) {
        logInfo.type = 'error';
        logInfo.message = `#(${user.id})~${user.name} ${user.lastName} has an (platform) error with setting credit card: ${operationInfo.error}`;
        return res.status(409).json(operationInfo.error);
      }
      return res.status(200).send({
        id: user.id,
        status: 'ok',
      });
    })
    .catch(err => next(err));
};

// Edit user
export const updateCheckout = async function (req, res, next) {
  const logInfo = {
    user: null,
    admin: null,
    type: '',
    message: '',
  };
  try {
    const user = await updateBillingInfo(req, res, next);
    logInfo.user = user.id;
    createCard(user.customerId, req.body.token)
      .then((card) => {
        updateDefaultCard(user.customerId, card.id)
          .then(async (customer) => {
            const infoCharge = {
              code: null,
              discount: null,
              extra_info: null,
              finalAmount: 0,
            };

            // Simulate order
            const subscription = await db.Subscription.findAll({
              where: { userId: user.get('id') },
              include: [
                db.User,
                db.Product,
                db.Coupon,
                db.Skip,
                {
                  model: db.Order,
                  limit: 1,
                  order: [
                    ['createdAt', 'DESC'],
                  ],
                  required: false,
                },
                {
                  model: db.Order,
                  as: 'orderCount',
                  attributes: ['cronId'],
                  where: {
                    boxType: {
                      $not: 'Meal Maker',
                    },
                  },
                  required: false,
                },
              ],
            });

            subscription.forEach((s) => {
              s.set('status', 'active');
              LogDBUserStatus({
                userId: user.id,
                userStatus: 'active',
                userStatusDate: moment(),
              });
            });

            const appliedDiscounts = [];
            const simulation = await simulateCron(subscription, moment(subscription[0].get('startFrom')).startOf('day'));
            const mealOrders = simulation.filter(o => o.boxType === 'First Box');

            const referral = await Referral.findOne({ where: { me: user.get('id') } });
            const boxNumberByUser = await getUserLastBoxNumber(user.get('id'));

            if (mealOrders.length > 0 && user.get('firstCharge') === null && boxNumberByUser === 0) {
              let subscriptionPrice = 0;
              let isCouponForever = false;
              let promotion = 0;
              mealOrders.forEach((order) => {
                subscriptionPrice += order.productPrice;
                if (referral) {
                  subscriptionPrice -= config.referralFirstDiscount / mealOrders.length;
                }
              });

              if (referral) {
                infoCharge.code = 'Discount by referral';
                infoCharge.discount = 'USD 20';
                appliedDiscounts.push({
                  type: 'subscriptionReferral',
                  amount: config.referralFirstDiscount / subscription.length,
                  referreer: user.get('id'),
                });
              }

              const coupon = subscription[0].get('coupon');
              // infoCharge.finalAmount += productPrice * amount
              if (coupon) {
                if (coupon.get('duration') === 'forever') isCouponForever = true;
              }

              if (coupon && (coupon.get('productId') === null || coupon.get('productId') === subscription[0].get('product').get('id'))) {
                const amountOff = coupon.get('amountOff') / subscription.length;
                const percentOff = coupon.get('percentOff');

                try {
                  await redeemCoupon(coupon, infoCharge);
                } catch (err) {
                  console.log(err);
                }

                logInfo.type = 'info';
                logInfo.message = `Coupon ${coupon.get('code')} was redeemed by ${user.get('name')} ${user.get('lastName')}`;
                LogDB(logInfo);

                infoCharge.code = coupon.get('code');
                infoCharge.discount = (amountOff) ? `USD ${amountOff / 100}` : `${percentOff}%`;
                promotion += subscriptionPrice - calcCouponDiscount(percentOff, amountOff, subscriptionPrice);
                appliedDiscounts.push({
                  type: 'subscriptionCoupon',
                  couponId: coupon.get('id'),
                  amount: subscriptionPrice - calcCouponDiscount(percentOff, amountOff, subscriptionPrice),
                });
                subscriptionPrice = calcCouponDiscount(percentOff, amountOff, subscriptionPrice);
              }

              infoCharge.finalAmount = subscriptionPrice;

              const logPrice = subscriptionPrice < 0 ? 0 : subscriptionPrice;

              try {
                const chargeResponse = await generateFirstCharge(user.customerId, infoCharge, appliedDiscounts);
                user.set('firstCharge', chargeResponse.id);
                user.set('firstChargeAmount', infoCharge.finalAmount);
                await user.save();
                if (referral) {
                  logInfo.type = 'info';
                  logInfo.message = 'U$S 20 discount by referral was applied.';
                  LogDB(logInfo);
                }
                logInfo.type = 'info';
                logInfo.message = `First charge was applied on "Change Credit Card" link (U$S ${logPrice}) with id ${chargeResponse.id}.`;
                LogDB(logInfo);

                // send confirmation email
                sendConfirmationEmail(user.get('id'));
              } catch (err) {
                if (err) console.log(err.message);
                logInfo.type = 'error';
                logInfo.message = `First charge couldn't be applied on "Change Credit Card" link (U$S ${logPrice}). Error: ${err.message}.`;
                LogDB(logInfo);
              }
            }


            updateUserStatusActive(user.id)
              .then(async () => {
                const logInfo = {
                  user: null,
                  admin: null,
                  type: '',
                  message: '',
                };
                logInfo.user = user.id;
                logInfo.type = 'info';
                logInfo.message = 'CC updated OK';
                LogDB(logInfo);

                // Outbound Event 'Signup'
                const outboundId = await User.find({
                  where: {
                    id: user.get('id'),
                  },
                  attributes: ['outbound'],
                });

                triggerOutboundEvent({
                  user: outboundId.get('outbound'),
                  event: 'Signup',
                })
                  .then(() => console.log(`Outbound Trigger Event Signup Ok! (${user.get('id')})`))
                  .catch(() => console.log(`Outbound Trigger Event Signup Failed! (${user.get('id')})`));

                res.status(200).send({ status: customer });
              })
              .catch(err => next(err));
          })
          .catch(err => next(err));
      })
      .catch(err => next(err));
  } catch (error) {
    return next(error);
  }
};

// Create a new user
export const checkoutUser = async function (req, res, next) {
  req.assert('name', 'Name is required').notEmpty();
  req.assert('lastName', 'Lastname is required').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  // req.assert('zipCode', 'Only San Franciso zip codes').isValidZipCode();
  req.assert('child', 'Expected array').isArray();
  req.assert('token', 'Token is required').notEmpty();
  const logInfo = {
    user: null,
    admin: null,
    type: '',
    message: '',
  };
  try {
    const result = await req.getValidationResult();
    const errors = result.array();
    if (errors.length > 0) {
      return res.status(400).send(errors);
    }
    const userReq = await setCouponToSubscription(req.body);
    if (userReq.error) {
      return res.status('409').json({ error: userReq.error });
    }

    const user = await createUser(userReq.data);
    if (!user) {
      return res.status('409').json({ error: 'User repeated' });
    }
    logInfo.user = user.get('id');
    logInfo.type = 'info';
    logInfo.message = `New user -> #(${user.get('id')})~${user.get('name')} ${user.get('lastName')}`;
    LogDB(logInfo);
    setCard(user.get('id'), req.body)
      .then((operationInfo) => {
        if (operationInfo.errorStripe) {
          logInfo.type = 'error';
          logInfo.message = `#(${user.get('id')})~${user.get('name')} ${user.get('lastName')} has an (stripe) error with setting credit card: ${JSON.stringify(operationInfo.errorStripe)}`;
          LogDB(logInfo);
          return res.status(402).json({
            stripe_error: operationInfo.errorStripe,
            user_hash: user.get('hash'),
          });
        }
        if (operationInfo.error) {
          logInfo.type = 'error';
          logInfo.message = `#(${user.get('id')})~${user.get('name')} ${user.get('lastName')} has an (platform) error with setting credit card: ${operationInfo.error}`;
          return res.status(409).json(operationInfo.error);
        }
        const referralCode = userReq.data.referralCode ? userReq.data.referralCode : null;

        updateIntegration(user.get('id'), referralCode)
          .then(async (msg) => {
            console.log(msg);

            // Outbound Event 'Signup'
            const outboundId = await User.find({
              where: {
                id: user.get('id'),
              },
              attributes: ['outbound'],
            });

            triggerOutboundEvent({
              user: outboundId.get('outbound'),
              event: 'Signup',
            })
              .then(() => console.log(`Outbound Trigger Event Signup Ok! (${user.get('id')})`))
              .catch(() => console.log(`Outbound Trigger Event Signup Failed! (${user.get('id')})`));
          })
          .catch(err => console.log(err));

        return res.status(200).send({
          id: user.get('id'),
          status: 'ok',
        });
      })
      .catch(err => next(err));
  } catch (err) {
    logInfo.type = 'error';
    logInfo.message = `Database error in checkoutUser -> Name:${err.name} ~ Message: ${err.message}`;
    LogDB(logInfo);
    return next(err);
  }
};

// Return de number of users
export const usersCount = (req, res, next) => {
  User.findAndCountAll().then((result) => {
    res.status(200).send({ count: result.count });
  }, (err) => {
    next(err);
  });
};

const addLastBoxNumberToUser = async (user) => {
  const plainUser = user.get({ plain: true });
  const boxNumber = await getUserLastBoxNumber(plainUser.id);
  plainUser.boxNumber = boxNumber;
  return plainUser;
};

// Valid email
export const validEmail = (req, res, next) => {
  req.assert('email', 'Email is not valid').isEmail();
  req.getValidationResult().then((result) => {
    const { email } = req.body;
    const errors = result.array();
    if (errors.length > 0) {
      res.status(400).send(errors);
    } else {
      User.findAll({
        where: {
          email: req.body.email,
        },
      }).then(async (user) => {
        if (user.length === 0) {
          res.status(200).send({ valid: true });
          // Send new lead to zendesk

          sendHubspotForm(configHubspot.portalId, configHubspot.forms.signup, {
            email,
            // firstname: name,
            // lastname: lastName,
            // zip: zipCode,
            lead_status: 'Qualified',
          })
            .then(() => console.log(`Hubspot Form Signup > Submit form for ${req.body.email} success`))
            .catch(err => console.log(`Hubspot Form Signup > Can't submit form for ${req.body.email}`));

          const zendeskData = { email, lead_status: 'qualified' };
          try {
            const userHasNameOnZendesk = await isUserCreatedOnZendesk(req.body.email);
            if (!userHasNameOnZendesk) { zendeskData.name = 'unnamed'; }
          } catch (err) {
            console.log(err);
          }
          upsertZendesk(zendeskData).then(() => {
            console.log('Email validation => Upsert zendesk OK');
          })
            .catch(err => console.log(err));
          return;
        }
        res.status(409).send('Email repeated');
      })
        .catch((err) => {
          next(err);
        });
    }
  });
};

export const outOfAreaLead = (req, res, next) => {
  // req.assert('name', 'Name is required').notEmpty();
  // req.assert('lastName', 'Lastname is required').notEmpty();
  // req.assert('email', 'Email is not valid').isEmail();
  // req.assert('zipCode', 'Only San Franciso zip codes').isInt();
  req.getValidationResult().then((result) => {
    const { name, lastName, email, zipCode, state } = req.body;
    const errors = result.array();
    if (errors.length > 0) {
      res.status(400).send(errors);
    } else {
      User.findAll({
        where: {
          email: req.body.email,
        },
      }).then((user) => {
        if (user.length === 0) {
          res.status(200).send({ valid: true });
          // Send new lead to zendesk
          sendHubspotForm(configHubspot.portalId, configHubspot.forms.signup, {
            email: email,
            firstname: name,
            lastname: lastName,
            zip: zipCode,
            lead_status: 'Out of Area',
          })
            .then(() => console.log(`Hubspot Form Signup > Submit form for ${req.body.email} success`))
            .catch(err => console.log(`Hubspot Form Signup > Can't submit form for ${req.body.email}`));

          upsertZendesk({
            email,
            name: `${name}  ${lastName}`,
            user_fields: {
              lead_status: 'out_of_area',
              zipcode: zipCode,
            },
          }).then(() => {
            console.log('Email validation => Upsert zendesk OK');
          })
            .catch(err => console.log(err));

          // notify RR Team Out of Area
          sendEmail({
            to: configSendgrid.SENDGRID_TO_EMAIL,
            substitutions: {
              body: `
                Name: ${name}<br />
                Lastname: ${lastName} <br />
                E-mail: ${email} <br />
                Zipcode: ${String(zipCode)} <br />
                State: ${state}
              `,
            },
            subject: 'Alert: Zipcode Out Of Area',
            template_id: configSendgrid.templates.rrTemplate,
          })
            .then(() => {
              console.log('Sendgrid Notification Out of area>> Success!');
            })
            .catch(err => console.log(`Sendgrid Notification Out of area>> Error! ${err}`));


          /* createHubspotContact({
            email: email,
            firstname: name,
            lastname: lastName,
            zip: zipCode,
            hs_lead_status: 'Out of Area',
          })
          .then(() => {
            console.log('Email validation => Upsert Hubspot OK');
          })
          .catch(err => console.log(err)); */

          return;
        }
        res.status(409).send('Email repeated');
      })
        .catch((err) => {
          next(err);
        });
    }
  });
};

export const getUsers = async (req, res, next) => {
  const sort = (req.query._sort) ? req.query._sort : 'id';
  const order = (req.query._order) ? req.query._order : 'DESC';
  const end = (req.query._end) ? req.query._end : 10;
  const offset = (req.query._start) ? req.query._start : 0;
  const lastCronId = await Cron.max('id');
  let clause = {};

  if (req.query.email) {
    clause.email = {
      $like: `%${req.query.email}%`,
    };
  }
  if (req.query.name) {
    clause.name = {
      $like: `%${req.query.name}%`,
    };
  }
  if (req.query.lastName) {
    clause.lastName = {
      $like: `%${req.query.lastName}%`,
    };
  }
  if (req.query.phoneNumber) {
    clause = [`replace(replace(replace(replace(replace(phone_number, '-', ''), '(', ''), '(', ''), ')', ''), ' ', '')
               LIKE '%${req.query.phoneNumber.replace(/[^\d,]+/g, '')}%'`, []];
  }

  if (req.query.zipCode) {
    clause.zipCode = {
      $like: `%${req.query.zipCode}%`,
    };
  }
  if (req.query.status) {
    clause.status = {
      $eq: req.query.status,
    };
  }
  if (req.query.q) {
    clause.email = {
      $like: `%${req.query.q}%`,
    };
  }

  const countUsers = await User.count({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: clause,
  });

  User.findAndCountAll({
    order: [[sort, order]],
    limit: Number(end) - Number(offset),
    offset: Number(offset),
    where: clause,
    include: [{
      model: Order,
      where: {
        $and: [
          { boxType: { $notLike: '%Babycook%' } },
          { boxType: { $notLike: '%Meal Maker%' } },
        ],
      },
      required: false,
    }, {
      model: AppliedDiscounts,
      where: {
        coupon: {
          $not: null,
        },
      },
      required: false,
      order: '"updatedAt" DESC',
      as: 'couponUsed',
      include: {
        model: Coupon,
        as: 'couponApplied',
      },
    }, {
      model: AppliedDiscounts,
      where: {
        nextDiscountType: {
          $not: null,
        },
        cronId: lastCronId,
      },
      required: false,
      as: 'nextDiscount',
    }, {
      model: Referral,
      as: 'userReferree',
      required: false,
    }, {
      model: Subscription,
      required: false,
    }],
  }).then((user) => {
    res.set('X-Total-Count', countUsers);
    res.status(200).send(user.rows);
  })
    .catch((err) => {
      next(err);
    });
};

export const updateUsersStatus = (req, res, next) => {
  const logInfo = {
    user: null,
    admin: null,
    type: '',
    message: '',
  };
  User.findAndCountAll({
    include: [Subscription],
  }).then((users) => {
    each(users.rows, (u, cb) => {
      const subscriptions = u.get('subscriptions');
      const newStatus = getSubscriptionStatus(subscriptions);
      u.set('status', newStatus);
      LogDBUserStatus({
        userId: u.id,
        userStatus: newStatus,
        userStatusDate: moment(),
      });
      u.save().then((userSaved) => {
        logInfo.type = 'info';
        logInfo.message = `${userSaved.get('name')} ${userSaved.get('lastName')} has new status, ${newStatus}`;

        if (newStatus === 'none') {
          try {
            const res = sendEmail({
              to: configSendgrid.SENDGRID_TO_EMAIL,
              substitutions: {
                body: `
                    Name: ${userSaved.get('name')} <br />
                    Lastname: ${userSaved.get('lastName')} <br />
                    E-mail: ${userSaved.get('email')}
                `,
              },
              subject: 'Alert: User with status None',
              template_id: configSendgrid.templates.rrTemplate,
            });
          } catch (err) {
            console.log(err);
          }
        }

        cb();
      });
    });
    res.status(200).send({ success: true });
  }, (err) => {
    next(err);
  });
};

// get user by id
export const getUserById = async (req, res, next) => {
  User.find({
    where: {
      id: req.params.id,
    },
    include: [Child],
  })
    .then(async (user) => {
      if (!user) {
        res.status(404).send('User not found');

        return;
      }
      const userWithBoxNumber = await addLastBoxNumberToUser(user);
      res.status(200).send(userWithBoxNumber);
    })
    .catch(err => next(err));
};

// get user by id
export const getUserByHash = (req, res, next) => {
  User.find({
    where: {
      hash: req.params.hash,
    },
    include: [Child],
  })
    .then((user) => {
      if (!user) {
        res.status(404).send('User not found');
        return;
      }

      res.status(200).send(user);
    })
    .catch(err => next(err));
};


export const updateUser = async (req, res, next) => {
  req.assert('zipCode', 'Zipcode is invalid').isValidZipCode();
  const logInfo = {
    user: req.body.id,
    admin: req.user.id,
    type: '',
    message: '',
  };
  try {
    const result = await req.getValidationResult();
    const errors = result.array();
    if (errors.length > 0) {
      return res.status(400).send(errors);
    }
    const dataToUpdate = {
      name: req.body.name,
      lastName: req.body.lastName,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      address: req.body.address,
      addressExtra: req.body.addressExtra,
      city: req.body.city,
      state: req.body.state,
      zipCode: req.body.zipCode,
    };
    const user = await User.findById(req.body.id);
    const oldEmail = user.get('email');
    const userUpdated = await user.update(dataToUpdate);
    const formatedData = formatUpdatedData(userUpdated.get('dataChanged'));
    logInfo.type = 'info';
    logInfo.message = `#(${req.user.id})~${req.user.name} update a user -> #(${req.body.id})~${JSON.stringify(formatedData)}`;
    LogDB(logInfo);
    const integrationStatus = await updateIntegration(req.body.id);
    res.status(200).json({ success: true });
  } catch (err) {
    logInfo.type = 'error';
    logInfo.message = `Database error in updateUser -> Name:${err.name} ~ Message: ${err.message}`;
    LogDB(logInfo);
    return next(err);
  }
};

const ZIPCODE_EXCEPTIONS = {
  33198: {
    city: 'Medley',
    state: 'FL',
    zip: '33198',
  },
};

export const checkZipcode = (req, res, next) => {
  const zipCode = req.body.zipCode;
  if (zipCode === '') return res.status(200).send({ valid: false });
  const zipCodeData = checkUSAZipcodeWithData(zipCode);
  if (zipCodeData === false) {
    if (Object.prototype.hasOwnProperty.call(ZIPCODE_EXCEPTIONS, zipCode)) {
      const zipCodeExceptionPayload = ZIPCODE_EXCEPTIONS[zipCode];
      return res.status(200).send({ valid: true, data: zipCodeExceptionPayload });
    }
    return res.status(200).send({ valid: false });
  }
  return res.status(200).send(zipCodeData);
};

export const setNoPaymentLead = async function (req, res, next) {
  req.assert('email', 'Email is not valid').isEmail();
  try {
    const result = await req.getValidationResult();
    const errors = result.array();
    if (errors.length > 0) {
      return res.status(400).send(errors);
    }

    res.status(200).send({ status: 'ok' });

    const { email, name, lastName, address, city, state, zipCode, phoneNumber } = req.body;
    const fieldsHubspot = {
      email,
      city,
      firstname: name,
      lastname: lastName,
      lead_status: 'No Payment',
      zip: zipCode,
      phone: phoneNumber,
      state,
      address: address,
    };
    const customFieldsZendesk = {
      lead_status: 'no_payment',
    };

    const childs = req.body.childs;

    if (childs.length > 0) {
      const firstChild = childs[0];
      customFieldsZendesk.kid_1_name = firstChild.name;
      customFieldsZendesk.kid_1_dob = moment.utc(firstChild.birthDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
      const hbKid1Dob = new Date(childs[0].birthdate);
      hbKid1Dob.setUTCHours(0);
      hbKid1Dob.setUTCMinutes(0);
      hbKid1Dob.setUTCSeconds(0);
      fieldsHubspot.kid_1_dob = hbKid1Dob.getTime();
      fieldsHubspot.kid_1_name = childs[0].name;
    }

    if (childs.length > 1) {
      const secondChild = childs[1];
      customFieldsZendesk.kid_2_name = secondChild.name;
      customFieldsZendesk.kid_2_dob = moment.utc(secondChild.birthDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
      const hbKid1Dob = new Date(childs[1].birthdate);
      hbKid1Dob.setUTCHours(0);
      hbKid1Dob.setUTCMinutes(0);
      hbKid1Dob.setUTCSeconds(0);
      fieldsHubspot.kid_1_dob = hbKid1Dob.getTime();
      fieldsHubspot.kid_1_name = childs[1].name;
    }

    sendHubspotForm(configHubspot.portalId, configHubspot.forms.signup, fieldsHubspot)
      .then(() => console.log(`Hubspot Form Signup > Submit form for ${req.body.email} success`))
      .catch(err => console.log(`Hubspot Form Signup > Can't submit form for ${req.body.email}`));

    upsertZendesk({
      email: req.body.email,
      user_fields: customFieldsZendesk,
    })
      .then(() => {
        console.log('No payment lead => Upsert zendesk OK');
      })
      .catch(err => console.log(err.result));
  } catch (error) {
    console.log('Error', error);
    return next(error);
  }
};

export const deleteUser = (req, res, next) => {
  User.findOne({
    where: {
      id: req.params.id,
    },
    include: [Child, Order, Subscription],
  })
    .then(async (user) => {
      const { email, customerId } = user;

      try {
        if (!user) {
          res.status(404).json({ message: 'User not found' });
        }

        await deleteAllChilds(user);
        await deleteAllOrders(user);
        await deleteAllSubscriptions(user);
        const status = await deleteAllReferral(user);
        // console.log(status);

        const userDestroyed = await User.destroy({ where: { id: req.params.id } });

        if (userDestroyed) {
          deleteContact(email).then(() => {
            console.log('Deleted from Zendesk');
          })
            .catch(err => console.log('Zendesk delete fails', err));

          deleteStripeCard(customerId).then(() => {
            console.log('Deleted from stripe');
          })
            .catch(err => console.log(`Stripe delete fails => customerId ${customerId}:`, err.raw.message));

          return res.status(200).send({ status: 'ok' });
        }
      } catch (err) {
        next(err);
      }
    })
    .catch(err => next(err));
};

export const syncUsers = (req, res) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };
  User.findAll({
    order: [
      ['id', 'DESC'],
    ],
  })
    .then((users) => {

      res.status(200).json({ success: true });
      eachLimit(users, 1, async (user, cb) => {
        try {
          const status = await updateIntegration(user.get('id'));
          console.log(`Integration success> ${user.get('email')}`);
          setTimeout(cb, 500);
          return;
        } catch (error) {
          logInfo.user = user.get('id');
          logInfo.type = 'error';
          const errorToShow = (error.response) ? `Autopilot error rate limit ${error.response.data.message}` : `Zendesk error rate limit -> ${error.result}`;
          logInfo.message = `#(${req.user.id})~${req.user.name} has an error when ran Sync Users ${errorToShow}. User NOT SYNC -> #(${user.get('id')})~${user.get('name')}`;
          LogDB(logInfo);
          console.log(errorToShow);
          setTimeout(cb, 500);
          return;
        }
      }, (err) => {
        if (err) {
          return console.log(err);
        }
        console.log('Sync users end succesfuly.');
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} ran a syncUsers.`;
        LogDB(logInfo);
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const checkUser = async (req, res, next) => {
  req.assert('user_id', 'ID is required').notEmpty();
  try {
    const result = await req.getValidationResult();
    const errors = result.array();
    if (errors.length > 0) {
      return res.status(400).send(errors);
    }
    const user = await User.findById(Number(req.body.user_id));
    if (!user) {
      return res.status(404).json({ error: 'referral code not valid' });
    }
    if (user.get('name').toUpperCase() !== req.body.name) {
      return res.status(409).json({ conflict: 'id didnt match with user' });
    }
    return res.status(200).json({ valid: true });
  } catch (err) {
    next(err);
  }
};

export const downloadCSV = async (req, res, next) => {
  const logInfo = {
    user: null,
    admin: req.user.id,
    type: '',
    message: '',
  };

  const columns = ['id', 'customer_id', 'name', 'last_name', 'email', 'zip_code', 'address', 'address_extra', 'city', 'state', 'phone_number', 'hash', 'first_charge', 'billing_name', 'billing_lastname', 'billing_address', 'billing_address_extra', 'billing_city', 'billing_state', 'billing_zip_code', 'has_mealmaker', 'delivered_mealmaker', 'status', 'credit', 'beaba', 'beabaFlow'];

  const lastCronId = await Cron.max('id');

  User.findAll({
    attributes: [...columns],
    include: [
      {
        model: Order,
        where: {
          $and: [
            { boxType: { $notLike: '%Babycook%' } },
            { boxType: { $notLike: '%Meal Maker%' } },
          ],
        },
        required: false,
      },
      {
        model: Child,
        required: false,
      }, {
        model: AppliedDiscounts,
        where: {
          coupon: {
            $not: null,
          },
        },
        required: false,
        order: '"updatedAt" DESC',
        as: 'couponUsed',
        include: {
          model: Coupon,
          as: 'couponApplied',
        },
      }, {
        model: AppliedDiscounts,
        where: {
          nextDiscountType: {
            $not: null,
          },
          cronId: lastCronId,
        },
        required: false,
        as: 'nextDiscount',
        order: '"updatedAt" DESC',
      }, {
        model: Referral,
        as: 'userReferree',
        required: false,
      }, {
        model: Subscription,
        required: false,
      },
    ],
  })
    .then((users) => {
      const data = [];
      users.map((obj) => {
        const user = obj.toJSON();

        const boxNumber = user.orders.length === 0 ? 0 : user.orders[user.orders.length - 1].boxNumber;
        let nextDiscount = '-';
        if (user.nextDiscount.length) {
          if (user.nextDiscount[0].nextDiscountType === 'amount') nextDiscount = `USD ${user.nextDiscount[0].nextDiscountAmount / 100}`;
          if (user.nextDiscount[0].nextDiscountType === 'percent') nextDiscount = `USD ${user.nextDiscount[0].nextDiscountAmount / 100} (${user.nextDiscount[0].nextDiscountPercent}%)`;
        }

        let currentDiscount = '-';
        if (user.subscriptions) {
          if (user.subscriptions.length !== 0) {
            if (user.subscriptions[0].typeNextDiscount !== null) currentDiscount = user.subscriptions[0].typeNextDiscount === 'amount' ? `${user.subscriptions[0].nextDiscount / 100} USD` : `${(95 * user.subscriptions[0].nextDiscount / 100)} (${user.subscriptions[0].nextDiscount}%)`;
          }
        }

        data.push({
          ...user,
          frequency: user.subscriptions.length && user.subscriptions[0].frequency,
          boxNumber,
          Revenue: user.orders.map(order => order.productPrice).reduce((a, b) => a + b, 0),
          child1: user.children[0] ? moment.utc(user.children[0].birthdate).format('YYYY-MM-DD') : null,
          child2: user.children[1] ? moment.utc(user.children[1].birthdate).format('YYYY-MM-DD') : null,
          userReferree: user.userReferree.length ? 'Yes' : 'No',
          nextDiscount,
          currentDiscount,
          couponUsed: user.couponUsed.length ? user.couponUsed[0].couponApplied.code : '-',
        });
      });

      const fields = [...columns, 'frequency', 'boxNumber', 'Revenue', 'child1', 'child2', 'userReferree', 'nextDiscount', 'currentDiscount', 'couponUsed'];
      const fieldNames = [...columns, 'Subscription Frequency', 'Box Number', 'Revenue', 'Child 1', 'Child 2', 'Referral User', 'Last Discount', 'Next Discount', 'Coupon Used'];
      const opts = {
        data,
        fields,
        fieldNames,
      };
      const filename = 'users.csv';
      exportCsv(opts, filename, () => {
        const file = path.join(filename);
        res.download(file);
        logInfo.type = 'info';
        logInfo.message = `#(${req.user.id})~${req.user.name} download the users csv.`;
        LogDB(logInfo);
      });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in downloadCSV(find users) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      next(err);
    });
};

export const getUserByIdWithDetails = (req, res, next) => {
  User.find({
    where: {
      id: req.params.id,
    },
    include: [
      Child,
      Order,
      db.Log,
      {
        model: Subscription,
        include: [db.Skip, db.Product],
      },
    ],
  })
    .then((user) => {
      if (!user) {
        res.status(404).send('User not found');

        return;
      }
      res.status(200).send(user);
    })
    .catch(err => next(err));
};

export const getUserTimeActiveInMonths = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT
        TimeActive,
        Sum(Active) + Sum(Cancelled) as Total,
        Sum(Active) as Active,
        Sum(Cancelled) as Cancel
      FROM
      (SELECT
        CASE s.status
              WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.start_from)/30)
              WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.start_from)/30)
              ELSE NULL
          END AS TimeActive,
          CASE s.status
              WHEN 'canceled' THEN 1
              WHEN 'active' THEN 0
              WHEN 'pending' THEN 0
              WHEN 'paused' THEN 0
              ELSE NULL
          END AS Cancelled,
          CASE s.status
              WHEN 'active' THEN 1
              WHEN 'pending' THEN 0
              WHEN 'paused' THEN 0
              WHEN 'canceled' THEN 0
              ELSE NULL
          END AS Active
      FROM user
      INNER JOIN child AS c ON user.id = c.user_id
      INNER JOIN subscription AS s ON user.id = s.user_id
      ORDER BY
        TimeActive) as TimeActive
      WHERE TimeActive IS NOT NULL AND TimeActive >= 0
        GROUP BY TimeActive
      ORDER BY
        TimeActive
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};

export const getRetentionInfoJson = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
    SELECT user.id, user.status, ROUND(DATEDIFF(s.start_from, c.birthdate)/30) AS 'BabyAgeAtStart', ROUND(DATEDIFF(s.cancellation_date, c.birthdate)/30) AS 'BabyAgeAtCancellation', DATE_FORMAT(s.created_at, "%Y-%m") AS 'SignupDate', s.cancellation_date AS 'CancellationDate', o.product_price, bn.BoxNumber, cp.Coupon,
    CASE s.status
          WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.created_at)/30)
          WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.created_at)/30)
          ELSE NULL
      END AS 'time_active',
      IF (referral.me IS NOT NULL, true, false) AS isReferred,
    userReferrer.id AS referrerId
    FROM user
    INNER JOIN subscription AS s ON user.id = s.user_id
    LEFT JOIN child AS c ON user.id = c.user_id
    LEFT JOIN referral ON referral.me = user.id
    LEFT JOIN user AS userReferrer ON referral.referred_by = userReferrer.id
    LEFT JOIN
    (SELECT user_id, product_price
      FROM \`order\`
      WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%' AND box_type NOT LIKE 'Pre-Cron Box' AND box_number = 1
      GROUP BY user_id) AS o
    ON user.id = o.user_id
    LEFT JOIN
    (SELECT user_id, MAX(box_number) AS 'BoxNumber'
    FROM raisedreal.\`order\`
    WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%' AND box_type NOT LIKE 'Pre-Cron Box'
    GROUP BY user_id) AS bn
    ON user.id = bn.user_id
    LEFT JOIN
    (SELECT
      DISTINCT \`applied_discounts\`.user as User,
      \`coupon\`.code as Coupon
    FROM
      \`applied_discounts\`
    INNER JOIN
      \`coupon\`
    ON
      \`coupon\`.id = \`applied_discounts\`.coupon
    INNER JOIN
      \`user\`
    ON
      \`user\`.id = \`applied_discounts\`.user
    INNER JOIN
      \`subscription\`
    ON
      \`subscription\`.user_id = \`user\`.id
    WHERE
      coupon IS NOT NULL) AS cp
    ON user.id = cp.User
    GROUP BY id;
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};

export const getDimensionsByUserId = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT
      u.id AS 'UserId',
      u.status,
      u.state,
      TRUNCATE(pp.product_price/100, 2) AS 'FirstCharge',
      bn.BoxNumber,
      FLOOR(DATEDIFF(s.start_from, c.birthdate)/30) AS 'BabyAgeAtStart',
      FLOOR(DATEDIFF(s.cancellation_date, c.birthdate)/30) AS 'BabyAgeAtCancellation',
      FLOOR(DATEDIFF(CURDATE(), c.birthdate)/30) AS 'BabyAgeToday',
      DATE_FORMAT(s.created_at, "%Y-%m") AS 'SignupDate',
      DATE_FORMAT(s.cancellation_date, "%Y-%m-%d") AS 'CancellationDate',
      s.cancel_reason AS 'CancellationReason',
      DATE_FORMAT(s.start_from, "%Y-%m-%d") AS 'StartFrom',
      COALESCE(cp.Coupon, 'No Coupon') AS 'Coupon',
      cp.coupon_amount AS 'CouponAmount',
      CASE s.status
            WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.created_at)/30)
            WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.created_at)/30)
            ELSE NULL
        END AS 'timeActive',
        IF (referral.me IS NOT NULL, 'Yes', 'No') AS 'isReferred',
      user_referrer.id AS 'referrerId'

      FROM \`user\` AS u
      LEFT JOIN subscription AS s ON u.id = s.user_id
      LEFT JOIN referral ON referral.me = u.id
      LEFT JOIN user AS user_referrer ON referral.referred_by = user_referrer.id
      LEFT JOIN
      (SELECT * FROM child GROUP BY user_id) AS c ON u.id = c.user_id

      LEFT JOIN
        (SELECT user_id, MAX(box_number) AS 'BoxNumber'
        FROM \`order\`
        WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%' AND box_type NOT LIKE 'Pre-Cron Box'
        GROUP BY user_id) AS bn
        ON u.id = bn.user_id

      LEFT JOIN
        (SELECT o.user_id, o.product_price FROM \`order\` AS o
        WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%' AND box_type NOT LIKE 'Pre-Cron Box' AND box_number = 1
        GROUP BY o.user_id) AS pp ON u.id = pp.user_id

      LEFT JOIN
        (SELECT DISTINCT
        \`applied_discounts\`.user,
        \`coupon\`.code as Coupon,
        \`applied_discounts\`.coupon_amount
        FROM \`applied_discounts\`
        INNER JOIN \`coupon\` ON \`coupon\`.id = \`applied_discounts\`.coupon
        WHERE coupon IS NOT NULL) AS cp ON u.id = cp.user

      GROUP BY u.id
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};

const getUserStatusOnDate = (user, date) => {
  const dateStatus = user.logs
  .filter(log => moment(log.userStatusDate) <= moment(date).add(1, 'hour'))
  .sort((logA, logB) => moment(logA.userStatusDate) - moment(logB.userStatusDate));
  return dateStatus.length > 0 ? dateStatus[dateStatus.length - 1].userStatus : null;
};

const getLastUserBoxNumber = (user, date) => {
  const cronOrder = user.orders.filter(order => moment(order.createdAt).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD'));
  if (cronOrder.length > 0) {
    return cronOrder[0].boxNumber;
  }
  const ordersToCron = user.orders.filter(order => moment(order.createdAt) < moment(date));
  return ordersToCron.length > 0 ? ordersToCron[ordersToCron.length - 1].boxNumber : null;
};

/**
 * Gets baby age in months at the moment user subscribed
 */
export const getBabyAgeAtStart = (user) => {
  if (user.children.length === 0) {
    return null;
  }
  const childrenBirthday = moment(user.children[0].birthdate);
  const userSubscriptionDate = moment(user.subscriptions[0].createdAt);
  const babyAgeAtStart = userSubscriptionDate.diff(childrenBirthday, 'months');
  return babyAgeAtStart;
};

/**
 * Gets baby age in months at the moment user cancelled the subscription
 */
export const getBabyAgeAtCancellation = (user) => {
  if (user.children.length === 0) {
    return null;
  }
  if (user.subscriptions[0].cancellationDate === null) { // If user hasn't cancelled.
    return null;
  }
  const childrenBirthday = moment(user.children[0].birthdate);
  const userSubscriptionCancelDate = moment(user.subscriptions[0].cancellationDate);
  const babyAgeAtCancellation = userSubscriptionCancelDate.diff(childrenBirthday, 'months');
  return babyAgeAtCancellation;
};

/**
 * Gets current baby age in months
 */
export const getBabyAgeToday = (user) => {
  if (user.children.length === 0) {
    return null;
  }
  const childrenBirthday = moment(user.children[0].birthdate);
  return moment().diff(childrenBirthday, 'months');
};

/**
 * Gets user time active in months.
 */
const getUserTimeActive = (user) => {
  const userSubscriptionDate = moment(user.subscriptions[0].createdAt);
  if (user.subscriptions[0].status === 'canceled') {
    return moment(user.subscriptions[0].cancellationDate).diff(userSubscriptionDate, 'months');
  }

  if (user.subscriptions[0].status === 'active') {
    return moment().diff(userSubscriptionDate, 'months');
  }
  return null;
};

const getStatusOfUsersPerCron = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const cronDays = await Cron.findAll();
      const usersWithOrders = await User.findAll({
        include: [
          { model: Subscription },
          { model: Child },
          {
            model: Order,
            where: {
              $and: [
                { boxType: { $notLike: 'Meal Maker' } },
                { boxType: { $notLike: '%Babycook%' } },
                { boxType: { $notLike: 'Pre-Cron Box' } },
              ],
            },
            required: false,
          },
          {
            model: AppliedDiscounts,
            where: {
              coupon: {
                $not: null,
              },
            },
            required: false,
            order: ['updatedAt', 'DESC'],
            as: 'couponUsed',
            include: {
              model: Coupon,
              as: 'couponApplied',
            },
          },
          {
            model: Log,
            where: { message: { $eq: null } },
            required: false,
          },
        ],
      });

      const plainCronDates = cronDays.map(cronDay => cronDay.get({ plain: true }));
      const plainUsersWithOrders = usersWithOrders.map(user => user.get({ plain: true }));

      const data = plainCronDates.map((plainCronDate) => {
        const usersAtDate = plainUsersWithOrders.map((user) => {
          if (user.subscriptions.length === 0) return;
          if (moment(user.subscriptions[0].createdAt) <= moment(plainCronDate.createdAt)) {
            return {
              cronDay: moment(plainCronDate.createdAt).format('YYYY-MM-DD'),
              userId: user.id,
              status: getUserStatusOnDate(user, plainCronDate.createdAt),
              boxNumber: getLastUserBoxNumber(user, plainCronDate.createdAt),
              babyAgeAtStart: getBabyAgeAtStart(user),
              babyAgeAtCancellation: getBabyAgeAtCancellation(user),
              babyAgeToday: getBabyAgeToday(user),
              cancellationDate: user.subscriptions[0].cancellationDate !== null ? moment(user.subscriptions[0].cancellationDate).format('YYYY-MM-DD') : null,
              timeActive: getUserTimeActive(user),
              state: user.subscriptions[0].status,
              cancellationReason: user.subscriptions[0].cancelReason,
              signupDate: moment(user.subscriptions[0].createdAt).format('YYYY-MM-DD'),
              startFromDate: moment(user.subscriptions[0].startFrom).format('YYYY-MM-DD'),
              coupon: user.couponUsed.length > 0 ? user.couponUsed[0].couponApplied.code : null,
            };
          }
        });
        return usersAtDate;
      });

      const dataFlat = data.reduce((acc, val) => acc.concat(val), []);
      resolve(dataFlat);
    } catch (err) {
      reject(err);
    }
  });
};

export const serveUserStatusPerCron = async (req, res, next) => {
  try {
    const statusData = await getStatusOfUsersPerCron();
    res.status(200).send(statusData);
  } catch (error) {
    next(error);
  }
};

export const getUserStatusPerCronCsv = async (req, res, next) => {
  try {
    const data = await getStatusOfUsersPerCron();

    const fields = [
      'cronDay',
      'userId',
      'status',
      'boxNumber',
      'babyAgeAtStart',
      'babyAgeAtCancellation',
      'babyAgeToday',
      'cancellationDate',
      'timeActive',
      'state',
      'cancellationReason',
      'signupDate',
      'startFromDate',
      'coupon',
    ];

    const opts = {
      data,
      fields,
    };

    const filename = 'status-per-cron.csv';
    const csv = await json2csv(opts);

    fs.writeFile(filename, csv, (err) => {
      if (err) throw err;
      const file = path.join(filename);
      res.download(file);
    });
  } catch (error) {
    next(error);
  }
};

export const getSkipsDatesWithUserInfo = async (req, res, next) => {
  try {
    const result = await sequelize.query(`
      SELECT
      DATE_FORMAT(date, "%Y-%m-%d") AS 'SkipDate',
      u.id AS 'UserId',
      u.status,
      u.state,
      TRUNCATE(pp.product_price/100, 2) AS 'FirstCharge',
        bn.BoxNumber,
      FLOOR(DATEDIFF(s.start_from, c.birthdate)/30) AS 'BabyAgeAtStart',
      FLOOR(DATEDIFF(s.cancellation_date, c.birthdate)/30) AS 'BabyAgeAtCancellation',
      FLOOR(DATEDIFF(CURDATE(), c.birthdate)/30) AS 'BabyAgeToday',
      DATE_FORMAT(s.created_at, "%Y-%m") AS 'SignupDate',
      DATE_FORMAT(s.cancellation_date, "%Y-%m-%d") AS 'CancellationDate',
      s.cancel_reason AS 'CancellationReason',
      DATE_FORMAT(s.start_from, "%Y-%m-%d") AS 'StartFrom',
      COALESCE(cp.Coupon, 'No Coupon') AS 'Coupon',
      CASE s.status
            WHEN 'canceled' THEN ROUND(DATEDIFF(s.cancellation_date, s.created_at)/30)
            WHEN 'active'   THEN ROUND(DATEDIFF(CURDATE(), s.created_at)/30)
            ELSE NULL
        END AS 'timeActive',
        IF (referral.me IS NOT NULL, 'Yes', 'No') AS 'isReferred',
      user_referrer.id AS 'referrerId'

      FROM \`skip\`
      LEFT JOIN subscription AS s ON skip.subscription = s.id
      LEFT JOIN \`user\` AS u ON s.user_id = u.id
      LEFT JOIN referral ON referral.me = u.id
      LEFT JOIN user AS user_referrer ON referral.referred_by = user_referrer.id
      LEFT JOIN
      (SELECT * FROM child GROUP BY user_id) AS c ON u.id = c.user_id

      LEFT JOIN
          (SELECT user_id, MAX(box_number) AS 'BoxNumber'
          FROM \`order\`
          WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%'
          GROUP BY user_id) AS bn
          ON u.id = bn.user_id

      LEFT JOIN
        (SELECT o.user_id, o.product_price FROM \`order\` AS o
        WHERE box_type NOT LIKE 'Meal Maker' AND box_type NOT LIKE '%Babycook%' AND box_type NOT LIKE 'Pre-Cron Box' AND box_number = 1
        GROUP BY o.user_id) AS pp ON u.id = pp.user_id

      LEFT JOIN
        (SELECT DISTINCT
        \`applied_discounts\`.user,
        \`coupon\`.code as Coupon,
        \`applied_discounts\`.coupon_amount
        FROM \`applied_discounts\`
      INNER JOIN \`coupon\` ON \`coupon\`.id = \`applied_discounts\`.coupon
      WHERE coupon IS NOT NULL) AS cp ON u.id = cp.user
    `, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};
