export default (sequelize, DataTypes) => {
  sequelize.import('../Subscription/model');

  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    customerId: {
      type: DataTypes.STRING,
      field: 'customer_id',
    },
    credit: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    name: DataTypes.STRING,
    lastName: {
      type: DataTypes.STRING,
      field: 'last_name',
    },
    email: DataTypes.STRING,
    status: DataTypes.STRING,
    zipCode: {
      type: DataTypes.STRING,
      field: 'zip_code',
    },
    address: DataTypes.STRING,
    addressExtra: {
      type: DataTypes.STRING,
      field: 'address_extra',
    },
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    phoneNumber: {
      type: DataTypes.STRING,
      field: 'phone_number',
    },
    hash: DataTypes.STRING,
    firstCharge: {
      type: DataTypes.STRING,
      field: 'first_charge',
    },
    billingName: {
      type: DataTypes.STRING,
      field: 'billing_name',
    },
    billingLastName: {
      type: DataTypes.STRING,
      field: 'billing_lastname',
    },
    billingAddress: {
      type: DataTypes.STRING,
      field: 'billing_address',
    },
    billingAddressExtra: {
      type: DataTypes.STRING,
      field: 'billing_address_extra',
    },
    billingCity: {
      type: DataTypes.STRING,
      field: 'billing_city',
    },
    billingState: {
      type: DataTypes.STRING,
      field: 'billing_state',
    },
    billingZipCode: {
      type: DataTypes.STRING,
      field: 'billing_zip_code',
    },
    hasMealMaker: {
      type: DataTypes.BOOLEAN,
      field: 'has_mealmaker',
    },
    deliveredMealMaker: {
      type: DataTypes.BOOLEAN,
      field: 'delivered_mealmaker',
      defaultValue: 0,
    },
    hubspotId: {
      type: DataTypes.INTEGER,
      field: 'hubspot_id',
    },
    beaba: {
      type: DataTypes.BOOLEAN,
      field: 'beaba',
    },
    beabaFlow: {
      type: DataTypes.BOOLEAN,
      field: 'beabaFlow',
    },
    company: {
      type: DataTypes.STRING,
      field: 'company',
    },
    firstChargeAmount: {
      type: DataTypes.INTEGER,
      field: 'first_charge_amount',
    },
    outbound: {
      type: DataTypes.STRING,
      field: 'outbound',
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
