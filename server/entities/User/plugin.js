import Stripe from 'stripe';
import hash from 'object-hash';
import moment from 'moment';
import { each, eachLimit } from 'async';
import zipcodes from 'zipcodes';

import config from '../../config/config';
import sendgridConfig from '../../config/sendgrid';
import configHubspot from '../../config/hubspot';
import { sendEmail } from '../../utils/sendgrid';
import { sendHubspotForm, triggerHubspotWorkflow } from '../../utils/hubspot';
import { LogDB, LogDBUserStatus } from '../Log/plugin';
import { upsertZendesk, ZENDESK_ID_START_SLUG } from '../../utils/zendesk';
import { redeemCoupon, calcCouponDiscount } from '../Coupon/plugin';
import { updateIntegration, getSubscriptionStatus } from '../../utils';
import { PENDING } from '../../services/status';
import { registerAppliedDiscount } from '../AppliedDiscounts/plugin';
import { getDiscountByBox } from '../../helper/boxListsValues';


const db = require('../../db/connection');

const stripe = Stripe(config.stripeApiKey);
const User = db.User;
const Subscription = db.Subscription;
const Child = db.Child;
const Product = db.Product;
const Coupon = db.Coupon;
const Referral = db.Referral;
const Order = db.Order;

export const retieveCustomer = (customerId) => {
  return stripe.customers.retrieve(customerId);
};

export const updateDefaultCard = (customerId, cardId) => {
  return new Promise((resolve, reject) => {
    stripe.customers.update(customerId, {
      default_source: cardId,
    }, (err, customer) => {
      if (err) reject(err);
      resolve(customer);
    });
  });
};

export const createCustomer = async function (user) {
  return stripe.customers.create({
    email: user.email,
    description: `Customer for ${user.email}`,
    shipping: {
      address: {
        city: user.billingCity,
        line1: user.billingAddress,
        line2: user.billingAddressExtra,
        postal_code: user.billingZipCode,
        state: user.billingState,
      },
      name: `${user.billingName} ${user.billingLastName}`,
    },
  });
};

export const updateStripeBillingAddress = (user) => {
  return stripe.customers.update(user.customerId, {
    shipping: {
      address: {
        city: user.billingCity,
        line1: user.billingAddress,
        line2: user.billingAddressExtra,
        postal_code: user.billingZipCode,
        state: user.billingState,
      },
      name: `${user.billingName} ${user.billingLastName}`,
    },
  });
};

export const createCard = (customerId, token) => {
  return new Promise((resolve, reject) => {
    stripe.customers.createSource(customerId, { source: token }, (err, card) => {
      if (err) {
        return reject(err);
      }
      resolve(card);
    });
  });
};

export const findEmailByUserId = (id) => {
  return new Promise((resolve, reject) => {
    User.findOne({
      where: {
        id,
      },
    })
    .then((user) => {
      resolve(user);
    })
    .catch(err => reject(err));
  });
};

export const sendReferralCodeByBoxNumber = (name, email, referralCode, userId, formatDate, sendDate) => {
  const sendMailData = {
    to: email,
    subject: 'You just got a discount Coupon',
    template_id: sendgridConfig.templates.referralEmailByBoxNumber,
    substitutions: {
      name,
      referralCode: referralCode,
      formatDate,
    },
  };

  if (sendDate !== null && sendDate !== undefined) {
    sendMailData.send_at = sendDate;
  }
  return new Promise((resolve, reject) => {
    sendEmail(sendMailData)
    .then(() => {
      console.log(`Sendgrid Referral Email By Box Number OK (${email})`);
      resolve('Sendgrid Referral Email By Box Number OK');
      LogDB({
        user: userId,
        message: `Referral by Box Number | Sendgrid Referral Email By Box Number OK (${email})`,
        type: 'info',
      });
    })
    .catch((err) => {
      console.log(`Sendgrid Referral Email By Box Number ERROR: (${email}) ${err}`);
      reject(err);
      LogDB({
        user: userId,
        message: 'Referral by Box Number | Send Referral Email By Box Number: ERROR!',
        type: 'info',
      });
    });
  });
};

export const breakageEmail = (userId, hash) => {
  const link = `${APP_URL}/cc?hash=${hash}`;
  findEmailByUserId(userId)
  .then((user) => {
    const userName = user.get('name') || null;
    const userEmail = user.get('email') || null;
    sendEmail({
      to: userEmail,
      template_id: sendgridConfig.templates.breakageEmail,
      substitutions: {
        name: userName,
        hash: link,
      },
    })
    .then(() => {
      LogDB({
        user: userId,
        message: `Breakage Email: ${userEmail} Ok!`,
        type: 'info',
      });
    })
    .catch((err) => {
      console.log(`Breakage Email ERROR: (${userEmail}) ${err}`);
      LogDB({
        user: userId,
        message: 'Breakage Email: ERROR!',
        type: 'info',
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
};

export const sendReferralEmail = (referralName, referralCode) => {
  const userId = referralCode.split('_')[1];
  findEmailByUserId(userId)
  .then((user) => {
    const userName = user.get('name') || null;
    const userEmail = user.get('email') || null;
    const userReferralCode = userName.toUpperCase().concat(userId).split(' ').join('');
    const referralUserName = referralName || null;
    sendEmail({
      to: userEmail,
      subject: 'You just made $25',
      template_id: sendgridConfig.templates.referralEmail,
      substitutions: {
        name: userName,
        referalName: referralUserName,
        referralCode: userReferralCode,
      },
    })
    .then(() => {
      console.log(`Sendgrid Referral Email OK (${userEmail})`);
      LogDB({
        user: userId,
        message: 'Send Referral Email: Ok!',
        type: 'info',
      });
    })
    .catch((err) => {
      console.log(`Sendgrid Referral Email ERROR: (${userEmail}) ${err}`);
      LogDB({
        user: userId,
        message: 'Send Referral Email: ERROR!',
        type: 'info',
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
};

export const deleteStripeCard = (customerId) => {
  return new Promise((resolve, reject) => {
    return stripe.customers.del(customerId, (err, confirmation) => {
      if (err) {
        return reject(err);
      }
      resolve(confirmation);
    });
  });
};

export const generateFirstCharge = (customerId, chargeData, appliedDiscounts, userId) => {
  const metadata = {};
  if (chargeData.discount !== null) {
    metadata.coupon_code = chargeData.code;
    metadata.coupon_discount = chargeData.discount;
    if (chargeData.extra_info) {
      metadata.extra_info = chargeData.extra_info;
    }
  }
  return new Promise((resolve, reject) => {
    // Don't charge if amount <= 0
    if (chargeData.finalAmount <= 0) {
      if (appliedDiscounts.length) {
        try {
          registerAppliedDiscount(appliedDiscounts, userId, 'free_charge');
        } catch (err) {
          console.log(`Cant register applied discount (${userId})`);
          LogDB({
            user: userId,
            type: 'error',
            message: 'Cant register applied discount',
          });
        }
      }
      resolve({ id: 'free charge' });
      return;
    }
    stripe.charges.create({
      amount: chargeData.finalAmount,
      currency: 'usd',
      customer: customerId,
      metadata,
    })
      .then((charge) => {
        if (appliedDiscounts.length) {
          try {
            registerAppliedDiscount(appliedDiscounts, userId, charge.id);
          } catch (err) {
            console.log(`Cant register applied discount (${user.get('email')})`);
            LogDB({
              user: user.get('id'),
              type: 'error',
              message: 'Cant register applied discount',
            });
          }
        }
        resolve(charge);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const createSubscription = async (userId, productReq, sub) => {
  const subscription = Subscription.build({ userId });
  subscription.status = 'pending';
  subscription.productId = productReq.productId;
  subscription.amount = productReq.quantity;
  subscription.couponId = (productReq.couponId) ? productReq.couponId : sub.couponId;
  subscription.startFrom = sub.startFrom;
  subscription.frequency = sub.frequency;
  subscription.breakFastCount = sub.breakfastMealsCount;
  return subscription.save();
};

export const createChilds = async (userId, childReq) => {
  const child = Child.build({ userId });
  child.name = childReq.name;
  child.food = childReq.foodType;
  child.birthdate = childReq.birthdate;
  return child.save();
};

const setUser = (obj) => {
  return {
    name: obj.name,
    lastName: obj.lastName,
    zipCode: obj.zipCode,
    address: obj.address,
    addressExtra: obj.addressExtra,
    company: obj.company,
    city: obj.city,
    state: obj.state,
    phoneNumber: obj.phoneNumber,
    billingName: obj.billingName,
    billingLastName: obj.billingLastName,
    billingAddress: obj.billingAddress,
    billingAddressExtra: obj.billingAddressExtra,
    billingCity: obj.billingCity,
    billingState: obj.billingState,
    billingZipCode: obj.billingZipCode,
    hasMealMaker: null, // Hardcoded to null since new Beaba flow
    beaba: obj.beaba,
    beabaFlow: obj.beabaFlow,

  };
};

export const updateBillingInfo = async (req, res, next) => {
  const editedInfo = {
    billingName: req.body.billingName,
    billingLastName: req.body.billingLastname,
    billingZipCode: req.body.billingZipCode,
    billingAddress: req.body.billingAddress,
    billingAddressExtra: req.body.billingAddressExtra,
    billingCity: req.body.billingCity,
    billingState: req.body.billingState,
  };

  return User.update(editedInfo, {
    where: {
      hash: req.params.hash,
    },
  })
    .then(() => {
      return User.findOne({
        where: { hash: req.params.hash },
      })
        .then((updatedUser) => {
          return updatedUser;
        })
        .catch(err => next(err));
    })
    .catch(err => next(err));
};

export const createUser = async (data) => {
  return User.findOrCreate({
    where: {
      email: data.email,
    },
    defaults: setUser(data),
    raw: true,
  })
    .spread(async (user, created) => {
      if (created) {
        const logInfo = {
          user: user.get('id'),
          admin: null,
          type: '',
          message: '',
        };
        const customer = await createCustomer(user);
        if (!customer) {
          return null;
        }
        user.customerId = customer.id;
        user.hash = (hash(user.dataValues));
        user.set('status', 'pending');
        LogDBUserStatus({
          userId: user.get('id'),
          userStatus: 'pending',
          userStatusDate: moment(),
        });
        const userId = user.get('id');
        const subscription = data.subscription;
        if (data.referralCode) {
          const userReferred = data.referralCode.substring(data.referralCode.indexOf('_') + 1);
          const referral = Referral.build({ referredBy: userReferred });
          referral.me = userId;
          referral.save();
          const userWhoReffers = await User.findById(userReferred);
          userWhoReffers.set('credit', userWhoReffers.get('credit') + config.referralFirstDiscount);
          userWhoReffers.save();
          logInfo.type = 'info';
          logInfo.message = `Referral Program -> ${user.get('name')} ${user.get('lastName')} was referred by ${userWhoReffers.get('email')}`;
          LogDB(logInfo);
        }
        data.child.forEach(async (c) => {
          await createChilds(userId, c);
        });

        subscription.product.forEach(async (p) => {
          await createSubscription(userId, p, subscription);
        });
        const userSaved = await user.save();

        const zendeskDataWithExternalID = {
          email: user.get('email'),
          external_id: `${ZENDESK_ID_START_SLUG}-${userId}`,
        };
        try {
          await upsertZendesk(zendeskDataWithExternalID);
        } catch (err) {
          console.log(err);
        }
        return userSaved;
      }
      return null;
    });
};

export const findUserById = (id) => {
  return new Promise((resolve, reject) => {
    User.findOne({
      where: {
        id,
      },
      include: [{
        model: Subscription,
        include: [Product, Coupon],
      }],
    })
      .then((user) => {
        resolve(user);
      })
      .catch(err => reject(err));
  });
};

export const updateUserStatusActive = (userId) => {
  return new Promise(async (resolve, reject) => {
    const user = await findUserById(userId);
    user.set('status', 'active');
    LogDBUserStatus({
      userId: user.get('id'),
      userStatus: 'active',
      userStatusDate: moment(),
    });
    await user.save();

    const subscriptions = user.get('subscriptions');

    eachLimit(subscriptions, 1, async (sub, cb) => {
      sub.set('status', 'active');
      await sub.save();
      cb();
    }, async () => {
      try {
        const integrated = await updateIntegration(userId);
      } catch (err) {
        console.log(err);
      }

      const logInfoUserStatusActive = {
        user: userId,
        admin: null,
        type: 'info',
        message: `User Update -> #(${userId}) [status changed to active]`,
      };
      LogDB(logInfoUserStatusActive);
      resolve();
    });
  });
};

// Send Transaction email with order summary
export const sendConfirmationEmail = (userId, referralName = null) => {
  return new Promise(async (resolve, reject) => {
    const user = await User.findOne({
      where: { id: userId },
      include: [{
        model: Subscription,
        include: [Product, Coupon],
      }],
    });
    const subscriptions = user.get('subscriptions');
    const customerId = user.get('customerId');
    const infoCharge = {
      code: null,
      discount: null,
      extra_info: null,
      finalAmount: 0,
    };
    let couponRedeemed = false;
    let isCouponForever = false;
    let promotion = 0;
    let chargeWithDiscount = 0;
    let chargeWithoutDiscount = 0;
    let mealsQuantity;
    let breakFastCount;
    let anytimeMealsCount;
    let frequency;
    let productPrice;

    each(subscriptions, async (sub) => {
      const amount = sub.get('amount');
      const productId = sub.get('product').get('id');
      const coupon = sub.get('coupon');
      productPrice = sub.get('product').get('price');
      frequency = sub.get('frequency');
      mealsQuantity = productId === 21 ? 12 : 24;
      breakFastCount = sub.get('breakFastCount');
      anytimeMealsCount = mealsQuantity - breakFastCount;

      let amountOff;
      let percentOff;

      if (coupon) {
        if (coupon.get('duration') === 'forever') isCouponForever = true;
      }
      let subscriptionPrice = productPrice * amount;
      chargeWithoutDiscount += subscriptionPrice;

      if (coupon && (sub.get('coupon').get('productId') === null || sub.get('coupon').get('productId') === productId)) {
        const boxList = JSON.parse(coupon.boxList);
        const duration = coupon.get('duration');

        if (duration === 'custom_boxes') {
          const firstBoxDiscount = getDiscountByBox(boxList, 1);
          amountOff = firstBoxDiscount.amountOff;
          percentOff = firstBoxDiscount.percentOff;

          const secondBoxDiscount = getDiscountByBox(boxList, 2);
          chargeWithoutDiscount = calcCouponDiscount(secondBoxDiscount.percentOff, secondBoxDiscount.amountOff, subscriptionPrice);
        } else {
          amountOff = coupon.get('amountOff') / subscriptions.length;
          percentOff = coupon.get('percentOff');
        }

        if (!couponRedeemed) couponRedeemed = true;

        infoCharge.code = coupon.get('code');
        infoCharge.discount = (amountOff) ? `USD ${amountOff / 100}` : `${percentOff}%`;
        promotion += subscriptionPrice - calcCouponDiscount(percentOff, amountOff, subscriptionPrice);
        subscriptionPrice = calcCouponDiscount(percentOff, amountOff, subscriptionPrice);
      }
      infoCharge.finalAmount += subscriptionPrice;
      chargeWithDiscount += infoCharge.finalAmount;
    });

    if (promotion !== 0) promotion = Number(promotion / 100).toFixed(2);
    if (chargeWithDiscount !== 0) chargeWithDiscount = Number(chargeWithDiscount / 100).toFixed(2);
    if (chargeWithoutDiscount !== 0) chargeWithoutDiscount = Number(chargeWithoutDiscount / 100).toFixed(2);
    if (referralName !== null && referralName !== undefined) chargeWithDiscount = ((productPrice - config.referralFirstDiscount) / 100);

    const productMarkup = `
      <tr>
        <td class="text">${subscriptions[0].get('amount')} box of ${mealsQuantity} meals every ${frequency === 2 ? 'two' : 'four'} weeks</td>
        <td class="price">$ ${productPrice / 100}</td>
      </tr>
    `;

    const recurringCharge = isCouponForever ? chargeWithDiscount : chargeWithoutDiscount;

    const promotionMarkup = () => {
      if (promotion !== 0) {
        return `
          <tr>
            <td class="small promotion">Promotion</td>
            <td class="price promotion">$ ${promotion}</td>
          </tr>
        `;
      } else if (referralName !== null && referralName !== undefined) {
        return `
          <tr>
            <td class="small promotion">Referral</td>
            <td class="price promotion">$ 20</td>
          </tr>
        `;
      }
      return '';
    };

    sendEmail({
      to: user.get('email'),
      subject: 'Order confirmed',
      template_id: sendgridConfig.templates.confirmationEmailNew,
      substitutions: {
        shippingAddress: `${user.get('name')} ${user.get('lastName')}, ${user.get('address')}, ${user.get('addressExtra')}, ${user.get('city')}, ${user.get('state')} ${user.get('zipCode')}, United States`,
        shippingDate: moment(subscriptions[0].get('startFrom'), 'YYYY-MM-DD HH:MM:SS').add(2, 'd').format('MM/DD'),
        frequency,
        mealsQuantity,
        breakFastCount,
        anytimeMealsCount,
        table1: `
          <div class="table-1">
            <table class="border">
              ${productMarkup}
              <tr>
                <td class="text">Access to the Baby Food Hotline</td>
                <td class="price">FREE with membership</td>
              </tr>
            </table>
          </div>
        `,
        table2: `
        <div class="table-2">
          <table class="border margin">
            ${promotionMarkup()}
            <tr>
              <td class="small">Shipping</td>
              <td class="price">FREE with membership</td>
            </tr>
            <tr>
              <td class="small">Today's total</td>
              <td class="price">$ ${chargeWithDiscount}</td>
            </tr>
            <tr>
              <td class="small">Next Charge</td>
              <td class="price">$ ${recurringCharge}</td>
            </tr>
          </table>
        </div>
        `,
        permalink: `${user.get('name').toUpperCase().split(' ').join('')}${user.get('id')}`,
      },
    })
      .then(() => {
        console.log(`Sendgrid mail New Customer OK (${user.get('email')})`);
        resolve();
      })
      .catch((err) => {
        console.log(`Sendgrid mail New Customer Error: ${err}`);
        reject(err);
      });
  });
};


// { token, referralCode }
export const setCard = (userId, userData, isUpdateCard) => {
  const cardToReturn = {
    data: null,
    error: null,
    errorStripe: null,
  };
  const logInfo = {
    user: userId,
    admin: null,
    type: '',
    message: '',
  };

  let isCouponForever = false;
  let promotion = 0;
  let chargeWithDiscount = 0;
  let chargeWithoutDiscount = 0;
  const appliedDiscounts = [];

  return new Promise((resolve, reject) => {
    findUserById(userId)
      .then((user) => {
        const subscriptions = user.get('subscriptions');
        const customerId = user.get('customerId');
        const infoCharge = {
          code: null,
          discount: null,
          extra_info: null,
          finalAmount: 0,
        };
        let couponRedeemed = false;
        each(subscriptions, async (sub) => {
          const amount = sub.get('amount');
          const productId = sub.get('product').get('id');
          const productPrice = sub.get('product').get('price');
          const coupon = sub.get('coupon');
          // infoCharge.finalAmount += productPrice * amount
          if (coupon) {
            if (coupon.get('duration') === 'forever') isCouponForever = true;
          }
          let subscriptionPrice = productPrice * amount;
          chargeWithoutDiscount += subscriptionPrice;

          if (coupon && (sub.get('coupon').get('productId') === null || sub.get('coupon').get('productId') === productId)) {
            let amountOff;
            let percentOff;

            /* If user uses a coupon of duration custom_boxes */
            if (coupon.duration === 'custom_boxes') {
              const boxList = JSON.parse(coupon.boxList);
              const boxOneDiscount = getDiscountByBox(boxList, 1);
              amountOff = boxOneDiscount.amountOff;
              percentOff = boxOneDiscount.percentOff;
            } else {
              amountOff = coupon.get('amountOff') / subscriptions.length;
              percentOff = coupon.get('percentOff');
            }

            // Will sum1 to times_redeemed on RR DB
            if (!couponRedeemed) {
              try {
                await redeemCoupon(coupon, user);
              } catch (err) {
                console.log(err);
              }
              couponRedeemed = true;
              logInfo.type = 'info';
              logInfo.message = `Coupon ${coupon.get('code')} was redeemed by ${user.get('name')} ${user.get('lastName')}`;
              LogDB(logInfo);
            }

            infoCharge.code = coupon.get('code');
            infoCharge.discount = (amountOff) ? `USD ${amountOff / 100}` : `${percentOff}%`;
            promotion += subscriptionPrice - calcCouponDiscount(percentOff, amountOff, subscriptionPrice); // Calc the discount
            appliedDiscounts.push({
              type: 'subscriptionCoupon',
              couponId: coupon.get('id'),
              amount: subscriptionPrice - calcCouponDiscount(percentOff, amountOff, subscriptionPrice),
            });
            subscriptionPrice = calcCouponDiscount(percentOff, amountOff, subscriptionPrice);

            // Patch for one-time coupons being applied twice bug
            if (coupon.get('duration') === 'once') {
              try {
                await sub.set('couponId', null);

                // Log coupon deletion
                logInfo.type = 'info';
                logInfo.message = `Coupon ${coupon.get('code')} is one-time use. Deleted from user`;
                LogDB(logInfo);
              } catch (error) {
                console.log('One-time coupon deletion error: ', error);
              }
            }
          }
          if (userData.referralCode) {
            infoCharge.code = 'Discount by referral';
            infoCharge.discount = 'USD 20';
            subscriptionPrice -= config.referralFirstDiscount / subscriptions.length;
            promotion += config.referralFirstDiscount / subscriptions.length;
            logInfo.type = 'info';
            logInfo.message = `Subscription id: ${sub.get('id')} has USD ${config.referralFirstDiscount / subscriptions.length} discount by referral.`;
            LogDB(logInfo);
            appliedDiscounts.push({
              type: 'subscriptionReferral',
              amount: config.referralFirstDiscount / subscriptions.length,
              referreer: parseInt(userData.referralCode.substring(userData.referralCode.indexOf('_') + 1)),
            });
          }
          infoCharge.finalAmount += subscriptionPrice;
          chargeWithDiscount += infoCharge.finalAmount;
        });
        createCard(customerId, userData.token)
          .then(async (card) => {
            if (isUpdateCard) {
              try {
                await updateDefaultCard(customerId, card.id);
              } catch (err) {
                if (err) console.log(err);
              }
            }
            generateFirstCharge(customerId, infoCharge, appliedDiscounts, user.get('id'))
              .then(async (ch) => {
                subscriptions.forEach((subscription) => {
                  subscription.set('status', 'active');
                  subscription.save();
                });

                const zendeskData = {
                  email: user.get('email'),
                  name: `${user.get('name')} ${user.get('lastName')}`,
                  user_fields: {
                    lead_status: 'customer',
                    customer_status: 'active',
                  },
                };

                // hubspot
                triggerHubspotWorkflow(configHubspot.workflows.newCustomer, user.get('email'))
                .then(() => console.log(`Hubspot workflow triggers ${configHubspot.workflows.newCustomer} OK`))
                  .catch(err => console.log(`Hubspot Trigger Id > ${configHubspot.workflows.newCustomer} --- Message > ${err}`));

                // sendgrid
                let referralName;
                if (Object.prototype.hasOwnProperty.call(userData, 'referralCode') && Object.prototype.hasOwnProperty.call(userData, 'name')) {
                  if (userData.referralCode !== null) {
                    sendReferralEmail(userData.name, userData.referralCode);
                    referralName = userData.name;
                  }
                }
                sendConfirmationEmail(user.get('id'), referralName);

                upsertZendesk(zendeskData)
                  .then(() => console.log('Zendesk upsert in generateCharge OK'))
                  .catch(err => console.log(`Zendesk upsert failed in generateCharge --- Message > ${err.result}`));

                sendHubspotForm(configHubspot.portalId, configHubspot.forms.signup, {
                  firstname: user.get('name'),
                  lastname: user.get('lastName'),
                  state: user.get('state'),
                  city: user.get('city'),
                  email: user.get('email'),
                  lead_status: 'Customer',
                  subscription_status: 'active',
                })
                  .then(() => console.log(`Hubspot Form Signup > Submit form for ${user.get('email')} success`))
                  .catch(err => console.log(`Hubspot Form Signup > Can't submit form for ${user.get('email')}`));

                user.set('firstChargeAmount', infoCharge.finalAmount);
                user.set('firstCharge', ch.id);
                const status = getSubscriptionStatus(subscriptions);
                user.set('status', status);
                LogDBUserStatus({
                  userId: user.get('id'),
                  userStatus: status,
                  userStatusDate: moment(),
                });

                if (status === 'none') {
                  sendEmail({
                    substitutions: {
                      body: `
                          Name: ${user.get('name')} <br />
                          Lastname: ${user.get('lastName')} <br />
                          E-mail: ${user.get('email')}
                      `,
                    },
                    subject: 'Alert: User with status None',
                    template_id: sendgridConfig.templates.rrTemplate,
                  })
                    .then(() => console.log('Sendgrid mail User Status None OK'))
                    .catch(err => console.log('Sendgrid mail User Status None Error'));
                }

                if (status === 'pending') {
                  sendEmail({
                    substitutions: {
                      body: `
                          Name: ${user.get('name')} <br />
                          Lastname: ${user.get('lastName')} <br />
                          E-mail: ${user.get('email')}
                      `,
                    },
                    subject: 'Alert: User with status Pending',
                    template_id: sendgridConfig.templates.rrTemplate,
                  })
                    .then(() => console.log('Sendgrid mail User Status Pending OK'))
                    .catch(err => console.log('Sendgrid mail User Status Pending Error'));
                }


                logInfo.user = user.get('id');
                logInfo.type = 'info';
                logInfo.message = `New charge -> ${ch.id}, amount -> ${infoCharge.finalAmount}`;
                LogDB(logInfo);
                user.save();
                cardToReturn.data = card;
                resolve(cardToReturn);
              })
              .catch((err) => {
                subscriptions.forEach((subscription) => {
                  subscription.set('status', PENDING);
                  subscription.save();
                });
                const status = getSubscriptionStatus(subscriptions);
                user.set('status', status);
                LogDBUserStatus({
                  userId: user.get('id'),
                  userStatus: status,
                  userStatusDate: moment(),
                });

                if (status === 'none') {
                  sendEmail({
                    substitutions: {
                      body: `
                          Name: ${user.get('name')} <br />
                          Lastname: ${user.get('lastName')} <br />
                          E-mail: ${user.get('email')}
                      `,
                    },
                    subject: 'Alert: User with status None',
                    template_id: sendgridConfig.templates.rrTemplate,
                  })
                    .then(() => console.log('Sendgrid mail User Status None OK'))
                    .catch(err => console.log('Sendgrid mail User Status None Error'));
                }

                sendHubspotForm(configHubspot.portalId, configHubspot.forms.signup, {
                  email: user.get('email'),
                  firstname: user.get('name'),
                  lastname: user.get('lastName'),
                  zip: user.get('zipCode'),
                  lead_status: 'Customer',
                  city: user.get('city'),
                  phone: user.get('phoneNumber'),
                  state: user.get('state'),
                  address: user.get('address'),
                })
                  .then(() => console.log(`Hubspot Form Signup > Submit form for ${user.get('email')} success`))
                  .catch(() => console.log(`Hubspot Form Signup > Can't submit form for ${user.get('email')}`));

                upsertZendesk({
                  email: user.get('email'),
                  name: `${user.get('name')} ${user.get('lastName')}`,
                  user_fields: {
                    customer_status: 'pending',
                  },
                });

                sendEmail({
                  substitutions: {
                    body: `
                        Name: ${user.get('name')} <br />
                        Lastname: ${user.get('lastName')} <br />
                        E-mail: ${user.get('email')}
                    `,
                  },
                  subject: 'Alert: User with status Pending',
                  template_id: sendgridConfig.templates.rrTemplate,
                })
                  .then(() => console.log('Sendgrid mail User Status Pending OK'))
                  .catch(() => console.log('Sendgrid mail User Status Pending Error'));

                cardToReturn.errorStripe = err.raw;
                logInfo.type = 'error';
                logInfo.message = `Stripe charge failed, the error was ${JSON.stringify(err.raw.message)}`;
                LogDB(logInfo);
                resolve(cardToReturn);
              });
          })
          .catch((err) => {
            if (err) {
              cardToReturn.errorStripe = err.raw;
              logInfo.type = 'error';
              logInfo.message = `Create card failed ${JSON.stringify(err.raw)}`;
              LogDB(logInfo);

              sendEmail({
                substitutions: {
                  body: `
                      Name: ${user.get('name')} <br />
                      Lastname: ${user.get('lastName')} <br />
                      E-mail: ${user.get('email')}
                  `,
                },
                subject: 'Alert: User with status Pending',
                template_id: sendgridConfig.templates.rrTemplate,
              })
                .then(() => console.log('Sendgrid mail User Status Pending OK'))
                .catch(() => console.log('Sendgrid mail User Status Pending Error'));
            }
            resolve(cardToReturn);
          });
      })
      .catch((err) => {
        cardToReturn.error = err;
        reject(cardToReturn);
      });
  });
};


export const findUserByEmail = function (email) {
  return User.findOne({
    where: {
      email,
    },
  });
};

export const checkUSAZipcodeWithData = (zipCode) => {
  const validStates = ['AL', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'];
  const response = zipcodes.lookup(zipCode);
  if (response === undefined) return false;
  return { valid: validStates.indexOf(response.state) !== -1, data: response };
};

export const checkUSAZipcode = (zipCode) => {
  const validStates = ['AL', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'];
  const response = zipcodes.lookup(zipCode);
  if (response === undefined) return false;
  return validStates.indexOf(response.state) !== -1;
};

export const stripeAddBillingAddressToActiveUsers = async () => {
  const activeUsers = await Subscription.findAll({
    where: { status: 'active' },
    include: [{
      model: User,
    }],
  });

  eachLimit(activeUsers, 1, async (sub, cb) => {
    try {
      const plainSubscription = sub.get({ plain: true });
      await updateStripeBillingAddress(plainSubscription.user);
      console.log(`Billing info updated for user #${plainSubscription.user.id}`);
      cb();
    } catch (error) {
      console.log(error);
    }
  }, () => console.log('Finished successfully: Billing info updated for all users'));
};
