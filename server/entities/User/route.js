import passport from 'passport';
import { checkoutUser, checkUser, usersCount, updateUsersStatus, validEmail, getUsers, getUserById, getUserByHash, updateCheckout, updateUser, setNoPaymentLead, outOfAreaLead, syncUsers, deleteUser, downloadCSV, updateCreditCard, getUserByIdWithDetails, checkZipcode, getUserTimeActiveInMonths, getRetentionInfoJson, getDimensionsByUserId, getSkipsDatesWithUserInfo, serveUserStatusPerCron, getUserStatusPerCronCsv } from './controller';
import { stripeAddBillingAddressToActiveUsers } from './plugin';

export default (router) => {
  router.post('/users/new',
    checkoutUser,
  );
  router.post('/users/email',
    validEmail,
  );
  router.post('/users/outofarea',
    outOfAreaLead,
  );
  router.post('/users/nopayment',
    setNoPaymentLead,
  );
  router.post('/users/referral/valid',
    checkUser,
  );
  router.get('/users/count',
    usersCount,
  );
  router.post('/users/check-zipcode',
    checkZipcode,
  );
  router.get('/users/status',
    updateUsersStatus,
  );
  router.get('/users/hash/:hash',
    getUserByHash,
  );
  router.put('/users/hash/:hash',
    updateCheckout,
  );
  router.put('/users/:hash/updateCreditCard',
    updateCreditCard,
  );

  // Admin routes with authentication.
  router.post('/users/sync', passport.authenticate('jwt', { session: false }),
    syncUsers,
  );
  router.get('/users/download',
    passport.authenticate('jwt', { session: false }),
    downloadCSV,
  );
  router.get('/users', passport.authenticate('jwt', { session: false }),
    getUsers,
  );
  router.get('/users/:id', passport.authenticate('jwt', { session: false }),
    getUserById,
  );
  router.put('/users/:id', passport.authenticate('jwt', { session: false }),
    updateUser,
  );
  router.delete('/users/:id', passport.authenticate('jwt', { session: false }),
    deleteUser,
  );
  router.get('/users/:id/details', passport.authenticate('jwt', { session: false }),
    getUserByIdWithDetails,
  );
  router.get('/users/stripe-add-billing-info-RGbVmixyfEsXgR0P23QQO7HXzrq0bc',
    stripeAddBillingAddressToActiveUsers,
  );

  // Data Source

  router.get('/users-time-active-in-months',
    getUserTimeActiveInMonths,
  );

  router.get('/users-retention-info',
    getRetentionInfoJson,
  );

  router.get('/dimensions-by-user-id',
    getDimensionsByUserId,
  );

  router.get('/users-status-per-cron-json',
    serveUserStatusPerCron,
  );

  router.get('/users-status-per-cron-csv',
    getUserStatusPerCronCsv,
  );

  router.get('/get-user-skips-with-info-NvyFfAUMDcmv8hGqHEhWpiudSIExlT',
    getSkipsDatesWithUserInfo,
  );
};
