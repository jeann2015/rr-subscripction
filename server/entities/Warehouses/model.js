export default (sequelize, DataTypes) => {
  return sequelize.define('warehouses', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    tagId: {
      type: DataTypes.INTEGER,
      field: 'tag_id',
      allowNull: false,
    },
    warehouse: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    freezeTableName: true,
    timestamps: false,
  });
};
