export const getDiscountByBox = (boxList, boxNumber) => {
  const getBoxCouponrules = (boxList, boxNumber) => {
    const validCoupons = boxList.filter(el => el.boxNumber === boxNumber || (boxNumber >= el.starts && boxNumber <= el.ends) || (boxNumber >= el.starts && el.ends === -1));
    return validCoupons;
  };
  let amountOff;
  let percentOff;
  let boxRules = getBoxCouponrules(boxList, boxNumber);
  if (boxRules.length > 0) {
    boxRules = boxRules[0];
    if (boxRules.type === 'ammountOff') {
      amountOff = boxRules.ammount;
    } else if (boxRules.type === 'percentOff') {
      percentOff = boxRules.ammount;
    }
  } else {
    amountOff = 0;
    percentOff = 0;
  }
  amountOff = (amountOff === undefined) ? null : amountOff;
  percentOff = (percentOff === undefined) ? null : percentOff;
  return { amountOff, percentOff };
};
