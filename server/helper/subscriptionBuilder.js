import moment from 'moment';

/**
 *
 * @param subscription: The subscription object. It defers if it comes from the
 * 'isFirstOrder' query.
 * @param isFirstOrder: Specifies whether we are coming from a firstOrder query
 * or not.
 */
export const getSubscriptionAsObject = (subscription, isFirstOrder) => {
  if (isFirstOrder) {
    return {
      subscriptionId: subscription.get('id'),
      status: subscription.get('status'),
      customerId: subscription.get('customer_id'),
      startFrom: subscription.get('start_from'),
      pausedUntil: subscription.get('paused_until'),
      subscriptionDuration: subscription.get('duration'),
      discountAmountOff: subscription.get('amount_off'),
      discountPercentOff: subscription.get('percent_off'),
      userId: subscription.get('user_id'),
      couponUseCount: subscription.get('coupon_use_count'),
      couponTimesUsable: subscription.get('times_usable'),

      // We need to check the coupon use count and the coupon times usable if they are special.
      isSpecialDiscount: subscription.get('duration') === 'custom',

      firstCharge: subscription.get('first_charge'),
      name: subscription.get('name'),
      lastName: subscription.get('last_name'),
      address: subscription.get('address'),
      addressExtra: subscription.get('address_extra'),
      email: subscription.get('email'),
      city: subscription.get('city'),
      state: subscription.get('state'),
      zipCode: subscription.get('zip_code'),
      price: subscription.get('price'),
      amount: subscription.get('amount'),
      boxType: subscription.get('box_type'),
      code: subscription.get('code'),
      nextSkipDate: subscription.get('next_skip_date'),
    };
  }

  // Did not come from firstOrder query

  const plainSub = subscription.get({ plain: true });
  const now = moment.utc();

  return {
    days: plainSub.days,
    diff: Math.round(now.diff(plainSub.lastOrder, 'days', true)),
    subscriptionId: plainSub.id,
    status: plainSub.status,
    pausedUntil: plainSub.paused_until,
    price: plainSub.price,
    amount: plainSub.amount,
    totalPrice: plainSub.price * plainSub.amount,
    discountAmountOff: plainSub.amount_off,
    discountPercentOff: plainSub.percent_off,
    couponUseCount: plainSub.coupon_use_count,
    couponTimesUsable: plainSub.times_usable,
    subscriptionDuration: plainSub.duration,
    lastOrder: plainSub.lastOrder,
    userId: plainSub.user_id,
    code: subscription.get('code'),
    name: subscription.get('name'),
    lastName: subscription.get('last_name'),
    nextDiscount: plainSub.next_discount,
    typeNextDiscount: plainSub.type_next_discount,

    customerId: plainSub.customer_id,
    address: plainSub.address,
    addressExtra: plainSub.address_extra,
    email: plainSub.email,
    city: plainSub.city,
    state: plainSub.state,
    zipCode: plainSub.zip_code,
    boxType: plainSub.box_type,
    nextSkipDate: plainSub.next_skip_date,

    // We need to check the coupon use count and the coupon times usable if they are special.
    isSpecialDiscount: plainSub.duration === 'custom',
  };
};

/**
 * Gets the needed parameters to generate a new order.
 * Make sure the subscription object is the one defined in this scope, or that it has
 * the required params.
 * Not getting stuff like productPrice, chargeToken or boxType
 * because sometimes, we must specify a null value to show that it's
 * a free or charge order.
 * @param subscription -> subscription object.
 * @param productPrice -> price of the product.
 * @param chargeToken
 * @param boxType
 */
export const getSubscriptionOrderData = (subscription, productPrice, chargeToken, boxType) => {
  return {
    userId: subscription.userId,
    customerId: subscription.customerId,
    subscriptionId: subscription.subscriptionId,
    chargeToken: chargeToken,
    name: subscription.name,
    lastName: subscription.lastName,
    address: subscription.address,
    addressExtra: subscription.addressExtra,
    email: subscription.email,
    city: subscription.city,
    state: subscription.state,
    zipCode: subscription.zipCode,
    boxType: boxType,
    productPrice: productPrice,
  };
};
