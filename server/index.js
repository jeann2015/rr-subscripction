import express from 'express';
import cors from 'cors';
import compression from 'compression';
import next from 'next';
import passport from 'passport';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';
import logger from 'morgan';
import postcssEasyImport from 'postcss-easy-import';
import postcssNext from 'postcss-cssnext';
import cssNano from 'cssnano';
import postcssMiddleware from 'postcss-middleware';
import path from 'path';
// import cssMiddleware from './middlewares/assets'
import './db/connection';
import { customValidator, corsOptions } from './utils';
import { development, production } from './middlewares/error';
import strategy from './middlewares/authenticate';
import apiRoutes from './routes';
// Import and Start Cron (Cron/Shipstation/Email)
import Cron from './services/cron';
import AuthCron from './services/auth_cron';
import CronShipstation from './services/shipstation_runner';
import CronHubspotWeeklyList from './services/hubspot_weekly';
import CronHubspotSendEmailTracking from './services/tracking_hubspot_cron';
import config from './config/config';
if (config.shouldRunCrons) {
  AuthCron();
  Cron();
  CronShipstation();
  CronHubspotWeeklyList();
  CronHubspotSendEmailTracking();
}

const dev = process.env.NODE_ENV !== 'production';
const appNext = next({ dev });
const handler = appNext.getRequestHandler();
const port = process.env.PORT || 3000;
const morganType = (dev) ? 'dev' : 'common';
const app = express();
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(passport.initialize());
passport.use(strategy);
app.use(compression());
app.use(logger(morganType));
app.use(expressValidator({ customValidators: customValidator }));
appNext.prepare().then(() => {});
app.use('/assets', postcssMiddleware({
  src: req => path.join(path.resolve(), path.join('public', req.path)),
  plugins: [
    postcssEasyImport({ prefix: '_' }),
    postcssNext({ warnForDuplicates: false }),
    cssNano(),
  ],
}));

// Routes ~ Api
app.use('/api', apiRoutes);
app.use('/admin', apiRoutes);
app.get('/healthcheck', (req, res) => {
  res.json({ success: true });
});

// Routes ~ WebApp
const webApp = express.Router();

webApp.get('*', (req, res) => {
  return handler(req, res);
});

app.use(webApp);

// error handlers
if (dev) {
  app.use(development);
} else {
  app.use(production);
}
// Listen node server
app.listen(port, (err) => {
  if (err) throw err;
  console.log(`> Ready on http://localhost:${port}`);
});

