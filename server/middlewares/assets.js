import postcssMiddleware from 'postcss-middleware';
import path from 'path';

export default postcssMiddleware({
  src: () => {
    return path.join('components', 'styles.css');
  },
  plugins: [
    require('postcss-easy-import')({ prefix: '_' }),
    require('postcss-cssnext')({ warnForDuplicates: false }),
    require('cssnano')(),
  ],
});
