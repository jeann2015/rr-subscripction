import passportJWT from 'passport-jwt';
import { jwtOptions } from '../config/config';

const db = require('../db/connection');

const Admin = db.Admin;

const JwtStrategy = passportJWT.Strategy;

export default new JwtStrategy(jwtOptions(), (jwtPayload, next) => {
  Admin.find({
    where: { id: jwtPayload.id },
    raw: true,
  }).then((admin) => {
    if (admin) {
      next(null, admin);
    } else {
      next(null, false);
    }
  }, (err) => {
    next(err);
  });
});
