export const development = (err, req, res) => {
  const customError = {
    message: err.message,
    error: err,
  };
  res.status(err.status || 500);
  console.log(err);
  res.format({
    json: () => {
      res.json(customError);
    },
    html: () => {
      res.render('error', { err: customError });
    },
  });
};

export const production = (err, req, res) => {
  const customError = {
    message: err.message || 'Not Found',
    error: false,
  };

  res.status(err.status || 500);
  res.format({
    json: () => {
      res.json(customError);
    },
    html: () => {
      res.render('error', customError);
    },
  });
};
