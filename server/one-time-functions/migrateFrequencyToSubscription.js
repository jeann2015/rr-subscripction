import { eachLimit } from 'async';
import db from '../db/connection';

const Subscription = db.Subscription;
const User = db.User;
const Product = db.Product;

export const getSubscriptions = async () => {
  try {
    const subscriptions = await db.Subscription.findAll({
      include: [
        db.User,
        db.Product,
      ],
    });
    return subscriptions;
  } catch (err) {
    console.log(`An error occurred while getting subscriptions on DB ${err}`);
  }
};

export const isRegularBox = (productId) => {
  let result = false;
  if (productId === 3 || productId === 4 || productId === 5 || productId === 7 || productId === 8 || productId === 19 || productId === 20) {
    result = true;
  }
  return result;
};


export const updateFrequencyOnSubscription = (subscriptions = []) => {
  console.log('Updating Frequency');
  return new Promise(async (resolve, reject) => {
    eachLimit(subscriptions, 1, async (subscription, cb) => {
      const plain = subscription.get({ plain: true });
      if (plain.productId) {
        const days = plain.product.days;
        const weeks = days / 7;
        const subscriptionId = plain.id;
        Subscription.update({ frequency: weeks }, { where: { id: subscriptionId } })
        .then(() => {
          console.log(`Updated susbcription id # ${subscriptionId}`);
          cb();
        })
        .catch((err) => {
          console.log(err);
        });
      }
    }, (err) => {
      if (err) {
        console.log(err);
        reject(err);
      }
      resolve('Done');
    });
  });
};

export const migrateFrequencyData = async () => {
  try {
    const subscriptions = await getSubscriptions();
    await updateFrequencyOnSubscription(subscriptions);
    console.log('Finished');
  } catch (err) {
    console.log(err);
  }
};

migrateFrequencyData();

