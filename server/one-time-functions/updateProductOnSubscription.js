import { eachLimit } from 'async';
import db from '../db/connection';
import config from '../config/config';


const Subscription = db.Subscription;
const User = db.User;
const Product = db.Product;

export const getSubscriptions = async () => {
  try {
    const subscriptions = await db.Subscription.findAll({
      include: [
        db.User,
        db.Product,
      ],
    });
    return subscriptions;
  } catch (err) {
    console.log(`An error occurred while getting subscriptions on DB ${err}`);
  }
};

export const isRegularBox = (productId) => {
  let result = false;
  if (productId === 3 || productId === 4 || productId === 5 || productId === 7 || productId === 8 || productId === 19 || productId === 20) {
    result = true;
  }
  return result;
};

export const updateProductIdOnSubscription = (subscriptions = []) => {
  console.log('Updating Product Id on Subscription');
  return new Promise(async (resolve, reject) => {
    eachLimit(subscriptions, 1, async (subscription, cb) => {
      const plain = subscription.get({ plain: true });
      const subscriptionId = plain.id;
      if (plain.productId) {
        // WARNING => prodcutId should be the new 20 Meals product
        let newProductId = plain.productId;
        if (isRegularBox(plain.productId)) { newProductId = 23; }
        Subscription.update({ productId: newProductId }, { where: { id: subscriptionId } })
        .then(() => {
          console.log(`Updated product id on subscription ${subscriptionId}`);
          cb();
        })
        .catch((err) => {
          console.log(err);
        });
      }
    }, (err) => {
      if (err) {
        console.log(err);
        reject(err);
      }
      resolve('Done');
    });
  });
};


export const updateProductSubscription = async () => {
  try {
    const subscriptions = await getSubscriptions();
    await updateProductIdOnSubscription(subscriptions);
    console.log('Finished');
  } catch (err) {
    console.log(err);
  }
};

updateProductSubscription();

