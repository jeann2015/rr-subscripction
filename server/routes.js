import express from 'express';
import UserRoutes from './entities/User/route';
import AdminRoutes from './entities/Admin/route';
import OrderRoutes from './entities/Order/route';
import ChildRoutes from './entities/Child/route';
import CronRoutes from './entities/Cron/route';
import SubscriptionRoutes from './entities/Subscription/route';
import ProductRoutes from './entities/Product/route';
import CouponRoutes from './entities/Coupon/route';
import ReferralRoutes from './entities/Referral/route';
import LogRoutes from './entities/Log/route';
import RefundRoutes from './entities/Refund/route';
import AppliedDiscounts from './entities/AppliedDiscounts/route';
import MealsRoutes from './entities/Meals/route';
import StockRoutes from './entities/MealsStock/route';
import IngredientsRoutes from './entities/Ingredients/route';
import ExcludedIngredientsRoutes from './entities/ExcludedIngredients/route';

const router = express.Router();

router.get('/healthcheck', (req, res) => {
  res.json({ success: true });
});

// User routes
UserRoutes(router);
AdminRoutes(router);
OrderRoutes(router);
ChildRoutes(router);
CronRoutes(router);
SubscriptionRoutes(router);
ProductRoutes(router);
CouponRoutes(router);
ReferralRoutes(router);
LogRoutes(router);
RefundRoutes(router);
AppliedDiscounts(router);
MealsRoutes(router);
StockRoutes(router);
IngredientsRoutes(router);
ExcludedIngredientsRoutes(router);

export default router;
