import CronJob from 'cron';
import moment from 'moment';
import { createFirstOrders, getLatestOrders, updatePauseUntil } from './plugin';
import { getSubscriptionsAndRunOrders } from './cron_runner';
import config from '../config/config';

const cronJob = CronJob.CronJob;
const db = require('../db/connection');

const Cron = db.Cron;

const zeroDay = moment.utc(config.zeroDay, 'YYYYMMDD');
const dayWhenRepeat = config.dayWhenRepeat;

export const runOrders = () => {
  Cron.create()
    .then((cron) => {
      createFirstOrders(cron)
        .then(() => {
          getLatestOrders(cron.get('id'));
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const runExpiredPaused = () => {
  updatePauseUntil();
};

export const runCron = () => {
  console.log('Run Cron on auth_cron');
  const now = moment.utc();
  const diff = now.diff(zeroDay, 'days');
  const diffRemainder = diff % dayWhenRepeat;
  if (diffRemainder === 0) {
    getSubscriptionsAndRunOrders(true);
  } else {
    runExpiredPaused();
  }

  const cyclesPassed = (diff - diffRemainder) / dayWhenRepeat;
  const daysFirstCycle = (cyclesPassed + 1) * dayWhenRepeat;
  const firstShippingDate = zeroDay.clone().add(daysFirstCycle, 'days');

  console.log(`Auth Cron runned at ${now.format('MM-DD-YYYY kk:mm')}, next shipping date => ${firstShippingDate.format('MM-DD-YYYY kk:mm')}`);
};

const instance = () => {
  return new cronJob('00 00 00 * * *', () => {
    runCron();
  }, null, true, 'America/Los_Angeles');
};
export default instance;

