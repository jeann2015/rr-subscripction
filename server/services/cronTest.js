import config from '../config/config';
import { getSubscriptionsAndRunOrders } from './cron_runner';
import db from '../db/connection';

const forceCron = async () => {
  const orders = await getSubscriptionsAndRunOrders();
  console.log('Total Orders', orders.length);
};

console.log('Run');
forceCron();
