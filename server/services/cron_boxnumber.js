const groupBy = (list, keyGetter) => {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
};

export const incrementBoxNumber = (data) => {
  const { newOrders, orderCount } = data;
  const groupedByCron = groupBy(orderCount, (order) => {
    const plainOrder = order.get({ plain: true });
    return plainOrder.cronId;
  });

  const boxNumber = groupedByCron.size + 1;

  for (const order of newOrders) {
    order.set('boxNumber', boxNumber);

    if (boxNumber === 1) {
      const plainOrder = order.get({ plain: true });
      const mealsQuantity = plainOrder.boxType.split(' ')[0];
      const includeMigrated = plainOrder.boxType.includes('Migrated') ? ' Migrated' : '';
      order.set('boxType', `First Box ${mealsQuantity} Meals${includeMigrated}`);
    }
  }
  data.newOrders = newOrders;

  return {
    data,
  };
};
