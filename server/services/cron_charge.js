import Stripe from 'stripe';
import config from '../config/config';
import { getSubscriptionCycleNumber } from './cron_new';
import { applyCreditDiscount, applyNextDiscount, applyCustomBoxesDiscount, applyCouponDiscount, appliedDiscountsRegistry, shouldCalculateDiscount } from '../utils/calculate-discounts';
import { LogDB } from '../entities/Log/plugin';

const stripe = Stripe(config.stripeApiKey);

export const applyDiscounts = (data, appliedDiscounts) => {
  const plain = data.get({ plain: true });

  const logInfo = {
    user: plain.user.id,
    admin: null,
    type: 'info',
    message: '',
  };

  for (const discount of appliedDiscounts) {
    if (discount.type === 'userCredit') {
      const user = data.get('user');
      user.set('credit', discount.creditLeft);
    }

    if (discount.type === 'subscriptionNextDiscount') {
      data.set('nextDiscount', null);
      data.set('typeNextDiscount', null);
    }

    if (discount.type === 'subscriptionCoupon') {
      const coupon = data.get('coupon').get({ plain: true });

      // If coupon duration is null, applies only once
      if (coupon && coupon.duration === null) {
        coupon.duration = 'once';
      }

      if (coupon.duration === 'custom') {
        // Coupon has a max amount of times to redeem
        data.set('couponUseCount', plain.couponUseCount + 1);
        // Log coupon use count
        logInfo.message = `Coupon ${coupon.code} used ${plain.couponUseCount + 2} time out of ${plain.coupon.timesUsable}`;
        LogDB(logInfo);

        if (data.get('couponUseCount') >= plain.coupon.timesUsable - 1) {
          // Coupon was redeem to many times
          data.set('couponUseCount', 0);
          data.set('couponId', null);
          // Log coupon deletion
          logInfo.message = `Limit of uses for coupon ${coupon.code} reached. Deleted from user`;
          LogDB(logInfo);
        }
      }

      if (coupon.duration === 'custom_boxes') {
        const coupon = data.get('coupon').get({ plain: true });
        const boxList = JSON.parse(coupon.boxList);
      }

      if (coupon.duration === 'once') {
        // Coupon can be used only once. Delete Coupon
        data.set('couponUseCount', 0);
        data.set('couponId', null);
        logInfo.message = `Coupon ${coupon.code} is one-time use. Deleted from user`;
        LogDB(logInfo);
      }
    }
  }

  return data;
};

export const calculateDiscounts = (data, date) => {
  const plain = data.get({ plain: true });
  const productCost = plain.product.price;
  const fullPrice = productCost * plain.amount;

  let totalPrice = fullPrice;
  const appliedDiscounts = [];

  let simulated = false;
  if (date) {
    // Date is not passed on real cron. Just for simulations
    simulated = true;
  }

  // Normalize coupon. In the past duration was null.
  if (plain.coupon && plain.coupon.duration === null) {
    plain.coupon.duration = 'once';
  }
  let userCredit = plain.user.credit;

  /*
  * TYPES OF DISCOUNTS USED AT RR |2018 - 19|
  * 1. Credit
  * 2. NextDiscount
  * 3. Custom Boxes
  * 4. Coupon
  */

  // cycleCurrent counts current cycle being processed. Even if simulated
  const cycleCurrent = getSubscriptionCycleNumber(data, date);

  // Only calc after first cron || Have a new cycle.
  if (cycleCurrent >= 1 || plain.newCycleStart !== null) {
    if (plain.typeNextDiscount) {
      if (shouldCalculateDiscount) {
        const typeNextDiscount = plain.typeNextDiscount;
        const nextDiscount = plain.nextDiscount;
        const res = applyNextDiscount(typeNextDiscount, nextDiscount, totalPrice);
        totalPrice = res.productPrice;

        const registry = appliedDiscountsRegistry({ type: 'subscriptionNextDiscount', typeNextDiscount, nextDiscount });
        appliedDiscounts.push(registry);
      }
    }

    if (plain.coupon && plain.coupon.boxList) {
      const boxList = plain.coupon.boxList;
      const firstCharge = plain.user.firstCharge;
      const orders = plain.orders;
      const res = applyCustomBoxesDiscount(boxList, firstCharge, orders, totalPrice);
      totalPrice = res.productPrice;
    }

    if (plain.coupon && plain.coupon.amountOff || plain.coupon && plain.coupon.percentOff) {
      if (shouldCalculateDiscount) {
        const coupon = plain.coupon;
        const discountAmount = coupon.percentOff ? fullPrice * (coupon.percentOff * 0.01) : coupon.amountOff;
        const couponId = coupon.id;
        const couponUseCount = plain.couponUseCount;
        const res = applyCouponDiscount(coupon, totalPrice, couponUseCount, discountAmount);
        totalPrice = res.productPrice;

        const registry = appliedDiscountsRegistry({ type: 'subscriptionCoupon', couponId, discountAmount });
        appliedDiscounts.push(registry);
      }
    }

    if (plain.user.credit > 0) {
      if (shouldCalculateDiscount) {
        const res = applyCreditDiscount(userCredit, totalPrice);
        totalPrice = res.productPrice;
        userCredit = res.userCredit;

        const registry = appliedDiscountsRegistry({ type: 'userCredit', userCredit: plain.user.credit, creditLeft: userCredit });
        appliedDiscounts.push(registry);
      }
    }
  }

  const discountAmount = (fullPrice - totalPrice);
  const discountPercent = Number.parseFloat((100 - ((totalPrice * 100) / fullPrice)).toFixed(1));
  data.pricing = {
    fullPrice,
    totalPrice,
    discountAmount,
    discountPercent,
    appliedDiscounts,
  };
  const dataWithAppliedDiscounts = applyDiscounts(data, appliedDiscounts);
  if (
    plain.user.firstChargeAmount !== null &&
    (
      (!simulated) ||
      (simulated && cycleCurrent === 0)
    )
  ) {
    const fcAmount = plain.user.firstChargeAmount;
    // Remove firstChargeAmount
    dataWithAppliedDiscounts.get('user').set('firstChargeAmount', null);
    const dataFirstChargeAmount = {
      fullPrice: fullPrice,
      totalPrice: fcAmount,
      discountAmount,
      discountPercent,
      appliedDiscounts,
      data: dataWithAppliedDiscounts,
    };
    dataFirstChargeAmount.data.pricing = {
      fullPrice: fullPrice,
      totalPrice: fcAmount,
      discountAmount,
      discountPercent,
      appliedDiscounts,
    };
    return dataFirstChargeAmount;
  }
  return {
    fullPrice,
    totalPrice,
    discountAmount,
    discountPercent,
    appliedDiscounts,
    data: dataWithAppliedDiscounts,
  };
};

export const stripeCharge = (amount = 0, customerId, capture = true) => {
  return new Promise((resolve, reject) => {
    if (amount <= 0) {
      resolve({ id: 'free charge' });
      return;
    }

    stripe.charges.create({
      amount: Math.floor(amount), // prevent decimals
      currency: 'usd',
      customer: customerId,
      capture,
    })
      .then((charge) => {
        resolve(charge);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const stripeCapture = (captureId) => {
  return new Promise((resolve, reject) => {
    stripe.charges.capture(captureId)
      .then((charge) => {
        resolve(charge);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
