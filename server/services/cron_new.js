import moment from 'moment';
import { eachLimit, until } from 'async';
import cloneDeep from 'clone-deep';

import { sendEmail } from '../utils/sendgrid';
import configSendgrid from '../config/sendgrid';
import { updateIntegration } from '../utils';
import { triggerOutboundEvent } from '../utils/outbound';
import { ACTIVE, PENDING, PAUSED, CANCELED } from './status';
import { zeroDay, dayWhenRepeat } from '../entities/Cron/controller';
import { calculateDiscounts, stripeCharge, stripeCapture } from './cron_charge';
import { incrementBoxNumber } from './cron_boxnumber';
import { LogDB } from '../entities/Log/plugin';
import { registerAppliedDiscount } from '../entities/AppliedDiscounts/plugin';
import { breakageEmail } from '../entities/User/plugin';
import db from '../db/connection';
import { countStockConsumed } from '../entities/MealsStock/controller';
import { saveBoxDetail } from '../entities/BoxDetailHistory/controller';

const env = process.env.NODE_ENV;

export const getCronDate = (n) => {
  const now = moment.utc();
  const diff = now.diff(zeroDay, 'days');

  const diffRemainder = diff % dayWhenRepeat;
  const cyclesPassed = (diff - diffRemainder) / dayWhenRepeat;

  return zeroDay.clone().add((cyclesPassed + (n + 1)) * dayWhenRepeat, 'days');
};

export const checkSubscriptionStatus = (data, date) => {
  // Transform Sequelize Obj to plain
  const plain = data.get({ plain: true });

  if (plain.status === PENDING) {
    return {
      continue: false,
      data,
      log: 'Subscription has Pending status',
      status: PENDING,
    };
  }

  if (plain.status === 'none') {
    try {
      const res = sendEmail({
        to: configSendgrid.SENDGRID_TO_EMAIL,
        substitutions: {
          body: `
            E-mail: ${plain.user.email}
          `,
        },
        subject: 'Alert: User with status None',
        template_id: configSendgrid.templates.rrTemplate,
      });
    } catch (err) {
      console.log(err);
    }

    return {
      continue: false,
      data,
      log: 'Subscription has None status',
      status: 'none',
    };
  }

  if (plain.status === CANCELED) {
    return {
      continue: false,
      data,
      log: 'Subscription has Cancelled status',
      status: CANCELED,
    };
  }

  if (plain.status === PAUSED) {
    // If no date for pausedUntil return false
    if (!plain.pausedUntil) {
      return {
        continue: false,
        data,
        log: 'Subscription has Paused Status status without until date',
        status: PAUSED,
      };
    }

    // Check paused date
    const stillPaused = !moment.utc(plain.pausedUntil).isSameOrBefore(date, 'day');
    if (stillPaused) {
      return {
        continue: false,
        data,
        status: PAUSED,
        log: `Subscription has paused until ${plain.pausedUntil}`,
      };
    }

    if (!stillPaused) {
      data.set('pausedUntil', null);
      data.set('status', 'active');

      // Subscription is now active
      return {
        continue: true,
        data,
        status: ACTIVE,
      };
    }
  }

  return {
    continue: true,
    data,
    status: ACTIVE,
  };
};

export const checkSubscriptionStartFrom = (data, date) => {
  const plain = data.get({ plain: true });

  let startFrom;
  if (plain.newCycleStart != null) { // If cycle/product was updated, use new date.
    startFrom = plain.newCycleStart;
  } else {
    // Standarize startFrom day, many old orders have Tue or Wed as startFrom
    startFrom = plain.startFrom;
  }

  const shouldContinue = moment.utc(startFrom).isSameOrBefore(date, 'day');
  const diff = moment.utc(startFrom).diff(date, 'day');

  if (!shouldContinue) {
    return {
      continue: shouldContinue,
      data,
      diff,
      log: `Subscription Start at ${startFrom}`,
    };
  }

  return {
    continue: shouldContinue,
    data,
    diff,
  };
};

export const checkSubscriptionSkipDate = (data, date) => {
  // Transform Sequelize Obj to plain
  const plain = data.get({ plain: true });

  const skipDates = plain.skips;
  const skippingToday = skipDates.filter((skip) => {
    return moment.utc(skip.date).isSame(date, 'day');
  });

  if (skippingToday.length !== 0) {
    return {
      continue: false,
      data,
      skipDates,
      log: 'Subscription has Skip Dates',
    };
  }

  return {
    continue: true,
    data,
    skipDates,
  };
};

export const checkSubscriptionLastOrder = (data, date, simulate = false) => {
  const plain = data.get({ plain: true });

  if (simulate) {
    return {
      continue: true,
      data,
      orders: [],
    };
  }

  const orders = plain.orders || [];
  if (orders.length === 0) {
    return {
      continue: true,
      data,
      orders: orders,
    };
  }

  // Use current date instead of CronDate to calculate diff to last order
  const now = moment.utc();
  const orderDiff = now.diff(orders[0].createdAt, 'days');

  if (orderDiff > 1) {
    return {
      continue: true,
      data,
      lastOrder: orderDiff,
      orders: orders,
    };
  }

  return {
    continue: false,
    data,
    log: 'Subscription has last orders',
    lastOrder: orderDiff,
    orders: orders,
  };
};

export const standarizeStartFrom = (startFromDate) => {
  const startFrom = moment.utc(startFromDate).startOf('day');

  // Standarize startFrom day, many old orders have Tue or Wed as startFrom
  const isoWeekDay = startFrom.isoWeekday();
  if (isoWeekDay === 2) {
    startFrom.subtract(1, 'day');
  }

  if (isoWeekDay === 3) {
    startFrom.subtract(2, 'day');
  }

  if (isoWeekDay === 7) {
    startFrom.add(1, 'day');
  }

  return startFrom;
};

export const checkSubscriptionCycle = (data, date) => {
  const plain = data.get({ plain: true });

  const cycleDays = plain.frequency * 7;

  let startFrom;
  if (plain.newCycleStart != null) { // If cycle/product was updated, use new date.
    startFrom = plain.newCycleStart;
  } else {
    // Standarize startFrom day, many old orders have Tue or Wed as startFrom
    startFrom = standarizeStartFrom(data.startFrom);
  }

  const diffStartFrom = Math.round(moment.utc(date).startOf('day').diff(startFrom, 'days', true));
  const diffReminderToCycle = diffStartFrom % cycleDays;
  if (diffReminderToCycle !== 0) {
    return {
      continue: false,
      data,
      diffStartFrom,
      diffReminderToCycle,
      log: 'Subscription is out of cycle',
    };
  }

  return {
    continue: true,
    data,
    diffStartFrom,
    diffReminderToCycle,
  };
};

export const getSubscriptionCycleNumber = (data, date) => {
  const plain = data.get({ plain: true });

  const cycleDays = plain.frequency * 7;
  const startFrom = moment(data.startFrom).startOf('day').utc();
  const diffStartFrom = Math.round(moment(date).startOf('day').diff(startFrom, 'days', true));
  // When running cron, date is not passed, and diff is calculated with actual date.
  const diffReminderToCycle = diffStartFrom / cycleDays;
  return Math.floor(diffReminderToCycle);
};

export const shouldShipOrder = async (data, date, simulate = false) => {
  const subscriptionStatus = await checkSubscriptionStatus(data, date);
  if (!subscriptionStatus.continue) {
    return subscriptionStatus;
  }

  const subscriptionStartFrom = await checkSubscriptionStartFrom(subscriptionStatus.data, date);
  if (!subscriptionStartFrom.continue) {
    return subscriptionStartFrom;
  }

  const subscriptionSkipDate = await checkSubscriptionSkipDate(subscriptionStartFrom.data, date);
  if (!subscriptionSkipDate.continue) {
    return subscriptionSkipDate;
  }

  const subscriptionCycle = await checkSubscriptionCycle(subscriptionSkipDate.data, date);
  if (!subscriptionCycle.continue) {
    return subscriptionCycle;
  }

  // For dev:cron comment this line to create more orders
  const subscriptionLastOrder = await checkSubscriptionLastOrder(subscriptionSkipDate.data, date, simulate);

  if (!subscriptionLastOrder.continue) {
    return subscriptionLastOrder;
  }

  return {
    continue: true,
    data: subscriptionCycle.data,
  };
};

export const calculateNextShipment = async (data) => {
  // Loop on next 6 crons to find a shipping date
  let i = 0;

  let nextShipment = null;
  let result;
  const limit = 6;

  return new Promise(async (resolve) => {
    until(() => {
      return (nextShipment || i >= limit);
    }, async (cb) => {
      const dateCron = getCronDate(i);
      const result = await shouldShipOrder(data, dateCron, true);
      if (result.continue) {
        nextShipment = dateCron;
      }
      i++;
      return cb();
    }, (err, n) => {
      resolve(nextShipment);
    });
  });
};

/**
 * Provided a list of meals, filters those containing the provided ingredient.
 * @param {Array} meals Array of meals objects
 * @param {Object} ingredient
 * @returns {Array} Meals without the ingredient
 */
export const getMealsWithoutIngredient = (meals, ingredient) => {
  if (!meals || !ingredient) return new Error('2 parameters expected! Meals or ingredient missing.');

  const filtered = meals.filter((meal) => {
    return !meal.ingredients
      .map(ingr => ingr.id)
      .includes(ingredient.id);
  });

  return filtered;
};

/**
 * Provided a list of meals, filters those containing the provided ingredients.
 * @param {Array} meals Array of meals objects
 * @param {Array} ingredients Array of ingredients objects
 * @returns {Array} Meals without the provided ingredients
 */
export const getMealsWithoutTheseIngredients = (meals, ingredients = []) => {
  if (!ingredients.length) return meals;
  let toFilter = [...meals];
  for (const ingr of ingredients) {
    const filtered = getMealsWithoutIngredient(toFilter, ingr);
    toFilter = filtered;
  }
  return toFilter;
};

/**
 * Checks what meals had been removed in order to log the changes in the DB. Compares mealsPreFilter with mealsPostFilter.
 * @param {Array} mealsPreFilter - Array of meals before filters.
 * @param {Array} mealsPostFilter - Array of meals after filters.
 * @param {String} reason - String to display the reason of the change.
 */
const checkAndLogMealChanges = (mealsPreFilter, mealsPostFilter, userId, reason) => {
  const mealsChanged = mealsPreFilter.map(meal => meal.sku).filter(meal => !mealsPostFilter.map(meal => meal.sku).includes(meal));
  mealsChanged.forEach((meal) => {
    return LogDB({
      user: userId,
      admin: null,
      type: 'info',
      message: `Meal ${meal} will not be taken into account due to ${reason}.`,
    });
  });
};

/**
 * Returns an array containing meals (ordered by priority) with stock in user's corresponding warehouse
 * @param {Object} data - Subscription's data instance with meals and stock.
 * @returns {Array} Ordered array of meals
 */
export const getUserWarehouseMealsWithStock = (data) => {
  if (!data.meals || !data.user.TagDay) throw new Error('Meals or user tag-day is not present!');
  const userWarehouse = data.user.TagDay.TagDayName.warehouseId;
  const filteredSkus = Object.keys(data.stock[userWarehouse]).filter(sku => data.stock[userWarehouse][sku] >= 12);
  return data.meals
    .filter(meal => filteredSkus.includes(meal.sku))
    .sort((a, b) => a.priority - b.priority);
};

/**
 * Function to complete Box Detail with more units when meals availables are < 6.
 * We need to add more units in order to reach box quantity (12 or 24).
 * To do this, we double up meals quantity taking into account priority until reach size.
 * @param {Array} boxDetail - Box detail built with meals available.
 * @param {Number} toAdd - Amount of units left to reach quantity of box.
 * @param {Number} unitsPerMeal - Amount of units we are going to add for each meal in each 'pass'. Usually 2 or 4 depending on box size.
 */
export const addMeals = (boxDetail, toAdd, unitsPerMeal) => {
  if (!boxDetail || boxDetail.length === 0 || !unitsPerMeal || isNaN(toAdd)) {
    return ({
      error: true,
      message: 'A parameter is missing',
    });
  }

  if (toAdd <= 0) return boxDetail;

  const boxDetailCopy = [...boxDetail];
  let mealsLeft = toAdd;
  for (const meal of boxDetailCopy) {
    const qtyToBeAdded = mealsLeft >= unitsPerMeal ? unitsPerMeal : mealsLeft;
    meal.quantity += qtyToBeAdded;
    mealsLeft -= qtyToBeAdded;
    if (mealsLeft <= 0) return boxDetailCopy;
  }
  return addMeals(boxDetailCopy, mealsLeft, unitsPerMeal);
};

const completeBoxDetail = (boxDetail, breakfastMeals) => {
  const surplusMeals = boxDetail.slice(6).map(meal => meal.quantity)
  boxDetail.forEach(meal => {
    const isBreakfast = breakfastMeals.findIndex(el => el.sku === meal.mealSku)

    if (isBreakfast === -1 && surplusMeals.length > 0) {
      const quantityToAdd = surplusMeals.shift()
      meal.quantity += quantityToAdd
    }
    return meal;
  });
  return boxDetail.slice(0, 6);
};

const buildMealsBox = (meals, mealsToAdd, maxMealQuantity) => {
  const boxDetail = [];
  for (const meal of meals) {
    if (mealsToAdd <= 0) break;
    boxDetail.push({
      mealSku: meal.sku,
      quantity: mealsToAdd >= maxMealQuantity ? maxMealQuantity : mealsToAdd,
    });
    mealsToAdd -= maxMealQuantity;
  }
  return addMeals(boxDetail, mealsToAdd, maxMealQuantity)
};

const buildStandardMealsBox = ({ data, mealsOrdered, mealsPerBox, unitsPerMeal, boxConfiguration }) => {
  const dataCopy = cloneDeep(data);

  // If there is enough variety to build the box, build it with equal quantity of units for each one.
  if (mealsOrdered.length >= 6) {
    const boxDetail = mealsOrdered.slice(0, 6).map(meal => ({
      mealSku: meal.sku,
      quantity: unitsPerMeal,
    }));

    dataCopy.boxDetail = boxDetail;
    dataCopy.configuration = boxConfiguration;

    return dataCopy;
  }

  // If we don't have meals to build the box
  if (!mealsOrdered.length) {
    LogDB({
      user: dataCopy.userId,
      admin: null,
      type: 'info',
      message: 'BOX BUILDING ISSUE: Due to lack of stock and excluded ingredients, there are no meals to build the box. User will receive default box.',
    });

    const boxDetail = dataCopy.map(meal => meal.default).map(meal => ({
      mealSku: meal.sku,
      quantity: unitsPerMeal,
    }));

    dataCopy.boxDetail = boxDetail;
    dataCopy.configuration = 'standard';

    return dataCopy;
  }

  // If variety is not enough, we have to add more units of those available.
  const boxDetail = mealsOrdered.map(meal => ({
    mealSku: meal.sku,
    quantity: unitsPerMeal,
  }));
  const mealsToAdd = (mealsPerBox - boxDetail.length) * (unitsPerMeal);

  const completedBoxDetail = addMeals(boxDetail, mealsToAdd, unitsPerMeal);

  dataCopy.boxDetail = completedBoxDetail;
  dataCopy.configuration = boxConfiguration;

  return dataCopy;
};

/**
 * Function to build box detail for an order. Will take into account stock, user's excluded ingredients and product type.
 * @param {*} data - Subscription's data instance with meals and stock.
 */
export const buildBox = (data) => {
  const dataCopy = cloneDeep(data);
  const mealsPerBox = 6;
  const boxQty = Number.parseInt(dataCopy.product.boxType.split(' '), 10);
  const unitsPerMeal = boxQty / mealsPerBox;
  const excludedIngrs = dataCopy.excluded_ingredients.map(item => item.ingredient);
  const mealsWithStockByPriority = getUserWarehouseMealsWithStock(dataCopy);
  const mealsWithoutExcludedIngr = getMealsWithoutTheseIngredients(mealsWithStockByPriority, excludedIngrs);

  // Prepare order of meals for box building. Meals ordered by priority with default meals first.
  const defaultMeals = mealsWithoutExcludedIngr.filter(meal => meal.default);
  const nonDefaultMeals = mealsWithoutExcludedIngr.filter(meal => !meal.default && meal.kind === 'default');
  const breakfastMeals = mealsWithoutExcludedIngr.filter(meal => meal.kind === 'breakfast');

  const mealsOrdered = [...defaultMeals, ...nonDefaultMeals];

  // Define box configuration to know if a box had a default meal changed (custom).
  const boxConfiguration = defaultMeals.length === 6 ? 'standard' : 'custom';

  // Log meal changes is box is custom
  if (defaultMeals.length !== 6 && env !== 'test') {
    checkAndLogMealChanges(dataCopy.meals, mealsWithStockByPriority, data.userId, 'insufficient stock');
    checkAndLogMealChanges(mealsWithStockByPriority, mealsWithoutExcludedIngr, data.userId, 'excluded ingredient');
  }

  // Here we build the box with standard meals (no breakfast)
  const buildBoxWithStandardMeals = buildStandardMealsBox({ data: dataCopy, mealsOrdered, mealsPerBox, unitsPerMeal, boxConfiguration });

  // If user has no breaskfast meals, we finish box building
  if (!dataCopy.breakFastCount || dataCopy.breakFastCount === '0') {
    return buildBoxWithStandardMeals;
  } else {
    const breakfastsBoxDetail = buildMealsBox(breakfastMeals, dataCopy.breakFastCount, unitsPerMeal);
    const updatedBox = buildBoxWithStandardMeals;
    updatedBox.configuration = 'custom';

    const standardMealsToAdd = boxQty - breakfastsBoxDetail.reduce((prev, curr) => prev + curr.quantity, 0);

    if (standardMealsToAdd === 0) {
      updatedBox.boxDetail = breakfastsBoxDetail;
      return updatedBox;
    };

    const buildStandardPortion = buildMealsBox(mealsOrdered, standardMealsToAdd, unitsPerMeal);

    const boxDetailWithBreakfasts = [...breakfastsBoxDetail, ...buildStandardPortion];
    updatedBox.boxDetail = boxDetailWithBreakfasts;

    const completedBoxDetail = completeBoxDetail(boxDetailWithBreakfasts, breakfastMeals);
    updatedBox.boxDetail = completedBoxDetail;
    return updatedBox;
  }
};

export const processOrders = (data) => {
  const plain = data.get({ plain: true });
  let newOrders = [];
  const { user, product } = plain;
  const { pricing } = data;

  if (data.newOrders !== undefined) newOrders = data.newOrders;
  const productPrice = pricing.totalPrice / plain.amount;
  let indexOrder = 0;

  while (indexOrder < plain.amount) {
    newOrders.push(db.Order.build({
      userId: user.id,
      name: user.name,
      lastName: user.lastName,
      email: user.email,
      zipCode: user.zipCode,
      address: user.address,
      addressExtra: user.addressExtra,
      city: user.city,
      state: user.state,
      boxType: product.boxType,
      productPrice: productPrice,
      subscriptionId: data.id,
    }));
    indexOrder++;
  }

  data.newOrders = newOrders;

  return {
    data,
  };
};

export const simulateSubscription = async (data, date) => {
  const shouldGenerateOrder = await shouldShipOrder(data, date, true);
  if (shouldGenerateOrder.continue) {
    const calculatePrice = calculateDiscounts(shouldGenerateOrder.data, date);
    const orders = processOrders(data);
    const boxNumber = incrementBoxNumber(orders.data);
    return boxNumber;
  }

  return;
};

export const simulateSubscriptionStockConsume = async (data, date) => {
  try {
    const shouldGenerateOrder = await shouldShipOrder(data, date, true);
    if (shouldGenerateOrder.continue) {
      const dataWithBoxDetail = buildBox(shouldGenerateOrder.data, true);
      const stockConsumed = dataWithBoxDetail.boxDetail.map((meal) => {
        meal.quantity *= data.amount;
        meal.warehouseId = data.user.TagDay.TagDayName.warehouseId;
        return meal;
      });
      return stockConsumed;
    }
    return false;
  } catch (error) {
    console.log(error);
  }
};

export const processAuthSubscription = async (data, date) => {
  let dataInstance = data;
  const userPlain = dataInstance.get({ plain: true }).user;
  if (userPlain.firstCharge !== null) {
    console.log(`User ${userPlain.name}, id: ${userPlain.id} has a first charge`);
    return false;
  }
  const shouldGenerateOrder = await shouldShipOrder(data, date, false);
  if (!shouldGenerateOrder.continue) {
    return;
  }
  dataInstance = shouldGenerateOrder.data;
  const calculatePrice = calculateDiscounts(dataInstance);
  dataInstance = calculatePrice.data;
  let captureToken;
  let errCode;
  try {
    const charge = await stripeCharge(calculatePrice.data.pricing.totalPrice, userPlain.customerId, false);
    captureToken = charge.id;
  } catch (err) {
    errCode = err.raw.code;
    if (errCode === 'card_declined') {
      const userId = userPlain.id;
      const hash = userPlain.hash;
      breakageEmail(userId, hash);
    } else {
      console.log('Unexpected error while charging on Stripe');
    }
  }

  if (errCode !== 'card_declined' && errCode === undefined) {
    try {
      const saveUser = await dataInstance.update({
        captureToken: captureToken,
      })
      .then(() => { console.log(`Capture Token Created for user ${dataInstance.user.id}`); })
      .catch((err) => { console.log(`Some error ocurred while adding capture Token to DB ${err}`); });
    } catch (err) {
      console.log(`Cant save User #${dataInstance.user.id} on Database: ${err}`);
    }
  }
};

export const processSubscription = async (data, date, cronId) => {
  let dataInstance = data;
  const plain = data.get({ plain: true });
  let shouldSaveOrders = true;

  dataInstance.stockConsumed = {}; // Initialize to keep track of stock consumed

  const shouldGenerateOrder = await shouldShipOrder(data, date, false);
  const dataFormat = date.format('YYYY-MM-DD');
  dataInstance = shouldGenerateOrder.data;
  let logInfo = {};

  if (!shouldGenerateOrder.continue) {
    logInfo = {
      user: dataInstance.userId,
      admin: null,
      type: 'info',
      message: `CRON ${dataFormat}: ${shouldGenerateOrder.log}`,
    };
    LogDB(logInfo);
    return;
  }

  // Discount
  const calculatePrice = calculateDiscounts(dataInstance);
  dataInstance = calculatePrice.data;

  for (const discount of dataInstance.pricing.appliedDiscounts) {
    const { type, amount, couponId, creditLeft } = discount;

    if (type === 'subscriptionNextDiscount') {
      logInfo.message = `Next discount was applied for $${amount}`;
    }

    if (type === 'subscriptionCoupon') {
      logInfo.message = `A coupon (#${couponId}) was applied for $${amount / 100}`;
    }

    if (type === 'userCredit') {
      logInfo.message = `User had $${creditLeft / 100} of credit`;
    }


    logInfo.type = 'info';
    logInfo.user = dataInstance.userId;

    LogDB(logInfo);
  }

  const orders = processOrders(dataInstance);
  dataInstance = orders.data;

  const boxNumber = incrementBoxNumber(dataInstance);
  dataInstance = boxNumber.data;

  try {
    // TODO: Check if data is in dataInstance
    // TODO: Dont charge a new customer. This has a firstCharge in user data instance
    const userPlain = dataInstance.get({ plain: true }).user;

    let chargeToken;
    let charge;
    if (!userPlain.firstCharge) {
      // Users that did not enter on Capture Cron because of time between crons.
      const captureToken = dataInstance.captureToken;
      if (dataInstance.captureToken === null) {
        charge = await stripeCharge(calculatePrice.data.pricing.totalPrice, userPlain.customerId);
        chargeToken = charge.id;
      } else if (dataInstance.captureToken === 'free charge') {
        chargeToken = dataInstance.captureToken;
      } else {
        charge = await stripeCapture(captureToken);
        chargeToken = charge.id;
      }

      if (dataInstance.pricing.appliedDiscounts.length) {
        try {
          registerAppliedDiscount(dataInstance.pricing.appliedDiscounts, userPlain.id, chargeToken, cronId);
        } catch (err) {
          console.log(`Cant register applied discount (${userPlain.email})`);
          LogDB({
            user: userPlain.id,
            type: 'error',
            message: 'Cant register applied discount',
          });
        }
      }
    }

    if (userPlain.firstCharge) {
      chargeToken = userPlain.firstCharge;
      dataInstance.get('user').set('firstCharge', null);
    }
    if (userPlain.firstChargeAmount !== null) {
      dataInstance.get('user').set('firstChargeAmount', null);
    }

    // add stripe token to orders
    dataInstance.newOrders = dataInstance.newOrders.map((order) => {
      order.set('cronId', cronId);
      if (order.boxType === 'Meal Maker') {
        return order;
      }

      order.set('chargeToken', chargeToken);
      return order;
    });
  } catch (error) {
    console.log(`Stripe Error for Subscription #${plain.user.email}: ${error}`);

    dataInstance.set('status', PENDING);
    dataInstance.get('user').set('status', PENDING);

    // Send email to notify the error
    try {
      const res = sendEmail({
        to: configSendgrid.SENDGRID_TO_EMAIL,
        substitutions: {
          body: `
            E-mail: ${dataInstance.get('user').get('email')}
          `,
        },
        subject: 'Alert: User with status Pending',
        template_id: configSendgrid.templates.rrTemplate,
      });
    } catch (err) {
      console.log(err);
    }

    // in case stripe fail destroy orders
    shouldSaveOrders = false;

    logInfo.user = plain.user.id;
    logInfo.admin = null;
    logInfo.type = 'error';
    logInfo.message = `A stripe error ocurred when try to charge the user ${plain.user.email} -> ${error}. Updating user and subscription status.`;
    LogDB(logInfo);
  }

  // Save

  try {
    const saveSubscription = await dataInstance.save();
  } catch (err) {
    console.log(`Cant save Subscription #${dataInstance.id} on Database: ${err}`);
  }

  try {
    const saveUser = await dataInstance.get('user').save();
  } catch (err) {
    console.log(`Cant save User #${dataInstance.user.id} on Database: ${err}`);
  }

  try {
    const saveUser = await dataInstance.update({
      captureToken: null,
    })
    .then(() => { console.log('Capture token updated to null'); })
    .catch((err) => { console.log(`Error updating capture Token to null on DB ${err}`); });
  } catch (err) {
    console.log(`Cant Update Charge Token to null on User #${dataInstance.user.id} on Database: ${err}`);
  }

  // Build box
  let buildBoxDetail;
  try {
    buildBoxDetail = buildBox(dataInstance);
  } catch (error) {
    console.log(error);
  }

  // Consume stock and make sure its updated when processing next order
  for (const order of dataInstance.newOrders) {
    dataInstance.stockConsumed = countStockConsumed(dataInstance, buildBoxDetail.boxDetail);
  }

  eachLimit(dataInstance.newOrders, 1, async (order, cb) => {
    // TODO: refactor this block to away
    if (!shouldSaveOrders) {
      return cb();
    }

    try {
      // Add configuration to order instance
      order.set('configuration', buildBoxDetail.configuration);

      const saveOrder = await order.save();
      logInfo.user = plain.user.id;
      logInfo.type = 'info';
      logInfo.message = `New order -> ${saveOrder.get('id')} to ${saveOrder.name} ${saveOrder.lastName} (#${plain.user.id}).`;
      if (order.boxType !== 'Meal Maker') {
        logInfo.message += `Charge ID -> ${saveOrder.get('chargeToken')}`;
      }
      LogDB(logInfo);
      console.log(logInfo.message);

      // Save Box Detail
      saveBoxDetail(buildBoxDetail, saveOrder.get('id'));

      if (env !== 'test') {
        // Outbound Event 'Signup'
        const outboundId = await db.User.find({
          where: {
            id: plain.user.id,
          },
          attributes: ['outbound'],
        });

        triggerOutboundEvent({
          user: outboundId.get('outbound'),
          event: 'Order created',
        })
          .then(() => console.log(`Outbound Trigger Event Order created Ok! (${plain.user.id})`))
          .catch(() => console.log(`Outbound Trigger Event Order created Failed! (${plain.user.id})`));
      }
    } catch (err) {
      console.log(`Cant save Order on Database: ${err}`);
    }
    return cb();
  }, async (err) => {
    if (err) {
      console.log(err);
      return;
    }

    if (env !== 'test') {
      try {
        const integration = await updateIntegration(dataInstance.user.id);
        logInfo.user = plain.user.id;
        logInfo.type = 'info';
        logInfo.message = 'Successfull integration';
        LogDB(logInfo);
      } catch (err) {
        console.log(`User ${plain.user.id} had problems with Integration: ${err}`);
      }
    }
  });

  return dataInstance;
};
