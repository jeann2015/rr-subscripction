import moment from 'moment';
import { eachLimit } from 'async';
import { processSubscription, processAuthSubscription } from './cron_new';
import { LogDB } from '../entities/Log/plugin';
import db from '../db/connection';
import { updateWeeklyList } from '../utils/hubspot';
import { createCouponByBoxNumber } from '../services/hubspot_boxNumber';
import { getMeals } from '../entities/Meals/plugin';
import { getStockGroupedByWarehouse, saveStockConsumed } from '../entities/MealsStock/controller';
import config from '../config/config';

const env = process.env.NODE_ENV;

export const runAuthOrders = (subscriptions = []) => {
  const logInfo = {
    admin: null,
    type: 'info',
    message: 'New Auth Cron started',
  };

  return new Promise(async (resolve, reject) => {
    console.log('New Auth Cron Started');
    LogDB(logInfo);
    const orders = [];
    const now = moment(); // this is for production
    eachLimit(subscriptions, 1, async (subscription, cb) => {
      const subscriptionOrders = await processAuthSubscription(subscription, now);
      if (subscriptionOrders && subscriptionOrders.newOrders.length > 0) {
        for (const order of subscriptionOrders.newOrders) {
          orders.push(order.get({ plain: true }));
        }
      }
      cb();
    }, (err) => {
      if (err) {
        logInfo.type = 'error';
        logInfo.message = `Auth Cron fails: ${error}`;
        LogDB(logInfo);
        reject(err);
      }
      logInfo.type = 'info';
      logInfo.message = 'Auth Cron successfully finished';
      LogDB(logInfo);

      resolve(orders);
    });
  });
};


/**
 * @Todo Processing Meals to Default and BreakFast
 * @param meals
 * @param kind
 * @returns {Promise}
 * @constructor
 */
export const ProcessingMealsByKind = (meals,kind) => {
  return new Promise(async (resolve, reject) => {
    try {
    const plainMeals = meals.map((meal) => {
      if(kind === meal.kind){
        const plain = meal.get({plain: true});
        const ingredients = plain.meals_ingredients.map(ingr => ingr.ingredient);
        return ({
          sku: plain.sku,
          name: plain.name,
          default: plain.default,
          priority: plain.priority,
          ingredients,
        });
      }
    });
      resolve(plainMeals);
    } catch (error) {
      reject(error);
    }
  });
};

export const runOrders = (subscriptions = []) => {
  return new Promise(async (resolve, reject) => {
    const orders = [];
    const cron = await db.Cron.create();
    const meals = await getMeals();
    const stockData = await getStockGroupedByWarehouse();

    const now = env === 'test' ? moment().day(8).utc().hour(18) : moment(); // this is for production
    const logInfo = {
      admin: null,
      type: 'info',
      message: `New CRON start with ID ${cron.id}`,
    };
    console.log(logInfo.message);
    LogDB(logInfo);

    eachLimit(subscriptions, 1, async (subscription, cb) => {
      subscription.meals = meals; // Add default meals data to use it cron flow
      subscription.stock = stockData; // Add current stock
      // If user's zipcode is not in DB, we assign default warehouse
      if (!subscription.user.TagDay) {
        subscription.user.TagDay = {};
        subscription.user.TagDay.TagDayName = { warehouseId: config.fallbackWarehouse };
      }
      // Process subscription
      const subscriptionOrders = await processSubscription(subscription, now, cron.id);

      if (subscriptionOrders && subscriptionOrders.newOrders.length > 0) {
        for (const order of subscriptionOrders.newOrders) {
          orders.push(order.get({ plain: true }));
        }
        // Update available stock
        for (const sku in subscriptionOrders.stockConsumed) {
          stockData[subscription.user.TagDay.TagDayName.warehouseId][sku] -= subscriptionOrders.stockConsumed[sku];
        }
      }
      cb();
    }, async (err) => {
      if (err) {
        console.log(err);
        logInfo.type = 'error';
        logInfo.message = `CRON ${cron.id} fails: ${error}`;
        LogDB(logInfo);
        reject(err);
      }
      // Save final stock in DB
      await saveStockConsumed(stockData);

      resolve(orders);
    });
  });
};

export const getSubscriptionsAndRunOrders = async (isAuthCron) => {
  try {
    const subscriptions = await db.Subscription.findAll({
      where: {
        status: 'active',
      },
      include: [
        {
          model: db.User,
          include: [{
            model: db.TagDay,
            as: 'TagDay',
            include: [{
              model: db.TagDayName,
              as: 'TagDayName',
              include: [{ model: db.Warehouses, as: 'Warehouse' }],
            }],
          }],
        },
        db.Product,
        db.Coupon,
        db.Skip,
        {
          model: db.Order,
          limit: 1,
          order: [
            ['createdAt', 'DESC'],
          ],
          required: false,
        },
        {
          model: db.Order,
          as: 'orderCount',
          attributes: ['cronId'],
          where: {
            boxType: {
              $not: 'Meal Maker',
            },
          },
          required: false,
        },
        {
          model: db.ExcludedIngredients,
          include: [db.Ingredients],
        },
      ],
    });
    let orders;
    if (!isAuthCron) {
      orders = await runOrders(subscriptions);
      if (env !== 'test') {
        await Promise.all([updateWeeklyList(), createCouponByBoxNumber()]);
        console.log('Hubspot List and Coupon By Box Number Completed');
      }
    } else {
      orders = await runAuthOrders(subscriptions);
    }
    return orders;
  } catch (err) {
    const logInfo = {
      admin: null,
      type: 'error',
      message: `An error occurred while getting subscriptions on DB ${err}`,
    };
    LogDB(logInfo);
    console.log(`An error occurred while getting subscriptions on DB ${err}`);
  }
};
