import { eachLimit } from 'async';
import path from 'path';
import { exportCsv } from '../entities/Order/plugin';
import { simulateSubscription, simulateSubscriptionStockConsume, getCronDate } from './cron_new';
import { add3PLtoOrders } from '../utils';
import db from '../db/connection';
import { getMeals } from '../entities/Meals/plugin';

import  { ProcessingMealsByKind } from '../services/cron_runner';

export const simulateCron = async (subscriptions = [], date) => {
  return new Promise(async (resolve, reject) => {
    const orders = [];
    eachLimit(subscriptions, 1, async (subscription, cb) => {
      const subscriptionOrders = await simulateSubscription(subscription, date);
      if (subscriptionOrders) {
        const currentSubscriptionOrders = subscriptionOrders.data.newOrders.map(o => o.get({ plain: true }));
        orders.push(...currentSubscriptionOrders);
      }
      cb();
    }, (err) => {
      if (err) {
        console.log(err);
        return reject(err);
      }
      resolve(orders);
    });
  });
};

export const simulateCronAndGenerateCSV = async (date, req, res, next) => {
  try {
    const subscriptions = await db.Subscription.findAll({
      where: {
        status: 'active',
      },
      include: [
        db.User,
        db.Product,
        db.Coupon,
        db.Skip,
        {
          model: db.Order,
          limit: 1,
          order: [
            ['createdAt', 'DESC'],
          ],
          required: false,
        },
        {
          model: db.Order,
          as: 'orderCount',
          attributes: ['cronId'],
          where: {
            boxType: {
              $not: 'Meal Maker',
            },
          },
          required: false,
        },
      ],
    });

    const lastOrder = await db.Order.findOne({ order: [['id', 'DESC']] });
    const lastOrderId = parseInt(lastOrder.get('id'));

    const orders = await simulateCron(subscriptions, date);
    const with3Pl = await add3PLtoOrders(orders);
    const withIndex = with3Pl.map((order, index) => {
      order.id = lastOrderId + index + 1;
      return order;
    });

    const fields = ['id', 'userId', 'boxType', 'boxNumber', 'productPrice', '_3pl', 'ship_time', 'email', 'name', 'lastName', 'address', 'addressExtra', 'city', 'state', 'zipCode'];
    const fieldNames = ['ID', 'User Id', 'Plan', 'Box #', 'Amount Charged', '3PL', 'Ship Time', 'Email', 'First Name', 'Last Name', 'Street 1', 'Street 2', 'City', 'State', 'Zipcode'];
    const opts = {
      data: withIndex,
      fields,
      fieldNames,
    };

    const fileName = `simulatedOrders${date.format('MM-DD-YYYY')}.csv`;
    exportCsv(opts, fileName, () => {
      const file = path.join(fileName);
      res.download(file);
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
};

const simulateCronStockConsumption = (subscriptions = [], date, currentStock) => {
  return new Promise(async (resolve, reject) => {
    const stock = {};
    const meals = await getMeals();
    for (const subscription of subscriptions) {
      try {
        subscription.meals = meals;
        // eslint-disable-next-line
        const consumed = await simulateSubscriptionStockConsume(subscription, date);
        if (consumed) {
          for (const c of consumed) {
            if (!stock[c.warehouseId]) stock[c.warehouseId] = {};
            if (!stock[c.warehouseId][c.mealSku]) stock[c.warehouseId][c.mealSku] = 0;
            stock[c.warehouseId][c.mealSku] += c.quantity;
          }
        }
      } catch (error) {
        reject('Error while running stock simulation: ', error);
      }
    }

    const stockAfterCron = currentStock.map((meal) => {
      if (stock[meal.warehouse]) {
        const updatedStock = meal.stock - (stock[meal.warehouse][meal.mealSku] || 0);
        meal.localStock = updatedStock;
        return meal;
      }
      return meal;
    });

    resolve(stockAfterCron);
  });
};

export const runStockSimulation = (date) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cronDate = date || getCronDate(0).format('YYYY-MM-DD');
      const subs = await db.Subscription.findAll({
        where: { status: 'active' },
        include: [db.Product, db.Coupon, db.Skip, {
          model: db.User,
          include: [{
            model: db.TagDay,
            as: 'TagDay',
            include: [{
              model: db.TagDayName,
              as: 'TagDayName',
              include: [{ model: db.Warehouses, as: 'Warehouse' }],
            }],
          }],
        },
          {
            model: db.Order,
            limit: 1,
            order: [['createdAt', 'DESC']],
            required: false,
          },
          {
            model: db.Order,
            as: 'orderCount',
            attributes: ['cronId'],
            where: { boxType: { $not: 'Meal Maker' } },
            required: false,
          },
        ],
      });

      const stock = await db.MealsStock.findAll({});
      const plainStock = stock.map(item => item.get({ plain: true }));
      const simulated = await simulateCronStockConsumption(subs, cronDate, plainStock);

      resolve(simulated);
    } catch (error) {
      reject('Error running stock simulation: ', error);
    }
  });
};
