import { eachLimit } from 'async';
import moment from 'moment';
import config from '../config/config';
import db from '../db/connection';
import { sendReferralCodeByBoxNumber } from '../entities/User/plugin';
import { LogDB } from '../entities/Log/plugin';
import { getCronDate } from './cron_new';
import { sendEmail } from '../utils/sendgrid';
import { updateOutboundCouponByBoxNumber } from '../services/outbound';
import { triggerOutboundEvent } from '../utils/outbound';

const Order = db.Order;
const Cron = db.Cron;
const Coupon = db.Coupon;
const User = db.User;

const formatCodeCouponName = (name, lastName, id) => {
  // Not Null
  if (name === null || lastName === null || id === null) {
    return false;
  }
  // To String
  name = name.toString();
  lastName = lastName.toString();
  id = id.toString();
  let code = `REALFREE${name.substring(0, 1)}${lastName.substring(0, 1)}${id}`;
  code = code.replace(/ /g, '').toUpperCase();
  return code;
};

const createCoupon = (name, lastName, id) => {
  const code = formatCodeCouponName(name, lastName, id);
  if (!code) {
    return { message: 'Format code error', error: true };
  }

  return new Promise((resolve, reject) => {
    const couponPayload = {
      code,
      amountOff: 6588,
      maxRedemptions: 3,
      timesRedeemed: 0,
      duration: 'once',
      productId: 21,
      valid: 1,
      timesUsable: -1,
    };

    Coupon.findOne({
      where: {
        code,
      },
    })
    .then((coupon) => {
      if (coupon) {
        return resolve({ message: 'Coupon exists', error: true });
      }
      Coupon.create(couponPayload)
        .then((res) => {
          console.log('Referral by Box Number | Coupon created successfully');
          return resolve({ message: 'Coupon created successfully', error: false, code });
        })
        .catch((err) => {
          resolve({ message: 'Error creating coupon on DB', error: true });
        });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `Database error in createCoupon(find one coupon) -> Name:${err.name} ~ Message: ${err.message}`;
      LogDB(logInfo);
      next(err);
    });
  });
};

const handleCouponsError = (response, userId) => {
  const logInfo = {
    admin: null,
    user: null,
    type: 'info',
    message: '',
  };
  if (response.error) {
    let message = '';
    switch (response.message) {
      case 'Format code error':
        message = 'Format code error';
        break;
      case 'Coupon exists':
        message = 'Coupon exists';
        break;
      case 'Error creating coupon on DB':
        message = 'Error creating coupon on DB';
        break;
      default:
        break;
    }
    logInfo.type = 'error';
    logInfo.message = `Referral by Box Number | ${message} for user ${userId}`;
    LogDB(logInfo);
    console.log(`Referral by Box Number | ${message} for user ${userId}`);
    return true;
  }
  return false;
};

export const createCouponByBoxNumber = () => {
  console.log('Referral by Box Number | Create Coupon By Box Number Started');
  const logInfo = {
    admin: null,
    user: null,
    type: 'info',
    message: '',
  };
  return new Promise(async (resolve, reject) => {
    // Get the last cron
    const lastCron = await Cron.findOne({
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    // Get All the orders generated in the last cron
    const lastCronId = lastCron.get('id');
    console.log('Last Cron', lastCronId);
    const orders = await db.Order.findAll({
      where: {
        cron_id: lastCron.get('id'),
      },
    });
    if (orders.length === 0) {
      console.log(`There are no orders whit CRON ID # ${lastCronId}`);
      logInfo.type = 'info';
      logInfo.message = `Referral by Box Number | There are no orders whit CRON ID # ${lastCronId}`;
      LogDB(logInfo);
      return resolve(`There are no orders whit CRON ID # ${lastCronId}`);
    }

    const lastDate = getCronDate(0);
    const sendDate = moment().utc()
                    .add(71, 'hour')
                    .unix();

    // const sendDate = null; // Use for Testing

    const formatDate = moment.utc(lastDate).format('MM-DD-YYYY');
    eachLimit(orders, 1, async (order, complete) => {
      const plain = order.get({ plain: true });
      if (plain.boxNumber === 3) {
        try {
          const createdCoupon = await createCoupon(plain.name, plain.lastName, plain.userId);
          if (handleCouponsError(createdCoupon, plain.userId)) {
            resolve('Some Error Ocurred While creating coupon');
          } else {
            await sendReferralCodeByBoxNumber(plain.name, plain.email, createdCoupon.code, plain.userId, formatDate, sendDate);
            const outboundId = await User.find({
              where: {
                id: plain.userId,
              },
              attributes: ['outbound'],
            });
            triggerOutboundEvent({
              user: outboundId.outbound,
              event: 'Recieve Coupon By Box Number',
            })
            .then(() => console.log(`Outbound Trigger Event Recieve Coupon By Box Number Ok! (${plain.userId})`))
            .catch(() => console.log(`Outbound Trigger Event Recieve Coupon By Box Number Failed! (${plain.userId})`));
            updateOutboundCouponByBoxNumber(plain.userId, createdCoupon.code);
            resolve('Everything was sent Ok');
          }
        } catch (err) {
          logInfo.type = 'info';
          logInfo.message = `Referral by Box Number | An error occurred while creating coupon for user ${plain.userId} or sending email`;
          LogDB(logInfo);
          reject('An error occurred while creating coupon or sending email', err);
        }
      }
      complete();
    });
  });
};
