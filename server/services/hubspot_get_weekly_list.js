
import { hubspotGetWeeklyList, getHubspotBatchContactsById } from '../utils/hubspot';

const startHubspotGetWeeklyList = () => {
  hubspotGetWeeklyList(async (res) => {
    if (res.contacts) {
      const ids = res.contacts.map(c => c.vid);
      const detailContacts = await getHubspotBatchContactsById(ids);

      console.log('---------------------------------------');
      console.log('- Users of the Weekly List on Hubspot -');
      console.log('---------------------------------------');
      for (const contact in detailContacts) {
        console.log(detailContacts[contact].properties.email);
      }
    }
  });
};

startHubspotGetWeeklyList();
