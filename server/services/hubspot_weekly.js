import CronJob from 'cron';
import moment from 'moment';
import config from '../config/config';

import { updateWeeklyList, sendBoxNumberCode } from '../utils/hubspot';

const cronJob = CronJob.CronJob;

const zeroDay = moment.utc(config.zeroDay, 'YYYYMMDD');
const dayWhenRepeat = config.dayWhenRepeat;

const instance = () => {
  return new cronJob('00 45 10 * * *', () => {
    const now = moment.utc();
    const diff = now.diff(zeroDay, 'days');
    const diffRemainder = diff % dayWhenRepeat;
    if (diffRemainder === 0) {
      updateWeeklyList();
    }
  }, null, true, 'America/Los_Angeles');
};

export default instance;
