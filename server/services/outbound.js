import { eachLimit } from 'async';
import db from '../db/connection';
import { triggerOutboundEvent, outboundIntegration } from '../utils/outbound';

const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;

export const triggerOutboundOrderShippedEvent = async (orders) => {
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  eachLimit(orders, 10, async (order, cb) => {
    if (order.status === 'shipped') {
      const orderId = order.id;
      const user = await Order.find({
        attributes: ['user_id'],
        where: {
          shipstationOrder: orderId,
        },
        include: [User],
      });
      if (user === null) {
        console.error(`User for Order #${orderId} doesn't exist`);
      } else {
        const outboundId = user.user.outbound;
        const userId = user.user.id;
        if (outboundId !== null && userId !== null) {
          try {
            await triggerOutboundEvent({
              user: outboundId,
              event: 'Order shipped',
            });
            console.log(`Outbound Trigger Event Order shipped Ok! (${userId})`);
          } catch (err) {
            console.log(`Outbound Trigger Event Order shipped Failed! (${userId}) Error: ${err}`);
          }
        }
        await sleep(1000);
      }
    }
    cb();
  }, (err) => {
    if (err) {
      console.log(err);
    }
    console.log('Outbound Shipped Order Complete!');
  });
};

export const triggerOutboundStatusBySubscriptionId = async (subscriptionId, status) => {
  if (subscriptionId === null || subscriptionId === undefined || status === null || status === undefined) {
    return false;
  }
  const user = await Subscription.find({
    attributes: ['user_id'],
    where: {
      id: subscriptionId,
    },
    include: [User],
  });

  const outboundId = user.user.outbound;
  const userId = user.user.id;

  triggerOutboundEvent({
    user: outboundId,
    event: status,
  })
  .then(() => {
    console.log(`Outbound Trigger Event ${status} Ok! (${userId})`);
  })
  .catch(() => {
    console.log(`Outbound Trigger Event ${status} Failed! (${userId})`);
  });
};

export const updateOutboundAttribute = async (userWhoReferrsId, referredId) => {
  const outboundId = await User.findOne({
    attributes: ['outbound'],
    where: {
      id: userWhoReferrsId,
    },
  });
  const fieldsOutbound = {
    id: userWhoReferrsId,
    outbound: outboundId.outbound,
    attributes: {
      has_referred: referredId,
    },
  };

  outboundIntegration(fieldsOutbound)
    .then((res) => {
      console.log('Integrate Outbound Has Referred: Ok!');
    })
    .catch((err) => {
      console.log('Integrate Outbound: Can not update Referred user', err);
    });
};

export const updateOutboundCouponByBoxNumber = async (userId, couponName) => {
  const outboundId = await User.findOne({
    attributes: ['outbound'],
    where: {
      id: userId,
    },
  });
  const fieldsOutbound = {
    id: userId,
    outbound: outboundId.outbound,
    attributes: {
      coupon_by_box_number: couponName,
    },
  };

  outboundIntegration(fieldsOutbound)
    .then((res) => {
      console.log('Integrate Outbound Coupon by box number: Ok!');
    })
    .catch((err) => {
      console.log('Integrate Outbound: Can not update Coupon by box number user', err);
    });
};
