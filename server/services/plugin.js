import Stripe from 'stripe';
import moment from 'moment';
import { eachLimit, each } from 'async';
import config from '../config/config';
import { updateIntegration, getSubscriptionStatus, formatUpdatedData } from '../utils';
import { upsertZendesk } from '../utils/zendesk';
import { PENDING, PAUSED, CANCELED } from './status';
import { calcCouponDiscount } from '../entities/Coupon/plugin';
import { getPausedUntil } from '../entities/Subscription/plugin';
import { getSubscriptionAsObject, getSubscriptionOrderData } from '../helper/subscriptionBuilder';
import { LogDB } from '../entities/Log/plugin';

const db = require('../db/connection');

const stripe = Stripe(config.stripeApiKey);

const Order = db.Order;
const User = db.User;
const Subscription = db.Subscription;
const Referral = db.Referral;
const sequelize = db.sequelize;

const processDiscount = async (subscriptionObject, discountPercentOff, discountAmountOff, totalPrice) => {
  const finalPrice = calcCouponDiscount(discountPercentOff, discountAmountOff, totalPrice);

  if (subscriptionObject.isSpecialDiscount) {
    let dataToUpdate = { couponUseCount: ++subscriptionObject.couponUseCount };

    // If we can't use the coupon again, we should stop using it.
    if (subscriptionObject.couponUseCount >= subscriptionObject.couponTimesUsable) {
      dataToUpdate = {
        couponUseCount: 0,
        couponId: null,
      };

      const logInfo = {
        user: subscriptionObject.userId,
        admin: null,
        type: '',
        message: '',
      };

      logInfo.type = 'info';
      logInfo.message = `The coupon ${subscriptionObject.code} was deleted from subscription ${subscriptionObject.subscriptionId}`;
      LogDB(logInfo);
    }
    await Subscription.update(dataToUpdate, {
      where: {
        id: subscriptionObject.subscriptionId,
      },
    });
  }

  return finalPrice;
};

const processNextDiscount = async (subscriptionObject, subscription, totalPrice) => {
  const logInfo = {
    user: subscriptionObject.userId,
    admin: null,
    type: '',
    message: '',
  };

  switch (subscriptionObject.typeNextDiscount) {
    case 'percent':
      const discount = totalPrice * (subscriptionObject.nextDiscount * 0.01);
      totalPrice -= discount;
      break;
    case 'amount':
      totalPrice -= subscriptionObject.nextDiscount;
      break;
    default:
      break;
  }
  try {
    subscription.set('nextDiscount', null);
    subscription.set('typeNextDiscount', null);
    await subscription.save();
    logInfo.type = 'info';
    logInfo.message = `${subscriptionObject.name} ${subscriptionObject.lastName} used the feature 'next discount' in ${subscriptionObject.subscriptionId}. ${subscriptionObject.typeNextDiscount} ${subscriptionObject.nextDiscount} `;
    LogDB(logInfo);
  } catch (error) {
    logInfo.type = 'error';
    logInfo.message = `Database error in subscription save(to null in nextDiscount feature) -> Name:${error.name} ~ Message: ${error.message}`;
    LogDB(logInfo);
    console.log(error);
  }

  return totalPrice;
};

const checkUserCredit = async (subscriptionObject, totalPrice) => {
  const logInfo = {
    user: subscriptionObject.userId,
    admin: null,
    type: '',
    message: '',
  };

  try {
    const user = await User.findById(subscriptionObject.userId);
    if (user.get('credit') > 0) {
      let creditDiscount = 0;
      if (totalPrice - user.get('credit') >= 0) {
        totalPrice -= user.get('credit');
      } else {
        creditDiscount = user.get('credit') - totalPrice;
        totalPrice = 0;
      }
      logInfo.type = 'info';
      logInfo.message = `User: ${user.get('name')} ${user.get('lastName')} used referral program credit`;
      LogDB(logInfo);
      await User.update({
        credit: creditDiscount,
      }, {
        where: {
          id: subscriptionObject.userId,
        },
      });
    }
  } catch (error) {
    logInfo.type = 'error';
    logInfo.message = `Database error in findById(user credit cron.) -> Name:${error.name} ~ Message: ${error.message}`;
    LogDB(logInfo);
    console.log(error);
  }

  return totalPrice;
};

const checkShouldRemoveOnceCoupon = (subscriptionObject, subscription) => {
  if ((subscriptionObject.subscriptionDuration && subscriptionObject.subscriptionDuration === 'once')) {
    const logInfo = {
      user: subscription.get('user_id'),
      admin: null,
      type: '',
      message: '',
    };

    logInfo.user = subscriptionObject.userId;
    logInfo.type = 'info';
    logInfo.message = `The coupon ${subscriptionObject.code} was deleted from subscription ${subscriptionObject.subscriptionId}`;
    LogDB(logInfo);

    subscription.set('couponId', null);
    subscription.save();
  }
};

const logSubscriptionStatus = (subscriptionObject, callingContext) => {
  const status = subscriptionObject.status;
  const logInfo = {
    user: subscriptionObject.userId,
    admin: null,
    type: '',
    message: '',
  };

  console.log(`Subscription ${subscriptionObject.subscriptionId} status => ${status} [${callingContext}]`);
  logInfo.type = 'info';
  logInfo.message = `Subscription ${subscriptionObject.subscriptionId} status => ${status} [${callingContext}]`;
  LogDB(logInfo);
};

const logSubscriptionIsPaused = (subscriptionObject, callingContext) => {
  const dateToRestart = moment.utc(subscriptionObject.pausedUntil);
  const logInfo = {
    user: subscriptionObject.userId,
    admin: null,
    type: '',
    message: '',
  };

  console.log(`Subscription ${subscriptionObject.subscriptionId} is paused until ${dateToRestart.format('YYYY-MM-DD')} [${callingContext}]`);
  logInfo.type = 'info';
  logInfo.message = `Subscription ${subscriptionObject.subscriptionId} is paused until ${dateToRestart.format('YYYY-MM-DD')} [${callingContext}]`;
  LogDB(logInfo);
};

const logSubscriptionShouldSkip = (subscriptionObject, callingContext) => {
  const nextSkipDate = moment.utc(subscriptionObject.nextSkipDate);
  const logInfo = {
    user: subscriptionObject.userId,
    admin: null,
    type: '',
    message: '',
  };

  console.log(`Subscription ${subscriptionObject.subscriptionId} was skipped because the date ${nextSkipDate.format('YYYY-MM-DD')} was found on skip table [${callingContext}]`);
  logInfo.type = 'info';
  logInfo.message = `Subscription ${subscriptionObject.subscriptionId} was skipped because the date ${nextSkipDate.format('YYYY-MM-DD')} was found on skip table [${callingContext}]`;
  LogDB(logInfo);
};

// subscription is not the subscription object!!! it's the reference to the DB object!
const activateSubscription = async (subscriptionObject, subscription, callingContext) => {
  const userId = subscriptionObject.userId;
  const logInfo = {
    user: userId,
    admin: null,
    type: '',
    message: '',
  };

  logInfo.type = 'info';
  logInfo.message = `Subscription ${subscriptionObject.subscriptionId} set status active and remove paused.} [${callingContext}]`;
  LogDB(logInfo);
  subscription.set('pausedUntil', null);
  subscription.set('status', 'active');
  const user = await User.findById(userId);
  user.updateAttributes({ status: 'active' });
  subscription.save();
};


const generateNewOrder = (order) => {
  const newOrder = Order.build(order);
  const logInfo = {
    user: newOrder.userId,
    admin: null,
    type: '',
    message: '',
  };
  newOrder.save()
    .then((o) => {
      console.log(` New order -> ${o.get('id')} to ${newOrder.name} ${newOrder.lastName}. Charge ID -> ${newOrder.chargeToken}`);
      logInfo.type = 'info';
      logInfo.message = ` New order -> ${o.get('id')} to ${newOrder.name} ${newOrder.lastName}. Charge ID -> ${newOrder.chargeToken}`;
      LogDB(logInfo);
    })
    .catch((error) => {
      logInfo.type = 'warn';
      logInfo.message = `An error ocurred when try to save the new order -> Name:${error.name} ~ Message: ${error.message} [generateNewOrder]`;
      console.log(error);
    });
};

const stripeCharge = (amount, customerId) => {
  return new Promise((resolve, reject) => {
    if (amount <= 0) {
      resolve({ id: 'free charge' });
      return;
    }

    stripe.charges.create({
      amount,
      currency: 'usd',
      customer: customerId,
    })
      .then((charge) => {
        resolve(charge);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const generateCharge = (order) => {
  const logInfo = {
    user: order.userId,
    admin: null,
    type: '',
    message: '',
  };
  stripeCharge(order.productPrice, order.customerId)
    .then((charge) => {
      const newOrder = Order.build(order);
      newOrder.chargeToken = charge.id;
      newOrder.save()
        .then((o) => {
          console.log(` New order -> ${o.get('id')} to ${newOrder.name} ${newOrder.lastName}. Charge ID -> ${newOrder.chargeToken}`);
          logInfo.type = 'info';
          logInfo.message = ` New order -> ${o.get('id')} to ${newOrder.name} ${newOrder.lastName}. Charge ID -> ${newOrder.chargeToken}`;
          LogDB(logInfo);
        })
        .catch((error) => {
          logInfo.type = 'warn';
          logInfo.message = `An error ocurred when try to save the new order -> Name:${error.name} ~ Message: ${error.message} [generateCharge]`;
          console.log(error);
        });
    })
    .catch((err) => {
      logInfo.type = 'error';
      logInfo.message = `A stripe error ocurred when try to charge the user ${order.email} -> ${err.raw.message}. Updating user and subscription status.`;
      LogDB(logInfo);
      User.findOne({
        where: {
          id: order.userId,
        },
        include: [Subscription],
      })
        .then(async (user) => {
          each(user.get('subscriptions'), (subscription, cb) => {
            subscription.set('status', 'pending');
            subscription.save();
            cb();
          }, () => {
            if (err) {
              console.log(err);
              return;
            }
            logInfo.type = 'info';
            logInfo.message = `A stripe error ocurred when try to charge the user ${order.email}. Subscription(s) status updated. Updating user status.`;
            LogDB(logInfo);
          });
          user.set('status', 'pending');
          user.save();
          LogDBUserStatus({
            userId: user.get('id'),
            userStatus: 'pending',
            userStatusDate: moment(),
          });
          logInfo.type = 'info';
          logInfo.message = `A stripe error ocurred when try to charge the user ${order.email}. Subscription(s) status updated. User status updated.`;
          LogDB(logInfo);
        })
        .catch(error => console.log(error));
      upsertZendesk({
        id: order.userId,
        email: order.email,
        name: `${order.name} ${order.lastName}`,
        user_fields: {
          customer_status: 'pending',
        },
      });
    });
};


export const getLatestOrders = (cronId) => {
  const callingContext = 'getLatestOrders';
  const query = `
    SELECT DISTINCT s.id, s.status, s.start_from, s.paused_until, s.user_id, s.amount, s.next_discount, s.type_next_discount, s.coupon_use_count, sk.date AS next_skip_date,
            (SELECT created_at FROM \`order\`
              WHERE \`subscription_id\` = s.id
              ORDER BY created_at DESC LIMIT 1) AS lastOrder,
              p.\`box_type\`, p.\`price\`, p.\`days\`,
              c.\`percent_off\`,c.\`amount_off\`,c.\`code\`,c.\`duration\`,c.\`times_usable\`,
              u.customer_id,u.name,u.last_name,u.email,u.address,u.address_extra,u.city,u.state,u.zip_code, u.\`has_mealmaker\`, u.\`delivered_mealmaker\`, u.\`first_charge\`
      FROM \`subscription\` AS s
      LEFT OUTER JOIN \`product\` AS p ON s.\`product_id\` = p.\`id\`
      LEFT OUTER JOIN \`user\` AS u ON s.\`user_id\` = u.\`id\`
      LEFT OUTER JOIN \`coupon\` AS c ON s.\`coupon_id\` = c.\`id\`
      LEFT JOIN \`skip\` sk ON sk.\`subscription\` = s.\`id\` AND sk.\`date\` = (
        SELECT MIN(sk.\`date\`)
        FROM skip sk
        WHERE sk.\`subscription\` = s.\`id\` AND sk.\`date\` >= CURDATE()
      )
  `;
  const opts = {
    model: Subscription,
  };
  sequelize.query(query, opts)
    .then((subscriptions) => {
      const now = moment.utc();

      eachLimit(subscriptions, 1, async (sub, cb) => {
        const subscriptionObject = getSubscriptionAsObject(sub, false);

        const logInfo = {
          user: subscriptionObject.userId,
          admin: null,
          type: '',
          message: '',
        };

        const orderCreated = moment.utc(subscriptionObject.lastOrder);
        const diff = Math.round(now.diff(orderCreated, 'days', true));
        const subscriptionId = subscriptionObject.subscriptionId;
        const status = subscriptionObject.status;
        const pausedUntil = subscriptionObject.pausedUntil;
        let totalPrice = subscriptionObject.price * subscriptionObject.amount;
        const discountAmountOff = subscriptionObject.discountAmountOff;
        const discountPercentOff = subscriptionObject.discountPercentOff;
        const couponUseCount = subscriptionObject.couponUseCount;
        const couponTimesUsable = subscriptionObject.couponTimesUsable;
        const subscriptionDuration = subscriptionObject.subscriptionDuration;
        const nextSkip = moment.utc(subscriptionObject.nextSkipDate);

        // We need to check the coupon use count and the coupon times usable if they are special.
        const isSpecialDiscount = subscriptionDuration === 'custom';

        if (orderCreated.isSame(moment.utc(), 'day')) {
          // Last order created by cron on createFirstOrders
          console.log('Last order created by cron on createFirstOrders');
          return cb();
        }

        if (nextSkip.isSame(moment.utc(), 'day')) {
          logSubscriptionShouldSkip(subscriptionObject, callingContext);
          return cb();
        }

        const shouldLogStatus = (status === PENDING || status === CANCELED || (status === PAUSED && !pausedUntil));
        if (shouldLogStatus) {
          logSubscriptionStatus(subscriptionObject, callingContext);
          return cb();
        }

        const isPaused = status === PAUSED && pausedUntil;
        if (isPaused) {
          const dateToRestart = moment.utc(pausedUntil);
          const diffPausedUntil = Math.round(now.diff(dateToRestart, 'days', true));
          if (diffPausedUntil < 0) {
            logSubscriptionIsPaused(subscriptionObject, callingContext);
            return cb();
          }

          await activateSubscription(subscriptionObject, sub, callingContext);
        }

        if (diff >= subscriptionObject.days) {
          if (discountPercentOff || discountAmountOff) {
            const shouldApplyDiscount = ((isSpecialDiscount && couponUseCount < couponTimesUsable) || !isSpecialDiscount);
            if (shouldApplyDiscount) {
              totalPrice = await processDiscount(subscriptionObject, discountPercentOff, discountAmountOff, totalPrice);
              logInfo.type = 'info';
              logInfo.message = `Coupon ${subscriptionObject.code} was used by ${subscriptionObject.name} ${subscriptionObject.lastName} in ${subscriptionId}`;
              LogDB(logInfo);
            }
          }


          const hasNextDiscount = subscriptionObject.nextDiscount;
          if (hasNextDiscount) {
            totalPrice = await processNextDiscount(subscriptionObject, sub, totalPrice);
          }

          totalPrice = await checkUserCredit(subscriptionObject, totalPrice);

          // Prevents float numbers, stripe fire an error with floats in price.
          totalPrice = Number(totalPrice.toFixed(0));
          for (let i = 0; i < subscriptionObject.amount; i++) {
            const productPrice = totalPrice / subscriptionObject.amount;
            const dataForCharge = {
              cronId,
              ...getSubscriptionOrderData(subscriptionObject, productPrice, null, subscriptionObject.boxType),
            };

            generateCharge(dataForCharge);
          }
        }

        if ((subscriptionObject.days - diff) > 0) {
          logInfo.type = 'info';
          logInfo.message = `User: ${subscriptionObject.name} ${subscriptionObject.lastName} ${subscriptionObject.userId}. Days left to new order -> ${subscriptionObject.days - diff} days`;
          LogDB(logInfo);
          console.log(`User: ${subscriptionObject.name} ${subscriptionObject.lastName} ${subscriptionObject.userId}. Days left to new order -> ${subscriptionObject.days - diff} days`);
        }
        setTimeout(() => {
          cb();
        }, 500);
      });
    })
    .catch((err) => {
      const logInfo = {
        user: null,
        admin: null,
        type: 'error',
        message: `Database error in getLatestOrders -> Name:${err.name} ~ Message: ${err.message}`,
      };
      LogDB(logInfo);
      console.log(err);
    });
};

const getSubscriptions = (cron, query) => {
  const opts = {
    // raw: true,
    model: Subscription,
    // type: sequelize.QueryTypes.SELECT
  };

  return sequelize.query(query, opts);
};

export const createFirstOrders = (cron) => {
  return new Promise((resolve) => {
    const callingContext = 'createFirstOrders';
    const cronId = cron.get('id');
    /*eslint-disable */
    const query = `
      SELECT DISTINCT s.id, s.status, s.start_from, s.paused_until, s.user_id, s.amount, s.coupon_use_count, sk.date AS next_skip_date,
            (SELECT COUNT(*) FROM \`order\`
              WHERE \`subscription_id\` = s.id) AS orders,
              p.\`box_type\`, p.\`price\`,
              c.\`percent_off\`,c.\`amount_off\`,c.\`duration\`,c.\`code\`, c.\`times_usable\`,
              u.customer_id,u.name,u.last_name,u.email,u.address,u.address_extra,u.city,u.state,u.zip_code, u.\`has_mealmaker\`, u.\`delivered_mealmaker\`, u.\`first_charge\`
      FROM \`subscription\` AS s
      LEFT OUTER JOIN \`product\` AS p ON s.\`product_id\` = p.\`id\`
      LEFT OUTER JOIN \`user\` AS u ON s.\`user_id\` = u.\`id\`
      LEFT OUTER JOIN \`coupon\` AS c ON s.\`coupon_id\` = c.\`id\`
      LEFT JOIN \`skip\` sk ON sk.\`subscription\` = s.\`id\` AND sk.\`date\` = (
	    SELECT MIN(sk.\`date\`)
	    FROM \`skip\` sk
	    WHERE sk.\`subscription\` = s.\`id\` AND sk.\`date\` >= CURDATE()
      )
      HAVING orders = 0
    `;
    /*eslint-enable */
    const opts = {
      // raw: true,
      model: Subscription,
      // type: sequelize.QueryTypes.SELECT
    };
    sequelize.query(query, opts)
      .then((subscriptions) => {
        const now = moment.utc();
        const justSentMealMaker = [];
        const groupUsersById = subscriptions.reduce((obj, sub) => {
          obj[sub.get('user_id')] = obj[sub.get('user_id')] || [];
          obj[sub.get('user_id')].push(sub);
          return obj;
        }, {});
        eachLimit(subscriptions, 1, async (subscription, cb) => {
          const logInfo = {
            user: subscription.get('user_id'),
            admin: null,
            type: '',
            message: '',
          };

          const subscriptionObject = getSubscriptionAsObject(subscription, true);

          const subscriptionId = subscriptionObject.subscriptionId;
          const status = subscriptionObject.status;
          const startFrom = subscriptionObject.startFrom;
          const pausedUntil = subscriptionObject.pausedUntil;
          const subscriptionDuration = subscriptionObject.subscriptionDuration;
          const discountAmountOff = subscriptionObject.discountAmountOff;
          const discountPercentOff = subscriptionObject.discountPercentOff;
          const userId = subscriptionObject.userId;
          const couponUseCount = subscriptionObject.couponUseCount;
          const couponTimesUsable = subscriptionObject.couponTimesUsable;
          const nextSkip = moment.utc(subscriptionObject.nextSkipDate);

          // We need to check the coupon use count and the coupon times usable if they are special.
          const isSpecialDiscount = subscriptionDuration === 'custom';

          if (nextSkip.isSame(moment.utc(), 'day')) {
            logSubscriptionShouldSkip(subscriptionObject, callingContext);
            return cb();
          }

          const shouldLogStatus = (status === PENDING || status === CANCELED || (status === PAUSED && !pausedUntil));
          if (shouldLogStatus) {
            logSubscriptionStatus(subscriptionObject, callingContext);
            return cb();
          }

          const isPaused = status === PAUSED && pausedUntil;
          if (isPaused) {
            const dateToRestart = moment.utc(pausedUntil);
            const diffPausedUntil = Math.round(now.diff(dateToRestart, 'days', true));

            if (diffPausedUntil < 0) {
              logSubscriptionIsPaused(subscriptionObject, callingContext);
              return cb();
            }

            await activateSubscription(subscriptionObject, subscription, callingContext);
          }

          if (startFrom) {
            const dateToStart = moment.utc(startFrom);
            const diffStart = Math.round(now.diff(dateToStart, 'days', true));
            if (diffStart < 0) {
              console.log(`Subscription ${subscriptionObject.subscriptionId} starts on ${moment.utc(startFrom).format('MM-DD-YYYY')} [createFirstOrders]`);
              logInfo.type = 'info';
              logInfo.message = `Subscription ${subscriptionObject.subscriptionId} starts on ${moment.utc(startFrom).format('MM-DD-YYYY')} [createFirstOrders]`;
              LogDB(logInfo);
              return cb();
            }
          }

          const firstCharge = subscriptionObject.firstCharge;
          const hasMealMaker = subscriptionObject.hasMealMaker;
          const deliveredMealMaker = subscriptionObject.deliveredMealMaker;

          const updateUser = {};
          const shouldGetMealMaker = hasMealMaker && !deliveredMealMaker && justSentMealMaker.indexOf(userId) === -1;
          if (shouldGetMealMaker) {
            // Store that we sent Meal Maker to user
            justSentMealMaker.push(userId);
            console.log(`User: ${userId} will get a Meal Maker`);

            updateUser.deliveredMealMaker = true;
            const mealMaker = 'Meal Maker';
            const subscriptionOrderData = getSubscriptionOrderData(subscriptionObject, 0, null, mealMaker);
            subscriptionOrderData.subscriptionId = null;
            generateNewOrder({
              cronId,
              ...subscriptionOrderData,
            });
          }

          let totalPrice = subscriptionObject.price * subscriptionObject.amount;
          updateUser.firstCharge = null;

          if (discountPercentOff || discountAmountOff) {
            const shouldApplyDiscount = ((isSpecialDiscount && couponUseCount < couponTimesUsable) || !isSpecialDiscount);

            if (shouldApplyDiscount) {
              totalPrice = await processDiscount(subscriptionObject, discountPercentOff, discountAmountOff, totalPrice);
            }
          }

          Referral.findOne({
            where: {
              me: userId,
            },
          })
            .then((isReferral) => {
              if (isReferral) {
                totalPrice -= config.referralFirstDiscount / groupUsersById[userId].length;
              }
              for (let i = 0; i < subscription.amount; i++) {
                // Create new order for the selected product
                const productPrice = totalPrice / subscriptionObject.amount;
                const chargeToken = firstCharge || null;
                const boxType = 'First Box';

                generateNewOrder({
                  cronId,
                  ...getSubscriptionOrderData(subscriptionObject, productPrice, chargeToken, boxType),
                });

                checkShouldRemoveOnceCoupon(subscriptionObject, subscription);
              }
            });

          if (Object.keys(updateUser).length > 0) {
            User.update(updateUser, {
              where: {
                id: userId,
              },
            });
          }

          setTimeout(() => {
            cb();
          }, 500);
        }, () => {
          resolve();
        });
      })
      .catch((err) => {
        const logInfo = {
          user: null,
          admin: null,
          type: 'error',
          message: `Database error in createFirstOrders -> Name:${err.name} ~ Message: ${err.message}`,
        };
        LogDB(logInfo);
      });
  });
};

export const updatePauseUntil = async (cron) => {
  const dataToUpdate = {
    pausedUntil: null,
    status: 'active',
  };
  const logInfo = {
    user: null,
    admin: null,
    type: '',
    message: '',
  };
  try {
    const subscriptions = await getPausedUntil();
    each(subscriptions, async (subscription, cb) => {
      const subscriptionUpdated = await subscription.update(dataToUpdate);
      const formatedSubscriptionData = formatUpdatedData(subscriptionUpdated.get('dataChanged'));

      // Sometimes, this method gets called manually, so no cron invokes it.
      const logFromWho = cron ? `Cron #(${cron.get('id')}) update` : 'Manual update';

      logInfo.user = subscriptionUpdated.get('userId');
      logInfo.type = 'info';
      logInfo.message = `${logFromWho} the subscription #(${subscriptionUpdated.get('id')}), ${JSON.stringify(formatedSubscriptionData)}. UPDATING USER.`;
      LogDB(logInfo);
      const user = await User.find({
        where: {
          id: subscriptionUpdated.get('userId'),
        },
        include: [{
          model: Subscription,
          as: 'subscriptions',
        }],
      });
      const status = getSubscriptionStatus(user.subscriptions);
      const userUpdated = await user.update({ status });
      const formatedUserData = formatUpdatedData(userUpdated.get('dataChanged'));
      logInfo.user = userUpdated.get('id');
      logInfo.type = 'info';
      logInfo.message = `${logFromWho} the user #(${userUpdated.get('id')}), ${JSON.stringify(formatedUserData)} UPDATED USER.`;
      LogDB(logInfo);
      updateIntegration(user.id)
        .then((d) => {
          console.log(d);
        })
        .catch(err => console.log(err));
      cb();
    }, (err) => {
      if (err) {
        return console.log(err);
      }
      console.log('Update subscriptions and users ok.');
    });
  } catch (error) {
    logInfo.type = 'error';
    logInfo.message = 'Error in updatePausedUntil';
    LogDB(logInfo);
  }
};
