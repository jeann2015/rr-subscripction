import { eachLimit } from 'async';
import moment from 'moment';

import db from '../db/connection';
import { getUserLastBoxNumber } from '../entities/User/controller';

const sequelize = db.sequelize;
const Referral = db.Referral;
const User = db.User;

const getReferralsDBInfo = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const referrals = await Referral.findAll({
        include: [
          { all: true, required: true },
        ],
      });

      const referralsPlain = referrals.map(referral => referral.get({ plain: true }));
      resolve(referralsPlain);
    } catch (err) {
      reject(err);
    }
  });
};

const organizeData = async (referralObject) => {
  return new Promise(async (resolve, reject) => {
    // const { referralObject: { id, name, lastName, email, status } } = referralObject
    try {
      const referrerBoxNumber = await getUserLastBoxNumber(referralObject.userReferred.id);
      const referreeBoxNumber = await getUserLastBoxNumber(referralObject.userReferree.id);
      const referralData = {
        referrerUserId: referralObject.userReferred.id,
        referrerName: referralObject.userReferred.name,
        referrerLastName: referralObject.userReferred.lastName,
        referrerEmail: referralObject.userReferred.email,
        referrerBoxNumber: referrerBoxNumber,
        referrerStatus: referralObject.userReferred.status,
        referreeUserId: referralObject.userReferree.id,
        referreeName: referralObject.userReferree.name,
        referreeLastName: referralObject.userReferree.lastName,
        referreeEmail: referralObject.userReferree.email,
        referreeBoxNumber: referreeBoxNumber,
        referreeStatus: referralObject.userReferree.status,
        dateReferralUsed: moment(referralObject.subscriptionReferree.createdAt).format('MM-DD-YYYY'),
      };
      return resolve(referralData);
    } catch (err) {
      return reject(err);
    }
  });
};

export const getReferralsData = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const info = [];
      const refsData = await getReferralsDBInfo();
      eachLimit(refsData, 1, async (obj, cb) => {
        const organizedData = await organizeData(obj);
        info.push(organizedData);
        cb();
      }, () => {
        return resolve(info);
      });
    } catch (err) {
      return reject(err);
    }
  });
};
