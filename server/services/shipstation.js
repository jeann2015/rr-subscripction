
import config from '../config/shipstationConfig';
import { getTagIdsArray } from './shipstation_tag';
import { normalizeBoxTypes } from '../entities/Order/plugin';

const shipstationAPI = require('node-shipstation');

const shipstation = new shipstationAPI(
  config.shipstationApiKey,
  config.shipstationApiSecret,
);

// Returns Address model corresponding to Shipping address required by ShipStation
export const getShipToObject = (user) => {
  return {
    name: `${user.name} ${user.lastName}`,
    street1: user.address,
    street2: user.addressExtra,
    company: user.company,
    city: user.city,
    state: user.state,
    postalCode: user.zipCode,
    country: 'US',
    phone: user.phoneNumber,
  };
};

// Returns Address model corresponding to Billing address required by ShipStation
export const getBillToObject = (user) => {
  return {
    name: `${user.billingName} ${user.billingLastName}`,
    street1: user.billingAddress,
    street2: user.billingAddressExtra,
    company: user.company,
    city: user.billingCity,
    state: user.billingState,
    postalCode: user.billingZipCode,
    country: 'US',
    phone: user.phoneNumber,
  };
};

export const getItemObject = (item, unitPrice) => ({
  sku: item.mealSku,
  name: item.meal.name,
  quantity: item.mealQuantity,
  unitPrice,
  // productId,
});

// Return ShipStation's Order model based on user and order data.
export const getOrderModel = async (user, order, mealPrices) => {
  return new Promise(async (resolve, reject) => {
    try {
      const customerUsername = `${config.shipstationEnv}-${order.userId}`;
      const orderNumber = `${customerUsername}-${order.id}`;
      const unitaryPrice = mealPrices.filter(product => product.boxType === normalizeBoxTypes(order.boxType))[0].mealPrice;
      const items = order.boxDetail.map(meal => getItemObject(meal, unitaryPrice));
      const orderTagIds = await getTagIdsArray(order);

      resolve({
        orderNumber: orderNumber,
        orderDate: order.createdAt,
        orderStatus: 'awaiting_shipment',
        customerUsername: customerUsername,
        customerEmail: order.email,
        billTo: getBillToObject(user),
        shipTo: getShipToObject(user),
        customField1: (order.boxNumber !== null) ? `Box Number # ${order.boxNumber}` : '',
        items,
        tagIds: orderTagIds,
      });
    } catch (err) {
      reject(`getOrderModel Error: ${err}`);
    }
  });
};

// Function to send an array of orders to ShipStation
// TODO: This can be also used to update orders, providing orderId.
export const sendOrders = (orders) => {
  return new Promise((resolve, reject) => {
    shipstation.post('/orders/createorders', orders, (err, res) => {
      if (err) reject(err);
      resolve(res.body);
    });
  });
};

export const getOrders = () => {
  return new Promise((resolve, reject) => {
    shipstation.getOrders((err, res) => {
      if (err) reject(err);
      resolve(res.body);
    });
  });
};
