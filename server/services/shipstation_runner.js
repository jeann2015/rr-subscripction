import moment from 'moment';
import { eachLimit } from 'async';
import CronJob from 'cron';
import mainConfig from '../config/config';
import { LogDB } from '../entities/Log/plugin';
import db from '../db/connection';
import { getOrderModel, sendOrders } from './shipstation';
import { getMealsUnitaryPrice } from '../entities/Product/controller';

const cronJob = CronJob.CronJob;

const zeroDay = moment.utc(mainConfig.zeroDay, 'YYYYMMDD');
const dayWhenRepeat = mainConfig.dayWhenRepeat;

// Function to save ShipStation's order ID and status. Results param is sendOrders response.results
export const saveOrdersIdsAndStatus = async (orders, results) => {
  let hasErrors = false;
  eachLimit(results, 1, async (orderResult, cb) => {
    if (!orderResult.success) {
      console.log(`ShipStation error when adding order ${orderResult.orderNumber}: ${orderResult.errorMessage}`);
      cb();
    }

    const orderIdInOrderNumber = orderResult.orderNumber.split('-')[2];

    const order = orders.filter((order) => {
      const plain = order.get({ plain: true });
      return plain.id === parseInt(orderIdInOrderNumber);
    })[0];

    order.set('shipstationOrder', orderResult.orderId);
    order.set('shipstationStatus', 'awaiting_shipment');
    order.set('trackingEmail', 'not_sent');
    try {
      const saveOrder = await order.save();
      const logInfo = {
        admin: null,
        user: order.get('userId'),
        type: 'info',
        message: 'ShipStation -> Order successfully updated in database.',
      };
      LogDB(logInfo);
    } catch (err) {
      console.log(`Can't update Order #${order.id} on Database: ${err}`);
      const logInfo = {
        admin: null,
        user: order.get('userId'),
        type: 'error',
        message: 'ShipStation error -> Can\'t update Order.',
      };
      LogDB(logInfo);
      hasErrors = true;
    }

    cb();
  });

  if (hasErrors) {
    const logInfo = {
      admin: null,
      type: 'error',
      message: 'ShipStation error -> There were some errors when updating orders in database.',
    };
    LogDB(logInfo);

    return { hasErrors: true };
  }

  const logInfo = {
    admin: null,
    type: 'info',
    message: 'ShipStation -> All orders successfully updated in database.',
  };
  LogDB(logInfo);
  console.log(logInfo.message);

  return { hasErrors: false };
};

// Takes array and size (int) and returns a splitted version of the array,
// with sub-arrays of lenght size.
// [a, b, c, d, e, f, g], size = 3, => [[a, b, c], [d, e, f], [g]]
export const splitArray = (array, size) => {
  const splitted = [];
  for (let i = 0; i < array.length; i += size) {
    splitted.push(array.slice(i, i + size));
  }
  return splitted;
};

export const getOrdersAndSendToShipstation = async () => {
  let hasErrors = false;
  const lastCron = await db.Cron.findOne({
    order: [
      ['id', 'DESC'],
    ],
  });

  const orders = await db.Order.findAll({
    where: {
      cron_id: lastCron.get('id'),
      shipstation_order: null,
    },
    include: [
      db.User,
      {
        model: db.BoxDetailHistory,
        as: 'boxDetail',
        include: [db.Meals],
      },
    ],
  });

  const mealUnitaryPrices = await getMealsUnitaryPrice();
  const shipstationOrders = [];

  // Get ShipStation Order model for every order and add it to ordersToSend array.
  const ordersToSend = [];
  eachLimit(orders, 1, async (order, complete) => {
    try {
      const plain = order.get({ plain: true });
      const orderModel = await getOrderModel(plain.user, plain, mealUnitaryPrices);
      ordersToSend.push(orderModel);
      complete();
    } catch (error) {
      console.log(error);
    }
  }, () => {
    // ShipStation API can handle a max of 100 orders per request, so we split ordersToSend
    const splittedOrdersToSend = splitArray(ordersToSend, 100);
    eachLimit(splittedOrdersToSend, 1, async (chunk, cb) => {
      // Send chunk of orders to ShipStation. Callback returns response = {hasErrors: bool, results: []}
      try {
        const response = await sendOrders(chunk);
        shipstationOrders.push(...response.results);
        // TODO: decide best way to handle errors

        if (response.hasErrors) {
          const logInfo = {
            admin: null,
            type: 'error',
            message: 'ShipStation error -> There were some errors when adding orders to ShipStation',
          };
          LogDB(logInfo);
          console.log(logInfo.message);
          hasErrors = true;
        }

        const savedOrders = await saveOrdersIdsAndStatus(orders, response.results);
        if (savedOrders.hasErrors) {
          hasErrors = true;
        }
        cb();
      } catch (error) {
        console.log(error);
      }
    }, (err) => {
      if (err) {
        console.log(err);
        return;
      }
      if (!hasErrors) {
        const logInfo = {
          admin: null,
          type: 'info',
          message: 'All orders have been successfully added to ShipStation and updated in database.',
        };
        LogDB(logInfo);
        console.log(logInfo.message);
      }
      return;
    });
  });
};

const instance = () => {
  return new cronJob('00 30 10 * * *', () => {
    const now = moment.utc();
    const diff = now.diff(zeroDay, 'days');
    const diffRemainder = diff % dayWhenRepeat;
    if (diffRemainder === 0) {
      getOrdersAndSendToShipstation();
    }
  }, null, true, 'America/Los_Angeles');
};

export default instance;
