import moment from 'moment';
import { eachLimit, whilst } from 'async';

import config from '../config/shipstationConfig';
import db from '../db/connection';

const shipstationAPI = require('node-shipstation');

const shipstation = new shipstationAPI(
  config.shipstationApiKey,
  config.shipstationApiSecret,
);

const Order = db.Order;
const Cron = db.Cron;

const getOrders = async (lastDate, page) => {
  return new Promise((resolve, reject) => {
    shipstation.getOrders({ createDateStart: lastDate, pageSize: 500, page: page }, (err, res) => {
      if (err) reject(err);
      resolve(res.body);
    });
  });
};

const getFulfillments = (lastDate, page) => {
  return new Promise((resolve, reject) => {
    shipstation.get('/fulfillments', { createDateStart: lastDate, pageSize: 500, page: page }, (err, res) => {
      if (err) reject(err);
      resolve(res.body);
    });
  });
};

export const getShipstationOrdersSinceLastCron = async () => {
  const lastCronDate = await Cron.max('createdAt');
  const lastDate = moment(lastCronDate).format('YYYY-MM-DD');
  const status = [];
  let totalPages = null;

  // Uncomment to use a custom date
  // const lastDate = '2018-01-03';

  return new Promise((resolve, reject) => {
    // Get all orders created after last Cron.
    let page = 1;
    whilst(
      () => {
        if (totalPages === null) return true;
        return page <= totalPages;
      },
      async (cb) => {
        const orders = await getOrders(lastDate, page);
        orders.orders.map((order) => {
          status.push({ id: order.orderId, status: order.orderStatus });
        });
        totalPages = orders.pages;
        page++;
        cb();
      },
      (error, n) => {
        if (error) return reject(error);
        resolve(status);
      },
    );
  });
};

export const getShipstationTrackingNumbersSinceLastCron = async () => {
  const lastCronDate = await Cron.max('createdAt');
  const lastDate = moment(lastCronDate).format('YYYY-MM-DD');
  const trackingNumbers = [];
  let totalPages = null;

  // Uncomment to use a custom date
  // const lastDate = '2018-01-03';

  return new Promise((resolve, reject) => {
    // Get all orders created after last Cron.
    let page = 1;
    whilst(
      () => {
        if (totalPages === null) return true;
        return page <= totalPages;
      },
      async (cb) => {
        const orders = await getFulfillments(lastDate, page);
        orders.fulfillments.map((order) => {
          trackingNumbers.push({ id: order.orderId, trackingNumber: order.trackingNumber });
        });
        totalPages = orders.pages;
        page++;
        cb();
      },
      (error, n) => {
        if (error) return reject(error);
        resolve(trackingNumbers);
      },
    );
  });
};

export const saveOrderStatusToDb = async (statusArray) => {
  return new Promise(async (resolve, reject) => {
    eachLimit(statusArray, 1, async (order, cb) => {
      const dbOrder = await Order.find({
        where: {
          shipstationOrder: order.id,
        },
      });

      if (dbOrder === null) {
        console.log(`Can't find order ${order.id} on DB.`);
        return cb();
      }

      dbOrder.set('shipstationStatus', order.status);
      await dbOrder.save();
      cb();
    }, (err) => {
      if (err) reject(err);
      resolve({ message: 'All orders have been updated' });
    });
  });
};

export const saveTrackingNumbersToDb = async (trackingNumbersArray) => {
  return new Promise(async (resolve, reject) => {
    eachLimit(trackingNumbersArray, 1, async (order, cb) => {
      const dbOrder = await Order.find({
        where: {
          shipstationOrder: order.id,
        },
      });

      if (dbOrder === null) {
        console.log(`Can't find order ${order.id} on DB.`);
        return cb();
      }

      dbOrder.set('trackId', order.trackingNumber);
      dbOrder.set('tracked', 1);
      await dbOrder.save();
      cb();
    }, (err) => {
      if (err) reject(err);
      resolve({ message: 'All tracking numbers have been updated' });
    });
  });
};
