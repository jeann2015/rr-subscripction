import config from '../config/shipstationConfig';
import { normalizeBoxTypes } from '../entities/Order/plugin';

const shipstationAPI = require('node-shipstation');
const db = require('../db/connection');

const shipstation = new shipstationAPI(
  config.shipstationApiKey,
  config.shipstationApiSecret,
);

export const tagOrder = (orderId, tagId) => {
  return new Promise((resolve, reject) => {
    shipstation.addTagToOrder({
      orderId,
      tagId,
    }, (err, body) => {
      if (err) {
        console.log(err);
        reject(err);
      }
      resolve(body);
    });
  });
};

export const get3PLInfo = (order) => {
  return new Promise(async (resolve, reject) => {
    try {
      const tagDay = await db.TagDay.find({
        include: [{
          model: db.TagDayName,
          as: 'TagDayName',
          include: [{
            model: db.Warehouses,
            as: 'Warehouse' }],
        }],
        where: {
          zipCode: Number(order.zipCode),
        },
      });

      // If zip code is missing in DB, use fallback warehouse.
      if (!tagDay) {
        console.log(`Zip code ${order.zipCode} not found on Database (tagDay). Using default warehouse (Marshall, MN)`);

        const defaultWarehouse = await db.Warehouses.find({
          where: { warehouse: { $like: 'Marshall, MN' } },
        });
        resolve({
          transit: '2 DAY',
          _3pl: defaultWarehouse,
        });
      }

      resolve({
        transit: tagDay.TagDayName.transit,
        _3pl: tagDay.TagDayName.Warehouse,
      });
    } catch (error) {
      reject(error);
    }
  });
};

export const getTagIdsArray = async (order) => {
  return new Promise(async (resolve, reject) => {
    try {
      const _3plInfo = await get3PLInfo(order);
      const transitTag = _3plInfo.transit === '1 DAY' ? config.tag1Day : config.tag2Day;
      const warehouseTag = _3plInfo._3pl.tagId;
      const configTag = config[order.configuration];

      const quantityTagName = `tagQuantity${normalizeBoxTypes(order.boxType).split(' ')[0]}`;
      const quantityTag = config[quantityTagName];

      resolve([transitTag, warehouseTag, configTag, quantityTag]);
    } catch (err) {
      reject(err);
    }
  });
};
