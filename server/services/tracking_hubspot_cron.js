import CronJob from 'cron';
import { getOrdersWithTrackingIdData, updateEmailStatus } from '../entities/Order/plugin';
import { updateHubspotContactByEmailBatch } from '../utils/hubspot';
import { getTrackingLink } from '../utils';

const cronJob = CronJob.CronJob;

export const runCron = async () => {
  console.log('Run Cron on send_email_tracking_hubspot');
  const orders = await getOrdersWithTrackingIdData();
  if (orders.length === 0) return;

  const dataToSend = orders.map(order => ({
    email: order.user.email,
    properties: {
      tracking_link: getTrackingLink(order),
    },
  }));

  const sendData = await updateHubspotContactByEmailBatch(dataToSend);

  if (sendData === undefined) { // This is what the API responds if it was all OK.
    updateEmailStatus(orders);
  }
};

const instance = () => {
  return new cronJob('00 00 * * * *', () => {
    runCron();
  }, null, true, 'America/Los_Angeles');
};
export default instance;
