import chai from 'chai';
import { calculateDiscounts } from '../services/cron_charge';
import { applyCreditDiscount, applyNextDiscount, applyCustomBoxesDiscount, applyCouponDiscount, appliedDiscountsRegistry, shouldCalculateDiscount } from '../utils/calculate-discounts';
import { getCronDate } from '../services/cron_new';

import db from '../db/connection';

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;

// Apply Credit Discount Test
describe('Apply Credit Discount', () => {
  function testCase(_userCredit, _productPrice) {
    const userCredit = _userCredit;
    const productPrice = _productPrice;
    const res = applyCreditDiscount(userCredit, productPrice);
    expect(res).to.be.an('object');
    return res;
  }

  it('User has equal credit than Product Price', () => {
    const res = testCase(9500, 9500);
    expect(res.userCredit).to.equal(0);
    expect(res.productPrice).to.equal(0);
  });

  it('User has more credit than Product Price', () => {
    const res = testCase(10000, 9500);
    expect(res.userCredit).to.equal(500);
    expect(res.productPrice).to.equal(0);
  });

  it('User has less credit than Product Price', () => {
    const res = testCase(9000, 9500);
    expect(res.userCredit).to.equal(0);
    expect(res.productPrice).to.equal(500);
  });

  it('User has negative credit', () => {
    const res = testCase(-9000, 9500);
    expect(res.userCredit).to.equal(0);
    expect(res.productPrice).to.equal(9500);
  });

  // If product Negative => Product should be 0.
  it('Product Price is Negative', () => {
    const res = testCase(500, -9500);
    expect(res.userCredit).to.equal(500);
    expect(res.productPrice).to.equal(0);
  });
});


// Apply Next Discount Test
describe('Apply Next Discount', () => {
  function testCase(_discountType, _discountAmount, _productPrice) {
    const discountType = _discountType;
    const discountAmount = _discountAmount;
    const productPrice = _productPrice;
    const res = applyNextDiscount(discountType, discountAmount, productPrice);
    expect(res).to.be.an('object');
    return res;
  }

  // Percent
  it('Percent Next Discount of 45%', () => {
    const res = testCase('percent', 45, 9500);
    expect(res.productPrice).to.equal(5225);
  });

  it('Percent Next Discount of 100%', () => {
    const res = testCase('percent', 100, 9500);
    expect(res.productPrice).to.equal(0);
  });

  it('Percent Next Discount of 0%', () => {
    const res = testCase('percent', 0, 9500);
    expect(res.productPrice).to.equal(9500);
  });

  it('Percent Next Discount of 50% over 0 USD Product', () => {
    const res = testCase('percent', 50, 0);
    expect(res.productPrice).to.equal(0);
  });

  // If discount negative => Discount should be 0.
  it('Negative Percent Next Discount of 50%', () => {
    const res = testCase('percent', -50, 9500);
    expect(res.productPrice).to.equal(9500);
  });

  // Amount
  it('Amount Next Discount of 4500', () => {
    const res = testCase('amount', 4500, 9500);
    expect(res.productPrice).to.equal(5000);
  });

  it('Amount Next Discount of 9500', () => {
    const res = testCase('amount', 9500, 9500);
    expect(res.productPrice).to.equal(0);
  });

  it('Amount Next Discount of -500', () => {
    const res = testCase('amount', -500, 9500);
    expect(res.productPrice).to.equal(9500);
  });
});

/* INFO
* Should check on cron_chage => maxRedemptions vs timesRedeemed.
* Shoul check if coupon is valid or not (Actually we are checking this only on the checkout)
*
*
*
*
*/

describe('Apply Coupon Discount', () => {
  // Coupon
  const coupon1 = { id: 2298,
    productId: null,
    code: 'SPA',
    maxRedemptions: 999,
    timesRedeemed: 2,
    duration: 'once',
    valid: true,
    timesUsable: -1,
    boxList: null,
  };

  // Coupon
  const coupon2 = { id: 2298,
    productId: null,
    code: 'SPA',
    maxRedemptions: 999,
    timesRedeemed: 2,
    duration: 'custom',
    valid: true,
    boxList: null,
  };

  function testCase(_coupon, _productPrice, _couponUseCount, _discountAmount) {
    const coupon = _coupon;
    const productPrice = _productPrice;
    const couponUseCount = _couponUseCount;
    const discountAmount = _discountAmount;
    const res = applyCouponDiscount(coupon, productPrice, couponUseCount, discountAmount);
    expect(res).to.be.an('object');
    return res;
  }

  // Amount
  it('Coupon1 Amount CASE A', () => {
    coupon1.percentOff = null;
    coupon1.amountOff = 2500;
    const res = testCase(coupon1, 9500, 1, coupon1.amountOff);
    expect(res.productPrice).to.equal(7000);
  });

  it('Coupon1 Amount CASE B', () => {
    coupon1.percentOff = null;
    coupon1.amountOff = 9500;
    const res = testCase(coupon1, 9500, 1, coupon1.amountOff);
    expect(res.productPrice).to.equal(0);
  });

  it('Coupon1 Amount CASE C', () => {
    coupon1.percentOff = null;
    coupon1.amountOff = -1000;
    const res = testCase(coupon1, 9500, 1, coupon1.amountOff);
    expect(res.productPrice).to.equal(9500);
  });

  // Percent (It is already recieved with conversion to amount)
  it('Coupon1 Percent CASE A', () => {
    coupon1.percentOff = 1000;
    coupon1.amountOff = null;
    const res = testCase(coupon1, 9500, 1, coupon1.percentOff);
    expect(res.productPrice).to.equal(8500);
  });

  it('Coupon1 Percent CASE B', () => {
    coupon1.percentOff = -3000;
    coupon1.amountOff = null;
    const res = testCase(coupon1, 9500, 1, coupon1.percentOff);
    expect(res.productPrice).to.equal(9500);
  });

  it('Coupon1 Percent CASE C', () => {
    coupon1.percentOff = 9500;
    coupon1.amountOff = null;
    const res = testCase(coupon1, 9500, 1, coupon1.percentOff);
    expect(res.productPrice).to.equal(0);
  });

  // Custom Coupon
  it('Coupon2 Amount CASE A - Times Usable grater than couponUseCount', () => {
    coupon2.timesUsable = 3;
    coupon2.percentOff = null;
    coupon2.amountOff = 2500;
    const res = testCase(coupon2, 9500, 4, coupon2.amountOff);
    expect(res.productPrice).to.equal(9500);
  });

  it('Coupon2 Amount CASE A - Times Usable lower than couponUseCount', () => {
    coupon2.timesUsable = 3;
    coupon2.percentOff = null;
    coupon2.amountOff = 2500;
    const res = testCase(coupon2, 9500, 2, coupon2.amountOff);
    expect(res.productPrice).to.equal(7000);
  });

  it('Coupon2 Amount CASE A - Times Usable Negative', () => {
    coupon2.timesUsable = -1;
    coupon2.percentOff = null;
    coupon2.amountOff = 2500;
    const res = testCase(coupon2, 9500, 2, coupon2.amountOff);
    expect(res.productPrice).to.equal(9500);
  });
});

// describe('calculateDiscounts', () => {
//   it('User A - First Cron - Two Boxes, 50% Once, 1 Referral', async () => {
//     const cronDate = await getCronDate(0);
//     const subscription = db.Subscription.build({
//       id: 1,
//       userId: 1,
//       couponId: 341,
//       productId: 3,
//       status: 'active',
//       amount: 2,
//       startFrom: '2018-03-05T03:00:00.000Z',
//       pausedUntil: null,
//       cancelReason: null,
//       nextDiscount: null,
//       typeNextDiscount: null,
//       createdAt: '2018-03-02T15:57:49.000Z',
//       updatedAt: '2018-03-02T15:57:49.000Z',
//       couponUseCount: 0,
//       cancellationDate: null,
//       user:
//       { id: 1,
//         customerId: 'cus_CQ6TpX71AFk4lI',
//         credit: 2500,
//         name: 'A',
//         lastName: 'A',
//         email: 'qa+1@aerolab.co',
//         status: 'active',
//         zipCode: '90001',
//         address: 'A',
//         addressExtra: '',
//         city: 'Los Angeles',
//         state: 'CA',
//         phoneNumber: '(111) 111-1111',
//         hash: '09da9f02770bea017526334c3e2500164370522c',
//         firstCharge: 'ch_1C1GPxFNHnBZW9i2jsa9wrbr',
//         billingName: 'A',
//         billingLastName: 'A',
//         billingAddress: 'A',
//         billingAddressExtra: '',
//         billingCity: 'Los Angeles',
//         billingState: 'CA',
//         billingZipCode: '90001',
//         hasMealMaker: null,
//         deliveredMealMaker: false,
//         hubspotId: null,
//         beaba: true,
//         beabaFlow: false,
//         company: '',
//         firstChargeAmount: 9500 },
//       product:
//       { id: 3,
//         description: 'Regular Box every 14 days',
//         boxType: 'Regular',
//         price: 9500,
//         days: 14,
//         hidden: false },
//       coupon:
//       { id: 341,
//         productId: null,
//         code: 'doscajas',
//         percentOff: 50,
//         amountOff: null,
//         maxRedemptions: 4,
//         timesRedeemed: 3,
//         duration: 'once',
//         valid: true,
//         timesUsable: -1 },
//       skips: [],
//       orderCount: [],
//       orders: [],
//     }, {
//       include: [
//         { model: db.User, as: 'user' },
//         { model: db.Product, as: 'product' },
//         { model: db.Coupon, as: 'coupon' },
//         { model: db.Skip, as: 'skips' },
//         { model: db.Order, as: 'orders' },
//         { model: db.Order, as: 'orderCount' },
//       ],
//     });
//     const result = calculateDiscounts(subscription, cronDate);
//     expect(result.data.couponId).to.equal(null);
//     expect(result.data.user.credit).to.equal(2500);
//     expect(result.data.user.firstChargeAmount).to.equal(null);
//     expect(result.data.pricing.fullPrice).to.equal(19000);
//     expect(result.data.pricing.totalPrice).to.equal(9500);
//     expect(result.data.pricing.discountAmount).to.equal(9500);
//     expect(result.data.pricing.discountPercent).to.equal(50);
//     expect(result.appliedDiscounts.length).to.equal(1);
//     expect(result.appliedDiscounts).to.be.an('array');
//     expect(result.appliedDiscounts).contain.an.item.with.property('type', 'subscriptionCoupon');
//   });
// });
