import moment from 'moment';

import db, { sequelize } from '../db/connection';
import config from '../config/config';

const chai = require('chai');
// we are using the 'expect' style of Chai
const expect = chai.expect;
let request = require('supertest');


const stripe = require('stripe')(
  config.stripeApiKey,
);


const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;

// let config = require('../config/config')

// This gives context to istanbul coverage
// let app = require('../index.js')

const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

const userCCOne = {
  email: 'sergio+cc1@aerolab.co',
  name: 'Ser',
  lastName: 'B',
  address: 'A',
  addressExtra: '',
  city: 'BA',
  state: 'BA',
  zipCode: '94122', // This is a valid SF zip code
  phoneNumber: '(123) 123-1232',
  billingName: 'Ser',
  billingLastName: 'B',
  billingAddress: 'A',
  billingAddressExtra: '',
  billingCity: 'BA',
  billingState: 'BA',
  billingZipCode: '94122', // This is a valid SF zip code
  cardNumber: '4242 4242 4242 4242',
  cvc: '123',
  expiration: '',
  exp_month: '11',
  exp_year: '22',
  hash: '',
  validCC: false,
  stripeToken: null,
  stripeError: null,
  hasMealMaker: true,
  sameAddress: true,
  child: [
    {
      name: 'Tres Enero',
      birthDate: '2017-01-03T15:00:00.000Z',
      monthsOld: 4,
      formatedBirthDate: '2017-01-03',
      foodType: 'Milk/formula only',
      singleIngredient: true,
      show: true,
    },
    {
      name: 'Abril',
      birthDate: '2017-04-25T15:00:00.000Z',
      monthsOld: 1,
      formatedBirthDate: '2017-04-25',
      foodType: 'Purées',
      singleIngredient: false,
      show: true,
    },
  ],
  subscription: {
    product: [
      {
        productId: 2,
        name: 'Starter Box',
        description: '20 meals/box',
        price: 95,
        quantity: 1,
      },
    ],
    startFrom: '05-29-2017',
  },
  token: 'tok_1APazBFNHnBZW9i2NmYb37qT',
};

const userCCTwo = { ...userCCOne, email: 'sergio+cc2@aerolab.co' };
const userCCThree = { ...userCCOne, email: 'sergio+cc3@aerolab.co' };
const userCCFour = { ...userCCOne, email: 'sergio+cc4@aerolab.co' };

describe('Start Server', () => {
  it('Wait for DB', (done) => {
    setTimeout(() => {
      done();
    }, 500);
  });

  it('Force DB', (done) => {
    sequelize.sync({ force: true })
    .then(() => {
      setTimeout(() => {
        done();
      }, 1000);
    })
    .catch((err) => {
      console.log(err);
      setTimeout(() => {
        done();
      }, 1000);
    });
  });


  /*eslint-disable */
  it('Add default products', (done) => {
    sequelize.query(`
      INSERT INTO product (id, box_type, price, days, description)
      VALUES
        (1, 'Meal Maker', 0, 0, 'A Meal Maker only once'),
        (2, 'Starter', 9500, 14, 'A Starter box every 14 days'),
        (3, 'Regular', 9500, 14, 'A Regular box every 14 days'),
        (4, 'Regular', 9000, 14, 'A Regular box with discount'),
        (5, 'Regular', 9500, 28, 'A Regular box every 28 days'),
	      (6, 'Starter', 9500, 28, 'A Starte box every 28 days') 
    `).then(() => {
      done();
    });
  });
});
/*eslint-enable */

describe('CC Case 1 - Stripe OK', () => {
  it('Create Card Token', (done) => {
    const expiryDate = moment.utc().add(2, 'months');
    userCCOne.cvc = 123;
    userCCOne.cardNumber = '4242424242424242';
    userCCOne.expiration = expiryDate.format('MM/YY');
    userCCOne.exp_month = expiryDate.format('MM');
    userCCOne.exp_year = expiryDate.format('YY');

    stripe.tokens.create({
      card: {
        number: userCCOne.cardNumber,
        exp_month: userCCOne.exp_month,
        exp_year: userCCOne.exp_year,
        cvc: userCCOne.cvc,
      },
    }, (err, token) => {
      expect(err).to.be.an('null');
      userCCOne.token = token.id;
      done();
    });
  });

  it('Send New Card for User', (done) => {
    request
      .post('/api/users/new')
      .send({
        ...userCCOne,
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        expect(err).to.be.an('null');
        expect(res.body).to.be.object;
        expect(res.body).to.have.property('status', 'ok');
        done();
      });
  });

  it('Wait change', (done) => {
    setTimeout(() => {
      done();
    }, 2000);
  });

  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: userCCOne.email,
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.customerId).to.be.a('string');
      expect(user.firstCharge).to.be.a('string');
      userCCOne.hash = user.hash;
      done();
    });
  });

  it('Check Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'active');
      done();
    });
  });

  it('Perform credit-card change', (done) => {
    const expiryDate = moment.utc().add(2, 'months');
    const cvc = 342;
    const cardNumber = '4242424242424242';
    const exp_month = expiryDate.format('MM');
    const exp_year = expiryDate.format('YY');

    stripe.tokens.create({
      card: {
        number: cardNumber,
        exp_month,
        exp_year,
        cvc,
      },
    }, (err, token) => {
      expect(err).to.be.an('null');

      request
        .put(`/api/users/hash/${userCCOne.hash}`)
        .send({
          ...userCCOne,
          token: token.id,
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          expect(err).to.be.an('null');
          expect(res).to.have.property('status');
          done();
        });
    });
  });
});


describe('CC Case 2 - card_declined', () => {
  it('Create Card Token', (done) => {
    const expiryDate = moment.utc().add(2, 'months');
    userCCTwo.cvc = 123;
    userCCTwo.cardNumber = '4000000000000002';
    userCCTwo.expiration = expiryDate.format('MM/YY');
    userCCTwo.exp_month = expiryDate.format('MM');
    userCCTwo.exp_year = expiryDate.format('YY');

    stripe.tokens.create({
      card: {
        number: userCCTwo.cardNumber,
        exp_month: userCCTwo.exp_month,
        exp_year: userCCTwo.exp_year,
        cvc: userCCTwo.cvc,
      },
    }, (err, token) => {
      expect(err).to.be.an('null');
      userCCTwo.token = token.id;
      done();
    });
  });

  it('Send New Card for User', (done) => {
    request
      .post('/api/users/new')
      .send({
        ...userCCTwo,
      })
      .expect(402)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        expect(res.body).to.have.property('stripe_error');
        const stripeError = res.body.stripe_error;

        expect(stripeError).to.be.an('object');
        expect(stripeError).to.have.property('type', 'card_error');
        expect(stripeError).to.have.property('code', 'card_declined');
        done();
      });
  });

  it('Wait change', (done) => {
    setTimeout(() => {
      done();
    }, 1000);
  });

  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: userCCTwo.email,
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.customerId).to.be.an('string');
      expect(user.firstCharge).to.be.a('null');
      done();
    });
  });

  it('Check Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'pending');
      done();
    });
  });
});

describe('CC Case 3 - Charge fail', () => {
  it('Create Card Token', (done) => {
    const expiryDate = moment.utc().add(2, 'months');
    userCCThree.cvc = 123;
    userCCThree.cardNumber = '4000000000000341';
    userCCThree.expiration = expiryDate.format('MM/YY');
    userCCThree.exp_month = expiryDate.format('MM');
    userCCThree.exp_year = expiryDate.format('YY');

    stripe.tokens.create({
      card: {
        number: userCCThree.cardNumber,
        exp_month: userCCThree.exp_month,
        exp_year: userCCThree.exp_year,
        cvc: userCCThree.cvc,
      },
    }, (err, token) => {
      expect(err).to.be.an('null');
      userCCThree.token = token.id;
      done();
    });
  });

  it('Send New Card for User', (done) => {
    request
      .post('/api/users/new')
      .send({
        ...userCCThree,
      })
      .expect(402)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        expect(res.body).to.have.property('stripe_error');
        const stripeError = res.body.stripe_error;

        expect(stripeError).to.be.an('object');
        expect(stripeError).to.have.property('type', 'card_error');
        expect(stripeError).to.have.property('code', 'card_declined');

        done();
      });
  });

  it('Wait change', (done) => {
    setTimeout(() => {
      done();
    }, 1000);
  });

  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: userCCThree.email,
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.customerId).to.be.an('string');
      expect(user.firstCharge).to.be.a('null');
      done();
    });
  });

  it('Check Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'pending');
      done();
    });
  });
});

describe('CC Case 4 - Charge fail', () => {
  it('Create Card Token', (done) => {
    const expiryDate = moment.utc().add(2, 'months');
    userCCFour.cvc = 123;
    userCCFour.cardNumber = '4100000000000019';
    userCCFour.expiration = expiryDate.format('MM/YY');
    userCCFour.exp_month = expiryDate.format('MM');
    userCCFour.exp_year = expiryDate.format('YY');

    stripe.tokens.create({
      card: {
        number: userCCFour.cardNumber,
        exp_month: userCCFour.exp_month,
        exp_year: userCCFour.exp_year,
        cvc: userCCFour.cvc,
      },
    }, (err, token) => {
      expect(err).to.be.an('null');
      userCCFour.token = token.id;
      done();
    });
  });

  it('Send New Card for User', (done) => {
    request
      .post('/api/users/new')
      .send({
        ...userCCFour,
      })
      .expect(402)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        expect(res.body).to.have.property('stripe_error');
        const stripeError = res.body.stripe_error;

        expect(stripeError).to.be.an('object');
        expect(stripeError).to.have.property('type', 'card_error');
        expect(stripeError).to.have.property('code', 'card_declined');
        expect(stripeError).to.have.property('decline_code', 'fraudulent');
        done();
      });
  });

  it('Wait change', (done) => {
    setTimeout(() => {
      done();
    }, 1000);
  });

  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: userCCFour.email,
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.customerId).to.be.an('string');
      expect(user.firstCharge).to.be.a('null');
      done();
    });
  });

  it('Check Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'pending');
      done();
    });
  });
});
