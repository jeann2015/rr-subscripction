import db, { sequelize } from '../db/connection';
import { sendConfirmationEmail } from '../entities/User/plugin';
import config from '../config/config';

const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;
const request = require('supertest');

const stripe = require('stripe')(
    config.stripeApiKey,
);

const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;

describe('Send confirmation email', () => {
  it('Real User', async () => {
    try {
      const confirmationEmail = await sendConfirmationEmail(1716);
      console.log(confirmationEmail);
    } catch (err) {
      console.log(err);
    }
  });
});
