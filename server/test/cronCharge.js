import moment from 'moment';
import chai from 'chai';

import { getCronDate } from '../services/cron_new';
import { calculateDiscounts } from '../services/cron_charge';
import config from '../config/config';
import db from '../db/connection';

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;

describe('calculateDiscounts', () => {
  it('User A - First Cron - Two Boxes, 50% Once, 1 Referral', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: 341,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T15:57:49.000Z',
      updatedAt: '2018-03-02T15:57:49.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 1,
        customerId: 'cus_CQ6TpX71AFk4lI',
        credit: 2500,
        name: 'A',
        lastName: 'A',
        email: 'qa+1@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'A',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(111) 111-1111',
        hash: '09da9f02770bea017526334c3e2500164370522c',
        firstCharge: 'ch_1C1GPxFNHnBZW9i2jsa9wrbr',
        billingName: 'A',
        billingLastName: 'A',
        billingAddress: 'A',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: 9500 },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon:
      { id: 341,
        productId: null,
        code: 'doscajas',
        percentOff: 50,
        amountOff: null,
        maxRedemptions: 4,
        timesRedeemed: 3,
        duration: 'once',
        valid: true,
        timesUsable: -1 },
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.couponId).to.equal(null);
    expect(result.data.user.credit).to.equal(2500);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(9500);
    expect(result.data.pricing.discountAmount).to.equal(9500);
    expect(result.data.pricing.discountPercent).to.equal(50);
    expect(result.appliedDiscounts.length).to.equal(1);
    expect(result.appliedDiscounts).to.be.an('array');
    expect(result.appliedDiscounts).contain.an.item.with.property('type', 'subscriptionCoupon');
  });

  it('User A - Second Cron - Two Boxes, 50% Once, 1 Referral', async () => {
    const cronDate = await getCronDate(1);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T15:57:49.000Z',
      updatedAt: '2018-03-02T15:57:49.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 1,
        customerId: 'cus_CQ6TpX71AFk4lI',
        credit: 2500,
        name: 'A',
        lastName: 'A',
        email: 'qa+1@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'A',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(111) 111-1111',
        hash: '09da9f02770bea017526334c3e2500164370522c',
        firstCharge: null,
        billingName: 'A',
        billingLastName: 'A',
        billingAddress: 'A',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    // console.log(result)
    // console.log(result.data.pricing.appliedDiscounts)
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(16500);
    expect(result.data.pricing.discountAmount).to.equal(2500);
    expect(result.data.pricing.discountPercent).to.equal(13.2);
    expect(result.appliedDiscounts.length).to.equal(1);
    expect(result.appliedDiscounts).to.be.an('array');
    expect(result.appliedDiscounts).contain.an.item.with.property('type', 'userCredit');
  });

  it('User A - Third Cron - Two Boxes, 50% Once, 1 Referral', async () => {
    const cronDate = await getCronDate(2);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T15:57:49.000Z',
      updatedAt: '2018-03-02T15:57:49.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 1,
        customerId: 'cus_CQ6TpX71AFk4lI',
        credit: 0,
        name: 'A',
        lastName: 'A',
        email: 'qa+1@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'A',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(111) 111-1111',
        hash: '09da9f02770bea017526334c3e2500164370522c',
        firstCharge: null,
        billingName: 'A',
        billingLastName: 'A',
        billingAddress: 'A',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(19000);
    expect(result.data.pricing.discountAmount).to.equal(0);
    expect(result.data.pricing.discountPercent).to.equal(0);
    expect(result.appliedDiscounts.length).to.equal(0);
  });

  it('User B - First Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:03:55.000Z',
      updatedAt: '2018-03-02T16:04:03.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 2,
        customerId: 'cus_CQ6ZqtuoB8IVO1',
        credit: 7500,
        name: 'B',
        lastName: 'B',
        email: 'qa+b@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'B',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(222) 222-2222',
        hash: '4e1fae3b209dafe357a40771d2b50a40404c2912',
        firstCharge: 'ch_1C1GMYFNHnBZW9i29LeqvnZL',
        billingName: 'B',
        billingLastName: 'B',
        billingAddress: 'B',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: 7000 },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    // console.log(result)
    expect(result.data.couponId).to.equal(null);
    expect(result.data.user.credit).to.equal(7500);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(7000);
    expect(result.data.pricing.discountAmount).to.equal(0); // 0. Because is not taking in account fcAmount
    expect(result.data.pricing.discountPercent).to.equal(0);
    // expect(result.appliedDiscounts.length).to.equal(1);
    // expect(result.appliedDiscounts).to.be.an('array');
    // expect(result.appliedDiscounts).contain.an.item.with.property('type', 'subscriptionCoupon');
  });

  it('User B - Second Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(1);
    const subscription = db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:03:55.000Z',
      updatedAt: '2018-03-02T16:04:03.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 2,
        customerId: 'cus_CQ6ZqtuoB8IVO1',
        credit: 7500,
        name: 'B',
        lastName: 'B',
        email: 'qa+b@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'B',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(222) 222-2222',
        hash: '4e1fae3b209dafe357a40771d2b50a40404c2912',
        firstCharge: 'ch_1C1GMYFNHnBZW9i29LeqvnZL',
        billingName: 'B',
        billingLastName: 'B',
        billingAddress: 'B',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    // console.log(result)
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(2000);
    expect(result.data.pricing.discountAmount).to.equal(7500); // 0. Because is not taking in account fcAmount
    expect(result.appliedDiscounts.length).to.equal(1);
    expect(result.appliedDiscounts).to.be.an('array');
    expect(result.appliedDiscounts).contain.an.item.with.property('type', 'userCredit');
  });

  it('User B - Third Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(2);
    const subscription = db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:03:55.000Z',
      updatedAt: '2018-03-02T16:04:03.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 2,
        customerId: 'cus_CQ6ZqtuoB8IVO1',
        credit: 0,
        name: 'B',
        lastName: 'B',
        email: 'qa+b@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'B',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(222) 222-2222',
        hash: '4e1fae3b209dafe357a40771d2b50a40404c2912',
        firstCharge: 'ch_1C1GMYFNHnBZW9i29LeqvnZL',
        billingName: 'B',
        billingLastName: 'B',
        billingAddress: 'B',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    // console.log(result)
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(9500);
    expect(result.data.pricing.discountAmount).to.equal(0); // 0. Because is not taking in account fcAmount
    expect(result.appliedDiscounts.length).to.equal(0);
  });

  it('User C - First Cron - One Box, Refered', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 3,
      userId: 3,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:04:47.000Z',
      updatedAt: '2018-03-02T16:04:50.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 3,
        customerId: 'cus_CQ6aFX4rKPPTF8',
        credit: 0,
        name: 'C',
        lastName: 'C',
        email: 'qa+3@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'C',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(333) 333-3333',
        hash: '0ddde0f3322f48e2a26c7db18e42b8e5d143b24c',
        firstCharge: 'ch_1C1GNJFNHnBZW9i285diJ4lu',
        billingName: 'C',
        billingLastName: 'C',
        billingAddress: 'C',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: 7000 },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.couponId).to.equal(null);
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(7000);
    expect(result.data.pricing.discountAmount).to.equal(0); // 0. Because is not taking in account fcAmount
    expect(result.data.pricing.discountPercent).to.equal(0);
    expect(result.appliedDiscounts.length).to.equal(0);
  });

  it('User C - Second Cron - One Box, Refered', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 3,
      userId: 3,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:04:47.000Z',
      updatedAt: '2018-03-02T16:04:50.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 3,
        customerId: 'cus_CQ6aFX4rKPPTF8',
        credit: 0,
        name: 'C',
        lastName: 'C',
        email: 'qa+3@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'C',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(333) 333-3333',
        hash: '0ddde0f3322f48e2a26c7db18e42b8e5d143b24c',
        firstCharge: 'ch_1C1GNJFNHnBZW9i285diJ4lu',
        billingName: 'C',
        billingLastName: 'C',
        billingAddress: 'C',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.couponId).to.equal(null);
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(9500);
    expect(result.data.pricing.discountAmount).to.equal(0); // 0. Because is not taking in account fcAmount
    expect(result.data.pricing.discountPercent).to.equal(0);
    expect(result.appliedDiscounts.length).to.equal(0);
  });
});
