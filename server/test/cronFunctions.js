import moment from 'moment';
import chai from 'chai';

import { checkSubscriptionStatus, checkSubscriptionStartFrom, checkSubscriptionSkipDate, checkSubscriptionLastOrder, checkSubscriptionCycle, shouldAddMealMaker, getCronDate, shouldShipOrder, calculateNextShipment, simulateSubscription } from '../services/cron_new';
import { simulateCron, simulateCronAndGenerateCSV } from '../services/cron_simulator';
import { calculateDiscounts, applyDiscounts } from '../services/cron_charge';
import { runOrders } from '../services/cron_runner';
import config from '../config/config';
import db from '../db/connection';

const stripe = require('stripe')(
  config.stripeApiKey,
);

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;

// TODO: Separate runOrders test functions to index.js + runCron.js for each cron.
/* describe('runOrders', async () => {
  it('Empty simulation', async () => {
    const cronOrders = await runOrders();
    expect(cronOrders).to.be.an('array');
    expect(cronOrders.length).to.be.eq(0);
  });
});
 */

/* describe('simulateCron', async () => {
  it('Empty simulation', async () => {
    const subscriptions = [];
    const cronDate = await getCronDate(1);

    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).to.be.an('array');
    expect(simulation.length).to.be.eq(0);
  });

  it('One customer, 1 box, 1 MM', async () => {
    const subscriptions = [];
    const cronDate = await getCronDate(1);
    subscriptions.push(db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));

    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).to.be.an('array');
    expect(simulation.length).to.be.eq(2);
    expect(simulation).that.contains.something.like({ boxType: 'Meal Maker' });
    expect(simulation).that.contains.something.like({ boxType: 'Regular' });
  }); */

  /* it('Two customers, 3 boxes, 1 MM', async () => {
    const subscriptions = [];
    const cronDate = await getCronDate(1);
    subscriptions.push(db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));

    subscriptions.push(db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: await getCronDate(0),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 2,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: true,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));

    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).to.be.an('array');
    expect(simulation.length).to.be.eq(4);
    expect(simulation).that.contains.something.like({ boxType: 'Meal Maker' });
    expect(simulation).that.contains.something.like({ boxType: 'Regular' });
  });
}); */

/* describe('simulateSubscription', async () => {
  it('Should not have Meal Maker', async () => {
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 500,
      typeNextDiscount: 'amount',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 2500,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: 10,
        amountOff: null,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: null,
        valid: true,
        timesUsable: -1,
      },
      orders: [{
        trackId: '',
        tracked: false,
        id: null,
        userId: 1,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        boxType: 'Meal Maker',
        productPrice: 0,
        subscriptionId: null,
      }],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    });

    const result = await simulateSubscription(subscription, await getCronDate(0));
    const plainOrders = result.data.newOrders.map(o => o.get({ plain: true }));
    expect(plainOrders).to.be.an('array').that.not.contains.something.like({ boxType: 'Meal Maker' });

  });

  it('Should have Meal Maker', async () => {
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 500,
      typeNextDiscount: 'amount',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 2500,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: true,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: 10,
        amountOff: null,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: null,
        valid: true,
        timesUsable: -1,
      },
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    });

    const result = await simulateSubscription(subscription, await getCronDate(0));
    const plainOrders = result.data.newOrders.map(o => o.get({ plain: true }));
    expect(plainOrders).to.be.an('array').that.contains.something.like({ boxType: 'Meal Maker' });
  });
}); */
/*
describe('moveCronDates', () => {
  it('Next Cron', async () => {
    const result = await getCronDate(0);
    expect(moment.utc().isSameOrBefore(result)).to.equal(true);
    expect(moment.utc().isAfter(result)).to.equal(false);
  });

  it('Past Cron', async () => {
    const result = await getCronDate(-1);
    expect(moment.utc().isSameOrBefore(result)).to.equal(false);
    expect(moment.utc().isAfter(result)).to.equal(true);
  });
});

describe('shouldShipOrder', () => {
  it('All checks passed', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    // const shouldShipOn = await getCronDate(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      orders: [],
      skips: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await shouldShipOrder(subscription, cronDate);
    // console.log('a', result);
    // console.log(shouldShipOn);
    // expect(result.fullPrice).to.equal(9500);
    // expect(result.totalPrice).to.equal(9500);
    // expect(result.discountAmount).to.equal(0);
    // expect(result.discountPercent).to.equal(0);
  });
});

describe('calculateNextShipment', () => {
  it('Next Cron', async () => {
    const subscriptionDate = await getCronDate(-1);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: subscriptionDate.toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: subscriptionDate.toISOString(),
      updatedAt: subscriptionDate.toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      orders: [],
      skips: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await calculateNextShipment(subscription);
    const shouldShipOn = await getCronDate(0);
    expect(shouldShipOn.isSame(result, 'day')).to.equal(true);
  });

  it('28 days, half cron', async () => {
    const cronDate = await getCronDate(-1);
    const shouldShipOn = await getCronDate(1);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: cronDate.toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: cronDate.toISOString(),
      updatedAt: cronDate.toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 28 days',
          boxType: 'Regular',
          price: 9500,
          days: 28,
          hidden: false,
        },
      ],
      orders: [],
      skips: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await calculateNextShipment(subscription);
    expect(shouldShipOn.isSame(result, 'day')).to.equal(true);
  });

  it('14 days & skip first cron', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: cronDate.toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: cronDate.toISOString(),
      updatedAt: cronDate.toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      orders: [],
      skips: [
        {
          id: 1,
          subscription: 1,
          date: cronDate.toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await calculateNextShipment(subscription);
    const shouldShipOn = await getCronDate(1);
    expect(shouldShipOn.isSame(result, 'day')).to.equal(true);
  });

  it('14 days & skip two next cron', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: cronDate.toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: cronDate.toISOString(),
      updatedAt: cronDate.toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      orders: [],
      skips: [
        {
          id: 1,
          subscription: 1,
          date: cronDate.toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
        {
          id: 1,
          subscription: 1,
          date: cronDate.add(14, 'day').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await calculateNextShipment(subscription);
    const shouldShipOn = await getCronDate(2);
    expect(shouldShipOn.isSame(result, 'day')).to.equal(true);
  });

  it('14 days & paused until second cron', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'paused',
      amount: 1,
      startFrom: cronDate.toISOString(),
      pausedUntil: cronDate.add(13, 'days').toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: cronDate.toISOString(),
      updatedAt: cronDate.toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'paused',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      orders: [],
      skips: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Order, as: 'orders' },
        { model: db.Skip, as: 'skips' },
      ],
    });
    const result = await calculateNextShipment(subscription);
    const shouldShipOn = await getCronDate(1);
    expect(shouldShipOn.isSame(result, 'day')).to.equal(true);
  });
});

describe('checkSubscriptionStatus', () => {
  it('Active, should return true', async () => {
    const cronDate = moment.utc('2017-09-11').hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Canceled, should return false', async () => {
    const cronDate = moment.utc('2017-09-11').hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'canceled',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: '2017-10-02T17:00:25.000Z',
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Pending, should return false', async () => {
    const cronDate = moment.utc('2017-09-11').hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'pending',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: null,
      cancelReason: 'Transitioning to adult food',
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Paused on the past, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      // id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'paused',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: moment.utc().subtract(8, 'day').toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Paused until yesterday, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      // id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'paused',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: moment.utc().subtract(1, 'day').toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Paused until today, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const pauseUntil = moment.utc().toISOString();

    const subscription = db.Subscription.build({
      // id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'paused',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: moment.utc().toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Paused until tomorrow, should return false', async () => {
    const cronDate = moment.utc('2017-09-11').hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'paused',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: moment.utc().add(1, 'day').toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('None state, should return false', async () => {
    const cronDate = moment.utc('2017-09-11').hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'none',
      amount: 1,
      startFrom: '2017-04-05T00:00:00.000Z',
      pausedUntil: moment.utc().add(1, 'day').toISOString(),
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2017-06-02T20:53:19.000Z',
      updatedAt: '2017-10-11T19:46:58.000Z',
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = await checkSubscriptionStatus(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });
});

describe('checkSubscriptionStartFrom', () => {
  it('Today, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = checkSubscriptionStartFrom(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Yesterday, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(1, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(1, 'day').toISOString(),
      updatedAt: moment.utc().subtract(1, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = checkSubscriptionStartFrom(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('Tomorrow, should return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().add(1, 'days').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = checkSubscriptionStartFrom(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Next week, should return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().add(7, 'days').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
    });
    const result = checkSubscriptionStartFrom(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });
});

describe('checkSubscriptionSkipDate', () => {
  it('Skip Today, should return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [
        {
          id: 22,
          subscription: 1,
          date: moment.utc().toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Skip, as: 'skips' }],
    });
    const result = checkSubscriptionSkipDate(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Skip Today in multiple dates, should return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [
        {
          id: 1,
          subscription: 1,
          date: moment.utc().add(5, 'day').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
        {
          id: 2,
          subscription: 1,
          date: moment.utc().subtract(1, 'day').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
        {
          id: 3,
          subscription: 1,
          date: moment.utc().toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Skip, as: 'skips' }],
    });
    const result = checkSubscriptionSkipDate(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Skip Two Crons, should return false', async () => {
    const cronDate1 = moment.utc().hour(0).minute(0);
    const cronDate2 = moment.utc().add(14, 'days').hour(0).minute(0);

    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [
        {
          id: 1,
          subscription: 1,
          date: moment.utc().toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
        {
          id: 2,
          subscription: 1,
          date: moment.utc().add(14, 'days').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Skip, as: 'skips' }],
    });

    const result_1 = checkSubscriptionSkipDate(subscription, cronDate1);
    const result_2 = checkSubscriptionSkipDate(subscription, cronDate2);

    expect(result_1.continue).to.equal(false);
    expect(result_2.continue).to.equal(false);
  });

  it('Dont skip multiple dates, should return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [
        {
          id: 1,
          subscription: 1,
          date: moment.utc().add(14, 'day').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
        {
          id: 2,
          subscription: 1,
          date: moment.utc().subtract(14, 'day').toISOString(),
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Skip, as: 'skips' }],
    });
    const result = checkSubscriptionSkipDate(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });
});

describe('checkSubscriptionLastOrder', () => {
  it('Last Order Today', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      orders: [
        {
          id: 49,
          userId: 1,
          name: 'John',
          lastName: 'Kite',
          email: 'test+1@aerolab.co',
          zipCode: '90001',
          address: 'Somewhere',
          addressExtra: '',
          city: 'San Diego',
          state: 'CA',
          boxType: 'Regular',
          productPrice: 9500,
          subscriptionId: 1,
          chargeToken: 'ch_XXX',
          trackId: null,
          tracked: false,
          cronId: 1,
          createdAt: moment.utc().toISOString(),
          updatedAt: moment.utc().toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Order, as: 'orders' }],
    });
    const result = checkSubscriptionLastOrder(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Last Order Yesterday', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(1, 'day').toISOString(),
      updatedAt: moment.utc().subtract(1, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      orders: [
        {
          id: 49,
          userId: 1,
          name: 'John',
          lastName: 'Kite',
          email: 'test+1@aerolab.co',
          zipCode: '90001',
          address: 'Somewhere',
          addressExtra: '',
          city: 'San Diego',
          state: 'CA',
          boxType: 'Regular',
          productPrice: 9500,
          subscriptionId: 1,
          chargeToken: 'ch_XXX',
          trackId: null,
          tracked: false,
          cronId: 1,
          createdAt: moment.utc().subtract(1, 'day').toISOString(),
          updatedAt: moment.utc().subtract(1, 'day').toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Order, as: 'orders' }],
    });
    const result = checkSubscriptionLastOrder(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('Last Order Last Week', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(7, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(7, 'day').toISOString(),
      updatedAt: moment.utc().subtract(7, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      orders: [
        {
          id: 49,
          userId: 1,
          name: 'John',
          lastName: 'Kite',
          email: 'test+1@aerolab.co',
          zipCode: '90001',
          address: 'Somewhere',
          addressExtra: '',
          city: 'San Diego',
          state: 'CA',
          boxType: 'Regular',
          productPrice: 9500,
          subscriptionId: 1,
          chargeToken: 'ch_XXX',
          trackId: null,
          tracked: false,
          cronId: 1,
          createdAt: moment.utc().subtract(7, 'day').toISOString(),
          updatedAt: moment.utc().subtract(7, 'day').toISOString(),
        },
      ],
    }, {
      include: [{ model: db.Order, as: 'orders' }],
    });
    const result = checkSubscriptionLastOrder(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });
});

describe('checkSubscriptionCycle', () => {
  // TODO: Test cycles to start on Sun, Mon, Tue, Wed
  it('14 Days Cycle, cron today, return true', async () => {
    const cronDate = getCronDate(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('14 Days Cycle, +1 cron, return true', async () => {
    const cronDate = getCronDate(1);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-2),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: getCronDate(-2),
      updatedAt: getCronDate(-2),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });

  it('28 Days Cycle, -1 cron , return false', async () => {
    const cronDate = getCronDate(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 5,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: getCronDate(-1),
      updatedAt: getCronDate(-1),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 5,
          description: 'Regular Box every 28 days',
          boxType: 'Regular',
          price: 9500,
          days: 28,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('14 Days Cycle, cron tomorrow, return false', async () => {
    const cronDate = getCronDate(1).add(1, 'day');
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-2),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: getCronDate(-2),
      updatedAt: getCronDate(-2),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('14 Days Cycle, cron yesterday, return false', async () => {
    const cronDate = getCronDate(1).subtract(1, 'day');
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-2),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: getCronDate(-2),
      updatedAt: getCronDate(-2),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(false);
  });

  it('28 Days Cycle, cron today, return true', async () => {
    const cronDate = getCronDate(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 5,
      status: 'active',
      amount: 1,
      startFrom: getCronDate(-4),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: getCronDate(-4),
      updatedAt: getCronDate(-4),
      couponUseCount: 0,
      cancellationDate: null,
      product: [
        {
          id: 5,
          description: 'Regular Box every 28 days',
          boxType: 'Regular',
          price: 9500,
          days: 28,
          hidden: false,
        },
      ],
    }, {
      include: [{ model: db.Product, as: 'product' }],
    });
    const result = checkSubscriptionCycle(subscription, cronDate);
    expect(result.continue).to.equal(true);
  });
});

describe('shouldAddMealMaker', () => {
  it('Wants Machine, does not have it, return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(14, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(14, 'day').toISOString(),
      updatedAt: moment.utc().subtract(14, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
    }, {
      include: [{ model: db.User, as: 'user' }],
    });
    const result = shouldAddMealMaker(subscription, cronDate);
    expect(result.createMachineOrder).to.equal(true);
  });

  it('Received Machine, return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(14, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(14, 'day').toISOString(),
      updatedAt: moment.utc().subtract(14, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: true,
      },
    }, {
      include: [{ model: db.User, as: 'user' }],
    });
    const result = shouldAddMealMaker(subscription, cronDate);
    expect(result.createMachineOrder).to.equal(false);
  });

  it('Does not want Machine, return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(14, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(14, 'day').toISOString(),
      updatedAt: moment.utc().subtract(14, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: false,
        deliveredMealMaker: false,
      },
    }, {
      include: [{ model: db.User, as: 'user' }],
    });
    const result = shouldAddMealMaker(subscription, cronDate);
    expect(result.createMachineOrder).to.equal(false);
  });

  it('Simulate Future Cron without Machine, first cron, return true', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = shouldAddMealMaker(subscription, cronDate, true);
    expect(result.createMachineOrder).to.equal(true);
  });

  it('Simulate Future Cron without Machine, second cron, return false', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().subtract(14, 'day').toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().subtract(14, 'day').toISOString(),
      updatedAt: moment.utc().subtract(14, 'day').toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = shouldAddMealMaker(subscription, cronDate, true);
    expect(result.createMachineOrder).to.equal(false);
  });
});

describe('calculateDiscounts', () => {
  it('Full Price', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(9500);
    expect(result.data.pricing.discountAmount).to.equal(0);
    expect(result.data.pricing.discountPercent).to.equal(0);
    expect(result.appliedDiscounts.length).to.equal(0);
    expect(result.appliedDiscounts).to.be.an('array');
  });

  it('Next Discount 10%', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 10,
      typeNextDiscount: 'percent',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    const resultPlain = result.data.get({ plain: true });
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8550);
    expect(result.data.pricing.discountAmount).to.equal(950);
    expect(result.data.pricing.discountPercent).to.equal(10);
    expect(resultPlain.nextDiscount).to.equal(null);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionNextDiscount' });
  });

  it('Amount x2 Next Discount 10%', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 10,
      typeNextDiscount: 'percent',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    const resultPlain = result.data.get({ plain: true });
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(17100);
    expect(result.data.pricing.discountAmount).to.equal(1900);
    expect(result.data.pricing.discountPercent).to.equal(10);
    expect(resultPlain.nextDiscount).to.equal(null);
    expect(resultPlain.typeNextDiscount).to.equal(null);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionNextDiscount' });
  });

  it('Next Discount 10USD', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 1000,
      typeNextDiscount: 'amount',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    const resultPlain = result.data.get({ plain: true });
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8500);
    expect(result.data.pricing.discountAmount).to.equal(1000);
    expect(result.data.pricing.discountPercent).to.equal(10.5);
    expect(resultPlain.nextDiscount).to.equal(null);
    expect(resultPlain.typeNextDiscount).to.equal(null);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionNextDiscount' });
  });

  it('Coupon 10%', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: 10,
        amountOff: null,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: 'forever',
        valid: true,
        timesUsable: -1,
      },
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    const resultPlain = result.data.get({ plain: true });
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8550);
    expect(result.data.pricing.discountAmount).to.equal(950);
    expect(result.data.pricing.discountPercent).to.equal(10);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionCoupon' });
  });

  it('Coupon 10USD', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: null,
        amountOff: 1000,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: null,
        valid: true,
        timesUsable: -1,
      },
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8500);
    expect(result.data.pricing.discountAmount).to.equal(1000);
    expect(result.data.pricing.discountPercent).to.equal(10.5);
    const resultPlain = result.data.get({ plain: true });
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionCoupon', amount: 1000 });
  });

  it('Coupon 10USD with couponUseCount 1 and timesUsable 3, apply discount', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 1,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: null,
        amountOff: 1000,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: 'custom',
        valid: true,
        timesUsable: 3,
      },
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8500);
    expect(result.data.pricing.discountAmount).to.equal(1000);
    expect(result.data.pricing.discountPercent).to.equal(10.5);
    const resultPlain = result.data.get({ plain: true });
    expect(resultPlain.couponUseCount).to.equal(2);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionCoupon', amount: 1000 });
  });

  it('Coupon 10USD with couponUseCount 2 and timesUsable 3, apply discount', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 2,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: null,
        amountOff: 1000,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: 'custom',
        valid: true,
        timesUsable: 3,
      },
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8500);
    expect(result.data.pricing.discountAmount).to.equal(1000);
    expect(result.data.pricing.discountPercent).to.equal(10.5);
    const resultPlain = result.data.get({ plain: true });
    expect(resultPlain.couponUseCount).to.equal(0);
    expect(resultPlain.couponId).to.equal(null);
    expect(result.appliedDiscounts).to.be.an('array').that.contains.something.like({ type: 'subscriptionCoupon', amount: 1000 });
  });

  it('User credit 10USD', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 1000,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(8500);
    expect(result.data.pricing.discountAmount).to.equal(1000);
    expect(result.data.pricing.discountPercent).to.equal(10.5);
    const resultPlain = result.data.get({ plain: true });
    expect(resultPlain.user.credit).to.equal(0);
  });

  it('User credit 95USD', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 9500,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(0);
    expect(result.data.pricing.discountAmount).to.equal(9500);
    expect(result.data.pricing.discountPercent).to.equal(100);
    const resultPlain = result.data.get({ plain: true });
    expect(resultPlain.user.credit).to.equal(0);
  });

  it('User credit 100USD', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 10000,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(0);
    expect(result.data.pricing.discountAmount).to.equal(9500);
    expect(result.data.pricing.discountPercent).to.equal(100);
    // TODO: Better check of appliedDiscounts
    expect(result.data.pricing.appliedDiscounts[0].creditLeft).to.equal(500);
  });

  it('Multiple Discounts', async () => {
    const cronDate = moment.utc().hour(0).minute(0);
    const subscription = db.Subscription.build({
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: moment.utc().toISOString(),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: 500,
      typeNextDiscount: 'amount',
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 2500,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 5,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: {
        id: 265,
        productId: null,
        code: 'QA',
        percentOff: 10,
        amountOff: null,
        maxRedemptions: 9999,
        timesRedeemed: 3,
        duration: null,
        valid: true,
        timesUsable: -1,
      },
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
      ],
    });
    const result = calculateDiscounts(subscription, cronDate);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(5850);
    expect(result.data.pricing.discountAmount).to.equal(3650);
    expect(result.data.pricing.discountPercent).to.equal(38.4);
    // TODO: Better check of appliedDiscounts
    expect(result.data.pricing.appliedDiscounts.length).to.equal(3);
  });
});
 */

describe('Box Number', async () => {
  it('An old customer with orders', async () => {
    const subscriptions = [];
    subscriptions.push(db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [{
        id: 49,
        userId: 1,
        name: 'John',
        lastName: 'Kite',
        email: 'test+1@aerolab.co',
        zipCode: '90001',
        address: 'Somewhere',
        addressExtra: '',
        city: 'San Diego',
        state: 'CA',
        boxType: 'Regular',
        productPrice: 9500,
        subscriptionId: 1,
        chargeToken: 'ch_XXX',
        trackId: null,
        tracked: false,
        cronId: 1,
        createdAt: moment.utc().subtract(7, 'day').toISOString(),
        updatedAt: moment.utc().subtract(7, 'day').toISOString(),
        tracking_email: '',
        shipstationStatus: '',
        shipstationOrder: '',
        boxNumber: null,
      },
      {
        id: 49,
        userId: 1,
        name: 'John',
        lastName: 'Kite',
        email: 'test+1@aerolab.co',
        zipCode: '90001',
        address: 'Somewhere',
        addressExtra: '',
        city: 'San Diego',
        state: 'CA',
        boxType: 'Regular',
        productPrice: 9500,
        subscriptionId: 1,
        chargeToken: 'ch_XXX',
        trackId: null,
        tracked: false,
        cronId: 2,
        createdAt: moment.utc().subtract(7, 'day').toISOString(),
        updatedAt: moment.utc().subtract(7, 'day').toISOString(),
        tracking_email: '',
        shipstationStatus: '',
        shipstationOrder: '',
        boxNumber: null,
      }],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));
    const cronDate = getCronDate(1);
    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).that.contains.something.like({ boxNumber: null });
  });

  it('A new customer without orders', async () => {
    const subscriptions = [];
    subscriptions.push(db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));
    const cronDate = getCronDate(1);
    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).that.contains.something.like({ boxNumber: 1 });
  });

  it('A new customer with orders', async () => {
    const subscriptions = [];
    subscriptions.push(db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: await getCronDate(-1),
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: moment.utc().toISOString(),
      updatedAt: moment.utc().toISOString(),
      couponUseCount: 0,
      cancellationDate: null,
      skips: [],
      user: {
        id: 1,
        customerId: 'cus_BZdZ20A4wfo5Bv',
        credit: 0,
        name: 'Huong',
        lastName: 'Nguyen-Yap',
        email: 'test-151@aerolab.co',
        status: 'active',
        zipCode: '94611',
        address: '95 Linda Ave ',
        addressExtra: '210',
        city: 'Oakland ',
        state: 'Ca',
        phoneNumber: '(408) 799-0267',
        hash: 'f28564f980231dac079589a3d31c35a532c28aa6',
        firstCharge: null,
        billingName: 'Huong',
        billingLastName: 'Nguyen-Yap',
        billingAddress: '95 Linda Ave ',
        billingAddressExtra: '210',
        billingCity: 'Oakland ',
        billingState: 'Ca',
        billingZipCode: '94611',
        hasMealMaker: true,
        deliveredMealMaker: false,
      },
      product: [
        {
          id: 3,
          description: 'Regular Box every 14 days',
          boxType: 'Regular',
          price: 9500,
          days: 14,
          hidden: false,
        },
      ],
      coupon: null,
      orders: [{
        id: 49,
        userId: 1,
        name: 'John',
        lastName: 'Kite',
        email: 'test+1@aerolab.co',
        zipCode: '90001',
        address: 'Somewhere',
        addressExtra: '',
        city: 'San Diego',
        state: 'CA',
        boxType: 'Regular',
        productPrice: 9500,
        subscriptionId: 1,
        chargeToken: 'ch_XXX',
        trackId: null,
        tracked: false,
        cronId: 1,
        createdAt: moment.utc().subtract(7, 'day').toISOString(),
        updatedAt: moment.utc().subtract(7, 'day').toISOString(),
        tracking_email: '',
        shipstationStatus: '',
        shipstationOrder: '',
        boxNumber: 1,
      },
      {
        id: 49,
        userId: 1,
        name: 'John',
        lastName: 'Kite',
        email: 'test+1@aerolab.co',
        zipCode: '90001',
        address: 'Somewhere',
        addressExtra: '',
        city: 'San Diego',
        state: 'CA',
        boxType: 'Regular',
        productPrice: 9500,
        subscriptionId: 1,
        chargeToken: 'ch_XXX',
        trackId: null,
        tracked: false,
        cronId: 2,
        createdAt: moment.utc().subtract(7, 'day').toISOString(),
        updatedAt: moment.utc().subtract(7, 'day').toISOString(),
        tracking_email: '',
        shipstationStatus: '',
        shipstationOrder: '',
        boxNumber: 2,
      }],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
      ],
    }));
    const cronDate = getCronDate(1);
    const simulation = await simulateCron(subscriptions, cronDate);
    expect(simulation).that.contains.something.like({ boxNumber: 3 });
  });
});
