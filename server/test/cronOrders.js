import moment from 'moment';
import chai from 'chai';

import { checkSubscriptionStatus, checkSubscriptionStartFrom, checkSubscriptionSkipDate, checkSubscriptionLastOrder, checkSubscriptionCycle, shouldAddMealMaker, getCronDate, shouldShipOrder, calculateNextShipment, simulateSubscription } from '../services/cron_new';
import { simulateCron, simulateCronAndGenerateCSV } from '../services/cron_simulator';
import { calculateDiscounts, applyDiscounts } from '../services/cron_charge';
import { runOrders } from '../services/cron_runner';
import config from '../config/config';
import db from '../db/connection';

const stripe = require('stripe')(
  config.stripeApiKey,
);

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;


describe('User A', async () => {
  it('User A - First Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: 341,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T15:57:49.000Z',
      updatedAt: '2018-03-02T15:57:49.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 1,
        customerId: 'cus_CQ6TpX71AFk4lI',
        credit: 2500,
        name: 'A',
        lastName: 'A',
        email: 'qa+1@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'A',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(111) 111-1111',
        hash: '09da9f02770bea017526334c3e2500164370522c',
        firstCharge: 'ch_1C1GPxFNHnBZW9i2jsa9wrbr',
        billingName: 'A',
        billingLastName: 'A',
        billingAddress: 'A',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: 9500 },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon:
      { id: 341,
        productId: null,
        code: 'doscajas',
        percentOff: 50,
        amountOff: null,
        maxRedemptions: 4,
        timesRedeemed: 3,
        duration: 'once',
        valid: true,
        timesUsable: -1 },
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = await simulateSubscription(subscription, cronDate);
    for (const order of result.data.newOrders) {
      expect(order.productPrice).to.equal(4750);
      expect(order.boxType).to.equal('First Box');
      expect(order.boxNumber).to.equal(1);
    }
    expect(result.data.user.credit).to.equal(2500);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(9500);
    expect(result.data.pricing.discountAmount).to.equal(9500);
    expect(result.data.pricing.discountPercent).to.equal(50);
  });

  it('User A - Second Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(1);
    const subscription = db.Subscription.build({
      id: 1,
      userId: 1,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 2,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T15:57:49.000Z',
      updatedAt: '2018-03-02T15:57:49.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 1,
        customerId: 'cus_CQ6TpX71AFk4lI',
        credit: 2500,
        name: 'A',
        lastName: 'A',
        email: 'qa+1@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'A',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(111) 111-1111',
        hash: '09da9f02770bea017526334c3e2500164370522c',
        firstCharge: null,
        billingName: 'A',
        billingLastName: 'A',
        billingAddress: 'A',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = await simulateSubscription(subscription, cronDate);
    for (const order of result.data.newOrders) {
      expect(order.productPrice).to.equal(8250);
    }
    // console.log(result.data.newOrders)
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(19000);
    expect(result.data.pricing.totalPrice).to.equal(16500);
    expect(result.data.newOrders.length).to.equal(2);
  });
});

describe('User B', async () => {
  it('User B - First Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(0);
    const subscription = db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:03:55.000Z',
      updatedAt: '2018-03-02T16:04:03.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 2,
        customerId: 'cus_CQ6ZqtuoB8IVO1',
        credit: 7500,
        name: 'B',
        lastName: 'B',
        email: 'qa+b@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'B',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(222) 222-2222',
        hash: '4e1fae3b209dafe357a40771d2b50a40404c2912',
        firstCharge: 'ch_1C1GMYFNHnBZW9i29LeqvnZL',
        billingName: 'B',
        billingLastName: 'B',
        billingAddress: 'B',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: 7000 },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = await simulateSubscription(subscription, cronDate);
    expect(result.data.user.credit).to.equal(7500);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(7000);
    expect(result.data.newOrders.length).to.equal(1);
    expect(result.data.newOrders).contain.an.item.with.property('boxType', 'First Box');
    expect(result.data.newOrders).contain.an.item.with.property('productPrice', 7000);
  });

  it('User B - Second Cron - One Box, Refered, 3 Referral', async () => {
    const cronDate = await getCronDate(1);
    const subscription = db.Subscription.build({
      id: 2,
      userId: 2,
      couponId: null,
      productId: 3,
      status: 'active',
      amount: 1,
      startFrom: '2018-03-05T03:00:00.000Z',
      pausedUntil: null,
      cancelReason: null,
      nextDiscount: null,
      typeNextDiscount: null,
      createdAt: '2018-03-02T16:03:55.000Z',
      updatedAt: '2018-03-02T16:04:03.000Z',
      couponUseCount: 0,
      cancellationDate: null,
      user:
      { id: 2,
        customerId: 'cus_CQ6ZqtuoB8IVO1',
        credit: 7500,
        name: 'B',
        lastName: 'B',
        email: 'qa+b@aerolab.co',
        status: 'active',
        zipCode: '90001',
        address: 'B',
        addressExtra: '',
        city: 'Los Angeles',
        state: 'CA',
        phoneNumber: '(222) 222-2222',
        hash: '4e1fae3b209dafe357a40771d2b50a40404c2912',
        firstCharge: 'ch_1C1GMYFNHnBZW9i29LeqvnZL',
        billingName: 'B',
        billingLastName: 'B',
        billingAddress: 'B',
        billingAddressExtra: '',
        billingCity: 'Los Angeles',
        billingState: 'CA',
        billingZipCode: '90001',
        hasMealMaker: null,
        deliveredMealMaker: false,
        hubspotId: null,
        beaba: true,
        beabaFlow: false,
        company: '',
        firstChargeAmount: null },
      product:
      { id: 3,
        description: 'Regular Box every 14 days',
        boxType: 'Regular',
        price: 9500,
        days: 14,
        hidden: false },
      coupon: null,
      skips: [],
      orderCount: [],
      orders: [],
    }, {
      include: [
        { model: db.User, as: 'user' },
        { model: db.Product, as: 'product' },
        { model: db.Coupon, as: 'coupon' },
        { model: db.Skip, as: 'skips' },
        { model: db.Order, as: 'orders' },
        { model: db.Order, as: 'orderCount' },
      ],
    });
    const result = await simulateSubscription(subscription, cronDate);
    expect(result.data.user.credit).to.equal(0);
    expect(result.data.user.firstChargeAmount).to.equal(null);
    expect(result.data.pricing.fullPrice).to.equal(9500);
    expect(result.data.pricing.totalPrice).to.equal(2000);
    expect(result.data.newOrders.length).to.equal(1);
    expect(result.data.newOrders).contain.an.item.with.property('boxType', 'First Box');
    expect(result.data.newOrders).contain.an.item.with.property('productPrice', 2000);
  });
});
