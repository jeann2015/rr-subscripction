import chai from 'chai';
import cloneDeep from 'clone-deep';
import { getMealsWithoutIngredient, getMealsWithoutTheseIngredients, buildBox, getUserWarehouseMealsWithStock, addMeals } from '../services/cron_new';
import { sub15, mealsData, stockData } from './data';
import { countStockConsumed } from '../entities/MealsStock/controller';

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;

// TODO: Add stock variable
describe('getMealsWithoutIngredient()', () => {
  it('Should return error if parameter is missing', () => {
    const meals = [{
      sku: 'RRL70170',
      name: 'Peas + Mint',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 20, name: 'Peas' },
        { id: 21, name: 'Broccoli' },
        { id: 22, name: 'Olive Oil' },
        { id: 23, name: 'Mint' },
        { id: 3, name: 'Navy Beans' },
      ] },
    {
      sku: 'RRL70180',
      name: 'Blueberry + Sunbutter',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 13, name: 'Blueberry' },
        { id: 16, name: 'Zucchini' },
        { id: 17, name: 'SunflowerButter' },
        { id: 18, name: 'Cinnamon' },
        { id: 19, name: 'Hemp Seeds' },
      ] },
    ];
    const result = getMealsWithoutIngredient();
    const result2 = getMealsWithoutIngredient(meals);

    expect(result).to.exist;
    expect(result).to.be.an.instanceOf(Error);

    expect(result2).to.exist;
    expect(result2).to.be.an.instanceOf(Error);
  });

  it('Should return correctly filtered meals array', () => {
    const meals = [{
      sku: 'RRL70170',
      name: 'Peas + Mint',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 20, name: 'Peas' },
        { id: 21, name: 'Broccoli' },
        { id: 22, name: 'Olive Oil' },
        { id: 23, name: 'Mint' },
        { id: 3, name: 'Navy Beans' },
      ] },
    {
      sku: 'RRL70180',
      name: 'Blueberry + Sunbutter',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 13, name: 'Blueberry' },
        { id: 16, name: 'Zucchini' },
        { id: 17, name: 'SunflowerButter' },
        { id: 18, name: 'Cinnamon' },
        { id: 19, name: 'Hemp Seeds' },
      ] },
    {
      sku: 'RRL70190',
      name: 'Blueberry + Beet',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 13, name: 'Blueberry' },
        { id: 14, name: 'Beet' },
        { id: 5, name: 'Coconut Butter' },
        { id: 3, name: 'Navy Beans' },
        { id: 15, name: 'Ginger' },
      ] }];

    const result = getMealsWithoutIngredient(meals, { id: 21, name: 'Broccoli' });

    expect(result).to.eql([{
      sku: 'RRL70180',
      name: 'Blueberry + Sunbutter',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 13, name: 'Blueberry' },
        { id: 16, name: 'Zucchini' },
        { id: 17, name: 'SunflowerButter' },
        { id: 18, name: 'Cinnamon' },
        { id: 19, name: 'Hemp Seeds' },
      ] },
    {
      sku: 'RRL70190',
      name: 'Blueberry + Beet',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 13, name: 'Blueberry' },
        { id: 14, name: 'Beet' },
        { id: 5, name: 'Coconut Butter' },
        { id: 3, name: 'Navy Beans' },
        { id: 15, name: 'Ginger' },
      ],
    }]);

    const result2 = getMealsWithoutIngredient(meals, { id: 13, name: 'Blueberry' });
    expect(result2).to.eql([{
      sku: 'RRL70170',
      name: 'Peas + Mint',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 20, name: 'Peas' },
        { id: 21, name: 'Broccoli' },
        { id: 22, name: 'Olive Oil' },
        { id: 23, name: 'Mint' },
        { id: 3, name: 'Navy Beans' },
      ],
    }]);
  });
});

describe('getMealsWithoutTheseIngredients()', () => {
  it('Should return same meals array if no exluded ingredients array provided', () => {
    const meals = [{
      sku: 'RRL70010',
      name: 'Broccoli + Green Beans',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 43, name: 'Flax Seeds' },
        { id: 21, name: 'Broccoli' },
        { id: 34, name: 'Chickpeas' },
        { id: 37, name: 'Avocado Oil' },
        { id: 42, name: 'Green Beans' },
      ] },
    {
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] },
    {
      sku: 'RRL70030',
      name: 'Cauliflower + Kale',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 35, name: 'Cauliflower' },
        { id: 40, name: 'Kale' },
        { id: 32, name: 'White Quinoa' },
        { id: 41, name: 'Garlic' },
        { id: 37, name: 'Avocado Oil' },
      ] },
    ];

    const result = getMealsWithoutTheseIngredients(meals);
    expect(result).to.eql([{
      sku: 'RRL70010',
      name: 'Broccoli + Green Beans',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 43, name: 'Flax Seeds' },
        { id: 21, name: 'Broccoli' },
        { id: 34, name: 'Chickpeas' },
        { id: 37, name: 'Avocado Oil' },
        { id: 42, name: 'Green Beans' },
      ] },
    {
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] },
    {
      sku: 'RRL70030',
      name: 'Cauliflower + Kale',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 35, name: 'Cauliflower' },
        { id: 40, name: 'Kale' },
        { id: 32, name: 'White Quinoa' },
        { id: 41, name: 'Garlic' },
        { id: 37, name: 'Avocado Oil' },
      ] },
    ]);
  });

  it('Should filter meal with excluded ingredient', () => {
    const excluded_ingredients = [
      { id: 21, name: 'Broccoli' },
      { id: 34, name: 'Chickpeas' },
      { id: 1, name: 'Mango' },
    ];
    const meals = [{
      sku: 'RRL70010',
      name: 'Broccoli + Green Beans',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 43, name: 'Flax Seeds' },
        { id: 21, name: 'Broccoli' },
        { id: 34, name: 'Chickpeas' },
        { id: 37, name: 'Avocado Oil' },
        { id: 42, name: 'Green Beans' },
      ] },
    {
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] },
    {
      sku: 'RRL70030',
      name: 'Cauliflower + Kale',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 35, name: 'Cauliflower' },
        { id: 40, name: 'Kale' },
        { id: 32, name: 'White Quinoa' },
        { id: 41, name: 'Garlic' },
        { id: 37, name: 'Avocado Oil' },
      ] },
    ];

    const result = getMealsWithoutTheseIngredients(meals, excluded_ingredients);
    expect(result).to.eql([{
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] },
    {
      sku: 'RRL70030',
      name: 'Cauliflower + Kale',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 35, name: 'Cauliflower' },
        { id: 40, name: 'Kale' },
        { id: 32, name: 'White Quinoa' },
        { id: 41, name: 'Garlic' },
        { id: 37, name: 'Avocado Oil' },
      ] },
    ]);
  });

  it('Should filter more than one meal with excluded ingredient', () => {
    const excluded_ingredients = [
      { id: 21, name: 'Broccoli' },
      { id: 35, name: 'Cauliflower' },
      { id: 1, name: 'Mango' },
    ];
    const meals = [{
      sku: 'RRL70010',
      name: 'Broccoli + Green Beans',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 43, name: 'Flax Seeds' },
        { id: 21, name: 'Broccoli' },
        { id: 34, name: 'Chickpeas' },
        { id: 37, name: 'Avocado Oil' },
        { id: 42, name: 'Green Beans' },
      ] },
    {
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] },
    {
      sku: 'RRL70030',
      name: 'Cauliflower + Kale',
      default: false,
      kind: 'default',
      ingredients: [
        { id: 35, name: 'Cauliflower' },
        { id: 40, name: 'Kale' },
        { id: 32, name: 'White Quinoa' },
        { id: 41, name: 'Garlic' },
        { id: 37, name: 'Avocado Oil' },
      ] },
    ];

    const result = getMealsWithoutTheseIngredients(meals, excluded_ingredients);
    expect(result).to.eql([{
      sku: 'RRL70020',
      name: 'Butternut Squash + Banana',
      default: true,
      kind: 'default',
      ingredients: [
        { id: 10, name: 'Butternut Squash' },
        { id: 32, name: 'White Quinoa' },
        { id: 31, name: 'Banana' },
        { id: 5, name: 'Coconut Butter' },
        { id: 9, name: 'Cardamom' },
      ] }],
    );
  });
});

describe('getUserWarehouseMealsWithStock()', () => {
  it('Should return meals from correct warehouse', () => {
    const sub = cloneDeep(sub15);
    sub.stock = cloneDeep(stockData);
    sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70020 = 0;
    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 3;

    const result = getUserWarehouseMealsWithStock(sub);
    const filtered = sub.meals.filter(meal => meal.sku !== 'RRL70020' && meal.sku !== 'RRL70080');

    expect(result).to.have.deep.members(filtered);
  });

  it('Should return all meals if there is stock', () => {
    const sub = cloneDeep(sub15);
    sub.stock = cloneDeep(stockData);
    sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

    const result = getUserWarehouseMealsWithStock(sub);

    expect(result).to.have.deep.members(sub.meals);
  });

  it('Should return only meals with stock >= 12', () => {
    const sub = cloneDeep(sub15);
    sub.stock = cloneDeep(stockData);
    sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70010 = 0;
    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 3;
    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 12;
    sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70120 = 14;

    const result = getUserWarehouseMealsWithStock(sub);
    const filtered = sub.meals.filter(meal => meal.sku !== 'RRL70010' && meal.sku !== 'RRL70040');

    expect(result).to.have.deep.members(filtered);
  });
});

describe('addMeals()', () => {
  it('Should handle correctly missing parameters', () => {
    it('Should return error if a parameter is missing or its value is not correct', () => {
      const result = addMeals([], 0, 4);

      expect(result).toEqual(
        { error: true, message: 'A parameter is missing' },
      );

      const boxDetail = [
        { mealSku: 'RRL70010', quantity: 4 },
        { mealSku: 'RRL70020', quantity: 4 },
        { mealSku: 'RRL70040', quantity: 4 },
        { mealSku: 'RRL70050', quantity: 4 },
        { mealSku: 'RRL70080', quantity: 4 },
        { mealSku: 'RRL70110', quantity: 4 },
      ];

      const result2 = addMeals(boxDetail);

      expect(result2).toEqual(
        { error: true, message: 'A parameter is missing' },
      );

      const result3 = addMeals(boxDetail, 0);

      expect(result3).toEqual(
        { error: true, message: 'A parameter is missing' },
      );

      const result4 = addMeals(0, 4);

      expect(result4).toEqual(
        { error: true, message: 'A parameter is missing' },
      );
    });
  });

  it('Should return same box if quantity to add is <= 0', () => {
    const boxDetail = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70080', quantity: 4 },
      { mealSku: 'RRL70110', quantity: 4 },
    ];

    const result = addMeals(boxDetail, 0, 4);

    expect(result).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70080', quantity: 4 },
      { mealSku: 'RRL70110', quantity: 4 },
    ]);

    const boxDetail2 = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ];

    const result2 = addMeals(boxDetail2, 0, 4);

    expect(result2).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ]);

    const boxDetail3 = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ];

    const result3 = addMeals(boxDetail3, -2, 4);

    expect(result3).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ]);
  });

  it('Should correctly add meals requested', () => {
    const boxDetail = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ];

    const result = addMeals(boxDetail, 8, 4);

    expect(result).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 8 },
      { mealSku: 'RRL70020', quantity: 8 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ]);

    const boxDetail2 = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70060', quantity: 4 },
    ];

    const result2 = addMeals(boxDetail2, 1, 4);

    expect(result2).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 5 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70060', quantity: 4 },
    ]);

    const boxDetail3 = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70060', quantity: 4 },
    ];

    const result3 = addMeals(boxDetail3, 15, 4);

    expect(result3).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 8 },
      { mealSku: 'RRL70020', quantity: 8 },
      { mealSku: 'RRL70040', quantity: 8 },
      { mealSku: 'RRL70050', quantity: 7 },
      { mealSku: 'RRL70060', quantity: 4 },
    ]);

    const boxDetail4 = [
      { mealSku: 'RRL70060', quantity: 4 },
      { mealSku: 'RRL70180', quantity: 4 },
    ];

    const result4 = addMeals(boxDetail4, 16, 4);

    expect(result4).to.have.deep.members([
      { mealSku: 'RRL70060', quantity: 12 },
      { mealSku: 'RRL70180', quantity: 12 },
    ]);

    const boxDetail5 = [
      { mealSku: 'RRL70060', quantity: 4 },
    ];

    const result5 = addMeals(boxDetail5, 20, 4);

    expect(result5).to.have.deep.members([
      { mealSku: 'RRL70060', quantity: 24 },
    ]);
  });

  it('Should correctly keep into account units per meals to add', () => {
    const boxDetail = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ];

    const result = addMeals(boxDetail, 8, 2);

    expect(result).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 6 },
      { mealSku: 'RRL70020', quantity: 6 },
      { mealSku: 'RRL70040', quantity: 6 },
      { mealSku: 'RRL70050', quantity: 6 },
    ]);

    const boxDetail2 = [
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70020', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
    ];

    const result2 = addMeals(boxDetail2, 7, 3);

    expect(result2).to.have.deep.members([
      { mealSku: 'RRL70010', quantity: 7 },
      { mealSku: 'RRL70020', quantity: 7 },
      { mealSku: 'RRL70040', quantity: 5 },
      { mealSku: 'RRL70050', quantity: 4 },
    ]);
  });
});

describe('buildBox()', () => {
  describe('No excluded ingredients cases', () => {
    describe('Breakfast meals cases', () => {
      it('Should correctly build box with 2 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 2;
    
        const result = buildBox(sub);

        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(6);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 2 },
          { mealSku: 'RRL70010', quantity: 6 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70080', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });

      it('Should correctly build box with 4 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 4;
    
        const result = buildBox(sub);

        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(6);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 4 },
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70080', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });

      it('Should correctly build box with 6 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 6;
    
        const result = buildBox(sub);

        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(6);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 4 },
          { mealSku: 'RRL70250', quantity: 2 },
          { mealSku: 'RRL70010', quantity: 6 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });

      it('Should correctly build box with 12 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 12;
    
        const result = buildBox(sub);

        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(6);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 4 },
          { mealSku: 'RRL70250', quantity: 4 },
          { mealSku: 'RRL70270', quantity: 4 },
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });

      it('Should correctly build box with 16 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 16;
    
        const result = buildBox(sub);
  
        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(6);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 4 },
          { mealSku: 'RRL70250', quantity: 4 },
          { mealSku: 'RRL70270', quantity: 4 },
          { mealSku: 'RRL70260', quantity: 4 },
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70020', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });

      it('Should correctly build box with 24 breakfasts meals', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        // Breakfasts: 70240, 70250, 70270, 70260
        sub.meals = [...mealsData];
        sub.excluded_ingredients = [];
    
        // We add breakfast meals to sub
        sub.breakFastCount = 24;
    
        const result = buildBox(sub);
  
        expect(result.boxDetail.length).to.exist;
        expect(result.boxDetail.length).to.eql(4);
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70240', quantity: 8 },
          { mealSku: 'RRL70250', quantity: 8 },
          { mealSku: 'RRL70270', quantity: 4 },
          { mealSku: 'RRL70260', quantity: 4 },
        ]);
        expect(result.configuration).to.equal('custom');
      });
    });

    describe('No breakfast meals cases', () => {
      it('Should return default box detail if there is stock', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        sub.excluded_ingredients = []; // Remove excluded ingredients
  
        const result = buildBox(sub);
  
        expect(result).to.haveOwnProperty('boxDetail');
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70080', quantity: 4 },
          { mealSku: 'RRL70110', quantity: 4 },
        ]);
        expect(result).to.haveOwnProperty('configuration');
        expect(result.configuration).to.equal('standard');
      });
  
      it('Should return modified box detail if there is no stock', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        sub.excluded_ingredients = []; // Remove excluded ingredients
  
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70010 = 0;
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 0;
  
        const result = buildBox(sub);
  
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70030', quantity: 4 },
          { mealSku: 'RRL70020', quantity: 4 },
          { mealSku: 'RRL70060', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70080', quantity: 4 },
          { mealSku: 'RRL70110', quantity: 4 },
        ]);
        expect(result).to.haveOwnProperty('configuration');
        expect(result.configuration).to.equal('custom');
      });
  
      it('Should return modified box detail if there is no stock. Test 2', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        sub.excluded_ingredients = []; // Remove excluded ingredients
  
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70020 = 0;
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 3;
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 12;
  
        const result = buildBox(sub);
  
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70030', quantity: 4 },
          { mealSku: 'RRL70060', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70080', quantity: 4 },
          { mealSku: 'RRL70110', quantity: 4 },
        ]);
  
        expect(result.configuration).to.equal('custom');
      });
  
      it('Should return modified box detail if there is no stock, taking into account meal priority', () => {
        const sub = cloneDeep(sub15);
        sub.stock = cloneDeep(stockData);
        sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
        sub.excluded_ingredients = []; // Remove excluded ingredients
  
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70020 = 0;
        sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 0;
  
        // Change meals priorities. RRL70120 and RRL70060 will be top priority
        sub.meals[sub.meals.findIndex(meal => meal.sku === 'RRL70120')].priority = 1;
        sub.meals[sub.meals.findIndex(meal => meal.sku === 'RRL70060')].priority = 2;
  
        // Change priority of ex-top to some low-priority number
        sub.meals[sub.meals.findIndex(meal => meal.sku === 'RRL70010')].priority = 40;
        sub.meals[sub.meals.findIndex(meal => meal.sku === 'RRL70020')].priority = 41;
  
        const result = buildBox(sub);
  
        expect(result.boxDetail).to.have.deep.members([
          { mealSku: 'RRL70120', quantity: 4 },
          { mealSku: 'RRL70010', quantity: 4 },
          { mealSku: 'RRL70040', quantity: 4 },
          { mealSku: 'RRL70050', quantity: 4 },
          { mealSku: 'RRL70060', quantity: 4 },
          { mealSku: 'RRL70110', quantity: 4 },
        ]);
  
        expect(result.configuration).to.equal('custom');
      });
    });
  });

  describe('With excluded ingredients cases', () => {
    it('If standard box detail does not include excluded ingredients should return box without changes', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
      sub.excluded_ingredients = []; // Remove excluded ingredients

      const result = buildBox(sub);

      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70010', quantity: 4 },
        { mealSku: 'RRL70020', quantity: 4 },
        { mealSku: 'RRL70040', quantity: 4 },
        { mealSku: 'RRL70050', quantity: 4 },
        { mealSku: 'RRL70080', quantity: 4 },
        { mealSku: 'RRL70110', quantity: 4 },
      ]);
    });

    it('Should return modified box detail if subscription has excluded ingredients', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      // sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 0;

      const result = buildBox(sub);

      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70010', quantity: 4 },
        { mealSku: 'RRL70120', quantity: 4 },
        { mealSku: 'RRL70040', quantity: 4 },
        { mealSku: 'RRL70150', quantity: 4 },
        { mealSku: 'RRL70080', quantity: 4 },
        { mealSku: 'RRL70160', quantity: 4 },
      ]);
    });

    it('Should return modified box detail if we run out of stock and subscription has excluded ingredients', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 0;

      const result = buildBox(sub);

      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70010', quantity: 4 },
        { mealSku: 'RRL70120', quantity: 4 },
        { mealSku: 'RRL70040', quantity: 4 },
        { mealSku: 'RRL70150', quantity: 4 },
        { mealSku: 'RRL70180', quantity: 4 },
        { mealSku: 'RRL70160', quantity: 4 },
      ]);
    });
  });

  describe('Low stock and several excluded ingredients cases', () => {
    it('Test 1 - Should build box with 5 meals', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      sub.excluded_ingredients = [
        {
          id: 1,
          subscriptionId: 15,
          ingredientId: 3,
          ingredient: {
            id: 3,
            name: 'Navy Beans',
          },
        },
        {
          id: 2,
          subscriptionId: 15,
          ingredientId: 5,
          ingredient: {
            id: 5,
            name: 'Coconut Butter',
          },
        },
        {
          id: 3,
          subscriptionId: 15,
          ingredientId: 13,
          ingredient: {
            id: 13,
            name: 'Blueberry',
          },
        },
        {
          id: 4,
          subscriptionId: 15,
          ingredientId: 43,
          ingredient: {
            id: 43,
            name: 'Flax Seeds',
          },
        },
      ];

      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70020 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70110 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70120 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70160 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70170 = 1;

      const result = buildBox(sub);
      expect(result.boxDetail.length).to.exist;
      expect(result.boxDetail.length).to.eql(5);
      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70050', quantity: 8 },
        { mealSku: 'RRL70080', quantity: 4 },
        { mealSku: 'RRL70030', quantity: 4 },
        { mealSku: 'RRL70060', quantity: 4 },
        { mealSku: 'RRL70150', quantity: 4 },
      ]);
    });

    it('Test 2 - Should build box with 4 meals', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      sub.excluded_ingredients = [
        {
          id: 1,
          subscriptionId: 15,
          ingredientId: 3,
          ingredient: {
            id: 3,
            name: 'Navy Beans',
          },
        },
        {
          id: 2,
          subscriptionId: 15,
          ingredientId: 32,
          ingredient: {
            id: 32,
            name: 'White Quinoa',
          },
        },
        {
          id: 3,
          subscriptionId: 15,
          ingredientId: 29,
          ingredient: {
            id: 29,
            name: 'Date',
          },
        },
        {
          id: 4,
          subscriptionId: 15,
          ingredientId: 5,
          ingredient: {
            id: 5,
            name: 'Coconut Butter',
          },
        },
        {
          id: 5,
          subscriptionId: 15,
          ingredientId: 2,
          ingredient: {
            id: 2,
            name: 'Carrot',
          },
        },
      ];

      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 1;

      const result = buildBox(sub);
      expect(result.boxDetail.length).to.exist;
      expect(result.boxDetail.length).to.eql(4);
      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70010', quantity: 8 },
        { mealSku: 'RRL70080', quantity: 8 },
        { mealSku: 'RRL70060', quantity: 4 },
        { mealSku: 'RRL70180', quantity: 4 },
      ]);
    });

    it('Test 3 - Should build box with only 2 meals', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      sub.excluded_ingredients = [
        {
          id: 1,
          subscriptionId: 15,
          ingredientId: 3,
          ingredient: {
            id: 3,
            name: 'Navy Beans',
          },
        },
        {
          id: 2,
          subscriptionId: 15,
          ingredientId: 32,
          ingredient: {
            id: 32,
            name: 'White Quinoa',
          },
        },
        {
          id: 3,
          subscriptionId: 15,
          ingredientId: 29,
          ingredient: {
            id: 29,
            name: 'Date',
          },
        },
        {
          id: 4,
          subscriptionId: 15,
          ingredientId: 5,
          ingredient: {
            id: 5,
            name: 'Coconut Butter',
          },
        },
        {
          id: 5,
          subscriptionId: 15,
          ingredientId: 2,
          ingredient: {
            id: 2,
            name: 'Carrot',
          },
        },
      ];

      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70010 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 1;

      const result = buildBox(sub);
      expect(result.boxDetail.length).to.exist;
      expect(result.boxDetail.length).to.eql(2);
      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70060', quantity: 12 },
        { mealSku: 'RRL70180', quantity: 12 },
      ]);
    });

    it('Test 4 - Should build box with only 1 meal', () => {
      const sub = cloneDeep(sub15);
      sub.stock = cloneDeep(stockData);
      sub.meals = [...mealsData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110

      sub.excluded_ingredients = [
        {
          id: 1,
          subscriptionId: 15,
          ingredientId: 3,
          ingredient: {
            id: 3,
            name: 'Navy Beans',
          },
        },
        {
          id: 2,
          subscriptionId: 15,
          ingredientId: 32,
          ingredient: {
            id: 32,
            name: 'White Quinoa',
          },
        },
        {
          id: 3,
          subscriptionId: 15,
          ingredientId: 29,
          ingredient: {
            id: 29,
            name: 'Date',
          },
        },
        {
          id: 4,
          subscriptionId: 15,
          ingredientId: 5,
          ingredient: {
            id: 5,
            name: 'Coconut Butter',
          },
        },
        {
          id: 5,
          subscriptionId: 15,
          ingredientId: 2,
          ingredient: {
            id: 2,
            name: 'Carrot',
          },
        },
      ];

      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70010 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70040 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70060 = 1;
      sub.stock[sub.user.TagDay.TagDayName.warehouseId].RRL70080 = 1;

      const result = buildBox(sub);
      expect(result.boxDetail.length).to.exist;
      expect(result.boxDetail.length).to.eql(1);
      expect(result.boxDetail).to.have.deep.members([
        { mealSku: 'RRL70180', quantity: 24 },
      ]);
    });
  });
});

describe('countStockConsumed()', () => {
  it('Should return empty object if no input is given', () => {
    const dataInstance = { stockConsumed: {} };
    const boxDetail = [];

    const result = countStockConsumed(dataInstance, boxDetail);

    expect(result).to.be.an('object');
    expect(result).to.be.empty;
  });

  it('Should add box detail to empty object if order proccesed is the first one', () => {
    const dataInstance = { stockConsumed: {} };
    const boxDetail = [
      { mealSku: 'RRL70120', quantity: 4 },
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70060', quantity: 4 },
      { mealSku: 'RRL70110', quantity: 4 },
    ];

    const result = countStockConsumed(dataInstance, boxDetail);

    expect(result).to.deep.include({
      RRL70120: 4,
      RRL70010: 4,
      RRL70040: 4,
      RRL70050: 4,
      RRL70060: 4,
      RRL70110: 4,
    });
  });

  it('Should sum box detail quantity to current consumed stock', () => {
    const dataInstance = {
      stockConsumed: {
        RRL70120: 4,
        RRL70010: 4,
        RRL70040: 4,
        RRL70050: 4,
        RRL70060: 4,
        RRL70110: 4,
      },
    };

    const boxDetail = [
      { mealSku: 'RRL70120', quantity: 4 },
      { mealSku: 'RRL70010', quantity: 4 },
      { mealSku: 'RRL70040', quantity: 4 },
      { mealSku: 'RRL70050', quantity: 4 },
      { mealSku: 'RRL70060', quantity: 4 },
      { mealSku: 'RRL70110', quantity: 4 },
    ];

    const result = countStockConsumed(dataInstance, boxDetail);

    expect(result).to.deep.include({
      RRL70120: 8,
      RRL70010: 8,
      RRL70040: 8,
      RRL70050: 8,
      RRL70060: 8,
      RRL70110: 8,
    });
  });
});
