import chai from 'chai';
import cloneDeep from 'clone-deep';
import { getMealsWithoutIngredient, getMealsWithoutTheseIngredients, buildBox, getUserWarehouseMealsWithStock } from '../services/cron_new';
import { sub15, sub16, sub17, sub18, sub19, sub20, sub21, sub22, sub23, sub24, sub25, sub26, sub27, JustMealsDefaultData, stockData, JustMealsBreakFastData, MealsDefaultBreakFastData } from './dataBreakFast';
import { countStockConsumed } from '../entities/MealsStock/controller';

chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;

describe('buildBox()', () => {

    describe('No excluded ingredients cases', () => {

        it('Should return standard box detail if there is stock but without breakfast ' +
            'and 24 meals', () => {

          const sub = cloneDeep(sub15);
          sub.stock = cloneDeep(stockData);
          sub.meals = [...JustMealsDefaultData]; // Defaults: 70010, 70020, 70040, 70050, 70080, 70110
          sub.excluded_ingredients = []; // Remove excluded ingredients

          const result = buildBox(sub);

          expect(result).to.haveOwnProperty('boxDetail');
          expect(result.boxDetail).to.have.deep.members([
            { mealSku: 'RRL70010', quantity: 4 },
            { mealSku: 'RRL70020', quantity: 4 },
            { mealSku: 'RRL70040', quantity: 4 },
            { mealSku: 'RRL70050', quantity: 4 },
            { mealSku: 'RRL70080', quantity: 4 },
            { mealSku: 'RRL70110', quantity: 4 },
          ]);
          expect(result).to.haveOwnProperty('configuration');
          expect(result.configuration).to.equal('standard');
        });

        it('Should return standard box detail if there is stock but without breakfast ' +
            'and 12 meals', () => {
              const sub = cloneDeep(sub17);
              sub.stock = cloneDeep(stockData);
              sub.meals = [...JustMealsDefaultData];
              sub.excluded_ingredients = []; // Remove excluded ingredients

              const result = buildBox(sub);

              expect(result).to.haveOwnProperty('boxDetail');
              expect(result.boxDetail).to.have.deep.members([
                  { mealSku: 'RRL70010', quantity: 2 },
                  { mealSku: 'RRL70020', quantity: 2 },
                  { mealSku: 'RRL70040', quantity: 2 },
                  { mealSku: 'RRL70050', quantity: 2 },
                  { mealSku: 'RRL70080', quantity: 2 },
                  { mealSku: 'RRL70110', quantity: 2 },
              ]);
              expect(result).to.haveOwnProperty('configuration');
              expect(result.configuration).to.equal('standard');
          });

        it('Should return custom box detail if there is stock but without default ' +
            'and 24 meals', () => {
              const sub = cloneDeep(sub16);
              sub.stock = cloneDeep(stockData);
              sub.meals = [...JustMealsBreakFastData];
              sub.excluded_ingredients = []; // Remove excluded ingredients

              const result = buildBox(sub);

              expect(result).to.haveOwnProperty('boxDetail');
              expect(result.boxDetail).to.have.deep.members([
                  { mealSku: 'RRL70240', quantity: 8 },
                  { mealSku: 'RRL70250', quantity: 8 },
                  { mealSku: 'RRL70260', quantity: 4 },
                  { mealSku: 'RRL70270', quantity: 4 }
              ]);
              expect(result).to.haveOwnProperty('configuration');
              expect(result.configuration).to.equal('custom');
          });

        it('Should return custom box detail if there is stock but without default ' +
            'and 12 meals', () => {
              const sub = cloneDeep(sub18);
              sub.stock = cloneDeep(stockData);
              sub.meals = [...JustMealsBreakFastData];
              sub.excluded_ingredients = []; // Remove excluded ingredients

              const result = buildBox(sub);

              expect(result).to.haveOwnProperty('boxDetail');
              expect(result.boxDetail).to.have.deep.members([
                  { mealSku: 'RRL70240', quantity: 4 },
                  { mealSku: 'RRL70250', quantity: 4 },
                  { mealSku: 'RRL70260', quantity: 2 },
                  { mealSku: 'RRL70270', quantity: 2 }
              ]);
              expect(result).to.haveOwnProperty('configuration');
              expect(result.configuration).to.equal('custom');
          });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 12 meals', () => {
              const sub = cloneDeep(sub19);
              sub.stock = cloneDeep(stockData);
              sub.meals = [...MealsDefaultBreakFastData];
              sub.excluded_ingredients = []; // Remove excluded ingredients

              const result = buildBox(sub);

              expect(result).to.haveOwnProperty('boxDetail');
              expect(result.boxDetail).to.have.deep.members([
                  { mealSku: 'RRL70010', quantity: 2 },
                  { mealSku: 'RRL70020', quantity: 2 },
                  { mealSku: 'RRL70040', quantity: 2 },
                  { mealSku: 'RRL70050', quantity: 2 },
                  { mealSku: 'RRL70080', quantity: 2 },
                  { mealSku: 'RRL70240', quantity: 2 },
              ]);
              expect(result).to.haveOwnProperty('configuration');
              expect(result.configuration).to.equal('custom');
          });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 12 meals 6 breakfast y 6 default', () => {
            const sub = cloneDeep(sub21);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 2 },
                { mealSku: 'RRL70020', quantity: 2 },
                { mealSku: 'RRL70040', quantity: 2 },
                { mealSku: 'RRL70240', quantity: 2 },
                { mealSku: 'RRL70250', quantity: 2 },
                { mealSku: 'RRL70260', quantity: 2 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 12 meals 4 breakfast y 8 default', () => {
            const sub = cloneDeep(sub22);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 2 },
                { mealSku: 'RRL70020', quantity: 2 },
                { mealSku: 'RRL70040', quantity: 2 },
                { mealSku: 'RRL70050', quantity: 2 },
                { mealSku: 'RRL70240', quantity: 2 },
                { mealSku: 'RRL70250', quantity: 2 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 12 meals 8 breakfast y 4 default', () => {
            const sub = cloneDeep(sub27);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70240', quantity: 2 },
                { mealSku: 'RRL70250', quantity: 2 },
                { mealSku: 'RRL70260', quantity: 2 },
                { mealSku: 'RRL70270', quantity: 2 },
                { mealSku: 'RRL70010', quantity: 2 },
                { mealSku: 'RRL70020', quantity: 2 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals 4 breakfast 20 default', () => {
              const sub = cloneDeep(sub20);
              sub.stock = cloneDeep(stockData);
              sub.meals = [...MealsDefaultBreakFastData];
              sub.excluded_ingredients = []; // Remove excluded ingredients

              const result = buildBox(sub);

              expect(result).to.haveOwnProperty('boxDetail');
              expect(result.boxDetail).to.have.deep.members([
                  { mealSku: 'RRL70010', quantity: 4 },
                  { mealSku: 'RRL70020', quantity: 4 },
                  { mealSku: 'RRL70040', quantity: 4 },
                  { mealSku: 'RRL70050', quantity: 4 },
                  { mealSku: 'RRL70080', quantity: 4 },
                  { mealSku: 'RRL70240', quantity: 4 },
              ]);
              expect(result).to.haveOwnProperty('configuration');
              expect(result.configuration).to.equal('custom');
          });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals 8 breakfast 16 default ', () => {
            const sub = cloneDeep(sub23);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 4 },
                { mealSku: 'RRL70020', quantity: 4 },
                { mealSku: 'RRL70040', quantity: 4 },
                { mealSku: 'RRL70050', quantity: 4 },
                { mealSku: 'RRL70240', quantity: 4 },
                { mealSku: 'RRL70250', quantity: 4 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals 12 breakfast 12 default ', () => {
            const sub = cloneDeep(sub24);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 4 },
                { mealSku: 'RRL70020', quantity: 4 },
                { mealSku: 'RRL70040', quantity: 4 },
                { mealSku: 'RRL70240', quantity: 4 },
                { mealSku: 'RRL70250', quantity: 4 },
                { mealSku: 'RRL70260', quantity: 4 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals 16 breakfast 8 default ', () => {
            const sub = cloneDeep(sub25);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 4 },
                { mealSku: 'RRL70020', quantity: 4 },
                { mealSku: 'RRL70240', quantity: 4 },
                { mealSku: 'RRL70250', quantity: 4 },
                { mealSku: 'RRL70260', quantity: 4 },
                { mealSku: 'RRL70270', quantity: 4 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals 20 breakfast 4 default ', () => {
            const sub = cloneDeep(sub26);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];
            sub.excluded_ingredients = []; // Remove excluded ingredients

            const result = buildBox(sub);

            expect(result).to.haveOwnProperty('boxDetail');
            expect(result.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70240', quantity: 8 },
                { mealSku: 'RRL70250', quantity: 4 },
                { mealSku: 'RRL70260', quantity: 4 },
                { mealSku: 'RRL70270', quantity: 4 },
                { mealSku: 'RRL70010', quantity: 4 },
            ]);
            expect(result).to.haveOwnProperty('configuration');
            expect(result.configuration).to.equal('custom');
        });

    });

    describe('With excluded ingredients cases', () => {
        it('Should return standard box detail if there is stock but without breakfast ' +
            'and 24 meals and with exclude', () => {
            const sub = cloneDeep(sub15);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...JustMealsDefaultData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70040', quantity: 4 },
                { mealSku: 'RRL70110', quantity: 4 },
                { mealSku: 'RRL70060', quantity: 4 },
                { mealSku: 'RRL70120', quantity: 4 },
                { mealSku: 'RRL70150', quantity: 4 },
                { mealSku: 'RRL70160', quantity: 4 },
            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('standard');
        });

        it('Should return standard box detail if there is stock but without breakfast ' +
            'and 12 meals and with exclude', () => {
            const sub = cloneDeep(sub17);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...JustMealsDefaultData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 2 },
                { mealSku: 'RRL70040', quantity: 2 },
                { mealSku: 'RRL70080', quantity: 2 },
                { mealSku: 'RRL70120', quantity: 2 },
                { mealSku: 'RRL70150', quantity: 2 },
                { mealSku: 'RRL70160', quantity: 2 },
            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('standard');
        });

        it('Should return standard box detail if there is stock but without default ' +
            'and 12 meals and with exclude', () => {
            const sub = cloneDeep(sub18);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...JustMealsBreakFastData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70250', quantity: 4 },
                { mealSku: 'RRL70260', quantity: 4 },
                { mealSku: 'RRL70270', quantity: 4 },
            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('custom');
        });

        it('Should return standard box detail if there is stock but without default ' +
            'and 24 meals and with exclude', () => {
            const sub = cloneDeep(sub16);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...JustMealsBreakFastData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70250', quantity: 8 },
                { mealSku: 'RRL70260', quantity: 8 },
                { mealSku: 'RRL70270', quantity: 8 },
            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 12 meals and with exclude', () => {
            const sub = cloneDeep(sub19);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 4 },
                { mealSku: 'RRL70040', quantity: 4 },
                { mealSku: 'RRL70080', quantity: 4 },

            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('custom');
        });

        it('Should return custom box detail if there is stock breakfast ' +
            'and default with 24 meals and with exclude', () => {
            const sub = cloneDeep(sub20);
            sub.stock = cloneDeep(stockData);
            sub.meals = [...MealsDefaultBreakFastData];

            const resultWithExclude = buildBox(sub);

            expect(resultWithExclude).to.haveOwnProperty('boxDetail');
            expect(resultWithExclude.boxDetail).to.have.deep.members([
                { mealSku: 'RRL70010', quantity: 8 },
                { mealSku: 'RRL70040', quantity: 8 },
                { mealSku: 'RRL70080', quantity: 8 },
            ]);
            expect(resultWithExclude).to.haveOwnProperty('configuration');
            expect(resultWithExclude.configuration).to.equal('custom');
        });

    });
});


