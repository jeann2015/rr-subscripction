import fs from 'fs';
import path from 'path';

import db, { sequelize } from '../db/connection';
import config from '../config/config';
import { getSubscriptionsAndRunOrders } from '../services/cron_runner';
import { updateSubscriptionExcludedIngredients } from '../entities/ExcludedIngredients/controller';

const chai = require('chai');
// we are using the 'expect' style of Chai
const expect = chai.expect;
let request = require('supertest');


const stripe = require('stripe')(
  config.stripeApiKey,
);

const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

describe('Start Server and populate DB', () => {
  it('Wait for DB', (done) => {
    setTimeout(() => {
      done();
    }, 100);
  });

  // it('Add default admin', (done) => {
  //   // User: qa@aerolab.co
  //   // Pass: aerolab
  //   sequelize.query(`
  //     INSERT INTO admin (id, name, password, email) VALUES (NULL, 'QA', 'sha1$76d4fb01$1$5bda5dc8ec53f9c41d1b3f3011999e1ac9bd5d0e', 'qa@aerolab.co');
  //   `).then(() => {
  //     done();
  //   });
  // });

  // it('Add DB data', (done) => {
  //   fs.readFile(path.resolve(__dirname, 'test-db-data-breakfast.sql'), 'utf8', (err, data) => {
  //     if (err) throw err;
  //     sequelize.query(data).then(() => done());
  //   });
  // });

  // it('Set default meals and priority', (done) => {
  //   sequelize.query(`
  //     UPDATE meals SET meals.default = 1 WHERE sku IN ('RRL70020', 'RRL70040', 'RRL70110', 'RRL70120', 'RRL70160', 'RRL70170');
  //     UPDATE meals SET priority = 1 WHERE sku = 'RRL70150';
  //     UPDATE meals SET priority = 2 WHERE sku = 'RRL70010';
  //     UPDATE meals SET priority = 3 WHERE sku = 'RRL70040';
  //     UPDATE meals SET priority = 4 WHERE sku = 'RRL70110';
  //     UPDATE meals SET priority = 5 WHERE sku = 'RRL70120';
  //     UPDATE meals SET priority = 6 WHERE sku = 'RRL70170';
  //     UPDATE meals SET priority = 7 WHERE sku = 'RRL70190';
  //     UPDATE meals SET priority = 8 WHERE sku = 'RRL70020';
  //     UPDATE meals SET priority = 9 WHERE sku = 'RRL70030';
  //     UPDATE meals SET priority = 10 WHERE sku = 'RRL70050';
  //     UPDATE meals SET priority = 11 WHERE sku = 'RRL70060';
  //     UPDATE meals SET priority = 12 WHERE sku = 'RRL70080';
  //     UPDATE meals SET priority = 13 WHERE sku = 'RRL70160';
  //     UPDATE meals SET priority = 14 WHERE sku = 'RRL70180';
  //     UPDATE meals SET priority = 15 WHERE sku = 'RRL70200';
  //     UPDATE meals SET priority = 16 WHERE sku = 'RRL70210';
  //     UPDATE meals SET priority = 17 WHERE sku = 'RRL70220';
  //   `).then(() => {
  //     done();
  //   });
  // });
});

// cron_new.js
describe('cron_new.js tests', () => {
  require('./cron_new_breakfast.spec');
});
//
// const productPrices = {
//   '12Meals': 6588,
//   '24Meals': 11976,
//   '24MealsMigrated': 9500,
// };
//
// describe('updateSubscriptionExcludedIngredients() Tests', () => {
//   it('Should handle correctly if subscription id or ingredients params are missing', async () => {
//     const subId = 55;
//     const ingredients = [6, 17, 24];
//
//     const result = await updateSubscriptionExcludedIngredients({});
//     expect(result).not.to.be.undefined;
//     expect(typeof result).to.be.eq('object');
//     expect(result).to.eql({
//       success: false,
//       message: 'Error: A parameter is missing or wrong format',
//     });
//
//     const result2 = await updateSubscriptionExcludedIngredients({ subId });
//     expect(result2).to.eql({
//       success: false,
//       message: 'Error: A parameter is missing or wrong format',
//     });
//
//     const result3 = await updateSubscriptionExcludedIngredients({ ingredients });
//     expect(result3).to.eql({
//       success: false,
//       message: 'Error: A parameter is missing or wrong format',
//     });
//
//     const result4 = await updateSubscriptionExcludedIngredients({ subId: 'asdas', ingredients: [] });
//     expect(result4).to.eql({
//       success: false,
//       message: 'Error: A parameter is missing or wrong format',
//     });
//
//     const result5 = await updateSubscriptionExcludedIngredients({ subId, ingredients: {} });
//     expect(result5).to.eql({
//       success: false,
//       message: 'Error: A parameter is missing or wrong format',
//     });
//   });
//
//   it('Should add excluded ingredients to subscription', async () => {
//     const subId = 9;
//     const ingredients = [6, 17, 24];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Check sub has no excluded ingredients
//     const currentIngrs = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//     expect(currentIngrs.length).to.eql(0);
//
//     // Add ingredients
//     const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//     expect(updated).to.eql({
//       success: true,
//       message: 'Excluded ingredients successfully updated',
//     });
//
//     // Check update was done
//     const result = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//
//     const ingrs = result
//       .map(ingr => ingr.get({ plain: true }))
//       .map(plain => ({
//         subscriptionId: plain.subscriptionId,
//         ingredientId: plain.ingredientId,
//       }));
//
//     expect(result.length).to.eql(ingredients.length);
//     expect(ingrs).to.eql([
//       {
//         subscriptionId: subId,
//         ingredientId: ingredients[0],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredients[1],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredients[2],
//       },
//     ]);
//   });
//
//   it('Given an updated set of ingredients ids, should add the ones not present in DB', async () => {
//     const subId = 9;
//     const ingredients = [1, 5, 10];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Delete excluded ingredients for subscription if any
//     await db.ExcludedIngredients.destroy({
//       where: { subscriptionId: subId },
//     });
//
//     // Check sub has no excluded ingredients
//     const currentIngrs = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//     expect(currentIngrs.length).to.eql(0);
//
//     // We add the first set of ingredients
//     const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//     expect(updated).to.eql({
//       success: true,
//       message: 'Excluded ingredients successfully updated',
//     });
//
//     // And now the updated set (2 more ingredients)
//     const ingredientsUpdated = [...ingredients, 15, 20];
//     const updated2 = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients: ingredientsUpdated });
//     expect(updated2).to.eql({
//       success: true,
//       message: 'Excluded ingredients successfully updated',
//     });
//
//     // Check update was done
//     const result = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//
//     const ingrs = result
//       .map(ingr => ingr.get({ plain: true }))
//       .map(plain => ({
//         subscriptionId: plain.subscriptionId,
//         ingredientId: plain.ingredientId,
//       }));
//
//     expect(result.length).to.eql(ingredientsUpdated.length);
//     expect(ingrs).to.eql([
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[0],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[1],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[2],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[3],
//       },
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[4],
//       },
//     ]);
//   });
//
//   it('Should remove ingredients if they are not in the updated set', async () => {
//     const subId = 9;
//     const ingredients = [13, 19, 31];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Delete excluded ingredients for subscription if any
//     await db.ExcludedIngredients.destroy({
//       where: { subscriptionId: subId },
//     });
//
//     // Check sub has no excluded ingredients
//     const currentIngrs = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//     expect(currentIngrs.length).to.eql(0);
//
//     // We add the first set of ingredients
//     const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//     expect(updated).to.eql({
//       success: true,
//       message: 'Excluded ingredients successfully updated',
//     });
//
//     // And now the updated set (only 1 ingredient)
//     const ingredientsUpdated = ingredients.slice(0, 1);
//     const updated2 = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients: ingredientsUpdated });
//     expect(updated2).to.eql({
//       success: true,
//       message: 'Excluded ingredients successfully updated',
//     });
//
//     // Check update was done
//     const result = await db.ExcludedIngredients.findAll({
//       where: { subscriptionId: subId },
//     });
//
//     const ingrs = result
//       .map(ingr => ingr.get({ plain: true }))
//       .map(plain => ({
//         subscriptionId: plain.subscriptionId,
//         ingredientId: plain.ingredientId,
//       }));
//
//     expect(result.length).to.eql(ingredientsUpdated.length);
//     expect(ingrs).to.eql([
//       {
//         subscriptionId: subId,
//         ingredientId: ingredientsUpdated[0],
//       },
//     ]);
//   });
//
//   it('Should remove and add ingredients', async () => {
//     const subId = 9;
//     const ingredients = [4, 5, 6, 7, 8];
//     const ingredientsUpdated = [4, 5, 10, 12];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Delete excluded ingredients for subscription if any
//     try {
//       await db.ExcludedIngredients.destroy({
//         where: { subscriptionId: subId },
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check sub has no excluded ingredients
//     try {
//       const currentIngrs = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//       expect(currentIngrs.length).to.eql(0);
//     } catch (err) {
//       console.log(err);
//     }
//
//     // We add the first set of ingredients
//     try {
//       const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//       expect(updated).to.eql({
//         success: true,
//         message: 'Excluded ingredients successfully updated',
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // And now the updated set
//     try {
//       const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients: ingredientsUpdated });
//       expect(updated).to.eql({
//         success: true,
//         message: 'Excluded ingredients successfully updated',
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check update was done
//     try {
//       const result = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//
//       const ingrs = result
//         .map(ingr => ingr.get({ plain: true }))
//         .map(plain => ({
//           subscriptionId: plain.subscriptionId,
//           ingredientId: plain.ingredientId,
//         }));
//
//       expect(result.length).to.eql(ingredientsUpdated.length);
//       expect(ingrs).to.eql([
//         {
//           subscriptionId: subId,
//           ingredientId: ingredientsUpdated[0],
//         },
//         {
//           subscriptionId: subId,
//           ingredientId: ingredientsUpdated[1],
//         },
//         {
//           subscriptionId: subId,
//           ingredientId: ingredientsUpdated[2],
//         },
//         {
//           subscriptionId: subId,
//           ingredientId: ingredientsUpdated[3],
//         },
//       ]);
//     } catch (err) {
//       console.log(err);
//     }
//   });
//
//   it('Should ignore repeated ingredient ids', async () => {
//     const subId = 9;
//     const ingredients = [10, 10, 21, 25, 25];
//     const noRepeated = Array.from(new Set(ingredients));
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Delete excluded ingredients for subscription if any
//     try {
//       await db.ExcludedIngredients.destroy({
//         where: { subscriptionId: subId },
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check sub has no excluded ingredients
//     try {
//       const currentIngrs = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//       expect(currentIngrs.length).to.eql(0);
//     } catch (err) {
//       console.log(err);
//     }
//
//     // We add the first set of ingredients
//     try {
//       const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//       expect(updated).to.eql({
//         success: true,
//         message: 'Excluded ingredients successfully updated',
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check update was done
//     try {
//       const result = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//
//       const ingrs = result
//         .map(ingr => ingr.get({ plain: true }))
//         .map(plain => ({
//           subscriptionId: plain.subscriptionId,
//           ingredientId: plain.ingredientId,
//         }));
//
//       expect(result.length).to.eql(noRepeated.length);
//       expect(ingrs).to.eql([
//         {
//           subscriptionId: subId,
//           ingredientId: noRepeated[0],
//         },
//         {
//           subscriptionId: subId,
//           ingredientId: noRepeated[1],
//         },
//         {
//           subscriptionId: subId,
//           ingredientId: noRepeated[2],
//         },
//       ]);
//     } catch (err) {
//       console.log(err);
//     }
//   });
//
//   it('Should remove all ingredients', async () => {
//     const subId = 9;
//     const ingredients = [];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 9;
//
//     // Delete excluded ingredients for subscription if any
//     try {
//       await db.ExcludedIngredients.destroy({
//         where: { subscriptionId: subId },
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check sub has no excluded ingredients
//     try {
//       const currentIngrs = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//       expect(currentIngrs.length).to.eql(0);
//     } catch (err) {
//       console.log(err);
//     }
//
//     // We add the first set of ingredients
//     try {
//       const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//       expect(updated).to.eql({
//         success: true,
//         message: 'Excluded ingredients successfully updated',
//       });
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Check update was done
//     try {
//       const result = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//       expect(result.length).to.eql(0);
//     } catch (err) {
//       console.log(err);
//     }
//   });
//
//   it('Integrity test after table updates: Should end up with empty table', async () => {
//     try {
//       const result = await db.ExcludedIngredients.findAll({});
//       expect(result.length).to.eq(0);
//     } catch (err) {
//       console.log(err);
//     }
//   });
// });
//
// describe('Cron test', () => {
//   describe('Full stock cases', () => {
//     it('Get subscriptions and run cron', async () => {
//       await getSubscriptionsAndRunOrders(false);
//     });
//
//     it('Cron generated all orders', (done) => {
//       db.Order.findAll({})
//       .then((orders) => {
//         expect(orders.length).to.equal(13);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 1 - 12 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 1 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('standard');
//         expect(order.boxType).to.equal('First Box 12 Meals');
//         expect(order.productPrice).to.equal(productPrices['12Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 1 - 12 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 1 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 2)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70040',
//             'RRL70110',
//             'RRL70120',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     it('Check Sub 2 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 2 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('standard');
//         expect(order.boxType).to.equal('First Box 24 Meals');
//         expect(order.productPrice).to.equal(productPrices['24Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 2 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 2 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70040',
//             'RRL70110',
//             'RRL70120',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     it('Check Sub 3 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 3 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('standard');
//         expect(order.boxType).to.equal('First Box 24 Meals Migrated');
//         expect(order.productPrice).to.equal(productPrices['24MealsMigrated']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 3 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 3 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70040',
//             'RRL70110',
//             'RRL70120',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//   });
//
//   describe('Low stock cases', () => {
//     // Sub 4
//     it('Check Sub 4 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 4 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('custom');
//         expect(order.boxType).to.equal('First Box 24 Meals');
//         expect(order.productPrice).to.equal(productPrices['24Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 4 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 4 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70040',
//             'RRL70110',
//             'RRL70120',
//             'RRL70160',
//             'RRL70150',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     // Sub 5
//     it('Check Sub 5 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 5 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('custom');
//         expect(order.boxType).to.equal('First Box 24 Meals');
//         expect(order.productPrice).to.equal(productPrices['24Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 5 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 5 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         })
//         .then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70150',
//             'RRL70010',
//             'RRL70110',
//             'RRL70120',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     // Sub 6
//     it('Check Sub 6 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 6 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('custom');
//         expect(order.boxType).to.equal('First Box 24 Meals');
//         expect(order.productPrice).to.equal(productPrices['24Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 6 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 6 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         })
//         .then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70190',
//             'RRL70110',
//             'RRL70030',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     // Sub 7
//     it('Check Sub 7 - 24 Meals Box | Order', (done) => {
//       db.Order.findOne({
//         where: { userId: 7 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('custom');
//         expect(order.boxType).to.equal('First Box 24 Meals');
//         expect(order.productPrice).to.equal(productPrices['24Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 7 - 24 Meals Box | Box Detail', (done) => {
//       db.Order.findOne({
//         where: { userId: 7 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 4)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70050',
//             'RRL70040',
//             'RRL70190',
//             'RRL70030',
//             'RRL70150',
//             'RRL70010',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//
//     // Sub 8
//     it('Check Sub 8 - 12 Meals Box | Order - User with zipcode not present in tag_day table', (done) => {
//       db.Order.findOne({
//         where: { userId: 8 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.configuration).to.equal('custom');
//         expect(order.boxType).to.equal('First Box 12 Meals');
//         expect(order.productPrice).to.equal(productPrices['12Meals']);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//
//     it('Check Sub 8 - 12 Meals Box | Box Detail - Should fallback to Marshall warehouse', (done) => {
//       db.Order.findOne({
//         where: { userId: 8 },
//       })
//       .then((order) => {
//         db.BoxDetailHistory.findAll({
//           where: { orderId: order.id },
//         }).then((detail) => {
//           const plainMeals = detail.map(meal => meal.get({ plain: true }));
//           const skus = plainMeals.map(meal => meal.mealSku);
//
//           expect(plainMeals.length).to.equal(6);
//           expect(plainMeals.map(meal => meal.mealQuantity).every(el => el === 2)).to.be.true;
//           expect(skus).to.have.deep.members([
//             'RRL70020',
//             'RRL70190',
//             'RRL70110',
//             'RRL70030',
//             'RRL70160',
//             'RRL70170',
//           ]);
//         })
//         .then(() => { done(); })
//         .catch(err => done(err));
//       }).catch(err => done(err));
//     });
//   });
//
//   describe('With coupons cases', () => {
//     // Sub 11
//     it('Check Sub 11 - 24 Meals Box | User with one-use coupon: Discount was applied in checkout. Should not have coupon and a discount of $50 applied.', async () => {
//       // Check order was created and discount was applied
//       const order = await db.Order.findOne({
//         where: { userId: 11 },
//       });
//       expect(order).to.exist;
//       expect(order.productPrice).to.equal(productPrices['24Meals'] - 5000);
//       expect(order.boxNumber).to.equal(1);
//
//       // Check coupon was deleted
//       const sub = await db.Subscription.findOne({
//         where: { id: 11 },
//       });
//
//       expect(sub.couponId).to.be.null;
//     });
//
//     // Sub 12
//     it('Check Sub 12 - 24 Meals Box | User with forever-free coupon', async () => {
//       const order = await db.Order.findOne({
//         where: { userId: 12 },
//       });
//       expect(order).to.exist;
//       expect(order.productPrice).to.equal(0);
//       expect(order.boxNumber).to.equal(1);
//
//       // Check coupon is still linked to user
//       const sub = await db.Subscription.findOne({
//         where: { id: 12 },
//       });
//       expect(sub.couponId).to.equal(2);
//     });
//
//     // // Sub 13
//     // it('Check Sub 13 - 24 Meals Box | User with 20% discount coupon for 2 boxes', (done) => {
//     //   db.Order.findOne({
//     //     where: { userId: 13 },
//     //   })
//     //   .then((order) => {
//     //     expect(order).to.exist;
//     //     expect(order.productPrice).to.equal(productPrices['24Meals'] - (productPrices['24Meals'] * 0.2));
//     //     expect(order.boxNumber).to.equal(1);
//     //   })
//     //   .then(() => { done(); })
//     //   .catch(err => done(err));
//     // });
//
//     // Sub 14
//     it('Check Sub 14 - 24 Meals Box | User with $25 discount coupon for 3 boxes', (done) => {
//       db.Order.findOne({
//         where: { userId: 14 },
//       })
//       .then((order) => {
//         expect(order).to.exist;
//         expect(order.productPrice).to.equal(9476);
//         expect(order.boxNumber).to.equal(1);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     });
//   });
// });
//
// describe('Should update stock correctly', () => {
//   it('Stock is updated correctly for warehouse 8', (done) => {
//     db.MealsStock.findAll({})
//     .then((stock) => {
//       const plainStock = stock.map(el => el.get({ plain: true }));
//
//       const meal1 = plainStock.find(meal => meal.mealSku === 'RRL70020' && meal.warehouse === 8);
//       expect(meal1.stock).to.equal(36);
//
//       const meal2 = plainStock.find(meal => meal.mealSku === 'RRL70040' && meal.warehouse === 8);
//       expect(meal2.stock).to.equal(36);
//
//       const meal3 = plainStock.find(meal => meal.mealSku === 'RRL70110' && meal.warehouse === 8);
//       expect(meal3.stock).to.equal(36);
//
//       const meal4 = plainStock.find(meal => meal.mealSku === 'RRL70120' && meal.warehouse === 8);
//       expect(meal4.stock).to.equal(36);
//
//       const meal5 = plainStock.find(meal => meal.mealSku === 'RRL70160' && meal.warehouse === 8);
//       expect(meal5.stock).to.equal(36);
//
//       const meal6 = plainStock.find(meal => meal.mealSku === 'RRL70170' && meal.warehouse === 8);
//       expect(meal6.stock).to.equal(11);
//
//       const meal7 = plainStock.find(meal => meal.mealSku === 'RRL70150' && meal.warehouse === 8);
//       expect(meal7.stock).to.equal(46);
//     })
//     .then(() => { done(); })
//     .catch(err => done(err));
//   });
//
//   it('Stock is updated correctly for warehouse 1', (done) => {
//     db.MealsStock.findAll({})
//     .then((stock) => {
//       const plainStock = stock.map(el => el.get({ plain: true }));
//
//       const meal1 = plainStock.find(meal => meal.mealSku === 'RRL70020' && meal.warehouse === 1);
//       expect(meal1.stock).to.equal(3);
//
//       const meal2 = plainStock.find(meal => meal.mealSku === 'RRL70040' && meal.warehouse === 1);
//       expect(meal2.stock).to.equal(2);
//
//       const meal3 = plainStock.find(meal => meal.mealSku === 'RRL70110' && meal.warehouse === 1);
//       expect(meal3.stock).to.equal(46);
//
//       const meal4 = plainStock.find(meal => meal.mealSku === 'RRL70120' && meal.warehouse === 1);
//       expect(meal4.stock).to.equal(46);
//
//       const meal5 = plainStock.find(meal => meal.mealSku === 'RRL70160' && meal.warehouse === 1);
//       expect(meal5.stock).to.equal(46);
//
//       const meal6 = plainStock.find(meal => meal.mealSku === 'RRL70170' && meal.warehouse === 1);
//       expect(meal6.stock).to.equal(46);
//
//       const meal7 = plainStock.find(meal => meal.mealSku === 'RRL70150' && meal.warehouse === 1);
//       expect(meal7.stock).to.equal(46);
//
//       const meal8 = plainStock.find(meal => meal.mealSku === 'RRL70010' && meal.warehouse === 1);
//       expect(meal8.stock).to.equal(46);
//
//       const meal9 = plainStock.find(meal => meal.mealSku === 'RRL70200' && meal.warehouse === 1);
//       expect(meal9.stock).to.equal(50);
//     })
//     .then(() => { done(); })
//     .catch(err => done(err));
//   });
//
//   it('Stock is updated correctly for warehouse 2', (done) => {
//     db.MealsStock.findAll({})
//     .then((stock) => {
//       const plainStock = stock.map(el => el.get({ plain: true }));
//
//       const meal1 = plainStock.find(meal => meal.mealSku === 'RRL70020' && meal.warehouse === 2);
//       expect(meal1.stock).to.equal(44);
//
//       const meal2 = plainStock.find(meal => meal.mealSku === 'RRL70040' && meal.warehouse === 2);
//       expect(meal2.stock).to.equal(3);
//
//       const meal3 = plainStock.find(meal => meal.mealSku === 'RRL70110' && meal.warehouse === 2);
//       expect(meal3.stock).to.equal(44);
//
//       const meal4 = plainStock.find(meal => meal.mealSku === 'RRL70120' && meal.warehouse === 2);
//       expect(meal4.stock).to.equal(1);
//
//       const meal5 = plainStock.find(meal => meal.mealSku === 'RRL70160' && meal.warehouse === 2);
//       expect(meal5.stock).to.equal(44);
//
//       const meal6 = plainStock.find(meal => meal.mealSku === 'RRL70170' && meal.warehouse === 2);
//       expect(meal6.stock).to.equal(44);
//
//       const meal7 = plainStock.find(meal => meal.mealSku === 'RRL70150' && meal.warehouse === 2);
//       expect(meal7.stock).to.equal(0);
//
//       const meal8 = plainStock.find(meal => meal.mealSku === 'RRL70010' && meal.warehouse === 2);
//       expect(meal8.stock).to.equal(1);
//
//       const meal9 = plainStock.find(meal => meal.mealSku === 'RRL70200' && meal.warehouse === 2);
//       expect(meal9.stock).to.equal(50);
//
//       const meal10 = plainStock.find(meal => meal.mealSku === 'RRL70190' && meal.warehouse === 2);
//       expect(meal10.stock).to.equal(44);
//
//       const meal11 = plainStock.find(meal => meal.mealSku === 'RRL70030' && meal.warehouse === 2);
//       expect(meal11.stock).to.equal(44);
//     })
//     .then(() => { done(); })
//     .catch(err => done(err));
//   });
//
//   it('Stock is updated correctly for warehouse 4', (done) => {
//     db.MealsStock.findAll({})
//     .then((stock) => {
//       const plainStock = stock.map(el => el.get({ plain: true }));
//
//       const meal1 = plainStock.find(meal => meal.mealSku === 'RRL70020' && meal.warehouse === 4);
//       expect(meal1.stock).to.equal(1);
//
//       const meal2 = plainStock.find(meal => meal.mealSku === 'RRL70040' && meal.warehouse === 4);
//       expect(meal2.stock).to.equal(46);
//
//       const meal3 = plainStock.find(meal => meal.mealSku === 'RRL70110' && meal.warehouse === 4);
//       expect(meal3.stock).to.equal(1);
//
//       const meal4 = plainStock.find(meal => meal.mealSku === 'RRL70120' && meal.warehouse === 4);
//       expect(meal4.stock).to.equal(2);
//
//       const meal5 = plainStock.find(meal => meal.mealSku === 'RRL70160' && meal.warehouse === 4);
//       expect(meal5).to.be.undefined;
//
//       const meal6 = plainStock.find(meal => meal.mealSku === 'RRL70170' && meal.warehouse === 4);
//       expect(meal6.stock).to.equal(0);
//
//       const meal7 = plainStock.find(meal => meal.mealSku === 'RRL70150' && meal.warehouse === 4);
//       expect(meal7.stock).to.equal(46);
//
//       const meal8 = plainStock.find(meal => meal.mealSku === 'RRL70010' && meal.warehouse === 4);
//       expect(meal8.stock).to.equal(46);
//
//       const meal9 = plainStock.find(meal => meal.mealSku === 'RRL70200' && meal.warehouse === 4);
//       expect(meal9.stock).to.equal(50);
//
//       const meal10 = plainStock.find(meal => meal.mealSku === 'RRL70190' && meal.warehouse === 4);
//       expect(meal10.stock).to.equal(46);
//
//       const meal11 = plainStock.find(meal => meal.mealSku === 'RRL70030' && meal.warehouse === 4);
//       expect(meal11.stock).to.equal(46);
//
//       const meal12 = plainStock.find(meal => meal.mealSku === 'RRL70050' && meal.warehouse === 4);
//       expect(meal12.stock).to.equal(46);
//     })
//     .then(() => { done(); })
//     .catch(err => done(err));
//   });
// });
//
// describe('Case when stock is low and user has a lot of excluded ingredients, TEST 1: User 10', () => {
//   it('Update stock to test case', async () => {
//     await sequelize.query(`
//       UPDATE meals_stock SET stock = 1, local_stock = 1 WHERE warehouse = 3 AND meal_sku IN ('RRL70020', 'RRL70040', 'RRL70110', 'RRL70120', 'RRL70160', 'RRL70170');
//       UPDATE meals_stock SET stock = 20, local_stock = 20 WHERE warehouse = 3 AND meal_sku IN ('RRL70010', 'RRL70030', 'RRL70050', 'RRL70060', 'RRL70080', 'RRL70150', 'RRL70180', 'RRL70190', 'RRL70200', 'RRL70210', 'RRL70220');
//     `);
//   });
//
//   it('Activate subscription 10', async () => {
//     await sequelize.query(`
//       UPDATE \`user\` SET status = 'active' WHERE id = 10;
//       UPDATE \`subscription\` SET status = 'active' WHERE id = 10;
//     `);
//   });
//
//   it('Add excluded ingredients to subscription', async () => {
//     const subId = 10;
//     const ingredients = [43, 3, 13, 5];
//     // const ingredients = [10, 12, 13, 14, 16, 36];
//     const admin = { id: 1, name: 'Test Admin' };
//     const user = 10;
//
//     // Check sub has no excluded ingredients
//     try {
//       const currentIngrs = await db.ExcludedIngredients.findAll({
//         where: { subscriptionId: subId },
//       });
//       expect(currentIngrs.length).to.eql(0);
//     } catch (err) {
//       console.log(err);
//     }
//
//     // Add ingredients
//     try {
//       const updated = await updateSubscriptionExcludedIngredients({ admin, user, subId, ingredients });
//       expect(updated).to.eql({
//         success: true,
//         message: 'Excluded ingredients successfully updated',
//       });
//     } catch (err) {
//       console.log(err);
//     }
//   });
//
//   it('Run cron to create order for user 10', async () => {
//     await getSubscriptionsAndRunOrders(false);
//   });
//
//   it('Check order was created', (done) => {
//     db.Order.findAll({})
//     .then((orders) => {
//       expect(orders.length).to.equal(14);
//     })
//     .then(() => { done(); })
//     .catch(err => done(err));
//   });
//
//   it('Check Box Detail: Should build box with repeated meals due to lack of availability', (done) => {
//     db.Order.findOne({
//       where: { userId: 10 },
//     })
//     .then((order) => {
//       db.BoxDetailHistory.findAll({
//         where: { orderId: order.id },
//       }).then((detail) => {
//         const plainMeals = detail.map(meal => meal.get({ plain: true }));
//         const skus = plainMeals.map(meal => meal.mealSku);
//
//         expect(plainMeals.length).to.equal(5); // Expect 5 meals
//         expect(plainMeals.reduce((prev, curr) => prev + curr.mealQuantity, 0)).to.equal(24);
//         expect(skus).to.have.deep.members([
//           'RRL70150',
//           'RRL70030',
//           'RRL70050',
//           'RRL70060',
//           'RRL70080',
//         ]);
//       })
//       .then(() => { done(); })
//       .catch(err => done(err));
//     }).catch(err => done(err));
//   });
// });

// describe('new cron', () => {
//   const sumAMonday = moment().day(8).utc().hour(18)
//     .format('x');
//   tk.travel(new Date(sumAMonday));

//   it('Run next cron', async () => {
//     await getSubscriptionsAndRunOrders(false);
//   });

//   it('Sub 12', async () => {
//     const order = await db.Order.findOne({
//       where: { userId: 12 },
//     });
//     expect(order).to.exist;
//     expect(order.productPrice).to.equal(0);
//     expect(order.boxNumber).to.equal(2);

//     // Check coupon is still linked to user
//     const sub = await db.Subscription.findOne({
//       where: { id: 12 },
//     });
//     expect(sub.couponId).to.equal(2);

//     const logs = await db.Log.findAll({
//       where: { userId: 12 },
//     }).map(log => log.get({ plain: true }));

//     expect(logs.find(log => log.message === 'A coupon (#2) was applied for $119.76')).to.exist;
//   });
// });

// expect(logs.find(log => log.message === 'Coupon FREE-FOREVER used 2 time out of 2')).to.exist;
// expect(logs.find(log => log.message === 'Limit of uses for coupon FREE-FOREVER reached. Deleted')).to.exist;
