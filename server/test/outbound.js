import moment from 'moment';

import db, { sequelize } from '../db/connection';
import config from '../config/config';
import { outboundIntegration, triggerOutboundEvent } from '../utils/outbound';

const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;
const request = require('supertest');

const stripe = require('stripe')(
  config.stripeApiKey,
);

const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;

describe('Save Users on Outbound', () => {
  it('Save 1 user with email', async () => {
    const response = await outboundIntegration({
      email: 'aerolab--test--1@aerolab.co',
    });
    expect(response).to.equal(true);
  });

  it('Save 2 user with email', async () => {
    const response1 = await outboundIntegration({
      email: 'aerolab--test--1@aerolab.co',
    });

    const response2 = await outboundIntegration({
      email: 'aerolab--test--2@aerolab.co',
    });

    expect(response1).to.equal(true);
    expect(response2).to.equal(true);
  });

  it('Save user with all data', async () => {
    const response = await outboundIntegration({
      id: 1,
      firstName: 'Rocky',
      lastName: 'Balboa',
      email: 'test---phone---1@aerolab.co',
      phoneNumber: '+14154092222',
      attributes: {
        childs_name: 'Rambo, Ceci, Mara',
        childs_age: '2018/1/1, 2018/1/1, 2018/1/1',
        suscription_atatus: 'Customer',
        product: '14 days box',
        frequency: '14',
        referral_link: 'http://referral.link/',
        update_cc_link: 'http://update.cc/',
        date_order_created: '2018/1/1',
        date_order_shipped: '2018/1/1',
        first_ship_date: '2018/1/1',
        last_ship_date: '2018/1/1',
        skip_dates: '2018/1/1, 2018/1/1, 2018/1/1',
        next_ship_date: '2018/1/1, 2018/1/1, 2018/1/1',
      },
    });
    expect(response).to.equal(true);
  });
});


describe('Trigger Events', () => {
  it('Signup', async () => {
    try {
      const testTrackEvent = await triggerOutboundEvent({ event: 'Singup', user: '78f11ab468c3cf3e331467cb3bcb9eadc321ac87' });
      expect(true).to.equal(true);
    } catch (err) {
      expect(true).to.equal(false);
    }
  });

  it('Order created', async () => {
    try {
      const testTrackEvent = await triggerOutboundEvent({ event: 'Order created', user: '78f11ab468c3cf3e331467cb3bcb9eadc321ac87' });
      expect(true).to.equal(true);
    } catch (err) {
      expect(true).to.equal(false);
    }
  });
});
