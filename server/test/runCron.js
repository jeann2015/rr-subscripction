import db, { sequelize } from '../db/connection';
import { getSubscriptionsAndRunOrders } from '../services/cron_runner';
import config from '../config/config';

const chai = require('chai');
chai.use(require('chai-like'));
chai.use(require('chai-things'));

const expect = chai.expect;
let request = require('supertest');

const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;
const Product = db.Product;
const Coupon = db.Coupon;


const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

let cronId;
describe('Run First Cron', () => {
  // it('Move Subscription start_from two week old', (done) => {
    // sequelize.query(`
    //   UPDATE \`subscription\` SET \`start_from\` = DATE_SUB(\`start_from\`, INTERVAL 2 WEEK);
    //   UPDATE \`order\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
    //   UPDATE \`skip\` SET \`date\` = DATE_SUB(\`date\`, INTERVAL 2 WEEK);
    //   UPDATE \`cron\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
    // `).then(() => {
    //   done();
    // });
  // });

  it('Run Cron', async (done) => {
    console.log('Wait for Cron to finish');
    await getSubscriptionsAndRunOrders();
    done();
  });

  it('Get runned cron id', (done) => {
    Cron.findOne({
      order: 'id DESC',
      raw: true,
    })
    .then((cron) => {
      cronId = cron.id;
      done();
    });
  });
});

describe('Check First Cron - User Case 2 - Missing Payment Lead', () => {
  let caseId;
  it('Should not be stored', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+2@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      expect(user).to.be.a('null');
      done();
    });
  });
});

describe('Check First Cron - User Case 3 - Customer', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+3@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.id).to.not.equal(null);
      done();
    });
  });
  it('Check active Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'active');
      expect(subscription).to.have.property('productId', 3);
      expect(subscription).to.have.property('amount', 1);
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(2);
      expect(orders).that.contains.something.like({ boxType: 'Meal Maker' });
      expect(orders).that.contains.something.like({ boxType: 'Regular' });
      done();
    });
  });
});

describe('Check First Cron - User Case 4 - Customer, 2 Tiny Humans', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+4@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.id).to.not.equal(null);
      done();
    });
  });

  it('Check active Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((subscription) => {
      expect(subscription).to.have.property('status', 'active');
      expect(subscription).to.have.property('productId', 3);
      expect(subscription).to.have.property('amount', 2);
      done();
    });
  });

  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(3);
      expect(orders).that.contains.something.like({ boxType: 'Meal Maker' });
      expect(orders).that.contains.something.like({ boxType: 'Regular' });
      done();
    });
  });
});

describe('Check First Cron - User Case 5 - Customer, Delayed Shipping', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+5@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      expect(user.id).to.not.equal(null);
      done();
    });
  });
  it('Check active Subscription', (done) => {
    Subscription.find({
      include: [Product, Coupon],
      where: {
        user_id: caseId,
      },
    })
    .then((subscription) => {
      expect(subscription).to.not.equal(null);
      const plainSubscription = subscription.get({ plain: true });
      expect(plainSubscription).to.have.property('status', 'active');
      expect(plainSubscription).to.have.property('productId', 3);
      expect(plainSubscription).to.have.property('amount', 1);
      done();
    });
  });
  it('Check NO Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
    })
    .then((orders) => {
      expect(orders.length).to.equal(0);
      done();
    });
  });
});

describe('Check First Cron - User Case 6 - Customer, Regular box every 28 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+6@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });

  it('Check 1 active Subscription, regular box discount 10%', (done) => {
    Subscription.find({
      include: [Product, Coupon],
      where: {
        user_id: caseId,
      },
    })
    .then((subscription) => {
      expect(subscription).to.not.equal(null);
      const plainSubscription = subscription.get({ plain: true });
      expect(plainSubscription).to.have.property('status', 'active');
      expect(plainSubscription).to.have.property('productId', 5);
      expect(plainSubscription).to.have.property('amount', 1);
      done();
    });
  });

  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
    })
    .then((orders) => {
      expect(orders.length).to.equal(2);
      expect(orders).that.contains.something.like({ boxType: 'Meal Maker' });
      expect(orders).that.contains.something.like({ boxType: 'Regular' });
      done();
    });
  });
});

describe('Check First Cron - User Case 7 - Referred by User Case 3', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+7@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check 1 active Subscription', (done) => {
    Subscription.find({
      include: [Product, Coupon],
      where: {
        user_id: caseId,
      },
    })
    .then((subscription) => {
      expect(subscription).to.not.equal(null);
      const plainSubscription = subscription.get({ plain: true });
      expect(plainSubscription).to.have.property('status', 'active');
      expect(plainSubscription).to.have.property('productId', 3);
      expect(plainSubscription).to.have.property('amount', 1);
      done();
    });
  });

  it('Check Orders with $20 off due to referred code', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(2);

        /* for (const or of orders) {
          if (or.boxType === 'Meal Maker') {
            expect(or).to.have.property('productPrice', 0);
          }
          if (or.boxType === 'Starter') {
            const price = 9500;
            const discount = config.referralFirstDiscount;
            // User was referred, should have a discount on his first purchase.
            expect(or).to.have.property('productPrice', price - discount);
          }
        } */
        done();
      });
  });
});

describe('Check First Cron - User Case 8 - Customer With $20 OFF coupon, duration = 2', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+8@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check 1 active Subscription, regular box 20$ discount', (done) => {
    Subscription.findAll({
      include: [Product, Coupon],
      where: {
        user_id: caseId,
      },
    })
      .then((subscription) => {
        expect(subscription).to.not.equal(null);
        const plainSubscription = subscription.get({ plain: true });
        expect(plainSubscription).to.have.property('status', 'active');
        expect(plainSubscription).to.have.property('productId', 5);
        expect(plainSubscription).to.have.property('amount', 1);
        // expect(subscriptions[0].couponUseCount).to.equal(1);
        done();
      });
  });

  it('Check Orders and 20$ off', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(2);
        /* for (const or of orders) {
          if (or.boxType === 'Regular') {
            const price = 9500;
            const couponDiscount = 2000;
            // Price minus 10%
            expect(or).to.have.property('productPrice', price - couponDiscount);
          }
        } */
        done();
      });
  });
});

describe('Check First Cron - User Case 9 - Customer With one subscription, that will be skipped on second and third cron', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'test+unit+9@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check 1 active Subscription', (done) => {
    Subscription.find({
      where: {
        user_id: caseId,
      },
    })
      .then((subscription) => {
        expect(subscription).to.not.equal(null);
        const plainSubscription = subscription.get({ plain: true });
        expect(plainSubscription).to.have.property('status', 'active');
        expect(plainSubscription).to.have.property('productId', 5);
        expect(plainSubscription).to.have.property('amount', 1);
        done();
      });
  });

  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
      },
    })
      .then((orders) => {
        expect(orders.length).to.equal(2);
        done();
      });
  });
});

