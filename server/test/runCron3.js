import moment from 'moment';

import db, { sequelize } from '../db/connection';
import { getSubscriptionsAndRunOrders } from '../services/cron_runner';

const chai = require('chai');

const expect = chai.expect;
let request = require('supertest');


const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;

const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

let cronId;

describe('Pause every subscription except the #5 for one cron', () => {
  it('Get runned cron id', (done) => {
    Subscription.update({
      pausedUntil: moment.utc().add(1, 'day').format(),
      status: 'paused',
    }, {
      where: {
        status: 'active',
        id: {
          $ne: 5,
        },
      },
    })
    .then((rowsUpdated) => {
      // console.log(rowsUpdated)
      done();
    });
  });
});


describe('Run Third Cron', () => {
  it('Move Subscription start_from two week old', (done) => {
    sequelize.query(`
      UPDATE \`subscription\` SET \`start_from\` = DATE_SUB(\`start_from\`, INTERVAL 2 WEEK);
      UPDATE \`order\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
      UPDATE \`skip\` SET \`date\` = DATE_SUB(\`date\`, INTERVAL 2 WEEK);
      UPDATE \`cron\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
    `).then(() => {
      done();
    });
  });

  it('Run Cron', async (done) => {
    console.log('Wait for Cron to finish');
    await getSubscriptionsAndRunOrders();
    done();
  });

  it('Get runned cron id', (done) => {
    Cron.findOne({
      order: 'id DESC',
      raw: true,
    })
    .then((cron) => {
      cronId = cron.id;
      done();
    });
  });
});


describe('Check Third Cron - User Case 3 - Customer', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case3@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(0);
      done();
    });
  });
});

describe('Check Third Cron - User Case 4 - Customer, 2 Tiny Humans', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case4@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(0);
      done();
    });
  });
});

describe('Check Third Cron - User Case 5 - Customer, Delayed Shipping', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case5@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(0);
      done();
    });
  });
});

describe('Check Third Cron - User Case 6 - Customer, Regular box every 28 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case6@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });

  it('Check Has 1 order', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      expect(orders.length).to.equal(1);
      done();
    });
  });
});

describe('Check Third Cron - User Case 7 - Referred by User Case 3 - A Starter box every 14 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case7@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check Has no orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(0);
        done();
      });
  });
});

describe('Check Third Cron - User Case 9 - Customer With one subscription, that will be skipped on second and third cron', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case9@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });
  it('Check Order should be skipped due to skip table.', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(0);
        done();
      });
  });
});
