import moment from 'moment';

import db, { sequelize } from '../db/connection';
import { getSubscriptionsAndRunOrders } from '../services/cron_runner';

const chai = require('chai');

const expect = chai.expect;
let request = require('supertest');


const User = db.User;
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;
const Product = db.Product;
const Coupon = db.Coupon;

const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

let cronId;

describe('Resume users', () => {
  it('Get runned cron id', (done) => {
    Subscription.update({
      pausedUntil: null,
      status: 'active',
    }, {
      where: {
        status: 'paused',
      },
    })
    .then((rowsUpdated) => {
      // console.log(rowsUpdated)
      done();
    });
  });
});

describe('Run Fourth Cron', () => {
  it('Move Subscription start_from two week old', (done) => {
    sequelize.query(`
      UPDATE \`subscription\` SET \`start_from\` = DATE_SUB(\`start_from\`, INTERVAL 2 WEEK);
      UPDATE \`order\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
      UPDATE \`skip\` SET \`date\` = DATE_SUB(\`date\`, INTERVAL 2 WEEK);
      UPDATE \`cron\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
    `).then(() => {
      done();
    });
  });

  it('Run Cron', async (done) => {
    console.log('Wait for Cron to finish');
    await getSubscriptionsAndRunOrders();
    done();
  });

  it('Get runned cron id', (done) => {
    Cron.findOne({
      order: 'id DESC',
      raw: true,
    })
    .then((cron) => {
      cronId = cron.id;
      done();
    });
  });
});


describe('Check Fourth Cron - User Case 3 - Customer', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case3@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      console.log(orders);
      expect(orders.length).to.equal(1);
      done();
    });
  });
});

describe('Check Fourth Cron - User Case 4 - Customer, 2 Tiny Humans', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case4@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      console.log(orders);
      expect(orders.length).to.equal(3);
      done();
    });
  });
});

describe('Check Fourth Cron - User Case 5 - Customer, Delayed Shipping', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case5@aerolab.co',
      },
      raw: true,
    })
    .then((user) => {
      caseId = user.id;
      done();
    });
  });
  it('Check Orders', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
    .then((orders) => {
      console.log(orders);
      expect(orders.length).to.equal(1);
      done();
    });
  });
});

describe('Check Fourth Cron - User Case 6 - Customer, Regular box every 28 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case6@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check Has 2 order', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(2);
        for (const or of orders) {
          if (or.boxType === 'Starter') {
            expect(or).to.have.property('productPrice', 9500);
          }
          if (or.boxType === 'Regular') {
            const price = 9500;
            // Price minus 10%
            expect(or).to.have.property('productPrice', price - price * 0.10);
          }
        }
        done();
      });
  });
});

describe('Check Fourth Cron - User Case 7 - Referred by User Case 3 - A Starter box every 14 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case7@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check Has one order', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);

        for (const or of orders) {
          if (or.boxType === 'Starter') {
            const price = 9500;

            // User was referred, should not have a second discount on the next order.
            expect(or).to.have.property('productPrice', price);
          }
        }
        done();
      });
  });
});

describe('Check FourthCron - User Case 8 - Customer With $20 OFF coupon, duration = 2', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case8@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check 1 active Subscription, regular box 20$ discount', (done) => {
    Subscription.findAll({
      include: [Product, Coupon],
      where: {
        user_id: caseId,
      },
      raw: true,
    })
      .then((subscriptions) => {
        expect(subscriptions.length).to.equal(1);
        expect(subscriptions[0].couponUseCount).to.equal(0);
        done();
      });
  });

  it('Check Orders without 20$ off because it was already used twice', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);
        for (const or of orders) {
          if (or.boxType === 'Regular') {
            const price = 9500;
            // Price minus 10%
            expect(or).to.have.property('productPrice', price);
          }
        }
        done();
      });
  });
});

describe('Check FourthCron - User Case 9 - Customer With one subscription, that will be skipped on second and third cron', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case9@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });
  it('Check Order should be re-enabled. No skip date on skip table', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);
        done();
      });
  });
});

