import db, { sequelize } from '../db/connection';
// import config from '../config/config'
import { getSubscriptionsAndRunOrders } from '../services/cron_runner';

const chai = require('chai');

const expect = chai.expect;
let request = require('supertest');

// var stripe = require("stripe")(
//   config.stripeApiKey
// );
// import moment from 'moment'
// var db = require('../db/connection')
const User = db.User;
// var Product = db.Product
const Order = db.Order;
const Subscription = db.Subscription;
const Cron = db.Cron;
const Product = db.Product;
const Coupon = db.Coupon;


const baseUrl = 'http://localhost:3000';
request = request(baseUrl);

let cronId;
describe('Run Fifth Cron', () => {
  it('Move Subscription start_from two week old', (done) => {
    sequelize.query(`
      UPDATE \`subscription\` SET \`start_from\` = DATE_SUB(\`start_from\`, INTERVAL 2 WEEK);
      UPDATE \`order\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
      UPDATE \`skip\` SET \`date\` = DATE_SUB(\`date\`, INTERVAL 2 WEEK);
      UPDATE \`cron\` SET \`created_at\` = DATE_SUB(\`created_at\`, INTERVAL 2 WEEK);
    `).then(() => {
      done();
    });
  });

  it('Run Cron', async (done) => {
    console.log('Wait for Cron to finish');
    await getSubscriptionsAndRunOrders();
    done();
  });

  it('Get runned cron id', (done) => {
    Cron.findOne({
      order: 'id DESC',
      raw: true,
    })
      .then((cron) => {
        cronId = cron.id;
        done();
      });
  });
});

describe('Check Fifth Cron - User Case 6 - Customer, Regular box every 28 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case6@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check Has 2 order', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);
        done();
      });
  });
});

describe('Check Fifth Cron - User Case 7 - Referred by User Case 3 - A Starter box every 14 days', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case7@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });

  it('Check Has one order', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);

        for (const or of orders) {
          if (or.boxType === 'Starter') {
            const price = 9500;

            // User was referred, should not have a second discount on the next order.
            expect(or).to.have.property('productPrice', price);
          }
        }
        done();
      });
  });
});

describe('Check Fifth Cron - User Case 9 - Customer With one subscription, that will be skipped on second and third cron', () => {
  let caseId;
  it('Find User Id', (done) => {
    User.findOne({
      where: {
        email: 'sergio+case9@aerolab.co',
      },
      raw: true,
    })
      .then((user) => {
        caseId = user.id;
        done();
      });
  });
  it('Check Order should be re-enabled. No skip date on skip table', (done) => {
    Order.findAll({
      where: {
        user_id: caseId,
        cron_id: cronId,
      },
      raw: true,
    })
      .then((orders) => {
        expect(orders.length).to.equal(1);
        done();
      });
  });
});

