import { getDiscountByBox } from '../helper/boxListsValues';

export const validateAmountToBeGraterThanCero = (_value) => {
  const value = _value;
  if (value < 0 || value === null || value === undefined) return 0;
  return value;
};

export const applyCreditDiscount = (_userCredit, _productPrice) => {
  let productPrice = validateAmountToBeGraterThanCero(_productPrice);
  let userCredit = validateAmountToBeGraterThanCero(_userCredit);

  if (userCredit >= productPrice) {
    userCredit = userCredit - productPrice;
    productPrice = 0;
  }

  if (userCredit < productPrice) {
    productPrice -= userCredit;
    userCredit = 0;
  }
  return { productPrice, userCredit };
};

// Take into account this line: (!simulated && (cycleCurrent >= 1 || plain.newCycleStart !== null)) on _cron_charge
export const applyNextDiscount = (_discountType, _discountAmount, _productPrice) => {
  let discountAmount = _discountAmount;
  const discountType = _discountType;
  let productPrice = _productPrice;

  if (discountType === 'percent') {
    discountAmount = productPrice * (discountAmount * 0.01);
  }
  if (discountAmount < 0) {
    discountAmount = 0; // Simple validation for negative discounts error.
  }

  if (productPrice > discountAmount) {
    productPrice -= discountAmount;
  } else {
    productPrice = 0;
  }
  return { productPrice };
};

export const applyCustomBoxesDiscount = (_boxList, _userFirstCharge = null, _userOrders, _productPrice) => {
  let boxList = _boxList;
  const userFirstCharge = _userFirstCharge;
  const userOrders = _userOrders;
  let productPrice = _productPrice;
  let box_number;
  boxList = JSON.parse(boxList);

  if (userFirstCharge !== null) {
    box_number = 1;
  } else {
    box_number = userOrders[0].boxNumber + 1;
  }
  const actualCouponRules = getDiscountByBox(boxList, box_number);
  const amountOff = actualCouponRules.amountOff;
  let percentOff = actualCouponRules.percentOff;
  if (percentOff !== null && percentOff >= 0) {
    percentOff = percentOff * productPrice;
  }
  const discount = (amountOff === null) ? percentOff : amountOff;
  productPrice -= discount;

  if (productPrice < 0) {
    productPrice = 0;
  }

  return { productPrice };
};

export const applyCouponDiscount = (_coupon, _productPrice, _couponUseCount, _discountAmount) => {
  const coupon = _coupon;
  let productPrice = _productPrice;
  const discountAmount = validateAmountToBeGraterThanCero(_discountAmount);
  const couponUseCount = _couponUseCount;

  if (coupon.duration === 'custom') {
    if (couponUseCount < coupon.timesUsable) {
      productPrice -= discountAmount;
    }
  } else {
    productPrice -= discountAmount;
  }

  if (productPrice < 0) {
    productPrice = 0;
  }

  return { productPrice };
};

export const appliedDiscountsRegistry = ({ type, userCredit, creditLeft, typeNextDiscount, couponId, nextDiscount, discountAmount }) => {
  const discountObj = {};
  discountObj.type = type;

  switch (type) {
    case 'userCredit':
      discountObj.credit = userCredit;
      discountObj.creditLeft = creditLeft;
      break;

    case 'subscriptionNextDiscount':
      discountObj.amount = nextDiscount;
      discountObj.typeNextDiscount = typeNextDiscount;
      // Check percent here
      break;

    case 'subscriptionCoupon':
      discountObj.amount = discountAmount;
      discountObj.couponId = couponId;
      break;
    default:
  }
  return discountObj;
};

export const shouldCalculateDiscount = ({ discountType, simulated, cyclesSimulatedAhead }) => {
  let shouldCalculateDiscount = false;
  switch (discountType) {
    case 'userCredit':
      if ((!simulated && cycleCurrent >= 1) || (simulated && cyclesSimulatedAhead <= 2)) {
        shouldCalculateDiscount = true;
      }
      break;
    case 'subscriptionNextDiscount':
      if ((!simulated && (cycleCurrent >= 1 || plain.newCycleStart !== null)) || ((simulated && cyclesSimulatedAhead <= 1))) {
        shouldCalculateDiscount = true;
      }
      break;
    case 'subscriptionCoupon':
      if (simulated && cyclesSimulatedAhead === 0) {
        shouldCalculateDiscount = true;
      }
      break;
    default:
  }
  return shouldCalculateDiscount;
};
