
import { eachLimit, whilst } from 'async';
import config from '../config/hubspot';
import { LogDB } from '../entities/Log/plugin';
import { updateIntegration } from './index';

const db = require('../db/connection');

const User = db.User;
const Order = db.Order;
const Cron = db.Cron;

const Hubspot = require('./node-hubspot');

const hubspot = new Hubspot({
  apiKey: config.hubspotApi,
});

export const renderUserObject = (row) => {
  const res = { properties: [] };
  for (const o in row) {
    res.properties.push({
      property: o,
      value: row[o],
    });
  }

  return res;
};

export const updateHubspotContactByEmail = (user) => {
  return hubspot.contacts.createOrUpdate(user.email, renderUserObject(user));
};

export const updateHubspotContactByEmailBatch = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const toSend = data.map(user => ({
        email: user.email,
        ...renderUserObject(user.properties),
      }));
      const response = await hubspot.contacts.createOrUpdateBatch(toSend);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

export const upsertHubspotContact = async (user, userId) => {
  return new Promise(async (resolve, reject) => {

    if (user.hubspotId === null) {
      try {
        getVIDHubSpotByEmailUpdateUser(user);
        const userDB = await User.find({ where: { id: userId } });
        user.hubspotId = userDB.hubspotId;
        const responseContactInstance = await updateHubspotContactByEmail(user);
        const vId = responseContactInstance.vid;
        resolve(vId);
      } catch (err) {
        console.log(`Cant create Hubspot User ${err}`);
        reject(err);
      }
    } else {

      getVIDHubSpotByEmailUpdateUser(user);
      const userDB = await User.find({ where: { id: userId } });
      user.hubspotId = userDB.hubspotId;

      hubspot.contacts.update(userDB.hubspotId, renderUserObject(user), (error, res) => {
        if (error) {
          console.log(error.message);
          return reject(error.message);
        }
        resolve(hID);
      });
    }
  });
};

export const updateWeeklyList = () => {
  const logInfo = {
    admin: null,
    user: null,
    type: 'info',
    message: '',
  };
  return new Promise(async (resolve, reject) => {
    console.log('Hubspot: Sync Weekly List start');
    logInfo.type = 'info';
    logInfo.message = 'Hubspot: Sync Weekly List start';
    LogDB(logInfo);

    // get contacts
    let contactsOfTheList = [];
    let contactListInfo;
    const CONTACTS_PER_PAGE = 100;
    let total_pages;
    let current_page = 0;
    try {
      contactListInfo = await hubspot.lists.getOne(config.hubspotWeeklyList);
      total_pages = Math.ceil(contactListInfo.metaData.size / CONTACTS_PER_PAGE);
    } catch (err) {
      logInfo.type = 'error';
      logInfo.message = 'Hubspot: Cant get List Info';
      LogDB(logInfo);
    }
    let myVidOffset = 0;
    let hasMore = true;

    whilst(() => hasMore,
        async (cb) => {
          const contactPage = await hubspot.lists.getContacts(config.hubspotWeeklyList, { count: 100, vidOffset: myVidOffset });
          current_page++;
          myVidOffset = contactPage['vid-offset']; // Husbpot Answer
          hasMore = contactPage['has-more']; // Husbpot Answer
          contactsOfTheList.push(...contactPage.contacts);
          setTimeout(cb, 1000);
        }, async (err) => {
          if (contactsOfTheList.length) contactsOfTheList = contactsOfTheList.map(c => c.vid);
          try {
            const responseDelete = await hubspot.lists.deleteContacts(config.hubspotWeeklyList, contactsOfTheList);
            logInfo.type = 'info';
            logInfo.message = 'Hubspot: Weekly list emptied';
            LogDB(logInfo);
          } catch (err) {
            console.log(`Can't delete hubspot contacts ${err}`);
            logInfo.type = 'error';
            logInfo.message = 'Hubspot: Weekly list cant be emptied';
            LogDB(logInfo);
          }

          // get the contact ids from the db
          let hubspotIds = [];
          const usersNotFoundOnHubspot = [];
          try {
            const cron = await Cron.max('id');
            const orders = await Order.findAll({
              include: [{
                model: User,
              }],
              distinct: 'user_id',
              where: {
                cron_id: cron,
              },
            });
            hubspotIds = orders
                .map((o) => {
                  if (o.user.get('hubspotId') !== null) {
                    return o.user.get('hubspotId');
                  }
                  usersNotFoundOnHubspot.push(o.user.get('id'));
                })
                .filter(o => o !== undefined);
          } catch (err) {
            console.log(`Can't get contacts from db: ${err}`);
            logInfo.type = 'error';
            logInfo.message = 'Hubspot: Cant get contacts from db (last CRON)';
            LogDB(logInfo);
          }

          const removeDuplicatesUsersNotFoundOnHubspot = usersNotFoundOnHubspot.reduce((a, b) => {
            if (a.indexOf(b) < 0) a.push(b);
            return a;
          }, []);

          eachLimit(removeDuplicatesUsersNotFoundOnHubspot, 1, async (userId, cb) => {
            try {
              await updateIntegration(userId);
              const userUpdated = await User.findOne({ where: { id: userId } });
              hubspotIds.push(userUpdated.get('hubspotId'));
            } catch (err) {
              console.log(err);
            }
            setTimeout(cb, 500);
          }, async () => {
            // add new contacts
            try {
              const responseAdd = await hubspot.lists.addContacts(config.hubspotWeeklyList, { vids: hubspotIds });
              logInfo.type = 'info';
              logInfo.message = 'Hubspot: Weekly list is filled correctly';
              LogDB(logInfo);
              resolve(responseAdd);
            } catch (err) {
              console.log(`Can't add hubspot contacts ${err}`);
              logInfo.type = 'error';
              logInfo.message = `Hubspot:  Weekly list cant't be filled ${err}`;
              LogDB(logInfo);
              // reject(err);
              resolve();
            }
          });
        });
  });
};

export const createHubspotContact = (user) => {
  if (user.hubspotId) delete user.hubspotId;
  return hubspot.contacts.create(renderUserObject(user));
};

const getHubspotBatchContactsByEmails = (emails) => {
  return hubspot.contacts.getByEmailBatch(emails);
};

export const getHubspotBatchContactsById = (ids) => {
  return hubspot.contacts.getByIdBatch(ids);
};

const createHubspotBatchContactsByEmails = (batch) => {
  return hubspot.contacts.createOrUpdateBatch(batch);
};

export const hubspotGetWeeklyList = (cb) => {
  hubspot.lists.getContacts(config.hubspotWeeklyList, (error, res) => {
    if (error) {
      return console.log(error);
    }
    cb(res);
  });
};

export const triggerHubspotWorkflow = (workflowId, email, cb) => {
  return new Promise((resolve, reject) => {
    hubspot.workflows.trigger(workflowId, email, (error, res) => {
      if (error) {
        return reject(error);
      }
      resolve(res);
    });
  });
};

export const sendHubspotForm = (portalId, formGuid, body, cb) => {
  return new Promise((resolve, reject) => {
    hubspot.forms.send(portalId, formGuid, body, (error, res) => {
      if (error) {
        return reject(error);
      }
      resolve(res);
    });
  });
};

/**
 * @Author Jean Carlos Nunez
 * @param user
 * @return {Promise}
 */
export const getVIDHubSpotByEmailUpdateUser = ( user ) => {
  return new Promise((resolve, reject) => {
    return hubspot.contacts.getByEmail(user.email, async (error, res) => {
      if (error) {
        return reject(error);
      }
      const userDB = await User.find({where: {id: user.user_id}});
      userDB.set('hubspotId', res.vid);
      await userDB.save();
      resolve(res)
    });
  })
};

