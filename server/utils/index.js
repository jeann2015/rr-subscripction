import moment  from 'moment';
import zipcodes from 'zipcodes';
import { eachLimit } from 'async';
import { upsertHubspotContact} from './hubspot';
import { getOutboundIdentifyId, outboundIntegration } from './outbound';
import { upsertZendesk, deleteZendesk } from './zendesk';
import config from '../config/config';
import configSendgrid from '../config/sendgrid';
import { sendEmail } from './sendgrid';
import { get3PLInfo } from '../services/shipstation_tag';
import { getUserLastBoxNumber } from '../entities/User/controller';
import { calculateNextShipment } from '../services/cron_new';
import { LogDB } from '../entities/Log/plugin';
import { getExcludedIngredients } from '../entities/ExcludedIngredients/controller';
import { showKindMealsDefaultBreakfast } from '../../public/utils/index';

const db = require('../db/connection');
const User = db.User;
const Child = db.Child;
const Subscription = db.Subscription;
const Product = db.Product;
const Referral = db.Referral;
const Skip = db.Skip;
const Order = db.Order;

export const corsOptions = {
  exposedHeaders: 'X-Total-Count',
};

export const removeTimeFromDate = date => moment(date).format('YYYY-MM-DD');

export const customValidator = {
  isValidZipCode: (zip) => {
    const data = zipcodes.lookup(zip);
    if (!data) {
      return false;
    }
    return true;
  },
  isArray: (value) => {
    return Array.isArray(value);
  },
};

export const uuid = () => {
  const s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };
  return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}`;
};

export const capitalizeFirstChar = string => string.charAt(0).toUpperCase() + string.substring(1);

export const getSubscriptionStatus = (subscriptions) => {
  const hasSubscriptionsWithStatus = (status) => {
    return subscriptions.filter(s => s.status === status).length;
  };
  if (hasSubscriptionsWithStatus('active')) return 'active';
  if (hasSubscriptionsWithStatus('paused')) return 'pause';
  if (hasSubscriptionsWithStatus('canceled')) return 'cancel';
  if (hasSubscriptionsWithStatus('pending')) return 'pending';
  return 'none';
};

/**
 * Formats phone number to match Zendesk's required format
 * Format in db -> (626) 716-6711 || Zendesk require -> +16267166711
 * @param {string} phoneNumber - Phone number with DB format
 * @returns {string} - Formatted number
 */
export const formatPhoneForZendesk = (phoneNumber) => {
  const withoutParenthesis = phoneNumber.replace(/[{()}]/g, '');
  const withoutSpaces = withoutParenthesis.replace(' ', '');
  const formatedPhone = withoutSpaces.replace('-', '');
  return `${formatedPhone}`.toString();
};

export const upsertZendeskContact = (user, customFields) => {
  return upsertZendesk({
    id: user.get('id'),
    email: user.get('email'),
    name: `${user.get('name')} ${user.get('lastName')}`,
    phone: `${ formatPhoneForZendesk(user.get('phoneNumber')) }`,
    user_fields: customFields,
  });
};

export const findOneReferral = (userId) => {
  return Referral.findOne({
    include: [{
      model: User,
      as: 'userReferree',
    }, {
      model: User,
      as: 'userReferred',
    }],
    where: {
      me: userId,
    },
  });
};

const saveCancelReasonZendeskOptions = (cancelReason) => {
  const options = {
    Allergies: 'allergies',
    'Baby doesn\'t like': 'baby_doesnt_like_it',
    'Financial reasons': 'price/cost',
    'Lack of variety': 'lack_of_variety',
    'Moving/vacation': 'moving',
    'Only one box': 'Only one box',
    'Service issue': 'Service issue',
    'Too much food': 'too_much_food',
    'Transitioning to adult food': 'transitioning_to_adult_food',
    'Trying another service': 'trying_another_service',
    'Value/not worth the price': 'value/not_worth_the_price',
    Other: 'other',
  };
  return options[cancelReason];
};

const getSubscriptionModel = async (userId) => {
  const subscription = await db.Subscription.findAll({
    where: { userId: userId },
    include: [
      db.User,
      db.Product,
      db.Coupon,
      db.Skip,
      {
        model: db.Order,
        limit: 1,
        order: [
          ['createdAt', 'DESC'],
        ],
        required: false,
      },
      {
        model: db.Order,
        as: 'orderCount',
        attributes: ['cronId'],
        where: {
          boxType: {
            $not: 'Meal Maker',
          },
        },
        required: false,
      },
    ],
  });
  return subscription;
};

export const getTrackingLink = (order) => {
  return order.cronId < 64
    ? `https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=${order.trackId}&locale=en_US&cntry_code=us&language=english`
    : `https://www.ups.com/track?loc=en_US&tracknum=${order.trackId}&requester=NES&agreeTerms=yes/trackdetails`;
};

const getLastTrackingLinks = async (userId) => {
  const links = {};
  const lastOrder = await Order.findOne({
    where: {
      userId,
    },
    order: [['createdAt', 'DESC']],
  });

  if (lastOrder) {
    if (lastOrder.tracked && lastOrder.trackId !== '') {
      const boxTrackingLink = getTrackingLink(lastOrder);
      links.boxTrackingLink = boxTrackingLink;
      links.boxTracking = lastOrder.get('trackId');
    }
  }
  return links;
};

const getIngredientsString = (userOrders) => {
  if (!userOrders.length) return false;

  return userOrders[0].boxDetail
    .map(meal => meal.get({ plain: true }))
    .map(mealData => mealData.meal.meals_ingredients.map(ing => ing.ingredient.name))
    .map(ingr => ingr.join(' | '));
};

const getExcludedIngredientsString = async (subId) => {
  const excludedInfo = await getExcludedIngredients(subId);
  return excludedInfo.excludedIngredients.map(ingr => ingr.name).join(', ');
};

const getUTCTime = (date) => {
  const fixedDate = new Date(date);
  fixedDate.setUTCHours(0);
  fixedDate.setUTCMinutes(0);
  fixedDate.setUTCSeconds(0);
  return fixedDate.getTime();
};

export const updateIntegration = (userId) => {
  return new Promise((resolve, reject) => {
    User.findOne({
      where: {
        id: userId,
      },
      include: [{
        model: Subscription,
        include: [Product, Skip],
      }, {
        model: Child,
      }, {
        model: db.Order,
        limit: 1,
        order: [['cronId', 'DESC']],
        include: [
          {
            model: db.BoxDetailHistory,
            as: 'boxDetail',
            separate: true,
            include: [{
              model: db.Meals,
              include: [{
                model: db.MealsIngredients,
                include: [db.Ingredients],
              }],
            }],
          },
        ],
      }],
    })
        .then(async (user) => {
          if (!user) {
            const err = new Error('User not found in update integration');
            reject(err);
          }

          const adminUrl = `${config.adminUrl}/#/users/${userId}/show`;
          const subscriptions = user.get('subscriptions');
          const product = subscriptions[0].get('product');
          const skips = subscriptions[0].get('skips');
          const childs = user.get('children');
          const productFrequency = subscriptions[0].frequency;
          const orderModel = { email: user.get('email'), zipCode: user.get('zipCode') };
          const customerStatus = getSubscriptionStatus(subscriptions);
          const ingredientsString = getIngredientsString(user.orders);
          const boxNumber = await getUserLastBoxNumber(user.get('id'));
          const excludedIngredients = await getExcludedIngredientsString(subscriptions[0].get('id'));
          const _3plInfo = await get3PLInfo(orderModel);
          const trackingInfo = await getLastTrackingLinks(userId);
          const childsName = [];
          const childsAge = [];

          let outboundId = user.get('outbound');
          let startFrom;
          let signupDate;
          let regularBoxes = 0;
          let starterBoxes = 0;
          let skipString = '';
          let pausedUntil = '';
          let cancelReason = '';
          let nextShipment;
          let frequencyOfPlan = '14 days';

          // Set nextShipment
          try {
            const subscriptionModel = await getSubscriptionModel(user.id);
            nextShipment = await calculateNextShipment(subscriptionModel[0]);
          } catch (err) {
            console.log(err);
          }

          // If user does not have Outbound ID, set it.
          try {
            if (outboundId === null) {
              outboundId = getOutboundIdentifyId({ email: user.get('email') });
              user.set('outbound', outboundId);
              await user.save();
            }
          } catch (error) {
            console.log(error);
          }

          // Set startFrom and signupDate
          if (subscriptions.length > 0) {
            if (subscriptions[0].startFrom) {
              startFrom = subscriptions[0].startFrom;
              signupDate = subscriptions[0].createdAt;
            }
          }

          // Set frequencyOfPlan
          if (productFrequency) {
            frequencyOfPlan = `${productFrequency * 7} days`;
          }

          // Set skips string
          if (skips.length > 0) {
            skips.map((skip, index) => {
              skipString += moment(skip.get('date', 'HH:MM:SS')).startOf('day').format('YYYY-MM-DD');
              if (index < skips.length - 1) skipString += ' / ';
            });
          }

          // If status is none, send notification email.
          if (customerStatus === 'none') {
            try {
              const res = await sendEmail({
                to: configSendgrid.SENDGRID_TO_EMAIL,
                substitutions: {
                  body: `
                  Name: ${user.get('name')} <br />
                  Lastname: ${user.get('lastName')} <br />
                  E-mail: ${user.get('email')}
                `,
                },
                subject: 'Alert: User with status None',
                template_id: configSendgrid.templates.rrTemplate,
              });
            } catch (err) {
              console.log(err);
            }
          }

          subscriptions.forEach((sub) => {
            const boxType = sub.get('product').get('boxType')
                .toLowerCase();
            if (boxType === 'starter') {
              starterBoxes += sub.get('amount');
            }
            if (boxType === 'regular') {
              regularBoxes += sub.get('amount');
            }

            if (sub.get('pausedUntil') && pausedUntil === '') {
              pausedUntil = sub.get('pausedUntil');
            }

            if (sub.get('cancelReason') && cancelReason === '') {
              cancelReason = sub.get('cancelReason');
            }
          });

          const cancellationDate = subscriptions[0].get('cancellationDate');
          let cancellationDateRow;
          if (cancellationDate) {
            cancellationDateRow = getUTCTime(cancellationDate);
          }

          const SendSingleEmailTracking = {

            email: {"to":user.get('email')},


          }
          const mealsBreakfastDefaultDescription = showKindMealsDefaultBreakfast(product.boxType,subscriptions[0].breakFastCount);
          const fieldsHubspot = {
            hubspotid: user.get('hubspotId'),
            admin_url: adminUrl,
            amount_of_boxes: subscriptions[0].get('amount'),
            cancel_reason: cancelReason,
            city: user.get('city'),
            email: user.get('email'),
            firstname: user.get('name'),
            lastname: user.get('lastName'),
            lead_status: 'Customer',
            zip: user.get('zipCode'),
            phone: user.get('phoneNumber'),
            state: user.get('state'),
            address: user.get('address'),
            frequency_of_plan: frequencyOfPlan,
            subscription_status: user.get('subscriptions')[0].get('status'),
            box_number: boxNumber,
            skip_dates: skipString,
            tracking_link: trackingInfo.boxTrackingLink,
            box_type: user.get('orders').length ? user.get('orders')[0].boxType : '',
            n3pl: _3plInfo._3pl.warehouse,
            ship_time: _3plInfo.transit,
            referral_link: `${APP_URL}/r/${user.get('name').toUpperCase().split(' ').join('-')}_${user.get('id')}`,
            referral_code: `${user.get('name').toUpperCase().split(' ').join('')}${user.get('id')}`,
            cancellation_date: cancellationDateRow,
            beaba_lead: user.get('beabaFlow'),
            user_id: user.get('id'),
            referral_credit: user.get('credit'),
            meal_1: ingredientsString[0] || '',
            meal_2: ingredientsString[1] || '',
            meal_3: ingredientsString[2] || '',
            meal_4: ingredientsString[3] || '',
            meal_5: ingredientsString[4] || '',
            meal_6: ingredientsString[5] || '',
            // mealsDefaultBreakfast: mealsBreakfastDefaultDescription,
          };

          const newCycleStart = subscriptions[0].get('newCycleStart') ? moment(subscriptions[0].get('newCycleStart')).format('YYYY-MM-DD') : '';
          const nextDiscount = subscriptions[0].get('typeNextDiscount') ? `${subscriptions[0].get('typeNextDiscount')} ${subscriptions[0].get('nextDiscount')} OFF ` : '';

          let kid_1_name;
          let kid_age;
          let kid_1_dob;
          let kid_2_name;
          let kid_2_age;
          let kid_2_dob;

          if (childs.length > 0) {
            const firstChild = childs[0];
            kid_1_name = firstChild.name;
            kid_age = `${moment().diff(firstChild.birthdate, 'months', false)}`;
            kid_1_dob = getUTCTime(firstChild.birthdate);
            childsName.push(kid_1_name);
            childsAge.push(kid_age);
          }

          if (childs.length > 1) {
            const secondChild = childs[1];
            kid_2_name = secondChild.name;
            kid_2_age = `${moment().diff(secondChild.birthdate, 'months', false)}`;
            kid_2_dob = getUTCTime(secondChild.birthdate);
            childsName.push(kid_2_name);
            childsAge.push(kid_2_age);
          }

          const customFieldsZendesk = {
            customer_status: customerStatus,
            admin_url: adminUrl,
            next_shipment_date: moment(nextShipment, 'YYYY-MM-DD HH:MM:SS').format('YYYY-MM-DD') || null,
            box_number: String(boxNumber),
            frequency: frequencyOfPlan,
            product: product.get('description'),
            skip_dates: skipString,
            zip_code: user.get('zipCode'),
            next_discount: nextDiscount,
            kid_1_name,
            kid_age,
            kid_1_dob,
            kid_2_name,
            kid_2_age,
            kid_2_dob,
          };

          if (startFrom) {
            customFieldsZendesk.first_shipment = moment(startFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
          }

          const fields = {
            user_id: user.get('id'),
            email: user.get('email'),
            phoneNumber: `${ formatPhoneForZendesk(user.get('phoneNumber')) }`,
            attributes: {
              admin_url: adminUrl,
              box_number: String(boxNumber),
              cancel_reason: cancelReason,
              cancellation_pause_reason: saveCancelReasonZendeskOptions(cancelReason) || '',
              childs_age: childsAge.join(' / '),
              childs_name: childsName.join(' / '),
              customer_status: customerStatus,
              excluded_ingredients: excludedIngredients,
              first_shipment: getUTCTime(startFrom) || '',
              frequency: frequencyOfPlan,
              kid_1_name,
              kid_2_age,
              kid_2_name,
              kid_age,
              lead_status: 'customer',
              new_cycle_start: newCycleStart,
              next_discount: nextDiscount,
              next_shipment_date: moment(nextShipment, 'YYYY-MM-DD HH:MM:SS').format('YYYY-MM-DD'),
              paused_until: getUTCTime(pausedUntil) || '',
              product: product.get('description'),
              signup_date: moment(signupDate, 'YYYY-MM-DD HH:MM:SS').format('YYYY-MM-DD'),
              skip_dates: skipString,
              subscription: user.get('subscriptions')[0].get('status'),
              update_cc_link: `${APP_URL}/cc?hash=${user.get('hash')}`,
              zip_code: user.get('zipCode'),
            },
          };

          if (user.orders.length) {
            fields.attributes.date_order_shipped = moment(fields.attributes.date_order_created, 'YYYY-MM-DD').add(2, 'days').format('YYYY-MM-DD');
            fields.attributes.first_ship_date = moment(user.orders[0].get('createdAt'), 'YYYY-MM-DD HH:MM:SS').format('YYYY-MM-DD');
            fields.attributes.last_ship_date = moment(user.orders[user.orders.length - 1].get('createdAt'), 'YYYY-MM-DD HH:MM:SS').format('YYYY-MM-DD');
          }

          try {
            outboundIntegration(fields)
                .then((res) => {
                  LogDB({
                    user: user.get('id'),
                    message: 'Integrate Outbound: Ok!',
                    type: 'info',
                  });
                })
                .catch((err) => {
                  console.log(`Integrate Outbound: Can't update user ${user.get('email')}`);
                });

            try {
              await upsertZendeskContact(user, customFieldsZendesk);
              LogDB({
                user: user.get('id'),
                message: 'Integrate Zendesk: Ok!',
                type: 'info',
              });
            } catch (err) {
              console.log(`Error trying integrate Zendesk: ${err}`);
              LogDB({
                user: user.get('id'),
                message: `Integrate Zendesk: Error (${err})`,
                type: 'error',
              });
            }

            try {
              await upsertHubspotContact(fieldsHubspot, user.get('id'));
              LogDB({
                user: user.get('id'),
                message: 'Integrate Hubspot: Ok!',
                type: 'info',
              });
            } catch (err) {
              console.log(`Error trying integrate Hubspot: ${err}`);
              LogDB({
                user: user.get('id'),
                message: `Integrate Hubspot: Error (${err})`,
                type: 'error',
              });
            }

            resolve('Outbound, Zendesk & Hubspot Finish');

          } catch (error) {
            console.log(error);
            reject(error);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
  });
};

export const deleteContact = (email) => {
  return new Promise(async (resolve, reject) => {
    try {
      await deleteZendesk(email);
      resolve('Zendesk Deleted OK');
    } catch (error) {
      reject(error);
    }
  });
};

export const formatUSPhoneNumber = (phoneNumber) => {
  const cleanPhoneNumber = phoneNumber.replace(/\D+/g, '');
  const splitPhoneNumber = cleanPhoneNumber.match(/^(\d{3})(\d{3})(\d{4})$/);
  return `(${splitPhoneNumber[1]}) ${splitPhoneNumber[2]}-${splitPhoneNumber[3]}`;
};

export const formatUpdatedData = (arr) => {
  return arr.map((dc) => {
    let newFieldValue = dc[`new_${dc.field}`];
    if (newFieldValue === 'cancel') newFieldValue = 'cancelled'; // this line is only for cancelled state
    return `${dc.field}: ${dc[`old_${dc.field}`]} changed to ${newFieldValue}`;
  });
};

export const add3PLtoOrders = (orders) => {
  return new Promise((resolve, reject) => {
    const response = [];
    eachLimit(orders, 1, async (order, cb) => {
      try {
        const _3plInfo = await get3PLInfo(order);
        order._3pl = _3plInfo._3pl.warehouse;
        order.ship_time = _3plInfo.transit;
        response.push(order);
      } catch (err) {
        return reject(`add3PLtoOrders() Error for order ${order.id}: ${err}`);
      }
      cb();
    }, () => {
      resolve(response);
    });
  });
};
