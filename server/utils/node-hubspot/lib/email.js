import config from "../../../config/hubspot";
const https = require('https');
const querystring = require('querystring');
import { setUpdateOrderSentEmailTrackingHubSpot } from '../../../entities/Order/plugin';
import { renderUserObject } from '../../hubspot';

class Mail {

  constructor(client) {
    this.client = client;
  }

  static sendMail = (email, trackingLink, firstName, OrderId, cb) => {
    const properties = {
      emailId: config.sendEmailTraking.send,
      message: {
        to: `${email}`
      },
      contactProperties: [{"name": "firstname", "value": `${firstName}`}, {
        "name": "tracking_link",
        "value": `${trackingLink}`
      }]
    };
    const postData = querystring.stringify(properties);
    // const bodySerialized = renderUserObject(properties);
    const options = {
      hostname: 'api.hubapi.com',
      path: `/email/public/v1/singleEmail/send`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': postData.length,
      },
      body: properties,
    };
    try {

      const request = https.request(options, (response) => {
        response.setEncoding('utf8');
      });

      request.on('response', (e) => {
        console.log('e.statusCode', e.statusCode);
        if (e.statusCode === 204) {
          setUpdateOrderSentEmailTrackingHubSpot(OrderId)
          cb(null, e);
        }
        if (e.statusCode === 404 || e.statusCode === 500) {
          console.log(e.message);
        }
      });
      request.write(postData);
      request.end();
    } catch (err) {
      console.log(err);
    }

  }
}

module.exports = Mail;
