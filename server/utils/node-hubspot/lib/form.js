const https = require('https');
const querystring = require('querystring');

class Form {
  constructor(client) {
    this.client = client;
  }

  send = (hubspotPortalId, hubspotFormGuid, body, cb) => {
    const postData = querystring.stringify(body);
    const options = {
      hostname: 'forms.hubspot.com',
      path: `/uploads/form/v2/${hubspotPortalId}/${hubspotFormGuid}`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': postData.length,
      },
    };

    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
    });

    request.on('response', (e) => {
      console.log(e.statusCode);
      if (e.statusCode === 204) {
        cb(null, e);
      }
      if (e.statusCode === 404 || e.statusCode === 500) {
        cb(e);
      }
    });
    request.write(postData);
    request.end();
  };
}

module.exports = Form;
