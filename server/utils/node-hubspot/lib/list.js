class List {
  constructor(client) {
    this.client = client;
  }

  get(options, cb) {
    if (typeof options === 'function') {
      cb = options;
      options = {};
    }

    return this.client._request({
      method: 'GET',
      path: '/contacts/v1/lists',
      qs: options,
    }, cb);
  }

  getOne(id, cb) {
    if (!id || typeof (id) === 'function') {
      return cb(new Error('id parameter must be provided.'));
    }

    return this.client._request({
      method: 'GET',
      path: `/contacts/v1/lists/${id}`,
    }, cb);
  }

  getContacts(id, options, cb) {
    if (!id || typeof (id) === 'function') {
      return cb(new Error('id parameter must be provided.'));
    }

    if (typeof (options) === 'function') {
      cb = options;
      options = {};
    }

    return this.client._request({
      method: 'GET',
      path: `/contacts/v1/lists/${id}/contacts/all`,
      qs: options,
      qsStringifyOptions: { indices: false },
    }, cb);
  }

  getRecentContacts(id, options, cb) {
    if (!id || typeof (id) === 'function') {
      return cb(new Error('id parameter must be provided.'));
    }

    if (typeof (options) === 'function') {
      cb = options;
      options = {};
    }
    return this.client._request({
      method: 'GET',
      path: `/contacts/v1/lists/${id}/contacts/recent`,
      qs: options,
      qsStringifyOptions: { indices: false },
    }, cb);
  }

  addContacts(id, contactBody, cb) {
    if (!id || typeof (id) === 'function') {
      return cb(new Error('id parameter must be provided.'));
    }
    if (!contactBody || typeof (contactBody) === 'function') {
      return cb(new Error('contactBody parameter must be provided.'));
    }

    const body = contactBody;

    return this.client._request({
      method: 'POST',
      path: `/contacts/v1/lists/${id}/add`,
      body: body,
    }, cb);
  }

  deleteContacts(id, contacts, cb) {
    if (!id || typeof (id) === 'function') {
      return cb(new Error('contacts parameter must be provided.'));
    }
    if (!contacts || typeof (contacts) === 'function') {
      return cb(new Error('list parameter must be provided.'));
    }

    const body = { vids: contacts };

    return this.client._request({
      method: 'POST',
      path: `/contacts/v1/lists/${id}/remove`,
      body: body,
    }, cb);
  }

  createList(name, cb) {
    if (!name || typeof (name) === 'function') {
      return cb(new Error('name parameter must be provided.'));
    }

    const body = { name };

    return this.client._request({
      method: 'POST',
      path: '/contacts/v1/lists',
      body: body,
    }, cb);
  }
}

module.exports = List;
