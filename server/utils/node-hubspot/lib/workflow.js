class Workflow {
  constructor(client) {
    this.client = client;
  }

  trigger(workflowId, email, cb) {
    if (!workflowId || typeof (workflowId) === 'function') {
      return cb(new Error('workflowId parameter must be provided.'));
    }
    if (!email || typeof (email) === 'function') {
      return cb(new Error('email parameter must be provided.'));
    }

    return this.client._request({
      method: 'POST',
      path: `/automation/v2/workflows/${workflowId}/enrollments/contacts/${email}`,
    }, cb);
  }
}

module.exports = Workflow;
