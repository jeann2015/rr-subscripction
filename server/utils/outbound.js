
import outbound from 'outbound';
import hash from 'object-hash';

import config from '../config/config';

const ob = new outbound(config.outbound);

export const getOutboundIdentifyId = payload => payload.outbound || hash({ env: config.outboundEnv, email: payload.email });

export const outboundIntegration = (payload) => {
  const logInfo = {
    admin: null,
    user: payload.id,
    type: 'info',
    message: '',
  };

  if (payload.id) logInfo.user = payload.id;

  return new Promise((resolve, reject) => {
    ob.identify(getOutboundIdentifyId(payload), payload).then(res => resolve(true), err => reject(err));
  });
};

export const triggerOutboundEvent = ({ event, user }) => {
  return new Promise((resolve, reject) => {
    if (event === null) return reject('Cant trigger a Outbound Event: missing event id');
    if (user === null) return reject('Cant trigger a Outbound Event: missing event user_id');
    ob.track(user, event).then(resolve, reject);
  });
};
