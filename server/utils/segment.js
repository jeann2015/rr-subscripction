import Analytics from 'analytics-node';
import config from '../config/config';

export const analytics = new Analytics(config.segmentKey);
