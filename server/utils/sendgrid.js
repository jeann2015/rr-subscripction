
import config from '../config/sendgrid';

const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(config.sendgridAPI);

export const sendEmail = ({ to, subject, substitutions, template_id, send_at, body }) => {
  return new Promise((resolve, reject) => {
    let toEmail = to || config.sendgridToEmail;
    if (toEmail.length > 0) toEmail = toEmail.split(',');
    const data = {
      from: {
        name: config.sendgridFromName,
        email: config.sendgridFromEmail,
      },
      to: toEmail,
    };
    if (send_at !== undefined && send_at !== null) {
      data.send_at = send_at;
    }
    if (template_id) data.template_id = template_id;
    if (body) data.body = body;
    if (substitutions) data.substitutions = substitutions;
    if (subject) data.subject = subject;
    sgMail.send(data, false, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    });
  });
};

