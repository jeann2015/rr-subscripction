import zd from 'node-zendesk';
import config from '../config/config';

export const ZENDESK_ID_START_SLUG = `RR-USER-${config.zendeskEnvName}`;

export const zendesk = zd.createClient({
  token: config.zendeskKey,
  username: config.zendeskUser,
  remoteUri: `https://${config.zendeskUrl}/api/v2`,
  encoding: 'utf8',
});


export const isUserCreatedOnZendesk = async (email) => {
  return new Promise((resolve, reject) => {
    zendesk.search.query(email, (err, status, response) => {
      if (err) {
        return reject(err);
      }
      if (response.length !== 0) resolve(true);
      resolve(false);
    });
  });
};

export const upsertZendesk = (userData) => {
  if (userData.hasOwnProperty('id')) {
    delete userData.id;
  }
  if (userData.hasOwnProperty('user_fields')) {
    if (userData.user_fields.hasOwnProperty('cancellation_pause_reason')) {
      delete userData.user_fields.cancellation_pause_reason;
    }
  }
  return new Promise((resolve, reject) => {
    if (userData.id) {
      zendesk.users.updateMany({
        external_ids: `${ZENDESK_ID_START_SLUG}-${userData.id}`,
      },
      { user: userData }, (err, status, user) => {
        if (err) {
          return reject(err);
        }
        resolve(user);
      });
    } else {
      zendesk.users.createOrUpdate({ user: userData }, (err, status, user) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        resolve(user);
      });
    }
  });
};

export const deleteZendesk = (userEmail) => {
  return new Promise((resolve, reject) => {
    zendesk.users.list((err, status, userList) => {
      const user = userList.find(currentUser => (String(currentUser.email).toLowerCase() === String(userEmail).toLowerCase()));
      if (user) {
        zendesk.users.delete(user.id, (zendeskError) => {
          if (zendeskError) {
            return reject(zendeskError);
          }
          resolve(zendeskError);
        });
      } else {
        return reject('user not found');
      }
    });
  });
};
